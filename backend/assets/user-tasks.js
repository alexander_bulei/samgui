/**
* Page: manage-user-tasks.html
* VERSION: 09052016.1
* Author: Alexander Bulei
*/

var InitUserTasks = function () {
    return {
        init: function (options) {
          var _options =  options || {};
          var $table = $('#userTasksDT');
          var COLMAN = new COLMANAGER($table, _options.access.cols);
          COLMAN.init();	

          // check access
          controlActionBtnsAccess($table, _options.access.actions);

          makeResponsiveTable($table);
            //Datatable Initiating
          var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "bAutoWidth": false				
          });

          var isEditing = null;
          
          //Toggle Tasks
          function filterByStatus(action){
            $table.find('td.td-status').each(function(){
              var $row = $(this).parent();
              if (action == "show") {
                $row.show();
              } else {
                if ($(this).text() == _STATUS_INACTIVE){
                  $row.hide();
                }
              }
            });
          }
          
          $('#mut-toggle-tasks').click(function (e) {
              e.preventDefault();
              var $this = $(this);
              var action = $this.data("action");
              if (action == 'show'){
                $this.text("Show Inactive Tasks");
                $this.data("action", "hide");
                filterByStatus('hide');
              } else {
                $this.text("Hide Inactive Tasks");
                $this.data("action", "show");
                filterByStatus('show');
              }
          });
			
          bindChangeStatusDropDown($table);
			
          $('#userTaskViewDetails').on('shown.bs.modal', function () {
            $("#userTaskViewDetails-task-note").focus();
          });
          
          $("#userTaskViewDetails-save").click(function(){
            var $modal = $("#userTaskViewDetails");
            var data = {
              id: $modal.data("id"),
              taskName: $("#userTaskViewDetails-task-name").val(),
              itemName: $("#userTaskViewDetails-item-name").val(),
              taskNote: $("#userTaskViewDetails-task-note").val()
            }
            
            _options.saveDetails(data, function(){
              $modal.modal("hide");
            });
            
          });
      
          $table.on("click",".command-btn.details", function(){
            var $modal = $("#userTaskViewDetails");
            var id = $(this).closest('tr').attr('id');
            _options.getDetails(id, function(response){
              $("#userTaskViewDetails-task-name").val(response.taskName);
              $("#userTaskViewDetails-item-name").val(response.itemName);
              $("#userTaskViewDetails-task-note").val(response.taskNote);
              $modal.data("id", id).modal("show");
            });
            
          });
      
          //Edit row
          $table.on("click", 'a.edit', function (e) {
              e.preventDefault();

              var nRow = $(this).parents('tr')[0];

              if (isEditing !== null && isEditing != nRow) {
                  restoreRow(oTable, isEditing);
                  editRow(oTable, nRow);
                  isEditing = nRow;
              } else {
                  editRow(oTable, nRow);
                  isEditing = nRow;
              }
          });

          //Cancel Editing or Adding a Row
          $table.on("click", 'a.cancel', function (e) {
              e.preventDefault();
              if ($(this).attr("data-mode") == "new") {
                  var nRow = $(this).parents('tr')[0];
                  oTable.fnDeleteRow(nRow);
                  isEditing = null;
              } else {
                  restoreRow(oTable, isEditing);
                  isEditing = null;
              }
          });

          //Save an Editing Row
          $table.on("click", 'a.save', function (e) {
              e.preventDefault();
              if (this.innerHTML.indexOf("Save") >= 0) {
                var _canSave = true;
                _check = function(erroMsg){
                  var v = $input.is("input") ? $input.val() : $input.find("option:selected").val();
                  if (( v === "") || (v == null) || (v < 0)) {
                    _canSave = _canSave && false;
                    $input.addClass("required-error");
                    _Error_(erroMsg);
                  } else {
                    $input.removeClass("required-error");
                    _canSave = _canSave && true;
                  }						
                }
                
                if (_canSave){
                  saveRow(oTable, isEditing);
                  isEditing = null;
                  var action = $("#mut-toggle-tasks").data("action");
                  filterByStatus(action);
                }
              }
          });
    
          function editRow(oTable, nRow, isNew) {
              var aData = oTable.fnGetData(nRow);
              var jqTds = $('>td', nRow);
      
              // TASK NAME
              if (COLMAN.haveAccess(jqTds[0])){
                jqTds[0].innerHTML = '<input id="task-name-inline" style="max-width:120px;" type="text" class="form-control input-small" value="' + aData[0] + '">';
              }
              
              // DATE CREATED
              if (COLMAN.haveAccess(jqTds[1])){
                jqTds[1].innerHTML = '<input id="task-date-created-inline" style="max-width:120px;" type="text" class="form-control input-small" value="' + aData[1] + '">';
              }                
              
              // DATE Reminded
              if (COLMAN.haveAccess(jqTds[2])){
                jqTds[2].innerHTML = '<input id="task-date-reminded-inline" style="max-width:120px;" type="text" class="form-control input-small" value="' + aData[2] + '">';
              }                
              
              // DATE Escalated
              if (COLMAN.haveAccess(jqTds[3])){
                jqTds[3].innerHTML = '<input id="task-date-escalated-inline" style="max-width:120px;" type="text" class="form-control input-small" value="' + aData[3] + '">';
              }        

              // DATE Assigned
              if (COLMAN.haveAccess(jqTds[4])){
                jqTds[4].innerHTML = '<input id="task-date-assigned-inline" style="max-width:120px;" type="text" class="form-control input-small" value="' + aData[4] + '">';
              }                  
      
              // APPROVED
              if (COLMAN.haveAccess(jqTds[5])){
                var checkAttr = (aData[5].toUpperCase() == 'YES') ? ' checked="checked"' : '';  
                var $toggleBtn = $('<label><input class="checkbox-slider toggle colored-purple yesno" type="checkbox"'+checkAttr+'><span class="text"></span></label>');
                jqTds[5].innerHTML = $toggleBtn[0].outerHTML;        
              }
              
              // APPROVED BY
              if (COLMAN.haveAccess(jqTds[6])){
                jqTds[6].innerHTML = '<input id="task-approved-by-inline" style="max-width:120px;" type="text" class="form-control input-small" value="' + aData[6] + '">';
              }                
              
              // APPROVED DATE
              if (COLMAN.haveAccess(jqTds[7])){
                jqTds[7].innerHTML = '<input id="task-approved-by-inline" style="max-width:120px;" type="text" class="form-control input-small" value="' + aData[7] + '">';
              }                
      
              // STATUS 
              if (COLMAN.haveAccess(jqTds[8])){
                // status select
                var $statusSelect = $( $.parseHTML(aData[8])[0] );
                if ($statusSelect.find("select").length == 0){
                  $statusSelect = buildStatusDropDown();
                  $('option[value="'+aData[8]+'"]',$statusSelect).attr("selected", "selected");
                }
                
                jqTds[8].innerHTML = $statusSelect[0].outerHTML;
              }			
              
              // ACTIONS
              jqTds[9].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"><i class="fa fa-times"></i> Cancel</a>';
          }
    
          function restoreRow(oTable, nRow) {
              var aData = oTable.fnGetData(nRow);
              var jqTds = $('>td', nRow);

              for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                  oTable.fnUpdate(aData[i], nRow, i, false);
              }
              oTable.fnDraw();
              $('[data-toggle="tooltip"]',nRow).tooltip();
          }

          function saveRow(oTable, nRow) {
            var jqTds = $('>td', nRow),
                jqSelects = $('select', nRow),
                jqInputs = $('input', nRow);
				
            var rowData = { "id": nRow.id || -1 }
				
            if (COLMAN.haveAccess(jqTds[0])){
              rowData.taskName = jqInputs[0].value;
            }
            
            if (COLMAN.haveAccess(jqTds[1])){
              rowData.dateCreated = jqInputs[1].value;
            }
            
            if (COLMAN.haveAccess(jqTds[2])){
              rowData.dateReminded = jqInputs[2].value;
            }              
            
            if (COLMAN.haveAccess(jqTds[3])){
              rowData.dateEscalated = jqInputs[3].value;
            }              

          if (COLMAN.haveAccess(jqTds[4])){
              rowData.dateAssigned = jqInputs[4].value;
            }              
            
            if (COLMAN.haveAccess(jqTds[5])){
              rowData.approved = jqInputs[5].checked;
            }
            
            if (COLMAN.haveAccess(jqTds[6])){
              rowData.approvedBy = jqInputs[6].value;
            }              
            
            if (COLMAN.haveAccess(jqTds[7])){
              rowData.approvedDate = jqInputs[7].value;
            }                            
      
            if (COLMAN.haveAccess(jqTds[8])){
              rowData.status = jqSelects.eq(0).val();
            }        
				
            function boolToStr(b){
              return b ? 'Yes' : 'No';
            }
        
            function _callback(response){
              if (response.taskName) {
                oTable.fnUpdate(response.taskName, nRow, 0, false);
              }
              if (response.dateCreated) {
                oTable.fnUpdate(response.dateCreated, nRow, 1, false);
              }
              if (response.dateReminded) {
                oTable.fnUpdate(response.dateReminded, nRow, 2, false);
              }					
              if (response.dateEscalated) {
                oTable.fnUpdate(response.dateEscalated, nRow, 3, false);
              }					
              if (response.dateAssigned) {
                oTable.fnUpdate(response.dateAssigned, nRow, 4, false);
              }		              
              if (response.hasOwnProperty("approved")) {
                oTable.fnUpdate(boolToStr(response.approved), nRow, 5, false);
              }					
              if (response.approvedBy) {
                oTable.fnUpdate(response.approvedBy, nRow, 6, false);
              }
              if (response.approvedDate) {
                oTable.fnUpdate(response.approvedDate, nRow, 7, false);
              }
              
              if (response.status) {
                oTable.fnUpdate(response.status, nRow, 8, false);
              }

              var actionsHTML = "";
              if (_options.access.actions.viewDetails){
                actionsHTML += '<button type="button" class="btn btn-blue shiny btn-xs tooltip-blue command-btn details" data-toggle="tooltip" data-placement="top" data-original-title="View Details"><i class="fa fa-external-link"></i></button>';
              }
              
              if (_options.access.actions.edit){
                actionsHTML += '<a href="#" class="btn btn-palegreen shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>';
              }
              
              oTable.fnUpdate(actionsHTML, nRow, 9, false);
              oTable.fnDraw();
              $('[data-toggle="tooltip"]',nRow).tooltip();
            }
              
            _options.saveUpdateHandler(rowData, _callback);
          }
        }
    };
}();

$(window).bind("load", function () {
	var opts = {
		access: {
			actions: { edit: true, viewDetails: true },
			cols: { "taskName": "11", "dateCreated": "11", "dateReminded": "11", "dateEscalated":"11", "dateAssigned":"11", "approved": "11", "approvedBy": "11", "approvedDate": "11", "taskStatus": "11", "actions": "1"}
		},
		saveUpdateHandler: function(data, callback){
			var response = data; //DEMO
			console.warn(response);
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}
		},
    getDetails: function(id, callback){
      var data = {
        taskName: "Test Task",
        itemName: "Test Item",
        taskNote: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
      }
      callback(data);
    },
    saveDetails: function(data, callback){
      console.log(data);
      callback();
    }
	}
	InitUserTasks.init(opts);
});