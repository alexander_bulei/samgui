/**
* Page: manage-categories.html
* Revision: 2
* Modified Date: 18-01-2016
* Author: Alexander Bulei
*/

var InitiateCategoriesDT = function () {
    return {
        init: function (options) {
			var _options =  options || {};
			var $table = $('#categoriesDT');
			var COLMAN = new COLMANAGER($table, _options.access.cols);
			COLMAN.init();			
			// check access
			controlActionBtnsAccess($table, _options.access);

			makeResponsiveTable($table);
            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "bAutoWidth": false				
            });

			
            //Add New Row
            $('#categoriesDT_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '', '<a href="#" class="btn btn-blue shiny btn-xs tooltip-blue command-btn action-icon" data-toggle="tooltip" data-placement="top" data-original-title="Icon"><i class="fa fa-picture-o"></i></a>', '', '', '', '<a href="#" class="btn btn-warning shiny btn-xs tooltip-warning command-btn action-img-settings" data-toggle="tooltip" data-placement="top" data-original-title="Image Settings"><i class="fa fa-cog"></i></a>','<a href="#" class="btn btn-purple shiny btn-xs tooltip-purple command-btn action-rate-definitions" data-toggle="tooltip" data-placement="top" data-original-title="Rate Definitions"><i class="fa fa-bars"></i></a>', buildStatusDropDownHTML(),
                    '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;
				oTable.fnGetPageOfRow(nRow);
            });

            //Clone row
            $table.on("click", 'button.clone', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#cloneModal').data('id', id).modal('show');
            });

            $('#clone-btn').click(function () {
                var id = $('#cloneModal').data('id');
                var anSelected = $('#'+id);
                var data=[];
                $(anSelected).find('td').each(function(){data.push($(this).html());});
                var aiNew = oTable.fnAddData( data );
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                isEditing = nRow;
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                $('.publish-sign').closest('td').addClass('sign');
				oTable.fnGetPageOfRow(nRow);
                $('#cloneModal').modal('hide');
            });
            var isEditing = null;
			
			bindChangeStatusDropDown($table);
			bindClickActiveModalConfirm();
			bindClickInactiveModalConfirm();
			bindCancelStatusModal();			
			
            //Edit row
            $table.on("click", 'a.edit', function (e) {
                e.preventDefault();

                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }
            });

            //Call Delete Modal passing data-id
            $table.on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteModal').data('id', id).modal('show');
            });

            //Delete Row if Confirmed
            $('#delete-btn').click(function () {
				function deleteCallback(response){
					var nRow = $('#' + response.id);
					oTable.fnDeleteRow(nRow);
				}				
				
				var id = $('#deleteModal').data('id');
				_options.deleteHandler(id, deleteCallback);
                $('#deleteModal').modal('hide');
            });

            //Cancel Editing or Adding a Row
            $table.on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });
			
			
			var _DZ_ICON;
			$table.delegate("a.action-icon","click", function(){
				var $modal = $("#uploadIconModal"), $td = $(this).parent(),
					data = $td.data("json") || {};

				$("#uploadIconModal-preview").hide();
				
				if (_DZ_ICON) {
					_DZ_ICON.destroy();
					_DZ_ICON = false;
				}
					
				$("#newIconDropzone").removeClass("dropzone").addClass("dropzone");
				_DZ_ICON = new Dropzone("#newIconDropzone", {
					maxFiles: 1,
					autoProcessQueue: false,
					addRemoveLinks: true,
					url: "upload.php" // REPLACE WITH SERVER URL
				});
				_DZ_ICON.on("success", function(file, response) {
					var response = JSON.parse(response) || {};
					_DZ_ICON.$TD.data("json", { url: response.filePath });
				}).on("maxfilesexceeded", function(file){
					this.removeFile(file);
				});
				
				if (data.url){
					$("#uploadIconModal-preview").attr("src", data.url);
					$("#uploadIconModal-preview").show();
				}
					
				$modal.data("store", $td).modal("show");
			});
			
			$("#uploadIconModal-done").click(function(){
				if (_DZ_ICON) {
					_DZ_ICON.$TD = $("#uploadIconModal").data("store");
					if (_DZ_ICON.getQueuedFiles().length > 0) {                        
						_DZ_ICON.processQueue();  
					} else {                       
						_DZ_ICON.uploadFiles([]);
					}				
				}
				$("#uploadIconModal").modal("hide");
			});
				
			$table.delegate("a.action-img-settings","click", function(){
				var $modal = $("#categoryImageSettings"), $td = $(this).parent(),
					data = $td.data("json") || {};
				
				var $input = $modal.find("#cis-image-width").val("");
				if (data.imageWidth){
					$input.val(data.imageWidth);
				}
				
				$input = $modal.find("#cis-image-height").val("");
				if (data.imageHeight){
					$input.val(data.imageHeight);
				}
				
				$input = $modal.find("#cis-thumb-width").val("");
				if (data.thumbWidth){
					$input.val(data.thumbWidth);
				}

				$input = $modal.find("#cis-thumb-height").val("");
				if (data.thumbHeight){
					$input.val(data.thumbHeight);
				}
				
				$modal.data("store", $td).modal("show");
			});			

			$("#categoryImageSettings #cis-save").click(function(){
				var data = {
					imageWidth: $("#cis-image-width").val(),
					imageHeight: $("#cis-image-height").val(),
					thumbWidth: $("#cis-thumb-width").val(),
					thumbHeight: $("#cis-thumb-height").val()
				}
				
				var $store = $("#categoryImageSettings").data("store");
				$store.data("json", data);
				$("#categoryImageSettings").modal("hide");
			});
			
			$table.delegate("a.action-rate-definitions","click", function(){
				var $modal = $("#categoryRateDefs"), $td = $(this).parent(),
					data = $td.data("json") || {};
				
				_options.getRatesDefsDataHandler(function(response){
					var array = response.data || [];
					$modal.find('select').empty().append('<option value="-1">Select</option>');
					$.each(array, function(i, item){
						$modal.find('select').append('<option value="'+item.id+'">'+item.name+'</option>');
					});
					
					if (data.rate1){ 
						$("#crd-select-1").val(data.rate1);
					}
					if (data.rate2){ 
						$("#crd-select-2").val(data.rate2);
					}					
					if (data.rate3){ 
						$("#crd-select-3").val(data.rate3);
					}					
					if (data.rate4){ 
						$("#crd-select-4").val(data.rate4);
					}					
					if (data.rate5){ 
						$("#crd-select-5").val(data.rate5);
					}					
					if (data.rate6){ 
						$("#crd-select-6").val(data.rate6);
					}					
				});
				
				$modal.data("store", $td).modal("show");
			});			
			
			$("#crd-save").click(function(){
				var data = {
					rate1: $("#crd-select-1").val(),
					rate2: $("#crd-select-2").val(),
					rate3: $("#crd-select-3").val(),
					rate4: $("#crd-select-4").val(),
					rate5: $("#crd-select-5").val(),
					rate6: $("#crd-select-6").val()
				}
				
				var $store = $("#categoryRateDefs").data("store");
				$store.data("json", data);
				$("#categoryRateDefs").modal("hide");				
			});
			
            //Save an Editing Row
            $table.on("click", 'a.save', function (e) {
                e.preventDefault();
				if (this.innerHTML.indexOf("Save") >= 0) {
					var _canSave = true;
					_check = function(erroMsg){
						var v = $input.is("input") ? $input.val() : $input.find("option:selected").val();
						if (( v === "") || (v == null) || (v < 0)) {
							_canSave = _canSave && false;
							$input.addClass("required-error");
							_Error_(erroMsg);
						} else {
							$input.removeClass("required-error");
							_canSave = _canSave && true;
						}						
					}

					$input = $("#category-name-inline");
					_check('The Category Name is required field!');
					
					$input = $("#category-site-inline");
					_check('The Site is required field!');

					$input = $("#category-transtypes-inline");
					_check('The Transaction Type is required field!');
					
					$input = $("#category-caldef-inline");
					_check('The Calendar Default is required field!');

					if (_canSave){
						saveRow(oTable, isEditing);
						isEditing = null;
					}
				}
            });

			function _buildInlineSelect(fnString, callback, param, unique){
				var $select = $("<select id='"+unique+"' class='form-control full-width'></select>");
				$select.append('<option value="-1">Select</option>')
				function _callback(data){					
					var arr = data.data || [];					
					$.each(arr, function(i,obj){
						$op = $("<option value='"+obj.id+"'>"+obj.name+"</option>");
						$select.append($op);
					});
					callback($select);
				}

				_options[fnString](function(response){
					_callback(response);
				}, param);
			}
			
            function editRow(oTable, nRow, isNew) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
				
				// CATEGORY NAME
				if (COLMAN.haveAccess(jqTds[0])){
					jqTds[0].innerHTML = '<input id="category-name-inline" style="max-width:120px;" type="text" class="form-control input-small" value="' + aData[0] + '">';
				}	

				//SITE
				if (COLMAN.haveAccess(jqTds[1])){
					_buildInlineSelect('getSiteDataHandler', function($select){
						var $td = $(jqTds[1]), currV = $td.data("value");
						currV = (typeof currV != "undefined") ? currV : -1;
						$select.change(function(e){
							var v = $(this).val();
							$select.parent().data("value", v);
						});
						
						$('option[value="'+currV+'"]',$select).attr("selected", "selected");	
						$td.empty().append($select);
					},{},'category-site-inline');
				}
				
				//CATEGORY PARENT
				if (COLMAN.haveAccess(jqTds[2])){
					_buildInlineSelect('getCategoriesDataHandler', function($select){
						var $td = $(jqTds[2]), currV = $td.data("value");
						currV = (typeof currV != "undefined") ? currV : -1;
						$select.change(function(e){
							var v = $(this).val();
							$select.parent().data("value", v);
						});
						
						$('option[value="'+currV+'"]',$select).attr("selected", "selected");	
						$td.empty().append($select);
					},{},'category-parent-inline');
				}			

				//LISTING PAGE
				if (COLMAN.haveAccess(jqTds[3])){
					_buildInlineSelect('getListingPagesDataHandler', function($select){
						var $td = $(jqTds[3]), currV = $td.data("value");
						currV = (typeof currV != "undefined") ? currV : -1;
						$select.change(function(e){
							var v = $(this).val();
							$select.parent().data("value", v);
						});
						
						$('option[value="'+currV+'"]',$select).attr("selected", "selected");	
						$td.empty().append($select);
					},{},'category-listingpage-inline');
				}	

				// ICON - 4

				//TRANSACTION TYPE
				if (COLMAN.haveAccess(jqTds[5])){
					_buildInlineSelect('getTransactionTypesDataHandler', function($select){
						var $td = $(jqTds[5]), currV = $td.text();
						currV = (typeof currV != "undefined") ? currV : -1;
						$select.change(function(e){
							var v = $(this).val();
							$select.parent().data("value", v);
						});
						
						$('option[value="'+currV+'"]',$select).attr("selected", "selected");	
						$td.empty().append($select);
					},{},'category-transtypes-inline');
				}				
				
				//CALENDAR DEFAULTS
				if (COLMAN.haveAccess(jqTds[6])){
					_buildInlineSelect('getCalDefaultsDataHandler', function($select){
						var $td = $(jqTds[6]), currV = $td.text();
						currV = (typeof currV != "undefined") ? currV : -1;
						$select.change(function(e){
							var v = $(this).val();
							$select.parent().data("value", v);
						});
						
						$('option[value="'+currV+'"]',$select).attr("selected", "selected");	
						$td.empty().append($select);
					},{},'category-caldef-inline');
				}
				
				//CALENDAR DEFAULTS ONLY
				var checkAttr = (aData[7].toUpperCase() == 'YES') ? ' checked="checked"' : '';
				var $toggleBtn = $('<label><input class="checkbox-slider toggle colored-purple yesno" type="checkbox"'+checkAttr+'><span class="text"></span></label>');
				jqTds[7].innerHTML = $toggleBtn[0].outerHTML;				
				
				// IMAGE SETTIGNS - 8
				
				// RATE DEFINITIONS - 9
				
				// STATUS 
				if (COLMAN.haveAccess(jqTds[10])){
					// status select
					var $statusSelect = $( $.parseHTML(aData[10])[0] );
					if ($statusSelect.find("select").length == 0){
						$statusSelect = buildStatusDropDown();
						$('option[value="'+aData[10]+'"]',$statusSelect).attr("selected", "selected");
					}
					
					jqTds[10].innerHTML = $statusSelect[0].outerHTML;
				}			

				html = (typeof isNew != "undefined") ? ' data-mode="new"' : '';
                jqTds[11].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"'+html+'><i class="fa fa-times"></i> Cancel</a>';
            }
			
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
				$('[data-toggle="tooltip"]',nRow).tooltip();
            }

            function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
				editRow(oTable, nRow, true);
            }

            function saveRow(oTable, nRow) {
				var jqTds = $('>td', nRow),
					jqSelects = $('select', nRow),
					jqInputs = $('input', nRow);
				
				var rowData = { "id": nRow.id || -1 }
				
				
				if (COLMAN.haveAccess(jqTds[0])){
					rowData.name = jqInputs[0].value;
				}
				
				if (COLMAN.haveAccess(jqTds[1])){
					rowData.siteId = jqSelects.eq(0).val();
					rowData.site = jqSelects.eq(0).find('option:selected').text();
				}	
				
				if (COLMAN.haveAccess(jqTds[2])){
					rowData.categoryParentId = jqSelects.eq(1).val();
					rowData.categoryParent = (rowData.categoryParentId > -1) ? jqSelects.eq(1).find('option:selected').text() : "";
				}
				
				if (COLMAN.haveAccess(jqTds[3])){
					rowData.listingPageId = jqSelects.eq(2).val();
					rowData.listingPage = jqSelects.eq(2).find('option:selected').text();
				}				
				
				// ICON
				if (COLMAN.haveAccess(jqTds[4])){
					debugger;
					var data = $(jqTds[4]).data("json") || {};
					if (data.url) {
						rowData.icon = data.url;
					}
				}								
				
				if (COLMAN.haveAccess(jqTds[5])){
					rowData.transactionType = jqSelects.eq(3).find('option:selected').text();
				}		

				if (COLMAN.haveAccess(jqTds[6])){
					rowData.calendarDefault = jqSelects.eq(4).find('option:selected').text();
				}
				
				if (COLMAN.haveAccess(jqTds[7])){
					rowData.calendarDefaultOnly = jqInputs[1].checked;
				}

				// IMAGE SETTINGS
				if (COLMAN.haveAccess(jqTds[8])){
					rowData.imageSettings = $(jqTds[8]).data("json") || {};
				}	
				
				// RATE DEFINITIONS
				if (COLMAN.haveAccess(jqTds[9])){
					rowData.rateDefinitions = $(jqTds[9]).data("json") || {};
				}	
				
				if (COLMAN.haveAccess(jqTds[10])){
					rowData.status = jqSelects.eq(5).val();
				}			
				
				function _callback(response){
					function boolToStr(b){
						return b ? 'Yes' : 'No';
					}						
					
					if (response.name) {
						oTable.fnUpdate(response.name, nRow, 0, false);
					}
					if (response.site) {
						oTable.fnUpdate(response.site, nRow, 1, false);
						$(jqTds[1]).data("value", response.siteId);
					}
					if (response.categoryParentId) {
						oTable.fnUpdate(response.categoryParent, nRow, 2, false);
						$(jqTds[2]).data("value", response.categoryParentId);
					}					
					if (response.listingPageId) {
						oTable.fnUpdate(response.listingPage, nRow, 3, false);
						$(jqTds[3]).data("value", response.listingPageId);
					}				
					
					if (response.icon) {
						//
					}
					
					if (response.transactionType) {
						oTable.fnUpdate(response.transactionType, nRow, 5, false);
					}
					
					if (response.calendarDefault) {
						oTable.fnUpdate(response.calendarDefault, nRow, 6, false);
					}

					if (response.calendarDefaultOnly) {
						oTable.fnUpdate(boolToStr(response.calendarDefaultOnly), nRow, 7, false);
					}						
					
					if (response.imageSettings) {
						$(jqTds[8]).data("json", response.imageSettings);
					}					
					
					if (response.rateDefinitions) {
						$(jqTds[9]).data("json", response.rateDefinitions);
					}										
					
					if (response.status) {
						oTable.fnUpdate(response.status, nRow, 10, false);
					}

					var actionsHTML = "";
					if (_options.access.actions.edit){
						actionsHTML += '<a href="#" class="btn btn-palegreen shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>';
					}
					if (_options.access.actions.clone){
						actionsHTML += '<button type="button" class="btn btn-warning shiny btn-xs clone tooltip-warning command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Clone"><i class="fa fa-copy"></i></button>';
					}
					if (_options.access.actions.delete){
						actionsHTML += '<button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>';
					}
					
					oTable.fnUpdate(actionsHTML, nRow, 11, false);
					oTable.fnDraw();
					$('[data-toggle="tooltip"]',nRow).tooltip();
				}
				
				_options.saveUpdateHandler(rowData, _callback);
            }
        }

    };
}();

$(window).bind("load", function () {
	var opts = {
		access: {
			actions: { edit: true, delete: true, clone: true, new: true },
			cols: { 
				"categoryName": "11", 
				"categorySite": "11", 
				"categoryParent": "11", 
				"categoryListingPage": "11", 
				"categoryIcon": "11", 
				"categoryTransType": "11", 
				"categoryCalDef": "11", 
				"categoryCalDefOnly": "11",
				"categoryImageSettings": "11", 
				"categoryRateDef": "11", 
				"categoryStatus": "11", 
				"actions": "1"
			}
		},
		saveUpdateHandler: function(data, callback){
			var response = data; //DEMO
			console.warn(response);
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}
		},
		deleteHandler: function(id, callback){
			var response = { "id" : id }; //DEMO
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}		
		},
		getCategoriesDataHandler: function(callback){
			var response = {
				data: [
					{id: 1, name: "Category ZXC"},
					{id: 2, name: "Category ABC"},
					{id: 3, name: "Category KOP"}
				]
			};
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}	
		},
		getSiteDataHandler : function(callback){
			var response = {
				data: [
					{id: 1, name: "voli.com"},
					{id: 2, name: "lidl.com"},
					{id: 3, name: "t-mobile.com"}
				]
			};
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}	
		},
		getListingPagesDataHandler : function(callback){
			var response = {
				data: [
					{id: 1, name: "Page 1"},
					{id: 2, name: "Page 2"},
					{id: 3, name: "Page 3"}
				]
			};
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}	
		},
		getTransactionTypesDataHandler : function(callback){
			var response = {
				data:[
					{id: "Reservation", name: "Reservation" },
					{id: "Order", name: "Order" }
				]
			}
			callback(response);			
		},
		getCalDefaultsDataHandler : function(callback){
			var response = {
				data:[
					{id: "Daily", name: "Daily"},
					{id: "Hourly", name: "Hourly"}
				]
			}
			callback(response);
		},
		getRatesDefsDataHandler: function(callback){
			var response = {
				data:[
					{id: "1", name: "Rate 1"},
					{id: "2", name: "Rate 2"},
					{id: "3", name: "Rate 3"},
					{id: "4", name: "Rate 4"},
					{id: "5", name: "Rate 5"}
				]
			}
			callback(response);			
		}
	}	
	InitiateCategoriesDT.init(opts);
});