﻿/**
 * Extension for tasks
 * Version: 1.0
 * Description: Extension for tasks - assign/escalate users/roles | notify
 * Requires: jQuery & Bootstrap3
 * Author: alexander.bulei@gmail.com
 */
 
(function($, window, document, undefined){
	
	// const
	_USER_TMPL = 0, _ESCALATE_TMPL = 1, _NOTIFY_TMPL = 2, _USER_SELECT_TMPL = 3;
	_USER_TMPL_FILE = "tmpl/tasks-user-tmpl.html", _ESCALATE_TMPL_FILE = "tmpl/tasks-escalate-tmpl.html", _NOTIFY_TMPL_FILE = "tmpl/tasks-notify-tmpl.html", _USER_SELECT_TMPL_FILE = "tmpl/tasks-user-select-tmpl.html";
	
	// global
	var $body = $('body'),
		$modal = $body.find('[data-modal=assign]'), /*main modal dialog*/
		$super = $body.find('[data-modal=super]').detach().appendTo('body'); /* second dialog */		

		
	if (!$modal.length){
		$modal = $('<div class="modal modal-primary" style="display: none" data-modal="assign"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h4 class="modal-title">Choose Users</h4></div><div class="modal-body"><div class="checkbox"><label><input type="checkbox" name="user[]" value="specific" class="colored-success"><span class="text">Specific User(s)</span></label></div><div class="checkbox"><label><input type="checkbox" name="user[]" value="roles" class="colored-success"><span class="text">Specific Role(s)</span></label></div><div class="checkbox"><label><input type="checkbox" name="user[]" value="registered" class="colored-success"><span class="text">Registered User</span></label></div></div></div></div></div>');
		$modal.appendTo($body);
	}
	
	if (!$super.length){
		$super = $('<div style="display: none" data-modal="super" class="modal modal-primary task-config-popup"><div class="modal-dialog"><div class="modal-content"><form><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h4 class="modal-title">Change settings</h4></div><div class="modal-body"></div></form></div></div><div class="modal-mask"></div></div>');
		$super.appendTo($body);
	}
	
	// help functions
	
	function getTemplate(tmpl_type, fn){
		var _file;
		switch(tmpl_type) {
			case _USER_TMPL:
				_file = _USER_TMPL_FILE;
				break;
			case _ESCALATE_TMPL:
				_file = _ESCALATE_TMPL_FILE;
				break;
			case _NOTIFY_TMPL:
				_file = _NOTIFY_TMPL_FILE;
				break;
			case _USER_SELECT_TMPL:
				_file = _USER_SELECT_TMPL_FILE;
				break;
		}
		if (!_file) {
			throw "Invalid filename for given template type!";
		}
		$.ajax({
			url: _file,
			async: false,
			success: function(res){
				if ($.isFunction(fn)) { 
					fn(res) 
				}
			}
		})
	}
	
	var $userTmpl, $escalateTmpl, $notifyTmpl, $userSelectTmpl;
	
	getTemplate(_USER_TMPL, function(res){
		$userTmpl = $(res);
		$userTmpl.appendTo($body);
	});
	
	getTemplate(_ESCALATE_TMPL, function(res){
		$escalateTmpl = $(res);
		$escalateTmpl.appendTo($body);
	});	
	
	getTemplate(_NOTIFY_TMPL, function(res){
		$notifyTmpl = $(res);
		$notifyTmpl.appendTo($body);
	});		
	
	getTemplate(_USER_SELECT_TMPL, function(res){
		$userSelectTmpl = $(res);
		$userSelectTmpl.appendTo($body);
	});	
	
	$modal.delegate('[data-list] input[name=filter]', 'keyup', function(){
		var searchFor = $(this).val().toLowerCase(), 
			$rows = $(this).parents('[data-list]:first').find('[data-rows]');

		$rows.find('> .row').each(function(){
			var str = '';
			$(this).find('[data-field]').each(function(){
				str += $(this).is(':input') ? $(this).val().toLowerCase() : $(this).text().toLowerCase() + '|';
			});

			if (str.indexOf(searchFor) > -1) $(this).show(); else $(this).hide();
		});
	}).delegate('[data-list] :input', 'change.taskext', function(){
		var $input = $(this), data = null, 
			$div = $input.parents('[data-list]:first'),
			$rows = $div.find('[data-rows]'),
			$curr = $div.find('[data-curr]');
			/*$row = $div.find('[data-row=example]');*/
		if ($input.is('[name=filter]')) {

		} else if ($input.is('[name="user[]"]')) {
			if ($(this).parents('.currently').size()) {
				var $li = $(this).parents('.row:first').detach();
				$li.prependTo($rows);
			} else {
				var $li = $(this).parents('.row:first').detach();
				if (!$modal.is('[data-config]')) $li.find('.btn-settings').remove();
				$li.prependTo($curr);
			}
			var users = [];
			$div.find('[name="user[]"]:checked').each(function(){
				users.push($(this).val());
			});
			var _type = $modal.attr('data-target');
			data = {
				task_id : $modal.attr('data-id'),
				type : _type,
				form : $modal.attr('data-type'),				
			};
			data[_type] = users;
			$div.trigger('updated');
		} else {
			data = {
				task_id : $modal.attr('data-id'),
				type : $modal.attr('data-target'),
				form : $modal.attr('data-type'),
				config : $div.find(':input').serializeObject()
			};
		}
		if (data) {
			var $link = $modal.data('$link');
			saveObject($link, data);
		}
	}).delegate('[data-list] .btn-settings', 'click', function(){
		var $esc = $escalateTmpl,
			$not = $notifyTmpl,
			id = $(this).parents('.row:first').find('[data-field="id"]').val(),
			type = $(this).parents('[data-list]:first').attr('data-list');

		var $link = $modal.data('$link');
		var dataVal = getRowData($link);
		var config = getDataConfig(dataVal, type, id);

		showSuper($link, $esc.html() + $not.html(), config, $modal.attr('data-id'), type, id, 'assign');
		return false;
	}).delegate('[data-list]', 'updated', function(){
		var $div = $(this),
			$rows = $div.find('[data-rows]'),
			$curr = $div.find('[data-curr]');
		$div.find('[data-value="selected"]').text($curr.find('> .row').size());
		$div.find('[data-value="of"]').text($rows.attr('data-length'));
	});	
	
	$body.delegate('[data-modal=super] [data-dismiss="modal"], [data-modal=super] div.modal-mask', 'click', function(){
		$(this).parents('[data-modal]:first').remove();
		return false;
	}).delegate('[data-modal=super] :checkbox[name="target[]"]', 'change', function(){
		var $sup = $(this).parents('[data-modal]:first'), 
			$div = $userTmpl;

		$div.attr('data-title', $(this).attr('data-title'));
		if ($(this).val() === 'reg') {

		} else {
			showPopup($div, $sup.attr('data-parent'), $(this).val(), $sup.attr('data-type'));
		}
		return false;
	}).delegate('[data-modal=super] a[data-target]', 'click', function(){
		var $sup = $(this).parents('[data-modal]:first'), 
			$div = $userTmpl,
			$link = $sup.data("$link");

		$div.attr('data-title', $(this).attr('data-title'));
		$div.data('$link',$link);
		showPopup($div, $sup.attr('data-parent'), $(this).attr('data-target'), $sup.attr('data-type'));
		return false;
	}).delegate('[data-modal=super] :input', 'change', function(){
		var $sup = $(this).parents('[data-modal]:first');
		var data = {
			task_id : $sup.attr('data-parent'),
			type : $sup.attr('data-type'),
			form : $sup.attr('data-form') || $sup.attr('data-type'),
			id : $sup.attr('data-id'),
			config : $sup.find('form').serializeObject()
		};
		var $link = $sup.data('$link');
		saveObject($link, data);
	}).delegate('[data-modal=super] select[name=target]', 'change', function(){
		var $sup = $(this).parents('[data-modal]:first');
		showPopup($userTmpl, $sup.attr('data-id'), $(this).val());
	});	
	
	$body.delegate('[data-modal=super] [data-dismiss="modal"], [data-modal=super] div.modal-mask', 'click', function(){
		$(this).parents('[data-modal]:first').remove();
		return false;
	}).delegate('[data-modal=super] :checkbox[name="target[]"]', 'change', function(){
		var $sup = $(this).parents('[data-modal]:first'), 
			$div = $userTmpl;

		$div.attr('data-title', $(this).attr('data-title'));
		if ($(this).val() === 'reg') {

		} else {
			showPopup($div, $sup.attr('data-parent'), $(this).val(), $sup.attr('data-type'));
		}
		return false;
	}).delegate('[data-modal=super] a[data-target]', 'click', function(){
		var $sup = $(this).parents('[data-modal]:first'), 
			$div = $userTmpl;

		$div.attr('data-title', $(this).attr('data-title'));
		showPopup($div, $sup.attr('data-parent'), $(this).attr('data-target'), $sup.attr('data-type'));
		return false;
	}).delegate('[data-modal=super] :input', 'change', function(){
		var $sup = $(this).parents('[data-modal]:first');
		var data = {
			task_id : $sup.attr('data-parent'),
			type : $sup.attr('data-type'),
			form : $sup.attr('data-form') || $sup.attr('data-type'),
			id : $sup.attr('data-id'),
			config : $sup.find('form').serializeObject()
		};
		var $link = $sup.data('$link');
		saveObject($link, data);
	}).delegate('[data-modal=super] select[name=target]', 'change', function(){
		var $sup = $(this).parents('[data-modal]:first');
		showPopup($userTmpl, $sup.attr('data-id'), $(this).val());
	});	
	
	// helpers
	function getDataConfig(item, type, id){
		if (id == 0){
			if (item && $.type(item) != 'array'){
				return item;
			} else {
				return {};
			}
		}
		for (var i = 0; i < item[type].length; i++){
			if (item[type][i].id == id) return item[type][i].config;
		}
		return {};
	}

	function createSuper(html){
		var $div = $super.clone();
		$div.find('.modal-body').html(html);
		return $div.appendTo('body');
	}	
	
	function showSuper($link, html, config, parent, type, id, form){
		id = id || 0;
		form = form || 0;
		var $sup = createSuper(html);
		$sup.css('z-index', id ? 1100 : 1000);
		$sup.show().attr('data-form', form).attr('data-id', id).attr('data-type', type).attr('data-parent', parent);
		$sup.data("$link",$link);

		if ($.inArray(type, ['assign', 'escalate', 'notify']) !== -1) {
			var assing = getRowData($link);
			$sup.find('[data-target]').each(function(){
				var name = $(this).attr('data-target');
				if (assing[name] && $.type(assing[name]) == 'array' && assing[name].length > 0 
					|| assing[name] && assing[name] === 'on') {
					$(this).find(':checkbox').attr('checked', true);
				} else {
					$(this).find(':checkbox').removeAttr('checked');
				}
			});
		}

		$.each(config, function(key, value){
			$sup.find(':input[name="'+key+'"], :input[name="'+key+'[]"]').each(function(){
				var $inp = $(this);
				if ($inp.is('[name$="[]"]')){
					if ($.type(value) == 'array') {
						$.each(value, function(i, val){
							var $i = $sup.find(':input[name="'+key+'[]"][value="'+val+'"]');
							if ($i.is(':checkbox')) {
								$i.attr('checked', true);
							} else if ($i.is(':radio')) {
								$i.attr('checked', true);
								return false;
							}
						});
					} else {
						var $i = $sup.find(':input[name="'+key+'[]"][value="'+value+'"]');
						if ($i.is(':checkbox')) {
							$i.attr('checked', true);
						} else if ($i.is(':radio')) {
							$i.attr('checked', true);
							return false;
						}
					}
				} else {
					$inp.val(value);
				}
			});
		});
	}
	
	function getRowData($link){
		var result = $link.data('taskextVal') || {};
		if (typeof result == 'string') {
			result = (result == '') ? '{}' : result;
			result = JSON.parse(result);
		}
		return result;
	}
	
	function showPopup(popup, id, target, type){
		target = target || null;
		type = type || '';
		var $div = $(popup);
		var $link = popup.data('$link');

		$modal.find('.modal-title').text($div.attr('data-title'));
		$modal.find('.modal-body').html($div.html());
		if (type === 'assign') $modal.attr('data-config', true); else $modal.removeAttr('data-config');
		$modal.attr('data-type', type).attr('data-id', id).attr('data-target', target).modal();
		$modal.data("$link", $link);

		if (target) {
			fillPopup($modal, target);
		} else if ($div.is('[data-popup=assign]')) {
			task = getRowData($link);
			if (task.users && task.users.length){
				$modal.find('[name=target]').val('users').trigger('change');
			}
			if (task.roles && task.roles.length){
				$modal.find('[name=target]').val('roles').trigger('change');
			}
		}
	}

	function fillPopup($modal, target){		
		var $link = $modal.data("$link");
		task = getRowData($link);

		$modal.find('[data-list]').hide();
		var $div = $modal.find('[data-list=' + target + ']'),
			$rows = $div.find('[data-rows=' + target + ']'),
			$curr = $div.find('[data-curr=' + target + ']'),
			$row = $div.find('[data-row=example]'), 
			type = $modal.attr('data-type') || null,
			url = $div.attr('data-url') || null;

		$div.show();

		loadData(url, function(data){
			$rows.attr('data-length', data.length);
			$.each(data, function(index, item){
				processRow($row.clone(), item)
					.removeAttr('data-row').attr('data-id', item.id).show().appendTo($rows);
			});
			var task = getRowData($link), users = [];
			if (type && task[type] && task[type][target]) {
				users = task[type][target];
			} else if (task && task[target]) {
				users = task[target];
			}

			$.each(users, function(i, user){
				var $li = $rows.find('.row[data-id=' + user.id + ']').detach();
				$li.appendTo($curr).find('[name="user[]"]').attr('checked', true);
				if (!$modal.is('[data-config]')) $li.find('.btn-settings').remove();
				if ($.type(user.config) == 'object' && !$.isEmptyObject(user.config)){
					$li.find('.btn-settings > i').addClass('text-danger');
				}
			});
			$div.trigger('updated');
		});
	}

	function loadData(url, fn){
		if (!url) {
			fn([]);
			return false;
		}
		$.get(url, function(res){
			fn(res);
		}).fail(function(jqXHR, textStatus, errorThrown){
			alert(errorThrown);
		});
		return false;
	}
	
	function processRow(el, item){
		item = item || {};
		var $li = $(el);

		$.each(item, function(key, value){
			var $field = $li.find('[data-field="'+key+'"]');
			if ('boolean' == $.type(value)){
				$field.find('[data-value]').hide();
				$field.find('[data-value=' + (value ? 'true' : 'false') + ']').show();
			} else if ($field.is(':input')) {
				$field.val(value);
			} else {
				$field.text(value);
			}
		});
		return $li;
	}
	
	function saveObject($link, data){
		if (data.users && $.type(data.users) == 'array' && data.users.length == 0){
			data.users = '[]';
		}
		
		var getIndexIfObjWithAttr = function(array, attr, value) {
			for(var i = 0; i < array.length; i++) {
				if(array[i][attr] === value) {
					return i;
				}
			}
			return -1;
		}		
		
		var data_json = getRowData($link);		
		var newArr = data[data.type];
		var copyNewArr = $.extend([],newArr);
		
		if ((data.config) && (data.id)){
			if ($.isArray(data_json[data.type])) {
				var idx = getIndexIfObjWithAttr(data_json[data.type],'id', data.id);
				if (idx > -1){
					data_json[data.type][idx].config = data.config;
				}
			} else {
				$.each(data_json, function(i,obj){
					if (!data.config[i]){
						delete data_json[i];
					}
				});
				data_json = $.extend(data_json,data.config);
			}
		} else {
			if (data_json[data.type]) {
				$.each(data_json[data.type], function(i,obj){
					if ($.inArray(obj.id, newArr) == -1){
						data_json[data.type] = $.grep(data_json[data.type], function(item) {
							return item.id != obj.id;
						});				
					} else {
						copyNewArr.splice( $.inArray(data.id, copyNewArr), 1 );
					}
				});
			}

			if (copyNewArr.length){
				$.each(copyNewArr, function(i,value){
					var newObj = {};
					newObj['id'] = value;
					newObj['config'] = [];
					if (!data_json[data.type]){
						data_json[data.type] = [];
					}
					data_json[data.type].push(newObj);
				});
			}
		}
		$link.data('taskextVal', data_json);
	}	
			
	function showUserSelectModal($link){
		var _options = $link.data('taskext-opts');
		$modal.find('.modal-body').html($userTmpl.html());		
		$modal.off("change.taskext change.singleuser").delegate('[data-list] :input', 'change.singleuser', function(){
			var $input = $(this), data = null, 
				$div = $input.parents('[data-list]:first'),
				$rows = $div.find('[data-rows]'),
				$curr = $div.find('[data-curr]');
				
			if ($input.is('[name=filter]')) {

			} else if ($input.is('[name="user[]"]')) {				
				if ($(this).parents('.currently').size()) {
					var $li = $(this).parents('.row:first').detach();
					$li.prependTo($rows);
				} else {
					if (_options.singleUserSelect){
						$li = $curr.find('.row').detach();
						$li.prependTo($rows);
						$input = $li.find('input');
						if ($input.length) {
							$input[0].checked = false;
						}
					}
					var $li = $(this).parents('.row:first').detach();
					if (!$modal.is('[data-config]')) $li.find('.btn-settings').remove();
					$li.prependTo($curr);
				}
				var users = [];
				$div.find('[name="user[]"]:checked').each(function(){
					var $row = $(this).closest('.row');
					var userName = $row.find('div[data-field="firstname"]').text() + ' ' + $row.find('div[data-field="lastname"]').text();
					var obj = { 
						"id" : $(this).val(),
						"name":  userName
					};
					users.push(obj);
				});
				
				var data = {
					"users" : users 
				}				
				$link.data("taskextVal", data);
			}
		});
		
		var $div = $modal.find('[data-list="users"]'),					
			$rows = $div.find('[data-rows="users"]'),
			$curr = $div.find('[data-curr="users"]'),
			$row = $div.find('[data-row=example]'), 
			type = $modal.attr('data-type') || null,
			url = $div.attr('data-url') || null,
			target = "users";
			
		$div.show();

		loadData(url, function(data){
			$rows.attr('data-length', data.length);
			$.each(data, function(index, item){
				processRow($row.clone(), item)
					.removeAttr('data-row').attr('data-id', item.id).show().appendTo($rows);
			});
			var task = getRowData($link), users = [];
			if (type && task[type] && task[type][target]) {
				users = task[type][target];
			} else if (task && task[target]) {
				users = task[target];
			}

			$.each(users, function(i, user){
				var $li = $rows.find('.row[data-id=' + user.id + ']').detach();
				$li.appendTo($curr).find('[name="user[]"]').attr('checked', true);
				if (!$modal.is('[data-config]')) $li.find('.btn-settings').remove();
				if ($.type(user.config) == 'object' && !$.isEmptyObject(user.config)){
					$li.find('.btn-settings > i').addClass('text-danger');
				}
			});
			$div.trigger('updated');
		});	
		$modal.modal();
	}		
			
	/** END OF HELPERS **/
			
	$.fn.serializeFormObject = function(){
         var o = {};
         var a = this.serializeArray();
         $.each(a, function() {
             if (o[this.name]) {
                 if (!o[this.name].push) {
                     o[this.name] = [o[this.name]];
                 }
                 o[this.name].push(this.value || '');
             } else {
                 o[this.name] = this.value || '';
             }
         });
         return o;
    };			
			
	$.fn.serializeObject = function () {
		"use strict";

		var result = {};
		var extend = function (i, element) {
			var _name = element.name;
			var pattern = /\[\]/g;
			var forceArr = false;
			if (pattern.test(_name)){
				_name = _name.replace("[]", "");
				forceArr = true;
			}
			var node = result[_name];

			if ('undefined' !== typeof node && node !== null) {
				if ($.isArray(node)) {
					node.push(element.value);
				} else {
					result[_name] = [node, element.value];
				}
			} else {
				if (forceArr) {
					result[_name] = [];
					result[_name].push(element.value);
				} else {
					result[_name] = element.value;
				}
			}
		};

		$.each(this.serializeArray(), extend);
		return result;
	};
			
	$.fn.taskExtension = function(options){
		var opts = {			
			onlyUserSelect: false,
			singleUserSelect: false
		}
		
		opts = $.extend({}, opts, options || {});
		return this.each(function(){
			var $link = $(this);			
			if ($link.data('taskext')){
				return true;
			}
			$link.data('taskext-opts',opts);
			$link.data('taskext', true);
			$link.on("click.taskext", function(e){				
				var $self = $(this);
				var cmd = $self.data('taskextType');
				var dataVal = getRowData($self);				
				var _opts =  $self.data('taskext-opts');
				
				if (!_opts.onlyUserSelect) {
					switch (cmd) {
						case 'assign':
							var id = 0, $id = $(this).closest('tr').attr('id'),
								config = getDataConfig(dataVal, 'assign', id),
								html = $userSelectTmpl.html();
							showSuper($self ,html, config, $id, 'assign');
							break;
						case 'escalate':
							var id = 0, $id = $(this).closest('tr').attr('id'),
								config = getDataConfig(dataVal, 'escalate', id),							
								html = $escalateTmpl.html() + $userSelectTmpl.html();
							showSuper($self, html, config, $id, 'escalate');
							break;
						case 'notify':
							var id = 0, $id = $(this).closest('tr').attr('id'),
								config = getDataConfig(dataVal, 'notify', id),
								html = $notifyTmpl.html() + $userSelectTmpl.html();
							showSuper($self, html, config, $id, 'notify');
							break;
					}
				} else {
					showUserSelectModal($self);
				}
			});
		});
	}

})(jQuery, window, document);