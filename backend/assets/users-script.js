/**
* Javascript for manage-users.html
* Revision: 12
* Date: 14-07-2015
* Author: Alexander Bulei
*/

var _userTileTableOptions = {
	containerID : "tileview-table",
	next:  'Next',
	previous : 'Prev',
	perPage: 6,
	callback: function(pages,items){
		$(".tileview-info").html("Showing "+items.range.start+" to "+items.range.end+" of "+items.count+" entries");
	}
}

var InitiateUserManagerDataTable = function () {
    return {
        init: function (options) {			
			var $table = $('#userManagerDatatable');
			
			var _options =  options || {};
			// check access
			controlActionBtnsAccess($table, _options.access);
			
            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumns" : [
                    { sWidth: '8%' },
                    { sWidth: '8%' },
                    { sWidth: '8%' },
                    { sWidth: '6%' },
                    { sWidth: '4%' },
					{ sWidth: '4%' },
                    { sWidth: '20%' }
                ],
                "bAutoWidth": false
            });

            $("tfoot input.grid-input", $table).keyup(function () {
                /* Filter on the column (the index) of this element */
                oTable.fnFilter(this.value, $("tfoot input.grid-input").index(this));
            });


            var isEditing = null;

			bindChangeStatusDropDown($('#userManagerDatatable'));
			bindClickActiveModalConfirm();
			bindClickInactiveModalConfirm();
			bindCancelStatusModal();


            //Edit row
            $table.on("click", 'a.edit', function (e) {
                e.preventDefault();

                $('#editUserModal').modal('show');
                $("#CompanyState2").select2();
                $("#CompanyCountry2").select2();
                $("#companyForm").bootstrapValidator();

                $('.phone').mask('+9 (999) 999 99 99');
                $('.phone_us').mask('(999) 999-9999');

                /* change tel if country == US */
                $(document).on('change', 'select[name="country"]', function() {
                    var $country = $(this).val();
                    if ($country === "US") {
                        $('.phone').removeClass('phone').addClass('phone_us');
                    }
                    else {
                        $('.phone_us').removeClass('phone_us').addClass('phone');
                    }
                });
            });

            //Dashboard
            $table.on("click", 'button.dashboard', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#dashboardModal').data('id', id).modal('show');
            });

            //Clone row
            $table.on("click", 'button.clone', function (e) {
                e.preventDefault();

                var id = $(this).closest('tr').attr('id');
                $('#cloneModal').data('id', id).modal('show');

            });

            $('#clone-btn').click(function () {
                var id = $('#cloneModal').data('id');
                $('#cloneModal').modal('hide');
                $("#CompanyState3").select2();
                $("#CompanyCountry3").select2();
                $("#companyForm").bootstrapValidator();

                $('.phone').mask('+9 (999) 999 99 99');
                $('.phone_us').mask('(999) 999-9999');

                /* change tel if country == US */
                $(document).on('change', 'select[name="country"]', function() {
                    var $country = $(this).val();
                    if ($country === "US") {
                        $('.phone').removeClass('phone').addClass('phone_us');
                    }
                    else {
                        $('.phone_us').removeClass('phone_us').addClass('phone');
                    }
                });
                $('#cloneUserModal').data('id', id).modal('show');
            });

            //Call Delete Modal passing data-id
            $table.on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
				var $modal = $('#deleteModal');
				$modal.find(".modal-title").text("Are you sure you want to delete this user?");
				
				$("#delete-btn", $modal).data("dtTable", oTable);
                $('#deleteModal').data('id', id).modal('show');
            });

            //Delete Row if Confirmed
			/*
            $('#delete-btn').click(function () {
                var id = $('#deleteModal').data('id');
                var nRow = $('#'+id);
                oTable.fnDeleteRow(nRow);
                $('#deleteModal').modal('hide');
            });
			*/

            //Cancel Editing or Adding a Row
            $table.on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Save an Editing Row
            $table.on("click", 'a.save', function (e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
                    saveRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[4] + '">';
                jqTds[5].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[5] + '">';
                jqTds[6].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"><i class="fa fa-times"></i> Cancel</a>';
                $("#country").val(aData[0]);
            }
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
            }

            function editAddedRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                aData[0]
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[4] + '">';
                jqTds[5].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[5] + '">';
                jqTds[6].innerHTML = aData[6];
                $("#country").val(aData[0]);
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('.form-control', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                oTable.fnUpdate(jqInputs[4].value, nRow, 4, false);
                oTable.fnUpdate('<i class="fa fa-check publish-sign"></i>', nRow, 5, false);
                oTable.fnUpdate('<a href="#" class="btn btn-palegreen  shiny btn-xs edit"><i class="fa fa-edit"></i> Edit</a> <button type="button" class="btn  btn-sky shiny btn-xs activate"><i class="fa fa-power-off"></i> Activate</button> <button type="button" class="btn  btn-sky shiny btn-xs deactivate"><i class="fa fa-power-off"></i> Inactivate</button> <button type="button" class="btn btn-danger delete btn-smll shiny btn-xs"><i class="fa fa-trash-o"></i> Delete</button> ', nRow, 6, false);
                oTable.fnDraw();
            }
        }

    };
}();

var InitiateUserListDataTable = function () {
    return {
        init: function (options) {
			var $table = $('#userListDatatable');
			var _options =  options || {};
			// check access
			controlActionBtnsAccess($table, _options.access);
			
            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumns" : [
                    { sWidth: '8%' },
                    { sWidth: '8%' },
                    { sWidth: '8%' },
                    { sWidth: '8%' },
                    { sWidth: '6%' },
                    { sWidth: '4%' },
                    { sWidth: '20%' }
                ],
                "bAutoWidth": false
            });

            $("tfoot input.list-input", $table).keyup(function () {
                /* Filter on the column (the index) of this element */
                oTable.fnFilter(this.value, $("tfoot input.list-input").index(this));
            });


            var isEditing = null;

            $table.on("click", 'button.activate', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#activateModal').data('id', id).modal('show');
            });

            $table.on("click", 'button.dashboard', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#dashboardModal').data('id', id).modal('show');
            });

            //Activate if Confirmed
            $('#activate-btn').click(function () {
                var id = $('#activateModal').data('id');
                $('#' + id + ' i.publish-sign').removeClass('fa-times');
                $('#' + id + ' i.publish-sign').addClass('fa-check');
                $('#'+id+' button.activate').hide().next().show();
                $('#activateModal').modal('hide');
            });

            //Deactivate popup
            $table.on("click", 'button.deactivate', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#modalDeactivate').data('id', id).modal('show');
            });

            //Deactivate if Confirmed
            $('#deactivate-btn').click(function () {
                var id = $('#modalDeactivate').data('id');

                $('#'+id+' button.deactivate').hide().prev().show();
                $('#modalDeactivate').modal('hide');
            });

            //Edit row
            $table.on("click", 'a.edit', function (e) {
                e.preventDefault();

                $('#editUserModal').modal('show');
                $("#CompanyState2").select2();
                $("#CompanyCountry2").select2();
                $("#companyForm").bootstrapValidator();

                $('.phone').mask('+9 (999) 999 99 99');
                $('.phone_us').mask('(999) 999-9999');

                /* change tel if country == US */
                $(document).on('change', 'select[name="country"]', function() {
                    var $country = $(this).val();
                    if ($country === "US") {
                        $('.phone').removeClass('phone').addClass('phone_us');
                    }
                    else {
                        $('.phone_us').removeClass('phone_us').addClass('phone');
                    }
                });
            });

            //Clone row
            $table.on("click", 'button.clone', function (e) {
                e.preventDefault();

                var id = $(this).closest('tr').attr('id');
                $('#cloneModal').data('id', id).modal('show');

            });

            $('#clone-btn').click(function () {
                var id = $('#cloneModal').data('id');
                $('#cloneModal').modal('hide');
                $("#CompanyState3").select2();
                $("#CompanyCountry3").select2();
                $("#companyForm").bootstrapValidator();

                $('.phone').mask('+9 (999) 999 99 99');
                $('.phone_us').mask('(999) 999-9999');

                /* change tel if country == US */
                $(document).on('change', 'select[name="country"]', function() {
                    var $country = $(this).val();
                    if ($country === "US") {
                        $('.phone').removeClass('phone').addClass('phone_us');
                    }
                    else {
                        $('.phone_us').removeClass('phone_us').addClass('phone');
                    }
                });
                $('#cloneUserModal').data('id', id).modal('show');
            });


            //Call Delete Modal passing data-id
            $table.on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
				var $modal = $('#deleteModal');
				$modal.find(".modal-title").text("Are you sure you want to delete this user?");
				$("#delete-btn", $modal).data("dtTable", oTable);
                $('#deleteModal').data('id', id).modal('show');
            });


            //Delete Row if Confirmed
			/*
            $('#delete-btn').click(function () {
                var id = $('#deleteModal').data('id');
                var nRow = $('#'+id);
                oTable.fnDeleteRow(nRow);
                $('#deleteModal').modal('hide');
            });
			*/

            //Cancel Editing or Adding a Row
            $table.on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Save an Editing Row
            $table.on("click", 'a.save', function (e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
                    saveRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[4] + '">';
                jqTds[5].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[5] + '">';
                jqTds[6].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[6] + '">';
                jqTds[7].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"><i class="fa fa-times"></i> Cancel</a>';
            }
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
            }

            function editAddedRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                aData[0]
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[4] + '">';
                jqTds[5].innerHTML = '<input type="text" class="form-control input-small"  value="' + aData[5] + '">';
                jqTds[6].innerHTML = aData[6];
                $("#country").val(aData[0]);
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('.form-control', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                oTable.fnUpdate(jqInputs[4].value, nRow, 4, false);
                oTable.fnUpdate(jqInputs[5].value, nRow, 5, false);
                oTable.fnUpdate(jqInputs[6].value, nRow, 6, false);
                oTable.fnUpdate('<a href="#" class="btn btn-palegreen  shiny btn-xs edit"><i class="fa fa-edit"></i> Edit</a> <button type="button" class="btn  btn-sky shiny btn-xs activate"><i class="fa fa-power-off"></i> Activate</button> <button type="button" class="btn  btn-sky shiny btn-xs deactivate"><i class="fa fa-power-off"></i> Inactivate</button> <button type="button" class="btn btn-danger delete btn-smll shiny btn-xs"><i class="fa fa-trash-o"></i> Delete</button> ', nRow, 7, false);
                oTable.fnDraw();
            }
        }

    };
}();

var InitiateSkillNewDataTable = function () {
    return {
        init: function (options) {
			var $table =  $('#skilldatatable');
			var _options =  options || {};
			// check access
			controlActionBtnsAccess($table, _options.access);
			
            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumns" : [
                    { sWidth: '11%' },
                    { sWidth: '11%' },
                    { sWidth: '14%' }
                ],
                "bAutoWidth": false
            });

            //Add New Row
            $('#skilldatatable_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '',
                    '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                // ID per row. Gets how many rows have the table + 1.
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n; //nRow.id add an ID to the the row so maybe you can get the ID from database. If so you won't need the line above

                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;
            });

            var isEditing = null;

            //Edit row
            $table.on("click", 'a.edit', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }
            });


            //Call Delete Modal passing data-id
            $table.on("click", 'a.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
				var $modal = $('#deleteModal');
				$modal.find(".modal-title").text("Are you sure you want to delete this skill?");
				$("#delete-btn", $modal).data("dtTable", oTable);
                $('#deleteModal').data('id', id).modal('show');
            });

            //Delete Row if Confirmed
			/*
            $('#delete-btn').click(function () {
                var id = $('#deleteModal').data('id');
                var nRow = $('#'+id);
                oTable.fnDeleteRow(nRow);
                // ID is the row id to delete. Maybe you can hook up a AJAX Function to delete from database
                $('#deleteModal').modal('hide');
            });
			*/

            //Cancel Editing or Adding a Row
            $table.on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Save an Editing Row
            $table.on("click", 'a.save', function (e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
                    saveRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small" style="width: 108px !important;" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small" style="width: 74px !important;" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"><i class="fa fa-times"></i> Cancel</a>';
            }
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
            }

            function editAddedRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small" style="width: 108px !important;" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small extra-smll-2" style="width: 74px !important;" value="' + aData[1] + '">';
                jqTds[2].innerHTML = aData[2];
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('.form-control', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate('<a href="#" class="btn btn-palegreen tooltip-success shiny btn-xs edit command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a><a href="#" class="btn btn-danger delete tooltip-danger btn-xs shiny btn-xs command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>', nRow, 2, false);
                oTable.fnDraw();
				$('[data-toggle="tooltip"]',nRow).tooltip();
            }
        }

    };
}();

var InitiateSkillCloneDataTable = function () {
    return {
        init: function (options) {
			var _$table = $('#skillClonedatatable');
			var _options =  options || {};
			// check access
			controlActionBtnsAccess(_$table, _options.access);

            //Datatable Initiating
            var oTable = _$table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumns" : [
                    { sWidth: '11%' },
                    { sWidth: '11%' },
                    { sWidth: '14%' }
                ],
                "bAutoWidth": false
            });

            //Add New Row
            $('#skillClonedatatable_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '',
                    '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                // ID per row. Gets how many rows have the table + 1.
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n; //nRow.id add an ID to the the row so maybe you can get the ID from database. If so you won't need the line above

                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;
            });

            var isEditing = null;

            //Edit row
            $('#skillClonedatatable').on("click", 'a.edit', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }
            });


            //Call Delete Modal passing data-id
            $('#skillClonedatatable').on("click", 'a.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
				var $modal = $('#deleteModal');
				$modal.find(".modal-title").text("Are you sure you want to delete this skill?");
				$("#delete-btn", $modal).data("dtTable", oTable);
                $('#deleteModal').data('id', id).modal('show');
            });

            //Delete Row if Confirmed
			/*
            $('#delete-btn').click(function () {
                var id = $('#deleteModal').data('id');
                var nRow = $('#'+id);
                oTable.fnDeleteRow(nRow);
                // ID is the row id to delete. Maybe you can hook up a AJAX Function to delete from database
                $('#deleteModal').modal('hide');
            });
			*/

            //Cancel Editing or Adding a Row
            $('#skillClonedatatable').on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Save an Editing Row
            $('#skillClonedatatable').on("click", 'a.save', function (e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
                    saveRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small" style="width: 108px !important;" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small" style="width: 74px !important;" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"><i class="fa fa-times"></i> Cancel</a>';
            }
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
            }

            function editAddedRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small" style="width: 108px !important;" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small extra-smll-2" style="width: 74px !important;" value="' + aData[1] + '">';
                jqTds[2].innerHTML = aData[2];
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('.form-control', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate('<a href="#" class="btn btn-palegreen tooltip-success shiny btn-xs edit command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a><a href="#" class="btn btn-danger delete tooltip-danger btn-xs shiny btn-xs command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>', nRow, 2, false);
                oTable.fnDraw();
				$('[data-toggle="tooltip"]',nRow).tooltip();
            }
        }

    };
}();

var InitiateSkillEditDataTable = function () {
    return {
        init: function (options) {
			var _options =  options || {};
			var _$table = $('#skillEditdatatable');
			
			// check access
			controlActionBtnsAccess(_$table, _options.access);

            //Datatable Initiating
            var oTable = _$table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumns" : [
                    { sWidth: '11%' },
                    { sWidth: '11%' },
                    { sWidth: '14%' }
                ],
                "bAutoWidth": false
            });

            //Add New Row
            $('#skillEditdatatable_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '',
                    '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                // ID per row. Gets how many rows have the table + 1.
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n; //nRow.id add an ID to the the row so maybe you can get the ID from database. If so you won't need the line above

                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;
			});

            var isEditing = null;

            //Edit row
            $('#skillEditdatatable').on("click", 'a.edit', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }
            });


            //Call Delete Modal passing data-id
            $('#skillEditdatatable').on("click", 'a.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
				var $modal = $('#deleteModal');
				$modal.find(".modal-title").text("Are you sure you want to delete this skill?");
				$("#delete-btn", $modal).data("dtTable", oTable);
                $('#deleteModal').data('id', id).modal('show');
            });

            //Delete Row if Confirmed
			/*
            $('#delete-btn').click(function () {
                var id = $('#deleteModal').data('id');
                var nRow = $('#'+id);
                oTable.fnDeleteRow(nRow);
                // ID is the row id to delete. Maybe you can hook up a AJAX Function to delete from database
                $('#deleteModal').modal('hide');
            });
			*/

            //Cancel Editing or Adding a Row
            $('#skillEditdatatable').on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Save an Editing Row
            $('#skillEditdatatable').on("click", 'a.save', function (e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
                    saveRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small" style="width: 108px !important;" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small" style="width: 74px !important;" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"><i class="fa fa-times"></i> Cancel</a>';
            }
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
            }

            function editAddedRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small" style="width: 108px !important;" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small extra-smll-2" style="width: 74px !important;" value="' + aData[1] + '">';
                jqTds[2].innerHTML = aData[2];
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('.form-control', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate('<a href="#" class="btn btn-palegreen tooltip-success shiny btn-xs edit command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a><a href="#" class="btn btn-danger delete tooltip-danger btn-xs shiny btn-xs command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>', nRow, 2, false);
                oTable.fnDraw();
				$('[data-toggle="tooltip"]',nRow).tooltip();
            }
        }

    };
}();

var InitiateUserTileTable = function () {
    return {
        init: function (options) {
			var $table =  $('#tileview-table');
			var _options =  options || {};
			// check access
			controlActionBtnsAccess($table, _options.access);
			
            $table.on("click", 'button.dashboard', function (e) {
                e.preventDefault();
                var id = $(this).closest('li').attr('id');
                $('#dashboardModal').data('id', id).modal('show');
            });
		
		
            //Edit row button click
            $table.on("click", 'a.edit', function (e) {
                e.preventDefault();

                $('#editUserModal').modal('show');
                $("#CompanyState2").select2();
                $("#CompanyCountry2").select2();
                $("#companyForm").bootstrapValidator();

                $('.phone').mask('+9 (999) 999 99 99');
                $('.phone_us').mask('(999) 999-9999');

                /* change tel if country == US */
                $(document).on('change', 'select[name="country"]', function() {
                    var $country = $(this).val();
                    if ($country === "US") {
                        $('.phone').removeClass('phone').addClass('phone_us');
                    }
                    else {
                        $('.phone_us').removeClass('phone_us').addClass('phone');
                    }
                });
            });		
		
            $table.on("click", 'button.activate', function (e) {
                e.preventDefault();
                var id = $(this).closest('li').attr('id');
                $('#activateModal').data('id', id).modal('show');
            });

            //Activate if Confirmed
            $('#activate-btn').click(function () {
                var id = $('#activateModal').data('id');
                $('#'+id+' button.activate', $table).hide().next().show();
                $('#activateModal').modal('hide');
            });

            //Deactivate popup
            $table.on("click", 'button.deactivate', function (e) {
                e.preventDefault();
                var id = $(this).closest('li').attr('id');
                $('#modalDeactivate').data('id', id).modal('show');
            });

            //Deactivate if Confirmed
            $('#deactivate-btn').click(function () {
                var id = $('#modalDeactivate').data('id');
                $('#'+id+' button.deactivate', $table).hide().prev().show();
                $('#modalDeactivate').modal('hide');
            });

            //Clone row
            $table.on("click", 'button.clone', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#cloneModal').data('id', id).modal('show');
            });

			$('#clone-btn').click(function () {
                var id = $('#cloneModal').data('id');
                $('#cloneModal').modal('hide');
                $("#CompanyState3").select2();
                $("#CompanyCountry3").select2();
                $("#companyForm").bootstrapValidator();

                $('.phone').mask('+9 (999) 999 99 99');
                $('.phone_us').mask('(999) 999-9999');

                /* change tel if country == US */
                $(document).on('change', 'select[name="country"]', function() {
                    var $country = $(this).val();
                    if ($country === "US") {
                        $('.phone').removeClass('phone').addClass('phone_us');
                    }
                    else {
                        $('.phone_us').removeClass('phone_us').addClass('phone');
                    }
                });
                $('#cloneUserModal').data('id', id).modal('show');
            });
			
            //Call Delete Modal passing data-id
            $table.on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('li').attr('id');
				var $modal = $('#deleteModal');
				$modal.find(".modal-title").text("Are you sure you want to delete this user?");				
				$("#delete-btn", $modal).data("dtTable", $table);
				$("#delete-btn", $modal).data("tileview", true);
                $('#deleteModal').data('id', id).modal('show');
            });
           
            //Delete Row if Confirmed
			/*
            $('#delete-btn').click(function () {
                var id = $('#deleteModal').data('id');
                var nRow = $('#'+id, $table);
                nRow.remove();
				$(".tileview-holder").jPages("destroy").jPages(_userTileTableOptions);
				styleTilePagination($(".tileview-holder"));
                $('#deleteModal').modal('hide');
            });
			*/
		}
	}
}();

$(window).bind("load", function () {
	/*Sets Themed Colors Based on Themes*/
	themeprimary = getThemeColorFromCss('themeprimary');
	themesecondary = getThemeColorFromCss('themesecondary');
	themethirdcolor = getThemeColorFromCss('themethirdcolor');
	themefourthcolor = getThemeColorFromCss('themefourthcolor');
	themefifthcolor = getThemeColorFromCss('themefifthcolor');

	//Initialise dataTable
	var commonOpts = {
		access: {
			edit: true,
			delete: true
		}
	}
	
	
	InitiateUserManagerDataTable.init(commonOpts);
	InitiateUserListDataTable.init(commonOpts);
	InitiateUserTileTable.init(commonOpts);

	InitiateSkillNewDataTable.init(commonOpts);
	InitiateSkillCloneDataTable.init(commonOpts);
	InitiateSkillEditDataTable.init(commonOpts);
});

//Add New Row
$('#userListDatatable_new').click(function (e) {
	e.preventDefault();

	$('#newModal').modal('show');
	$("#CompanyState").select2();
	$("#CompanyCountry").select2();
	$("#companyForm").bootstrapValidator();

	$('.phone').mask('+9 (999) 999 99 99');
	$('.phone_us').mask('(999) 999-9999');

	/* change tel if country == US */
	$(document).on('change', 'select[name="country"]', function() {
		var $country = $(this).val();
		if ($country === "US") {
			$('.phone').removeClass('phone').addClass('phone_us');
		}
		else {
			$('.phone_us').removeClass('phone_us').addClass('phone');
		}
	});
});

$('#userManagerDatatable_new').click(function (e) {
	e.preventDefault();

	$('#newModal').modal('show');
	$("#CompanyState").select2();
	$("#CompanyCountry").select2();
	$("#companyForm").bootstrapValidator();

	$('.phone').mask('+9 (999) 999 99 99');
	$('.phone_us').mask('(999) 999-9999');

	/* change tel if country == US */
	$(document).on('change', 'select[name="country"]', function() {
		var $country = $(this).val();
		if ($country === "US") {
			$('.phone').removeClass('phone').addClass('phone_us');
		}
		else {
			$('.phone_us').removeClass('phone_us').addClass('phone');
		}
	});
});

$('#userTileDatatable_new').click(function (e) {
	e.preventDefault();

	$('#newModal').modal('show');
	$("#CompanyState").select2();
	$("#CompanyCountry").select2();
	$("#companyForm").bootstrapValidator();

	$('.phone').mask('+9 (999) 999 99 99');
	$('.phone_us').mask('(999) 999-9999');

	/* change tel if country == US */
	$(document).on('change', 'select[name="country"]', function() {
		var $country = $(this).val();
		if ($country === "US") {
			$('.phone').removeClass('phone').addClass('phone_us');
		}
		else {
			$('.phone_us').removeClass('phone_us').addClass('phone');
		}
	});
});
$('.btn-info').click(function() {
	$('#showModal').modal('show');
});

$('#grid_view').click(function() {
	$('#list_view').removeClass('active');
	$('#tile_view').removeClass('active');
	$('#grid_view').addClass('active');

	$('.tile-view-table').fadeOut('slow');
	$('.list-view-table').fadeOut('slow');
	$('.grid-view-table').delay(600).fadeIn('slow');
});

$('#list_view').click(function() {
	$('#grid_view').removeClass('active');
	$('#tile_view').removeClass('active');
	$('#list_view').addClass('active');

	$('.tile-view-table').fadeOut('slow');
	$('.grid-view-table').fadeOut('slow');
	$('.list-view-table').delay(600).fadeIn('slow');
});

$('#tile_view').click(function() {
	$('#list_view').removeClass('active');
	$('#grid_view').removeClass('active');
	$('#tile_view').addClass('active');

	$('.list-view-table').fadeOut('slow');
	$('.grid-view-table').fadeOut('slow');
	
	$('#tileview-table').css('visibility','hidden');
	
	$('.tile-view-table').delay(600).fadeIn('slow', function(){
		$(".tileview-holder").jPages(_userTileTableOptions);		
		$("#tileview-select-perpage").change(function(){
			var newPerPage = parseInt( $(this).val() );
			_options = $.extend(true,_options,{ perPage : newPerPage });
			$(".tileview-holder").jPages("destroy").jPages(_options);
			styleTilePagination($(".tileview-holder"));
		});
		$('#tileview-table').css('visibility','visible');
		styleTilePagination($(".tileview-holder"));
	});
});

$(function(){
	/* bind delete confirm event for common modal dialogs */	
	$('#delete-btn').click(function () {
		var $this = $(this);
		var id = $('#deleteModal').data('id');
		var _table = $this.data("dtTable");
		var nRow = $('#'+id, _table);
		var _isTileview = $this.data("tileview") || false;
		
		if (_isTileview) {
			nRow.remove();
			$(".tileview-holder").jPages("destroy").jPages(_userTileTableOptions);
			styleTilePagination($(".tileview-holder"));			
		} else {
			_table.fnDeleteRow(nRow);
		}
		
		$('#deleteModal').modal('hide');
	});	
	
	// load template
	$.ajax({
		url: 'tmpl/users-dashboard.html',
		async: false,
		success: function(res){
			var $modal = $("#dashboardModal");
			$modal.find(".modal-body").append(res);
		}
	});
	
	
	var myDropzone = new Dropzone("#newPicDropzone");
	myDropzone.on("success", function(file) {
		/* TODO: UPDATE IMAGE IN DASHBOARD */
	});
	
	$("a.upload-new-pic").click(function(e){
		var $modal = $("#uploadImageModal");
		$modal.data("picture", $(this).data("picture")).modal("show");
	});
	
	$("#new-picture-done").click(function(e){
		var where = $modal.data("picture");
		$modal.modal("hide");
	});	
})