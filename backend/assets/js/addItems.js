

var $newLine = '<li class="dd-item dd2-item">' +

    '<div class="dd-handle dd2-handle bg-info"><i class="drag-icon fa fa-arrows-alt "></i></div><div class="dd2-content no-hover"><span class="heading">New item</span>' +
    '<div class="pull-right">' +
    '<a href="#" class="btn btn-danger shiny btn-xs delete" data-toggle="modal" data-target="#modalDelete"><i class="fa fa-trash-o"></i> Delete</a>' +
    '<a href="#" class="btn btn-palegreen shiny btn-xs edit"><i class="fa fa-edit"></i> Edit</a>' +
    '</div>' +
    '</div>' +
    '<div class="widget list-edit">' +
    '<div class="widget-body">' +
    '<form role="form">' +
    '<div class="row">' +
    '<div class="col-xs-3 item">' +
    '<div class="form-group">' +
    '<label for="heading"> Heading </label>' +
    '<input type="text" placeholder="" name="heading" data-bv-field="heading" value="" class="form-control">' +
    '</div>' +
    '</div>' +
    '<div class="col-xs-3 item">' +
    '<div class="form-group">' +
    '<label for="Permissions">Permissions</label>' +
    '<input type="text" placeholder="" name="Permissions" data-bv-field="Permission" class="form-control" >' +
    '</div>' +
    '</div>' +
    '<div class="col-xs-3 item line">' +
    '<div class="form-group">' +
    '<label for="lineItem">Line item</label>' +
    '<input type="text" placeholder="" name="lineItem" data-bv-field="lineItem" class="form-control" >' +
    '</div>' +
    '</div>' +
    '<div class="col-xs-3 item">' +
    '<div class="form-group" >' +
    '<label for="url"> URL/Link </label>' +
    '<input type="url" placeholder="" name="url" data-bv-field="url" class="form-control" >' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</form>' +
    '</div>' +
    '</div>' +
    '</li>';

$(document).on('click', '#addListElem', function(e) {
    $(this).closest('.dd.shadowed').children('.dd-list').prepend($newLine).bootstrapValidator({
        // Only disabled elements are excluded
        // The invisible elements belonging to inactive tabs must be validated
        excluded: [':disabled'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        submitHandler: function(validator, form, submitButton) {
            // Do nothing
        },
        fields: {
            heading: {
                validators: {
                    notEmpty: {
                        message: 'The heading is required'
                    }
                }
            },
            lineItem: {
                validators: {
                    notEmpty: {
                        message: 'The lineItem is required'
                    }
                }
            },
            url: {
                validators: {
                    notEmpty: {
                        message: 'The address is required'
                    }
                }
            }
        }
    });
});

$(document).on('click', '.edit', function(e) {
    $(this).closest('.dd-item').children('.list-edit').toggleClass('hide');
});


$(document).on('input', 'form input', function(e) {

    var $this = $(this);
    var $name = $(this).attr('name');

    if ($name === 'heading') {
        $(this).closest('.head').siblings('.line').find('input').each(function() {
            $(this).val('').attr('placeholder', '');
            $(this).siblings('.help-block').hide().siblings('.form-control-feedback').hide();
            $(this).closest('.has-feedback').removeClass('has-error has-success');
        });
    }
    if ($name === 'lineItem') {
        $(this).closest('form').find('input[name=heading]')
            .val('').attr('placeholder', '')
            .siblings('.help-block').hide()
            .siblings('.form-control-feedback').hide()
            .closest('.has-feedback').removeClass('has-error').removeClass('has-success');

    }


});

$(document).on('keydown', 'form input', function(e) {
    var $form = $(this).closest('form');
    var $ddItem = $(this).closest('.dd-item');

    if (e.keyCode == 13) {

        /* stop sending form... */
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();

        /* adding input value to heading */
        if ($(this).attr('name') === 'url') {
            var $text = $(this).closest('form').find('input[name=lineItem]').val();

        } else {
            var $text = $(this).val();
        }

        $ddItem.children('.dd-handle').children('.heading').text($text);
        $ddItem.children('.list-edit').toggleClass('hide');
    }
});

