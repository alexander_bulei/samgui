/**
 * Column Chooser
 * Version: 1.1
 * Description: Column chooser for simple table
 * Requires: jQuery, Bootstrap3 & Bootstrap Multiselect
 * Author: alexander.bulei@gmail.com
 * Copyright: Copyright 2015 Alexander Bulei 
 */

function _init_chooser(){
	$("table[data-cchooser]").each(function() {
		var $table = $(this);
		var $select = $( $table.data("cchooserSelector") );
		
		var checkColumn = function($table,index,show){
			if (show) {
				$table.find('td:nth-child('+ index +'),th:nth-child('+index+')').show();
			} else {
				$table.find('td:nth-child('+ index +'),th:nth-child('+index+')').hide();
			}
		}
		
		if ($select.length){
			$select.empty().attr("multiple","multiple");
			var uncheckedArr = $table.data("cchooserUnchecked") || [];
			var excludeArr = $table.data("cchooserExclude") || [];
			
			var $ths = $table.find("thead tr:not(.search_inputs)").find("th");
			$.each($ths, function(i,th){
				var $th = $(th);
				var $op = $("<option>");
				var _text = $th.text();
				var thIndex = parseInt($th.index())+1;
				if (_text) {
					if ($.inArray(thIndex,excludeArr) == -1) {
						$op.val($th.index()).text(_text).appendTo($select);
					}
					if ($.inArray(thIndex,uncheckedArr) == -1){
						$op.attr("selected","selected");
					} else {
						checkColumn($table,thIndex,false);
					}
				}
			});
			
			$select.data("tableControl", $table);
			
			if ($select.data("multiselect")){
				$select.multiselect('destroy');
			}
			
			$select.multiselect({
				onChange: function(option, checked) {
					var _$select = this.$select;
					var _$table =  $(_$select.data("tableControl"));
					var colIndex = parseInt(option.val())+1;
					checkColumn(_$table,colIndex,checked);
				},
				buttonText: function(options) {
					//return options.length + ' columns';
					return 'Show/Hide Columns';
                }
            });
		}		
	});		
} 
 
;(function($, document, window, undefined) {
	_init_chooser();
})(jQuery, document, window);