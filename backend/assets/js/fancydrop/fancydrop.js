﻿/**
* Fancy Drop
* Version: 1.0.3
* Description: Dropdown with FuncyTree
* Requires: jQuery v1.10+, jQuery UI, Bootstrap, FancyTree
* Author: Alexander Bulei 
* © Copyright Merlix LLC 2016
**/

;(function($, document, window, undefined) {
    // Optional, but considered best practice by some
    "use strict";

    // Name the plugin so it's only in one place
    var pluginName = 'fancydrop';

    // Default options for the plugin as a simple object
    var defaults = {
        width: '120px',
        height: 24,
        selectedLabel: '{count} items(s) selected',
        fancyTreeOptions: {
            extensions: ["glyph"],
            toggleEffect: false,
            icon: false,
            checkbox: true,
            glyph: {
                map: {
                    doc: "",
                    docOpen: "",
                    checkbox: "fa fa-square-o",
                    checkboxSelected: "fa fa-check-square-o",
                    checkboxUnknown: "fa fa-square",
                    dragHelper: "fa arrow-right",
                    dropMarker: "fa long-arrow-right",
                    error: "fa fa-warning",
                    expanderClosed: "fa fa-caret-right",
                    expanderLazy: "fa fa-angle-right",
                    expanderOpen: "fa fa-caret-down",
                    folder: "",
                    folderOpen: "",
                    loading: "fa fa-spinner fa-pulse"
                }
            },
            init: function(event, data){
                var selArr = data.tree.getSelectedNodes();
                if (selArr.length){
                    var thisObj = $(this).data("fancydrop");
                    thisObj.setChecked(selArr);
                }                
            },
            select: function(event, data) {
                var selArr = data.tree.getSelectedNodes(),
                    thisObj = $(this).data("fancydrop");
                thisObj.setChecked(selArr);
            }
        },
        comboOpenIcon: 'fa-caret-down',
        comboCloseIcon: 'fa-caret-up',
        popupHeight: 'auto',
		disabled: false,
		readonly: false,
        showSearchPanel: true,
        toolbar: true,
        searchOptions: {
            defaultFilterId: 'contains',
            searchBy: 'title',
            matchClass: 'warning',
            caseSensitive: false,
            dropDownSearchOp: true,
            _filters: [{ id: 'contains', title: 'Contains', abbr: '~' },{ id: 'beginsWith', title: 'Begins with', abbr: '^' }, { id: 'equal', title: 'Equal', abbr: '==' } ]
        }
        
    };

    // Plugin constructor
    // This is the boilerplate to set up the plugin to keep our actual logic in one place
    function Plugin(element, options) {
        this.element = element;

        // Merge the options given by the user with the defaults
        this.options = $.extend(true, {}, defaults, options)

        // Attach data to the elment
        this.$element = $(element);
        this.$original = this.$element.clone();
        this.$element.data(name, this);

        this._selectedNodes = [];
        this.$tree = $('<div />');
        this._defaults = defaults;
        this.init();
    }

    Plugin.prototype = {
        destroy: function(){
            this.$overlay.remove();
            this.$treeWrapper.remove();
            this.$element.replaceWith( this.$original );
        },
        init: function() {
            var that = this;
            this.$overlay = createOverlay().on('click', function(e){ 
                    that.close();
                });

            this.$element.width( this.options.width );
            this.$combo = $('<a>')
                .addClass('form-control fancydrop-combo')
                .attr({
                    'style': 'height:' + this.options.height + 'px !important; line-height: ' +this.options.height + 'px',
                    'unselectable': 'on'
                })
                .on('click', this.toggleCombobox );                

			if (this.options.disabled) {
				this.$combo.addClass('ui-state-disabled');
			}
			
			if (this.options.readonly){
				this.setReadOnly(true);
			}
			
            this.$comboTitle = $('<span>')
                .addClass('fancydrop-combo-title')
                .html( this.options.placeholder );

              
            this.$comboIcon = $('<i>').addClass('fancydrop-combo-icon fa ' + this.options.comboOpenIcon);
                
            this.$treeWrapper = $('<div>')
                .attr( 'id', this.element.id + '-popup-container')
                .addClass( 'fancydrop-popup-container fancytree-colorize-selected' );                

                
            if (this.options.toolbar){
                this.$toolbar = $('<div/>').addClass('fancydrop-toolbar');
                this.$toolbar.append('<a href="#" data-action="select" class="btn btn-success btn-xs fancydrop-toolbar-item fancydrop-toolbar-select">Select</a>');
                
                if (this.options.fancyTreeOptions && (this.options.fancyTreeOptions.selectMode != 1)) { 
                    this.$toolbar.append('<a href="#" data-action="check" class="btn btn-default btn-xs fancydrop-toolbar-item fancydrop-toolbar-check-all">Check All</a>');
                    this.$toolbar.append('<a href="#" data-action="uncheck" class="btn btn-default btn-xs fancydrop-toolbar-item fancydrop-toolbar-uncheck-all">Uncheck All</a>');
                }
            }
            
            if (this.options.showSearchPanel) {
            
                this.$searchPanel = $('<div>')
                    .addClass('fancydrop-search-panel');
                    
                this.$searchFilterDiv = $('<div>')
                    .addClass('fancydrop-filter');

                this.$searchFiltersPopup = $('<ul>')
                    .addClass('fancydrop-filters-ul ui-widget ui-widget-content ui-corner-all')
                    .hide(); 
                    
                this.$searchPopupOverlay = createOverlay().on('click', function(e){ 
                    that.$searchFiltersPopup.hide();
                    that.$searchPopupOverlay.hide();
                });                    
                
                this.$searchFilterTitle = $('<span>')
                    .addClass('ui-widget fancydrop-filter-span')
                    .on('click', function(event){
						if (that.options.readonly) { return false; }
                        var self = $(this).data(); 
                        
                        if (!self.$searchFiltersPopup.is(':visible')) {
                            var zIndex = self.$overlay.css("zIndex");
                            zIndex = (typeof(zIndex) == 'number') ? zIndex : 999999;
                            zIndex++;
                            self.$searchPopupOverlay.css("z-index", zIndex).show();
                            zIndex++;
                            self.$searchFiltersPopup.css({
                                left: self.$searchFilterDiv.offset().left + (self.$searchFilterDiv.outerWidth()/2) + 'px',
                                top: self.$searchFilterDiv.offset().top + self.$searchFilterDiv.outerHeight() - 4 + 'px',
                                'z-index': zIndex 
                            })
                            .show();
                        }
                    });
                    
                this.$searchInput = $('<input>')
                    .attr('type','text')
                    .addClass('form-input input-xs fancydrop-search-input')
                    .on("keyup-change input paste", function(event){
                        var $this = $(this),
                            self = $this.data(),
                            opt = $.extend(true, self.options.searchOptions, { _currFilter: self.filterData });
                        doSearchNode(self.treeData.tree, $this.val(), null, self.options.searchOptions)
                    });                
                
                $.each(this.options.searchOptions._filters, function(i, o){
                    if (o.id == that.options.searchOptions.defaultFilterId) {
                        that.$searchInput.data("filterData", o);
                        that.$searchFilterTitle.html(o.abbr)
                    }
                    var html = '<li><a href="javascript:void(0)" class="ui-corner-all"><span class="fancydrop-filters-title">'+o.abbr+'</span><span class="fancydrop-filters-name">'+o.title+'</span></a></li>'
                    var $li = $(html);
                    $li.find('a').data('filterData', o);
                    that.$searchFiltersPopup.append($li);
                });
                
                var filterLinks = that.$searchFiltersPopup.find('a');                
                filterLinks
                    .on('mouseover', function(){
                        $(this).addClass('ui-state-hover');
                    })
                    .on('mouseout', function(){
                        $(this).removeClass('ui-state-hover');
                    })
                    .on('click', function(){                                    
                        var self = $(this).data();                        
                        self.$searchFilterTitle.html(self.filterData.abbr);
                        self.$searchFiltersPopup.hide();
                        self.$searchPopupOverlay.hide();
                        self.$searchInput.data( "filterData", self.filterData ).trigger('keyup-change');
                    })
                
            }
            
            // adjustments
            this.$tree.data('fancydrop', this).fancytree(this.options.fancyTreeOptions);
            this.treeData = this.$tree.data('ui-fancytree');
            this.$tree.removeAttr('style');
            this.$tree.find('.fancytree-container').addClass('fancydrop-treeview');
            this.$tree.css({ width: '100%' }).addClass('fancydrop-tree-wrapper');
            
            this.$treeWrapper.hide();

            // appends
            $('body')
                .append(this.$overlay)
                .append(this.$treeWrapper);
            this.$element.append(this.$combo);
            this.$combo
                .append(this.$comboTitle)
                .append(this.$comboIcon); 
                
            if (this.options.toolbar){
                this.$treeWrapper.append(this.$toolbar);
            }
            
            if (this.options.showSearchPanel) {
                this.$treeWrapper.append(this.$searchPanel);
                if (this.options.searchOptions.dropDownSearchOp) {
                    this.$searchPanel.css('padding-left','32px');
                    $('body')
                        .append(this.$searchFiltersPopup)
                        .append(this.$searchPopupOverlay);
                    this.$searchFilterDiv.append(this.$searchFilterTitle);
                    this.$searchPanel.append(this.$searchFilterDiv);
                }
                this.$searchPanel.append(this.$searchInput);
            }
            this.$treeWrapper.append(this.$tree);
            
            // share object
            this.$combo.data(this);
            
            if (this.options.showSearchPanel) {
                this.$searchInput.data(this);
                if (this.options.searchOptions.dropDownSearchOp) {
                    this.$searchFilterTitle.data(this);
                    filterLinks.data(this);
                }
            }
            
            var that = this;
            if (this.options.toolbar){
                this.$toolbar.find('a').click(function(e){
                    var $this = $(this), action = $this.data("action");
                    if (action == 'select'){
                        var selectedNodes = that.treeData.tree.getSelectedNodes();
                        that.setChecked(selectedNodes);
                        that.close();
                    } else if (action == 'check'){
                        that.treeData.tree.visit(function(node){
                            node.setSelected(true);
                        });
                    } else if (action == 'uncheck'){
                        that.treeData.tree.visit(function(node){
                            node.setSelected(false);
                        });                    
                    }
                });
            }
        },
		
        toggleCombobox: function (event) {
            var that = $(this).data();
            that._openned() ? that.close() : that.open();
        },
        getSelectedLabel: function(count){
            return count ? this.options.selectedLabel.replace('{count}', count) : "";
        },
        setTitle: function(title){
            this.$comboTitle.html(title);
        },
        open: function(){
            var that = this;            
            if ((!that.treeData) || (that.options.disabled)) { return false; }
            if (that.options.popupHeight != 'auto') {
                var popupH = that.options.popupHeight,
                    treeH;
                treeH = popupH;
                that.$treeWrapper.height(popupH);
                if (that.options.showSearchPanel) {
                    treeH = popupH - that.$searchPanel.outerHeight(true);                    
                }
                that.$tree.css({ height: treeH + 'px' })
            }				
            
            that.$overlay.show();
            that.$element.toggleClass('ui-corner-all ui-corner-top');                
            that.$comboIcon.removeClass(that.options.comboOpenIcon).addClass(that.options.comboCloseIcon);
            that.$treeWrapper.css({
                'position': 'absolute',
                'min-width':  that.$element.width() + 'px',
                'left': that.$element.offset().left + 'px',
                'top': that.$element.offset().top + that.$element.outerHeight() + 'px'
            });
            
            that.$treeWrapper.show(1, function(){
                if (that.options.showSearchPanel) {
                    setTimeout(function(){
                        that.$searchInput.focus();                        
                    },0)
                
                }                
            });
        },
        close: function(){
            var that = this;
            if (!that.treeData){ return false; }            
            that.$overlay.hide();            
            that.$element.toggleClass('ui-corner-top ui-corner-all');
            that.$comboIcon.removeClass(that.options.comboCloseIcon).addClass(that.options.comboOpenIcon);
            that.$treeWrapper.hide();
        },

        _openned: function(){            
            return this.$treeWrapper.is(":visible");
        },
        setDisabled: function(value){
			this.options.disabled = value;
			value ? this.$combo.addClass("ui-state-disabled") : this.$combo.removeClass("ui-state-disabled");
		},
		setReadOnly: function(value){
			this.options.readonly = value;
			var cmd = value ? 'disable' : 'enable';
			this.treeData.element.fancytree(cmd);
			if (this.options.showSearchPanel) {
				if (value){
					this.$searchPanel.addClass('ui-state-disabled');
					this.$searchInput.attr("readonly","true");
				} else {
					this.$searchPanel.removeClass('ui-state-disabled');
					this.$searchInput.removeAttr("readonly");
				}
			}
		},
        setChecked: function(selectedArray){
            var that = this;
            that._selectedNodes = [];
            $.each(selectedArray, function(i, node){
                that._selectedNodes.push(node.key);
            });
            that.setTitle( that.getSelectedLabel(that._selectedNodes.length) );            
        },
        getChecked: function(){
            return this._selectedNodes;
        }
    };

    $.fn[pluginName] = function(options) {
        if (typeof options == 'string'){
            var dataObj = this.data('plugin_' + pluginName);
            if ($.isFunction(dataObj[options])){
                return dataObj[options]();
            }
        } else {
            return this.each(function() {
                if (!$.data(this, 'plugin_' + pluginName)) {
                    $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
                }
            });
        }
    };    
    
    function doSearchNode(treeData, query, startFrom, searchOpts){
        var startNode = (!startFrom) ? treeData.getRootNode() : startFrom;        
        startNode.visit(function (node) {
            // 'startWith','equal','contains'
            var regExp;
            if (searchOpts._currFilter.id == 'contains') {
                regExp = new RegExp(query, (!searchOpts.caseSensitive) ? 'i' : '');
            } else if (searchOpts._currFilter.id == 'beginsWith') {        
                regExp = new RegExp('^' + query, (!searchOpts.caseSensitive) ? 'i' : '')
            } else if (searchOpts._currFilter.id == 'equal') {
                regExp = new RegExp('^' + query + '$', (!searchOpts.caseSensitive) ? 'i' : '');
            }
             
            if (regExp) {
                var str = (searchOpts.searchBy == 'title') ? node.title : node.key;
                if ((regExp.test(str)) && (query !== '')) {
                        node.makeVisible();
                        $(node.span).find('.fancytree-title').addClass(searchOpts.matchClass);                    
                        //node.activateSilently();
                        //return false;
                } else {
                    $(node.span).find('.fancytree-title').removeClass(searchOpts.matchClass);
                }
            }
        });    
    }
    
    function createOverlay(){
        return $('<div/>').addClass('fancydrop-overlay');
    }

})(jQuery, document, window);