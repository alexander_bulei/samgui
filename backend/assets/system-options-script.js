/**
* Javascript for manage-system-options.html
* Revision: 2
* Date: 14-07-2015
* Author: Alexander Bulei
*/

var InitiateSysOptionsDataTable = function () {
    return {
        init: function (options) {
			var _options =  options || {};
			var $table = $('#sysOptionsDT');
			
			// check access
			controlActionBtnsAccess($table, _options.access);
			
            //Datatable Initiating			
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
				responsive: true
            });
            var i = oTable.fnGetData().length + 1;

            //Add New Row
            $('#sysOptionsDT_new').click(function (e) {
                e.preventDefault();
				var siteDD, categDD, optTypeDD, optUnitDD;
				
				_buildInlineSelect('site-select', 'getSiteDataHandler', function($select){
					siteDD = $select[0].outerHTML;
				});

				_buildInlineSelect('category-select', 'getCategoryDataHandler', function($select){
					categDD = $select[0].outerHTML;
				});
				
				_buildInlineSelect('opt-type-select', 'getOpTypeDataHandler', function($select){
					optTypeDD = $select[0].outerHTML;
				});

				var aiNew = oTable.fnAddData(['', siteDD, categDD, optTypeDD, buildStatusDropDownHTML(), '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>']);
				
				var nRow = oTable.fnGetNodes(aiNew[0]);
				var n = (oTable.fnGetData().length + 1);
				nRow.id = n;
				editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;			
				
            });

            //Edit row
            $table.on("click", 'a.edit', function (e) {
                e.preventDefault();

                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }

            });

            var isEditing = null;

			bindChangeStatusDropDown($table);
			bindClickActiveModalConfirm();
			bindClickInactiveModalConfirm();
			bindCancelStatusModal();

            //Call Delete Modal passing data-id
            $table.on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteModal').data('id', id).modal('show');
            });

            //Delete Row if Confirmed
            $('#delete-btn').click(function () {
				
                var id = $('#deleteModal').data('id');
				
				function _callback(response){
					if (!response.errorMsg) {
						var nRow = $('#' + response.id);
						oTable.fnDeleteRow(nRow);
					} else {
						Notify(response.errorMsg, 'bottom-right', '5000', 'danger', 'fa-times', true);
					}
				}				

				_options.deleteHandler(id,_callback);
				$('#deleteModal').modal('hide');
            });

            //Clone row
            $table.on("click", 'button.clone', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#cloneModal').data('id', id).modal('show');
            });

            $('#clone-btn').click(function () {
                var id = $('#cloneModal').data('id');
                var anSelected = $('#'+id);
                var data=[];
                $(anSelected).find('td').each(function(){data.push($(this).html());});
                var aiNew = oTable.fnAddData( data );
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow, true);
                isEditing = nRow;
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                $('#cloneModal').modal('hide');
            });		
			
            //Cancel Editing or Adding a Row
            $table.on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Save an Editing Row
            $table.on("click", 'a.save', function (e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
					var _canSave = true;
					_check = function(erroMsg){
						var v = $input.is("input") ? $input.val() : $input.find("option:selected").data("id");
						if (( v === "") || (v == null) || (v < 0)) {
							_canSave = _canSave && false;
							$input.addClass("required-error");
							_Error_(erroMsg);
						} else {
							$input.removeClass("required-error");
							_canSave = _canSave && true;
						}						
					}
					
					$input = $(".inline-sysop-name");
					_check('The System Option Name is required field!');
					
					$input = $("#opt-type-select");
					_check('The System Option Type is required field!');

					if (_canSave){					
						saveRow(oTable, isEditing);
						isEditing = null;
					}
                }
            });
			
			function _buildInlineSelect(name, fn, callback){
				var $select = $("<select id='"+name+"' class='full-width'></select>");
				
				function _callback(data){					
					var arr = data.data || [];					
					$.each(arr, function(i,obj){
						$op = $("<option data-id='"+obj.id+"' value='"+obj.name+"'>"+obj.name+"</option>");
						$select.append($op);
					});
					callback($select);
				}
				
				_options[fn](_callback);				
			}

            function editRow(oTable, nRow, isNew) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small full-width inline-sysop-name" value="' + aData[0] + '">';
				
				_buildInlineSelect('site-select', 'getSiteDataHandler', function($select){					
					$('option[value="'+aData[1]+'"]',$select).attr("selected", "selected");
					jqTds[1].innerHTML = $select[0].outerHTML;
				});

				_buildInlineSelect('category-select', 'getCategoryDataHandler', function($select){
					$('option[value="'+aData[2]+'"]',$select).attr("selected", "selected");
					jqTds[2].innerHTML = $select[0].outerHTML;
				});
				
				_buildInlineSelect('opt-type-select', 'getOpTypeDataHandler', function($select){
					$('option[value="'+aData[3]+'"]',$select).attr("selected", "selected");
					jqTds[3].innerHTML = $select[0].outerHTML;
				});
				
				// status select
				var $statusSelect = $( $.parseHTML(aData[4])[0] );
				if ($statusSelect.find("select").length == 0){
					$statusSelect = buildStatusDropDown();
					$('option[value="'+aData[4]+'"]',$statusSelect).attr("selected", "selected");
				}				
				
                jqTds[4].innerHTML = $statusSelect[0].outerHTML;
				var str = (typeof isNew != "undefined") ? ' data-mode="new"' : "";
                jqTds[5].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" '+str+'><i class="fa fa-times"></i> Cancel</a>';

            }
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
            }

            function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
				editRow(oTable, nRow, true);
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
				var jqSelects = $('select', nRow);
				
				var rowData = {
					"id": nRow.id || -1,
					"sysOptionName": jqInputs[0].value,
					"siteId": jqSelects.eq(0).val(),
					"categoryId": jqSelects.eq(1).val(),
					"optionTypeId": jqSelects.eq(2).val(),					
					"status": jqSelects.eq(4).val()
				}

				function _callback(response){
					if (!response.errorMsg){
						var _id, _out;
						
						oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
						
						// non-required
						_id = jqSelects.eq(0).find("option:selected").data("id");
						_out = (_id < 0) ? "" : jqSelects.eq(0).val();
						oTable.fnUpdate(_out, nRow, 1, false);

						// non-required
						_id = jqSelects.eq(1).find("option:selected").data("id");
						_out = (_id < 0) ? "" : jqSelects.eq(1).val();
						oTable.fnUpdate(_out, nRow, 2, false);
						
						oTable.fnUpdate(jqSelects.eq(2).val(), nRow, 3, false);
						oTable.fnUpdate(jqSelects.eq(3).val(), nRow, 4, false);						
						// command buttons
						oTable.fnUpdate('<a href="#" class="btn btn-palegreen shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a><button type="button" class="btn btn-warning shiny btn-xs clone tooltip-warning command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Clone"><i class="fa fa-copy"></i></button><button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>', nRow, 5, false);
						oTable.fnDraw();
						$('[data-toggle="tooltip"]',nRow).tooltip();
					} else {
						Notify(response.errorMsg, 'bottom-right', '5000', 'danger', 'fa-times', true);
					}
				}
				
				//save
				_options.saveUpdateHandler(rowData, _callback);
            }
        }

    };
}();

$(window).bind("load", function () {
	//Initialise dataTable
	var initOp = {
		access: {
			edit: true,
			delete: true
		},		
		getSiteDataHandler : function(callback){
			//var response = { "errorMsg" : "Some Error!"}; // response with error 
			var response = {
				data: [
					{id: -1, name: "Select"},
					{id: 0, name: "NC Tourism"},
					{id: 1, name: "Dummy"}
				]
			};
			callback(response);
		},
		getCategoryDataHandler: function(callback){
			//var response = { "errorMsg" : "Some Error!"}; // response with error 
			var response = {
				data: [
					{id: -1, name: "Select"},
					{id: 0, name: "Vacation Rental"},
					{id: 1, name: "Dummy"}
				]
			};
			callback(response);
		},
		getOpTypeDataHandler: function(callback){
			var response = {
				data: [
					{id: -1, name: "Select an option"},
					{id: 0, name: "Checkbox"},
					{id: 1, name: "Textbox"},
					{id: 2, name: "Radio"},
					{id: 3, name: "Combo"}
				]
			};
			callback(response);			
		},		
		deleteHandler: function(id, callback){
			var response = { "id" : id };
			//var response = { "errorMsg" : "Some Error!"}; // response with error 
			callback(response);
		},
		saveUpdateHandler: function(data, callback){
			var response = { categories: data };
			callback(response);
		}		
	}
	InitiateSysOptionsDataTable.init(initOp);
});