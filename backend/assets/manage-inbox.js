/**
* Manage Inbox
* Revision: 1.4-RC
* Last Modified: 21-12-2015
* Author: Alexander Bulei
*/
var CONST_INBOX = 'inbox', CONST_SENT = 'sent', CONST_TRASH = 'trash', CONST_IMP = 'important',
	CONST_TYPE_LEAD = 1, CONST_TYPE_OUT = 2;
var MANAGEINBOX = {
	options: {
		maxRecords: 10
	},
	renderStart: 0,
	view: CONST_INBOX,
	data: [],
	typesChecked: [0,1,2],
	notifyError: function(msg){
		Notify(msg, 'bottom-right', '5000', 'danger', 'fa-times', true);
	},
	notifySuccess: function(msg){
		Notify(msg, 'bottom-right', '5000', 'success', 'fa-check', true)
	},
	promptMsg: function(title, msg, confirmClick){
		var $modal = $("#modal-danger");
		$modal.find('.modal-title').html(title);
		$modal.find('.modal-body').html(msg);
		$modal.find('button.btn-danger').one('click', function(e){
			if ($.isFunction(confirmClick)){
				confirmClick();
			}
		});
		$modal.modal("show");
	},
	userMsg: function(msg){
		var $elem = this.panels.$reader.find("li.message-box");
		$elem.find('span.msg').text(msg);
		$elem.fadeIn(500).delay(1500).fadeOut(500);
	},
	convertToMailView: function(extObj){
		var o = {
			id: extObj.id,
			type: extObj.type,
			sender: extObj.sender,
			senderEMail: extObj.senderEMail,
			subject: extObj.subject,
			attachments: extObj.attachments,
			body: extObj.body,
			replies: extObj.replies
		};
		
		if (o.type == CONST_TYPE_LEAD){
			o.extSubjectKeyPairs = extObj.extSubjectKeyPairs;
		}		
		
		var obj = this.userFriendlyDT(extObj.datetime);
		o.datetime = obj.output;
		return o;
	},
	userFriendlyDT: function(DTStr){
		var result = {
			momentObj: moment(DTStr, this.options.dateFormat)
		}

		var momToday = moment(), strDT = "",
			REF_TODAY = momToday.format('DD/MM/YYYY'),
			REF_YESTERDAY = momToday.clone().subtract(1,'d').format('DD/MM/YYYY'),
			CURR = result.momentObj.format('DD/MM/YYYY');
		
		if (CURR == REF_TODAY){ // TODAY
			strDT = 'Today, ' + result.momentObj.format('HH:mm');
		} else if (CURR == REF_YESTERDAY){ // YESTERDAY
			strDT = 'Yesterday, ' + result.momentObj.format('HH:mm');
		} else {
			strDT = result.momentObj.format('DD MMM, HH:mm');
		}
		result.output = strDT;
		return result;
	},
	convertToMailList: function(extObj, mom){
		var o = {
			id: extObj.id,
			type: extObj.type,
			folder: extObj.folder,
			origFolder: extObj.origFolder,
			isNew: extObj.isNew,
			important: extObj.important,
			sender: extObj.sender,
			senderEMail: extObj.senderEMail,
			subject: extObj.subject,
			haveAttachment: extObj.haveAttachment,
		};
		
		if (o.type == CONST_TYPE_LEAD){
			o.extSubjectKeyPairs = extObj.extSubjectKeyPairs;
		}
		
		if ((typeof mom == "undefined") || (mom)){
			var _obj_ = this.userFriendlyDT(extObj.datetime);
			o.momentObj = _obj_.momentObj;
			o.datetime = _obj_.output;
		}
		return o;
	},
	sort: function(){
		var sortByDateDescAndTimeAscDateObj = function (objA, objB) {
			 var results;
			 results = objA.momentObj.year() < objB.momentObj.year() ? 1 : objA.momentObj.year() > objB.momentObj.year() ? -1 : 0;
			 if (results === 0) results = objA.momentObj.month() < objB.momentObj.month() ? 1 : objA.momentObj.month() > objB.momentObj.month() ? -1 : 0;
			 if (results === 0) results = objA.momentObj.date() < objB.momentObj.date() ? 1 : objA.momentObj.date() > objB.momentObj.date() ? -1 : 0;
			 if (results === 0) results = objA.momentObj.hour() > objB.momentObj.hour() ? 1 : objA.momentObj.hour() < objB.momentObj.hour() ? -1 : 0;
			 if (results === 0) results = objA.momentObj.minute() > objB.momentObj.minute() ? 1 : objA.momentObj.minute() < objB.momentObj.minute() ? -1 : 0;
			 //if (results === 0) results = objA.momentObj.second() > objB.momentObj.second() ? 1 : objA.momentObj.second() < objB.momentObj.second() ? -1 : 0;
			 return results;
		}		
		this.currData.sort(sortByDateDescAndTimeAscDateObj);
	},
	getAll: function(callback){
		var self = this;
		self.request(this.options.url, function(response){
			self.pure = [];
			self.data = $.map(response, function(obj){
				self.pure.push( self.convertToMailList(obj,false) );
				return self.convertToMailList(obj);
				
			});
			callback(self.data);
			self.updateCounters();
		});
	},
	send: function(url, params, callback){
		this.request(url, callback, params);
	},
	request: function(url, callback, params){
		var self = this;
		$.ajax({
			url: url,
			data: params || {},
			success: function(response){
				callback(response);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				self.notifyError(textStatus + " : " + errorThrown);
			}
		})
	},
	updateNav: function(dir){
		var len = this.currData.length, newStart;
		this.$header.find('a.list-next, a.list-prev').removeClass("disabled");
		if (dir == 'next'){
			newStart = this.renderStart + this.options.maxRecords;
			if (newStart <= len){
			   this.renderStart = newStart;
			}				
		}else if (dir == 'prev'){
			newStart = this.renderStart - this.options.maxRecords;
			this.renderStart = (newStart < 0)  ? 0 : newStart;
		}

		if (this.renderStart == 0){
			this.$header.find('a.list-prev').addClass("disabled");
		}
		
		if ((this.renderStart + this.options.maxRecords) > len){
			this.$header.find('a.list-next').addClass("disabled");
		}
	},
	changeMsgFolder: function(data, newFolder){
		var self = this;
		var params = {},
			multi = $.isArray(data),
			revert = (typeof newFolder == "undefined"),
			_type_;
		
		if (multi){
			params.multiple = true;
			params.data = $.map(data, function(obj){
				_type_ = revert ? self.currData[obj.index].origFolder : newFolder;
				return { id: obj.id, changed: { folder: _type_ } }
			});
		} else {
			params.id = data.id;
		}
		
		function fn(){
			if (multi){
				$.each(data, function(i, obj){
					_type_ = revert ? self.currData[obj.index].origFolder : newFolder;
					self.updateItemInLists(obj.id, obj.index, { folder : _type_ });
				});
			} else {
				_type_ = revert ? self.currData[obj.index].origFolder : newFolder;
				self.updateItemInLists(data.id, -1, { folder : _type_ });
			}
			self.updateCounters();
			self.render(true);				
		}
		
		if ((self.options.handlers.update) && (self.options.handlers.update.length)){
			self.request(self.options.handlers.update, function(response){
				fn();
			}, params);
		} else {
			fn();
		}										
		
	},
	removePermanently: function(data){
		var self = this;
		var params = { data: data },
			multi = $.isArray(data);
		
		params = multi ? $.extend({},params,{multiple: true}) : params;
		
		function fn(){
			if (multi){
				$.each(data, function(i, obj){
					self.removeItemFromLists(obj.id);
				});
			} else {
				self.removeItemFromLists(data.id);
			}
			self.updateCounters();
			self.render(true);
			self.userMsg(data.length + " item(s) have been removed permanently.");
		}
		
		self.promptMsg('Remove permanently.','On confirm will remove permanently. Are you sure?', function(){
			if ((self.options.handlers.remove) && (self.options.handlers.remove.length)){
				self.request(self.options.handlers.remove, function(response){
					fn();
				}, params);
			} else {
				fn();
			}
		});
	},
	bindsComposer: function(){
		var self = this;
		self.panels.$composer.delegate('a[data-action]','click', function(e){
			e.preventDefault();
			var $this = $(this), action = $this.data("action");
			if (action == 'cancel'){
				self.composerDropzone.destroy();
				self.hidePanel(self.panels.$composer);
			} else if (action == 'send'){
				self.sendComposerMail();
			}
		});
	},
	sendComposerMail: function(){
		var self = this;
		if (self.composerDropzone) {
			if (self.composerDropzone.getQueuedFiles().length > 0) {                        
				self.composerDropzone.processQueue();  
			} else {                       
				self.composerDropzone.uploadFiles([]);
			}			
			self.composerDropzone.processQueue();
		}
	},
	getMailById: function(id){
		var self = this;
		var res = $.grep(self.data, function(obj){
			return obj.id == id;
		});
		return res[0];
	},
	binds: function(){
		var self = this;
		
		self.options.$mailContainer.delegate('a[href="#"]','click', function(e){
			e.preventDefault();
		});
		
		self.$header.find('a.list-next').click(function(e){
			e.preventDefault();
			self.updateNav('next');
			self.render(false);
		});
		
		self.$header.find('a.list-prev').click(function(e){
			e.preventDefault();
			self.updateNav('prev');
			self.render(false);
		});		
		
		self.$header.find('.filterByType input').change(function(e){
			self.typesChecked = [];
			self.$header.find('.filterByType input:checked').each(function(i, check){
				self.typesChecked.push( $(check).data("type") );
			});
			self.render(true);
		});
		
		var search_timer = null;
		self.$header.find('#fontawsome-search').keyup(function(e){
			if (search_timer != null){
				clearTimeout(search_timer);
			}
			var v = $(this).val();
			search_timer = setTimeout(function(){
				search_timer = null;
				self.render(true, v);
			},200);
		});
		
		self.$sidebar.find('a[data-folder]').click(function(e){
			e.preventDefault();
			
			$('.right-panel:not(.reader)').each(function(i, panel){
				self.hidePanel($(panel));
			});
			
			var folder = $(this).data("folder");
			if (!$(this).hasClass("active")){
				self.view = folder;
				if (self.view == CONST_TRASH){
					self.$header.find('.revert-button').show();
				} else {
					self.$header.find('.revert-button').hide();
				}
				self.render(true);
			}
		});
		
		self.$header.find('a[data-action]').click(function(e){
			e.preventDefault();
			var $this = $(this), action = $this.data("action"), checkedList = [];
			
			function getChecked(){
				var $chks = self.$mailList.find('input[type="checkbox"]:checked'), result = [];
				if (!$chks.length) return;
				$chks.each(function(i, check){
					var $check = $(check), $li = $check.closest('li.list-item');
					result.push( { id:  $li.data("id"), index :  $li.data("index") });
				});
				return result;
			}
			
			if (action == "remove"){
				checkedList = getChecked();
				if (self.view == CONST_TRASH){
					self.removePermanently(checkedList);					
				} else {
					self.userMsg(checkedList.length + " item(s) have been moved to the trash.");
					self.changeMsgFolder(checkedList, CONST_TRASH);
				}
			} else if (action == "revert"){
				checkedList = getChecked();
				self.changeMsgFolder(checkedList);
				self.userMsg(checkedList.length + " item(s) have been restored.");
			} else if (action == "new"){
				self.initComposer();
				self.showPanel(self.panels.$composer);				
			} else if (action == "reply"){
				checkedList = getChecked();
				if ((typeof checkedList != "checkedList") && (checkedList.length)){
					self.request(self.options.handlers.mailView, function(response){
						self.initComposer();
						
						var $newBody = $('<div><br><br><br><div class="body-quoted" style="border-left:2px solid #2dc3e8;padding-left:15px;"></div></div>')
						$newBody.find('.body-quoted').append(response.body);
						response.body = $newBody.get(0).outerHTML;
						self.fillComposer(response);						
						self.showPanel(self.panels.$composer, function(){
							self.panels.$composer.find(".note-editable").trigger('focus');
						});
					}, { id: checkedList[0].id });
				}
			} else if (action == "forward"){
				checkedList = getChecked();
				if ((typeof checkedList != "checkedList") && (checkedList.length)){
					self.request(self.options.handlers.mailView, function(response){
						e.preventDefault();

						self.initComposer();

						function _quote_(title, msg){
							return $('<div class="body-quoted" style="border-left:2px solid #2dc3e8;padding-left:15px;margin-top:5px;"><div style="background:#f5f5f5;padding:5px;">'+title+'</div>'+msg+'</div>');
						}
						
						var $newBody = $('<div></div>'), title;
						
						title = response.sender + ' - ' + response.datetime + ' :';
						$newBody.append( _quote_(title, response.body) );
						
						if ((response.replies) && (response.replies.length)){
							$.each(response.replies, function(i, reply){
								var dtObj = self.userFriendlyDT(reply.datetime);
								title = reply.sender + ' - ' + dtObj.output;
								var $m = _quote_(title,reply.body);
								$m.css("margin-left:10px;");
								$newBody.find('.body-quoted:last').append($m);
							});
						}
						
						response.body = $newBody.get(0).outerHTML;
						response.senderEMail = '';
						
						self.fillComposer(response);
						self.panels.$viewer.hide();
						if (self.viewerDropzone){ 
							self.viewerDropzone.destroy(); 
						}
						self.showPanel(self.panels.$composer, function(){
							self.panels.$composer.find("#to").trigger('focus');
						});					
					}, { id: checkedList[0].id });
				}
			}
		});
		
		self.options.$mailContainer.delegate('a.mail-important','click', function(e){
			e.preventDefault();
			var $this = $(this), $li = $this.closest('li.list-item'), id = $li.data("id"), index = $li.data("index"),
				newValue = (!$this.hasClass("stared"));
			$this.toggleClass("stared").find('i').toggleClass("fa-star fa-star-o");

			function fn(){
				self.updateItemInLists(id, index, { important : newValue });
				self.updateCounters();
				if (self.view == CONST_IMP){
					self.render(true);
				}
			}
			
			if ((self.options.handlers.update) && (self.options.handlers.update.length)){
				self.request(self.options.handlers.update, function(response){
					fn()
				}, { id: id });
			} else {
				fn()
			}
		});
		
		self.$mailList.delegate('a.mail-openner','click', function(e){
			e.preventDefault();
			var id = $(this).closest('li.list-item').data("id");
			self.openMail(id);
		});
		
		self.$mailList.delegate('input[type="checkbox"]').change(function(){
			var $checks = self.$mailList.find('input[type="checkbox"]:checked');
			if ($checks.length > 1) {
				self.$header.find('a[data-action="forward"]').parent().hide();
				self.$header.find('a[data-action="reply"]').parent().hide();
			} else {
				if ($checks.length == 1) {
					self.$header.find('a[data-action="reply"]').parent().show();
					self.$header.find('a[data-action="forward"]').parent().show();
				} else {
					self.$header.find('a[data-action="reply"]').parent().hide();
					self.$header.find('a[data-action="forward"]').parent().hide();
				}				
			}
		});
		
		self.bindsComposer();
		self.bindsMailViewer();
	},
	globalDropzoneOpts: function(){
		return {
			url: this.options.handlers.sendUrl,
			parallelUploads: 100,
			autoQueue: false,
			uploadMultiple: true,
			addRemoveLinks: true,
		}
	},
	bindsMailViewer: function(){
		var self = this;
		
		self.panels.$viewer.delegate('a.reply-to-message, a[data-action="reply"]','click', function(e){
			e.preventDefault();
			var $wrapper = self.panels.$viewer.find('.mail-reply');
			var mail = self.panels.$viewer.data("data");
			var $repEditor = $('<div class="mail-reply-editor-section dropzone"><div class="mail-reply-to">Reply to: <span>'+mail.sender+' <span>('+mail.senderEMail+')</span></span><a class="reply-editor-close" href="#" style="float:right;color:#969696;font-size:12px;margin-right: 5px;"><i class="fa fa-times"></i></a></div><div class="mail-reply-editor"></div><div class="mail-reply-footer"><button class="btn btn-sky shiny btn-send">Send</button><a href="#" class="attachment-click"><i class="glyphicon glyphicon-paperclip"></i></a></div><div class="attachment-preview"></div></div>');
			$repEditor.insertBefore($wrapper);

			var dz_opts = $.extend({}, self.globalDropzoneOpts(), {
				previewsContainer: $repEditor.find(".attachment-preview").get(0),
				clickable: $repEditor.find('a.attachment-click').get(0),
				init: function(){					
					this.on("sendingmultiple", function(file, xhr, formData) {
						formData.append("to", self.panels.$viewer.find("#to").val());
						formData.append("subject", self.panels.$viewer.find("#subject").val());
						formData.append("message", self.panels.$viewer.find("#email-text").code());
						
						if (self.panels.$viewer.find('#cc').is(":visible")){
							formData.append("cc", self.panels.$viewer.find('#cc').val());
						}
						
						if (self.panels.$viewer.find('#bcc').is(":visible")){
							formData.append("bcc", self.panels.$viewer.find('#bcc').val());
						}
					});
					
					this.on("successmultiple", function(files, response) {
						self.notifySuccess("Your reply have been sent!");
						
						self.hidePanel(self.panels.$viewer);
						if (self.viewerDropzone){ 
							self.viewerDropzone.destroy(); 
						}					
					});
					
					this.on("errormultiple", function(files, response) {
						self.notifyError(response);
					});				
				}
			});
			self.viewerDropzone = new Dropzone(document.body, dz_opts);
			
			$repEditor.find('.mail-reply-editor').summernote({ height: 300 });
			self.panels.$viewer.find(".mail-reply").hide();
			$(window).scrollTop( $repEditor.offset().top );
		});
		
		self.panels.$viewer.delegate('a[data-action="remove"]','click', function(e){
			e.preventDefault();
			self.hidePanel(self.panels.$viewer);
			if (self.viewerDropzone){ 
				self.viewerDropzone.destroy(); 
			}
			var mail = self.panels.$viewer.data("data");
			
			if (self.view == CONST_TRASH){
				self.removePermanently(mail);
			} else {
				self.userMsg("The message have been moved to the trash.");
				self.changeMsgFolder(mail, CONST_TRASH);
			}
		});
		
		self.panels.$viewer.delegate('a.reply-editor-close','click', function(e){
			e.preventDefault();
			self.viewerDropzone.destroy();
			$(this).closest('.mail-reply-editor-section').remove();
			self.panels.$viewer.find(".mail-reply").show();
		});
		
		self.panels.$viewer.delegate('a.forward-message','click', function(e){
			e.preventDefault();
			var mail = self.panels.$viewer.data("data");
			
			self.initComposer();

			function _quote_(title, msg){
				return $('<div class="body-quoted" style="border-left:2px solid #2dc3e8;padding-left:15px;margin-top:5px;"><div style="background:#f5f5f5;padding:5px;">'+title+'</div>'+msg+'</div>');
			}
			
			var $newBody = $('<div></div>'), title;
			
			title = mail.sender + ' - ' + mail.datetime + ' :';
			$newBody.append( _quote_(title, mail.body) );
			
			if ((mail.replies) && (mail.replies.length)){
				$.each(mail.replies, function(i, reply){
					var dtObj = self.userFriendlyDT(reply.datetime);
					title = reply.sender + ' - ' + dtObj.output;
					var $m = _quote_(title,reply.body);
					$m.css("margin-left:10px;");
					$newBody.find('.body-quoted:last').append($m);
				});
			}
			
			mail.body = $newBody.get(0).outerHTML;
			mail.senderEMail = '';
			
			self.fillComposer(mail);
			self.panels.$viewer.hide();
			if (self.viewerDropzone){ 
				self.viewerDropzone.destroy(); 
			}
			self.showPanel(self.panels.$composer, function(){
				self.panels.$composer.find("#to").trigger('focus');
			});
		});
		
		self.panels.$viewer.delegate('.btn-send','click', function(e){
			if (self.viewerDropzone) {
				if (self.viewerDropzone.getQueuedFiles().length > 0) {                        
					self.viewerDropzone.processQueue();  
				} else {                       
					self.viewerDropzone.uploadFiles([]);
				}			
				self.viewerDropzone.processQueue();
			}			
		});
	},
	removeItemFromLists: function(id){
		this.currData = $.grep(this.currData, function(obj){
			return obj.id != id;
		});
		
		this.pure = $.grep(this.pure, function(obj){
			return obj.id != id;
		});
		
		this.data = $.grep(this.data, function(obj){
			return obj.id != id;
		});
	},
	updateItemInLists: function(mailId, index, newdata){
		// pure, data, currData
		if (index > -1){
			if ((newdata.folder) && (newdata.folder == CONST_TRASH)){
				newdata.origFolder = this.currData[index].folder;
			}
			this.currData[index] = $.extend(true, this.currData[index], newdata);
		} else {
			for (var i = 0, len = this.currData.length; i < len; i++) {
				if (this.currData[i].id == mailId){
					if ((newdata.folder) && (newdata.folder == CONST_TRASH)){
						newdata.origFolder = this.currData[i].folder;
					}
					this.currData[i] = $.extend(true, this.currData[i], newdata);
					break;
				}				
			}
		}
		for (var i = 0, len = this.pure.length; i < len; i++) {
			if (this.pure[i].id == mailId){
				if ((newdata.folder) && (newdata.folder == CONST_TRASH)){
					newdata.origFolder = this.pure[i].folder;
				}
				this.pure[i] = $.extend(true, this.pure[i], newdata);
				break;
			}
		}
		this.data = $.extend(true, this.data, this.pure);
	},
	init: function(opts){
		var self = this;
		this.options = $.extend(true, this.options, opts);
		// panels
		this.panels = {
			$reader : this.options.$mailContainer.find("#mail-reader"),
			$composer: this.options.$mailContainer.find("#compose-message-panel"),
			$viewer: this.options.$mailContainer.find('#mail-viewer-panel')
		}		
		this.$mailList = this.panels.$reader.find('.mail-body ul.mail-list');
		this.$header = this.panels.$reader.find('.mail-header');
		this.$sidebar = this.options.$mailContainer.find('.mail-sidebar');
		
		this.binds();
		this.getAll(function(response){
			self.render(true);
		});
	},
	filter: function(filterString){
		var self = this;
		
		function contain(txt, string){
			return (typeof string != "undefined") && (string.toLowerCase().indexOf(txt.toLowerCase()) > -1);
		}

		self.currData = $.grep(self.data, function(obj){
			var inQuery = ($.inArray(parseInt(obj.type), self.typesChecked) > -1);
			if (typeof filterString != "undefined"){
				inQuery = inQuery && (contain(filterString,obj.sender) || contain(filterString,obj.body) || contain(filterString,obj.senderEMail) || contain(filterString,obj.subject));
			}
			
			if (self.view == CONST_IMP){
				return (inQuery && (obj.important) && (obj.folder != CONST_TRASH));
			} else {
				return (obj.folder == self.view) && inQuery;	
			}
		});			
	},
	render: function(filterAndSort, filterString){
		var self = this;
		
		if ((typeof filterAndSort != "undefined") && (filterAndSort)){
			self.filter(filterString);
			self.sort();
		}
		self.$mailList.empty();
		self.$header.find('a[data-action="reply"]').parent().hide();
		self.$header.find('a[data-action="forward"]').parent().hide();
		var end = self.renderStart + self.options.maxRecords, len = this.currData.length;
		end = (end > len) ? len : end;
		self.$header.find('.pages').text( ((self.renderStart == 0) ? (self.renderStart+1): self.renderStart) + '-' + end + ' of ' +  len);
		
		// render
		for (i = self.renderStart; i < end; i++) { 
			self.renderItem(this.currData[i], i);
		}
		
		self.updateNav();
		
	},
	renderItem: function(item, listIndex){
		var $tmpl = $('<li data-index="'+listIndex+'" data-id="'+item.id+'" class="list-item"><div class="item-check"><label><input type="checkbox"><span class="text"></span></label></div><div class="item-star"><a class="mail-important" href="#"><i class="fa"></i></a></div><div class="item-sender"><a href="#" class="col-name mail-openner">'+item.sender+'</a></div><div class="item-subject"><a href="#" class="with-label mail-openner">'+item.subject+'</a></div><div class="item-options"></div><div class="item-time">'+item.datetime+'</div></li>');
		
		if (item.type == CONST_TYPE_LEAD){
			$tmpl.find('.item-subject').prepend('<span class="label label-palegreen">Lead</span>');
			if ((item.extSubjectKeyPairs) && (item.extSubjectKeyPairs.length)){
				$tmpl.addClass("list-item-dbl-height");
				var $span = $('<span class="item-subject-ext-info"></span>');
				$.each(item.extSubjectKeyPairs, function(i, pair){
					$.each(pair, function(key, value){
						$span.append(key + ' : ' + value + '&nbsp;&nbsp;&nbsp;&nbsp;');
					});
				});
				$tmpl.find('.item-subject a').append($span);
			}
		} else if (item.type == CONST_TYPE_OUT){
			$tmpl.find('.item-subject').prepend('<span class="label label-darkorange">Outgoing</span>');
		}
		
		if (item.isNew) $tmpl.addClass("unread");
		if (item.important) {
			$tmpl.find('.item-star i').addClass("fa-star");
			$tmpl.find('.item-star a').addClass("stared");
		} else {
			$tmpl.find('.item-star i').addClass("fa-star-o");
		}
		
		if (item.haveAttachment) {
			$tmpl.find('.item-options').append('<a href="#"><i class="fa fa-paperclip"></i></a>');
		}
		
		this.$mailList.append($tmpl);
	},
	updateCounters: function(){
		this.stopWorker();
		this.startWorker();
		this.worker.postMessage(this.pure);
	},
	stopWorker: function(){
		if (typeof this.worker != "undefined"){
			this.worker.terminate();
			this.worker= undefined;
		}	
	},
	initComposer: function(opts){
		var self = this;
		var $html = $('<div class="mail-header"> <ul class="header-buttons"> <li> <a data-action="send" class="tooltip-primary" data-toggle="tooltip" data-original-title="Send"><i class="fa fa-external-link"></i> </a> </li><li> <a data-action="attach" class="tooltip-primary" data-toggle="tooltip" data-original-title="Attach"><i class="glyphicon glyphicon-paperclip"></i></a> </li><li> <a data-action="cancel" class="tooltip-primary" data-toggle="tooltip" data-original-title="Cancel"><i class="glyphicon glyphicon-remove"></i></a> </li></ul> </div><div class="mail-body"> <div class="mail-compose"> <form method="post" role="form" class="dropzone"> <div class="form-group bordered-left-4 bordered-themeprimary"> <label for="to">To:</label> <input type="text" class="form-control" id="to" tabindex="1"> <div class="field-options"> <a href="javascript:;" onclick="$(this).hide(); $(\'#cc\').parent().removeClass(\'hidden\'); $(\'#cc\').focus();">CC</a> <a href="javascript:;" onclick="$(this).hide(); $(\'#bcc\').parent().removeClass(\'hidden\'); $(\'#bcc\').focus();">BCC</a> </div></div><div class="form-group hidden bordered-left-4 bordered-themethirdcolor"> <label for="cc">CC:</label> <input type="text" class="form-control" id="cc" tabindex="2"> </div><div class="form-group hidden bordered-left-4 bordered-themefourthcolor"> <label for="bcc">BCC:</label> <input type="text" class="form-control" id="bcc" tabindex="2"> </div><div class="form-group bordered-left-4 bordered-themesecondary"> <label for="subject">Subject:</label> <input type="text" class="form-control" id="subject" tabindex="1"></div><div class="compose-message-editor"><div id="email-text"></div></div><div class="attachment-list"></div></form> </div></div>');
		
		
		var dz_opts = $.extend({}, self.globalDropzoneOpts(), {
			previewsContainer: $html.find(".attachment-list").get(0),
			clickable: $html.find('a[data-action="attach"]').get(0),
			init: function(){
				this.on("sendingmultiple", function(file, xhr, formData) {
					formData.append("to", self.panels.$composer.find("#to").val());
					formData.append("subject", self.panels.$composer.find("#subject").val());
					formData.append("message", self.panels.$composer.find("#email-text").code());
					
					if (self.panels.$composer.find('#cc').is(":visible")){
						formData.append("cc", self.panels.$composer.find('#cc').val());
					}
					
					if (self.panels.$composer.find('#bcc').is(":visible")){
						formData.append("bcc", self.panels.$composer.find('#bcc').val());
					}
				});
				
				this.on("successmultiple", function(files, response) {
					self.notifySuccess("Your email have been sent!");
					self.composerDropzone.destroy();
					self.hidePanel(self.panels.$composer);
				});
				
				this.on("errormultiple", function(files, response) {
					self.notifyError(response);
				});
			}
		});		
		self.composerDropzone = new Dropzone(document.body, dz_opts);
		
		self.panels.$composer.empty().append($html);
		$('#email-text').summernote({ height: $(window).height() - 310 });
		self.panels.$composer.find('[data-toggle="tooltip"]').tooltip();
	},
	fillComposer: function(data){
		var v;
		//TO
		v = (typeof data != "undefined") ? data.senderEMail : "";
		this.panels.$composer.find("#to").val(v);
		
		//SUBJECT
		v = (typeof data != "undefined") ? data.subject : "";
		this.panels.$composer.find("#subject").val(v);
		
		//SUBJECT
		v = (typeof data != "undefined") ? data.body : "";
		this.panels.$composer.find("#email-text").code(v);
		
	},
	startWorker: function(){
		var self = this;
		if(typeof(Worker) !== "undefined") {
			if(typeof(this.worker) == "undefined") {
				this.worker = new Worker("assets/inbox-worker.js");
			}
			
			this.worker.onmessage = function(event) {
				var counters = event.data;
				self.stopWorker();
				
				self.$sidebar.find('a[data-folder="inbox"] span.badge').text(counters.inbox);
				self.$sidebar.find('a[data-folder="important"] span.badge').text(counters.important);
				self.$sidebar.find('a[data-folder="sent"] span.badge').text(counters.sent);
				self.$sidebar.find('a[data-folder="trash"] span.badge').text(counters.trash);
			};
		} else {
			this.notifyError("No Web Worker support.");
		}
	},
	prepareViewer: function(data){
		var self = this;
		var $html = $('<div class="mail-header"><div class="mail-title"><strong>'+data.subject+'</strong></div><ul class="header-buttons pull-right"><li><a data-action="reply" class="tooltip-primary" data-toggle="tooltip" data-original-title="Reply"><i class="fa fa-mail-reply"></i></a></li><li><a data-action="remove" class="tooltip-primary" data-toggle="tooltip" data-original-title="Remove"><i class="glyphicon glyphicon-remove"></i></a></li><li><a data-action="print" class="tooltip-primary" data-toggle="tooltip" data-original-title="Print"><i class="fa fa-print"></i></a></li></ul></div><div class="mail-body"><div class="mail-info"><div class="mail-sender"><a href="#"><span>'+data.sender+'</span> ('+data.senderEMail+')</a> </div><div class="mail-date">'+data.datetime+'</div></div><div class="mail-body-section odd" style="margin-top:0;"><div class="mail-text">'+data.body+'</div></div><div class="mail-reply"><div class="reply-form"><div><a href="#" class="reply-to-message">Reply</a> or <a href="#" class="forward-message">Forward</a> this message...</div></div></div></div>');
		
		if ((data.extSubjectKeyPairs) && (data.extSubjectKeyPairs.length)){
			$html.eq(0).addClass("list-item-dbl-height");
			var $span = $('<span class="item-subject-ext-info"></span>');
			$.each(data.extSubjectKeyPairs, function(i, pair){
				$.each(pair, function(key, value){
					$span.append(key + ' : ' + value + '&nbsp;&nbsp;&nbsp;&nbsp;');
				});
			});
			$html.find('.mail-title').append($span);
		}
		
		if ((data.attachments) && (data.attachments.length)){
			var $attachment = $('<div class="mail-attachments"><h4><i class="fa fa-paperclip"></i>Attachments<span>('+data.attachments.length+')</span></h4><ul></ul></div>');
			$.each(data.attachments, function(i, attachment){
				var $li = $('<li><a href="'+attachment.url+'" class="thumb download"><img style="margin:auto;" src="assets/img/attach-file.png"></a><a href="#" class="name">'+attachment.name+' ( '+attachment.size+' )</a></li>');
				$attachment.find('ul').append($li);
			});
			$attachment.insertAfter( $html.find('div.mail-text') );
		}
		
		if ((data.replies) && (data.replies.length)){
			var $before = $html.find('.mail-reply');
			
			data.replies.sort(function(a,b){ return a.order - b.order; });
			
			$.each(data.replies, function(i, reply){
				var dtObj = self.userFriendlyDT(reply.datetime), $rep;
				
				if ((i % 2) == 0){
					$rep = $('<div class="mail-body-section"><div class="mail-body"><div class="mail-info"><div class="mail-date">'+dtObj.output+'</div><div class="mail-sender"><a href="#"><span>'+reply.sender+'</span> ('+reply.senderEMail+')</a></div></div><div class="mail-body-section"><div class="mail-text">'+reply.body+'</div></div></div>');
					$rep.addClass('even');
				} else {
					$rep = $('<div class="mail-body-section"><div class="mail-body"><div class="mail-info"><div class="mail-sender"><a href="#"><span>'+reply.sender+'</span> ('+reply.senderEMail+')</a> </div><div class="mail-date">'+dtObj.output+'</div></div><div class="mail-body-section"><div class="mail-text">'+reply.body+'</div></div></div>');
					$rep.addClass('odd');
				}
				
				$rep.insertBefore($before);
			});
		}
		
		this.panels.$viewer.empty().append($html).data("data", data);
		this.panels.$viewer.find('[data-toggle="tooltip"]').tooltip();
	},
	openMail: function(id){
		var self = this;
		
		self.request(self.options.handlers.mailView, function(response){
			var data = self.convertToMailView(response);
			self.prepareViewer(data);
			self.showPanel(self.panels.$viewer);
		});
	},
	showPanel: function($panel, callback){
		this.panels.$reader.hide("slide", { direction: "left" }, 100);
		$panel.show("slide", { direction: "right" }, 100, function(){
			if ($.isFunction(callback)) { callback(); }
		});
	},
	hidePanel: function($panel){
		this.panels.$reader.show("slide", { direction: "left" }, 100);
		$panel.hide("slide", { direction: "right" }, 100);
	}
}

$(function(){
	MANAGEINBOX.init({
		url: "inbox.json",
		$mailContainer: $(".mail-container"),
		dateFormat: "DD/MM/YYYY HH:mm", // http://momentjs.com/docs/#/parsing/string-format/
		handlers: {
			mailView: "inbox-view.json",
			update: "", // SERVER LINK TO UPDATE 
			remove: "", // SERVER LINK TO REMOVE
			sendUrl: "upload.php" // SERVER LINK TO SEND MESSAGE
		}
	});
});