/**
* Javascript for manage-country.html
* Revision: 12
* Date: 10-08-2015
* Author: Alexander Bulei
*/

var InitiateCountryDataTable = function () {
    return {
        init: function (options) {
			var _options =  options || {};
			var $table = $('#countrydatatable');
			
			// check access
			controlActionBtnsAccess($table, _options.access);
			
            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumns" : [
                    { sWidth: '18%' },
                    { sWidth: '10%' },
                    { sWidth: '16%' },
                    { sWidth: '10%' },
                    { sWidth: '4%' },
                    { sWidth: '18%' }
                ],
                "bAutoWidth": false
            });

            //Add New Row
            $('#countrydatatable_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '', buildStatusDropDownHTML(),
                    '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;
                $('.publish-sign').closest('td').addClass('sign');
            });

            var isEditing = null;

			bindChangeStatusDropDown($('#countrydatatable'));
			bindClickActiveModalConfirm();
			bindClickInactiveModalConfirm();
			bindCancelStatusModal();

            //Edit row
            $('#countrydatatable').on("click", 'a.edit', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }
            });


            //Call Delete Modal passing data-id
            $('#countrydatatable').on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteModal').data('id', id).modal('show');
            });

            //
            //Delete Row if Confirmed
            $('#delete-btn').click(function () {
                var id = $('#deleteModal').data('id');
                var nRow = $('#'+id);
                oTable.fnDeleteRow(nRow);
                $('#deleteModal').modal('hide');
            });

            //Cancel Editing or Adding a Row
            $('#countrydatatable').on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Save an Editing Row
            $('#countrydatatable').on("click", 'a.save', function (e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
					var _canSave = true, $input;
					
					_check = function(erroMsg){
						var v = $input.val();
						if (( v == "") || (v == null)) {
							_canSave = _canSave && false;
							$input.addClass("required-error");
							_Error_(erroMsg);
						} else {
							$input.removeClass("required-error");
							_canSave = _canSave && true;
						}						
					}
					
					// country name
					$input = $(".inline-country-name");
					_check('The Country Name is required field!');
					
					// country code
					$input = $(".inline-country-code");
					_check('The Country Code is required field!');
					
					// country tax
					$input = $(".inline-country-tax");
					_check('The Country Tax is required field!');

					// country ship code
					$input = $(".inline-country-ship-zone");
					_check('The Country Ship Zone is required field!');					
					
					if (_canSave){
						saveRow(oTable, isEditing);
						isEditing = null;
					}
                }
            });

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
				
				// status select
				var $statusSelect = $( $.parseHTML(aData[4])[0] );
				if ($statusSelect.find("select").length == 0){
					$statusSelect = buildStatusDropDown();
					$('option[value="'+aData[4]+'"]',$statusSelect).attr("selected", "selected");
				}
				
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small inline-country-name" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small inline-country-code" style="width: 95px !important;"  value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input type="text" class="form-control input-small inline-country-tax" value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<input type="number" class="form-control input-small inline-country-ship-zone" style="width: 95px !important;"  value="' + aData[3] + '">';
                jqTds[4].innerHTML = $statusSelect[0].outerHTML;
                jqTds[5].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"><i class="fa fa-times"></i> Cancel</a>';

            }
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
            }

            function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
				
				// status select
				var $statusSelect = $( $.parseHTML(aData[4])[0] );
				if ($statusSelect.find("select").length == 0){
					$statusSelect = buildStatusDropDown();
					$('option[value="'+aData[4]+'"]',$statusSelect).attr("selected", "selected");
				}
				
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small inline-country-name" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small extra-smll-2 inline-country-code" style="width: 95px !important;"  value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input type="text" class="form-control input-small inline-country-tax" value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<input type="text" class="form-control input-small extra-smll-2 inline-country-ship-zone" style="width: 95px !important;"  value="' + aData[3] + '">';
                jqTds[4].innerHTML = $statusSelect[0].outerHTML;
                jqTds[5].innerHTML = aData[5];
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
				var jqSelects = $('select', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                oTable.fnUpdate(jqSelects.eq(0).val(), nRow, 4, false);
                oTable.fnUpdate('<a href="#" class="btn btn-palegreen  shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a><button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>', nRow, 5, false);
                oTable.fnDraw();
				$('[data-toggle="tooltip"]',nRow).tooltip();
            }
        }

    };
}();


$(window).bind("load", function () {
	/*Sets Themed Colors Based on Themes*/
	themeprimary = getThemeColorFromCss('themeprimary');
	themesecondary = getThemeColorFromCss('themesecondary');
	themethirdcolor = getThemeColorFromCss('themethirdcolor');
	themefourthcolor = getThemeColorFromCss('themefourthcolor');
	themefifthcolor = getThemeColorFromCss('themefifthcolor');

	//Initialise dataTable
	var opts = {
		access: {
			edit: true,
			delete: true
		}
	}	
	InitiateCountryDataTable.init(opts);
});