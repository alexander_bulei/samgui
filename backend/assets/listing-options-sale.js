/**
* Javascript for manage-listing-options-sale.html
* Revision: 1
* Date: 10-02-2016
* Author: Alexander Bulei
*/

var _GLOBAL_OPTIONS;
var _VARIATION_DT;
var InitiateLOVariationDataTable = function () {
    return {
        init: function (options, popupMode) {
			var $table = $('#lo-variationsDT');
			var _options =  options || {};
			
			// check access
			controlActionBtnsAccess($table, _options.access);

            //Datatable Initiating
            _VARIATION_DT = $table.dataTable({
				"aLengthMenu": [-1],
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'>>",
				"oPaginate": false,
				"searching": false,
				"paging": false
            });

            //Add New Row
            $('#lo-variationsDT_new').click(function (e) {
				var aiNew = _VARIATION_DT.fnAddData(['', '','<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = _VARIATION_DT.fnGetNodes(aiNew[0]);
                var n = (_VARIATION_DT.fnGetData().length + 1);
                nRow.id = n;
                editAddedRow(_VARIATION_DT, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(_VARIATION_DT, isEditing);
				}				
                isEditing = nRow;
            });
			
            var isEditing = null;

            //Edit row
            $table.on("click", 'a.edit', function (e) {
				e.preventDefault();
                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(_VARIATION_DT, isEditing);
                    editRow(_VARIATION_DT, nRow);
                    isEditing = nRow;
                } else {
                    editRow(_VARIATION_DT, nRow);
                    isEditing = nRow;
                }				
            });

            //Call Delete Modal passing data-id
            $table.on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $("#lo-var-deleteModal").data('id', id).modal('show');
            });
			
            //Delete Row if Confirmed
            $('#delete-btn', $("#lo-var-deleteModal")).click(function () {
                var id = $("#lo-var-deleteModal").data('id');
				
				function _callback(response){
					if (!response.errorMsg) {
						var nRow = $('#' + response.id);
						_VARIATION_DT.fnDeleteRow(nRow);
					} else {
						Notify(response.errorMsg, 'bottom-right', '5000', 'danger', 'fa-times', true);
					}
				}								
				_options.deleteHandler(id,_callback);
                $("#lo-var-deleteModal").modal('hide');				
            });

            //Cancel Editing or Adding a Row
            $table.on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    _VARIATION_DT.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(_VARIATION_DT, isEditing);
                    isEditing = null;
                }
            });

            //Save an Editing Row
            $table.on("click", 'a.save', function (e) {
                e.preventDefault();
				saveRow(_VARIATION_DT, isEditing);
				isEditing = null;
            });
			
            function editRow(oTable, nRow, isNew) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small full-width inline-varOp-name" value="' + aData[0] + '">';
				jqTds[1].innerHTML = '<input type="text" class="form-control input-small full-width inline-varOp-amout" value="' + aData[1] + '">';
				var str = (typeof isNew != "undefined") ? ' data-mode="new"' : "";
                jqTds[2].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"'+ str +'><i class="fa fa-times"></i> Cancel</a>';

            }
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
            }

            function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
				editRow(oTable, nRow, true);				
            }

            function saveRow(oTable, nRow) {
                var jqElements = $('input', nRow);
				
				var rowData = {
					"id": nRow.id || -1,
					"optionVarName": jqElements[0].value,
					"amount": jqElements[1].value
				}

				function _callback(response){
					
					if (!response.errorMsg){
						var _out;
						var _rec = response.record;
						oTable.fnUpdate(_rec.optionVarName , nRow, 0, false);
						oTable.fnUpdate(_rec.amount, nRow, 1, false);
						oTable.fnUpdate('<a href="#" class="btn btn-palegreen shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a><button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>', nRow, 2, false);
						oTable.fnDraw();
						$('[data-toggle="tooltip"]',nRow).tooltip();
					} else {
						Notify(response.errorMsg, 'bottom-right', '5000', 'danger', 'fa-times', true);
					}
				}
				
				//save
				_options.saveUpdateHandler(rowData, _callback);
            }
			
        }

    };
}();

var InitiateLODataTable = function () {
    return {
        init: function (options, popupMode) {
			var $table = $('#listingOptionsDT');
			var _options =  options || {};

			// check access
			controlActionBtnsAccess($table, _options.access);
			
			// modals
			var $cloneModal = $('#lo-cloneModal');
			var $deleteModal = $('#lo-deleteModal');
			var $activeModal = $('#lo-activateModal');
			var $deactiveModal = $('#lo-modalDeactivate');
			var $varModal = $('#lo-varModal');
			var $newModal = $("#lo-newOpModal");
			
			$varModal.on("shown.bs.modal", function(){
				_VARIATION_DT.fnAdjustColumnSizing();
			});

            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                }
            });
			
			function _buildInlineSelect(name, fn, callback){
				var $select = $("<select id='"+name+"' class='full-width row-ctrl'></select>");
				
				function _callback(data){					
					var arr = data.data || [];					
					$.each(arr, function(i,obj){
						$op = $("<option data-id='"+obj.id+"' value='"+obj.name+"'>"+obj.name+"</option>");
						$select.append($op);
					});
					callback($select);
				}
				
				_options[fn](_callback);				
			}			

            //Add New Row
            $('#listingOptionsDT_new').click(function (e) {
				var $modal = $("#lo-newOpModal");
				var insertIntoSelect = function($select, response){
					var arr = response.data || [];					
					$select.empty();
					$.each(arr, function(i,obj){
						$op = $("<option value='"+obj.id+"'>"+obj.name+"</option>");
						$select.append($op);
					});							
				}
				
				_options.getOptionsNewData(function(response){
					var $select = $("#new-options-select",$modal);
					insertIntoSelect($select,response);
					$select.select2();
					$select.on("change", function (e) {
						if ($(this).val() > -1) {
							$("#new-favorites-select").select2("val","-1");
							$(".row-continue",$modal).show();
						}
					});
				});

				_options.getFavNewData(function(response){
					var $select = $("#new-favorites-select",$modal);
					insertIntoSelect($select,response);
					$select.select2();				
					$select.on("change", function (e) {
						if ($(this).val() > -1) {
							$("#new-options-select").select2("val","-1");
							$(".row-continue",$modal).show();
						}
					});					
				});
				
				_options.getListingNewData(function(response){
					var $select = $("#new-listing-select",$modal);
					insertIntoSelect($select,response);
					$select.select2();				
				});

				$newModal.modal('show');
            });
			
			
			// add new button
			$("#new-modal-add-btn","#lo-newOpModal").click(function(){
				var data = {
					opName: ($("#new-options-select").val() > -1) ? $("#new-options-select").find("option:selected").text() : $("#new-favorites-select").find("option:selected").text(),
					listingId: $("#new-listing-select").select2("val"),
					listingName: $("#new-listing-select").find("option:selected").text()
				}
				
				insertNewRow(data);
				$("#lo-newOpModal").modal("hide");
			});
			
			
			function insertNewRow(d){
				var _arr = [];
				_arr.push(d.opName);
				var html = '<label class="label-listing-modal" data-id="'+d.listingId+'">'+d.listingName+'</label>'
				_arr.push(html);
				_arr.push("0");
				_arr.push("No");
				_arr.push("");
				_arr.push("");
				_arr.push("No");
				_arr.push("No");
				_arr.push("Active");
				_arr.push('<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>');
					
                var aiNew = oTable.fnAddData(_arr);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;
			}

			//variation modal
			$table.on("click", 'button.variations', function (e) {
				e.preventDefault();
				var id = $(this).closest('tr').attr('id');
				$varModal.data('id', id).modal('show');
			});
			
            //Clone row
            $table.on("click", 'button.clone', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $cloneModal.data('id', id).modal('show');
            });

            $('#clone-btn', $cloneModal).click(function () {
                var id = $cloneModal.data('id');
                var anSelected = $('#'+id);
                var data=[];
                $(anSelected).find('td').each(function(){data.push($(this).html());});
                var aiNew = oTable.fnAddData( data );
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                isEditing = nRow;
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                $cloneModal.modal('hide');
            });
            var isEditing = null;

			bindChangeStatusDropDown($table);			
			bindClickActiveModalConfirm("lo-activateModal");
			bindClickInactiveModalConfirm("lo-modalDeactivate");
			bindCancelStatusModal("lo-activateModal","lo-modalDeactivate");
			
            //Edit row
            $table.on("click", 'a.edit', function (e) {
				e.preventDefault();
                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }				
            });

            //Call Delete Modal passing data-id
            $table.on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $deleteModal.data('id', id).modal('show');
            });
			
            //Delete Row if Confirmed
            $('#delete-btn', $deleteModal).click(function () {
                var id = $deleteModal.data('id');
				
				function _callback(response){
					if (!response.errorMsg) {
						var nRow = $('#' + response.id);
						oTable.fnDeleteRow(nRow);
					} else {
						Notify(response.errorMsg, 'bottom-right', '5000', 'danger', 'fa-times', true);
					}
				}								
				_options.deleteHandler(id,_callback);
                $deleteModal.modal('hide');				
            });

            //Cancel Editing or Adding a Row
            $table.on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Save an Editing Row
            $table.on("click", 'a.save', function (e) {
                e.preventDefault();
				if (this.innerHTML.indexOf("Save") >= 0) {
					var _canSave = true;
					_check = function(erroMsg){
						var v = $input.is("input") ? $input.val() : $input.find("option:selected").data("id");
						if (( v === "") || (v == null) || (v < 0)) {
							_canSave = _canSave && false;
							$input.addClass("required-error");
							_Error_(erroMsg);
						} else {
							$input.removeClass("required-error");
							_canSave = _canSave && true;
						}						
					}

					$input = $(".inline-listingOp-name");
					_check('The System Option Name is required field!');
					
					$input = $(".inline-lo-amount");
					var _v = $input.val();
					var $select = $input.next();
					var _type = $select.val();
					_canSave = _canSave && ($.isNumeric(_v) && (_v != ""));
					if (_canSave) {
						$input.removeClass("required-error");
						_canSave = _canSave && true;
						if (_type == '%'){
							if ((_v < 0) || (_v > 100)) {
								_canSave = _canSave && false;
								$input.addClass("required-error");
								_Error_("Amount should be between 0 and 100!");
							}
						}
					} else {
						_canSave = _canSave && false;
						$input.addClass("required-error");
						_Error_("Amount should be numeric only!");
					}
					
					if (!popupMode) {
						var $label = $("label.select-listing-modal");
						$label.removeClass("required-error");
						var v = $label.data("id");
						if ((v != null) && (v > -1)){
							_canSave = _canSave && true;
						} else {
							_canSave = _canSave && false;
							$label.addClass("required-error");
							_Error_("Select the listing!");
						}						
					}
					
					if (_canSave){
						saveRow(oTable, isEditing);
						isEditing = null;
					}
				}
            });
			
			$(document).delegate("label.select-listing-modal","click", function(e){
				var $label = $(this);
				var $slm = $("#selectListingModal");
				$slm.data("callback", function(data){
					$label.attr("data-id", data.id);
					$label.text(data.text);
				});
				$("#slm-listing-select", $slm).select2("val",$label.attr("data-id"));
				$slm.modal("show");				
			});
			
            function editRow(oTable, nRow, isNew) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
				
				// status select
				var $statusSelect = $( $.parseHTML(aData[7])[0] );
				if ($statusSelect.find("select").length == 0){
					$statusSelect = buildStatusDropDown();
					$('option[value="'+aData[7]+'"]',$statusSelect).attr("selected", "selected");
				}
				
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small full-width inline-listingOp-name row-ctrl" value="' + aData[0] + '">';
				
				if (!popupMode){
					var $label = $(aData[1]);
					$label.addClass("select-listing-modal aslink");
					jqTds[1].innerHTML = $label[0].outerHTML;
				} else {
					jqTds[1].innerHTML = aData[1];
				}
				
				var $span = ((aData[2] != null) && (aData[2] != "")) ? $(aData[2]) : '<span data-suffix="%">0<span>';
				var v = $span.text();
				var suffix = $span.data("type");
				v = v.replace(suffix,"");
				
				jqTds[2].innerHTML = '<div style="width:100px;"><input type="number" style="width:50px;padding:6px 2px;float:left;" class="form-control input-small inline-lo-amount row-ctrl no-webkit-spinner" value="' + v + '"><select class="ie-amount-type" style="width:40px;padding:0;height:30px!important;border-top-left-radius:0;border-bottom-left-radius:0;border-left-width:0;" class="form-control"><option value="%">%</option><option value="$">$</option></select></div>';
				var $td = $(jqTds[2]);
				
				$td.find('select').find('option[value="'+suffix+'"]').attr("selected","selected");

				var _chkAttr = aData[3].toUpperCase() == 'YES' ? ' checked="checked"' : '';
				jqTds[3].innerHTML = '<label><input class="checkbox-slider toggle yesno row-ctrl" type="checkbox"'+_chkAttr+'><span class="text" style="margin-top: 13px;"></span></label>';
				
				_buildInlineSelect('due-select','getDueDataHandler', function($select){
					$('option[value="'+aData[4]+'"]',$select).attr("selected", "selected");
					jqTds[4].innerHTML = $select[0].outerHTML;					
				});
				
				_buildInlineSelect('unit-measure-select','getUnitDataHandler', function($select){
					$('option[value="'+aData[5]+'"]',$select).attr("selected", "selected");
					jqTds[5].innerHTML = $select[0].outerHTML;					
				});
				
				var _chkAttr = aData[6].toUpperCase() == 'YES' ? ' checked="checked"' : '';
				jqTds[6].innerHTML = '<label><input class="checkbox-slider toggle yesno row-ctrl" type="checkbox"'+_chkAttr+'><span class="text" style="margin-top: 13px;"></span></label>';
				
				var _chkAttr = aData[7].toUpperCase() == 'YES' ? ' checked="checked"' : '';
				jqTds[7].innerHTML = '<label><input class="checkbox-slider toggle yesno row-ctrl" type="checkbox"'+_chkAttr+'><span class="text" style="margin-top: 13px;"></span></label>';				
				
                jqTds[8].innerHTML = $statusSelect[0].outerHTML;
				
				var str = (typeof isNew != "undefined") ? ' data-mode="new"' : "";
                jqTds[9].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"'+ str +'><i class="fa fa-times"></i> Cancel</a>';

            }
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
            }

            function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
				editRow(oTable, nRow, true);				
            }

            function saveRow(oTable, nRow) {
                var jqElements = $('.row-ctrl', nRow);
				var jqStatusDD = $(".status-drop",nRow);
				var jqAmountType = $('.ie-amount-type', nRow);
				var rowData  = {
						"id": nRow.id || -1,
						"optionName": jqElements[0].value,
						"amount": jqElements[1].value,
						"amountType": $(jqAmountType).val(),
						"required" : jqElements[2].checked,
						"dueId" : $(jqElements[3]).find("option:selected").data("id"),
						"dueText" : $(jqElements[3]).val(),
						"unit" : $(jqElements[4]).val(),
						"favorite" : jqElements[5].checked,
						"taxable" : jqElements[6].checked,
						"status": jqStatusDD.val()
					}
				
				rowData.listingStr = $("label.label-listing-modal",nRow).text();
				rowData.listing = $('label.label-listing-modal', nRow).data("id");
				
				function _callback(response){
					
					if (!response.errorMsg){
						var _out;
						var _rec = response.record;
						oTable.fnUpdate(_rec.optionName , nRow, 0, false);
						
						_out = '<label class="label-listing-modal" data-id="'+_rec.listing+'">'+_rec.listingStr+'</label>';
						oTable.fnUpdate(_out, nRow, 1, false);
						
						// 
						var v = _rec.amount;
						var _type = _rec.amountType;
						if (_type == "%") {
							v = v + '%'
						} else {
							v = _type + v;
						}
						
						_out = '<span data-type="'+_type+'">'+v+'</span>';
						oTable.fnUpdate(_out, nRow, 2, false);
						
						_out = html = (_rec.required) ? 'Yes' : 'No';
						oTable.fnUpdate(_out, nRow, 3, false);
						
						oTable.fnUpdate(_rec.dueText, nRow, 4, false);
						
						oTable.fnUpdate(_rec.unit, nRow, 5, false);
						
						_out = html = (_rec.favorite) ? 'Yes' : 'No';
						oTable.fnUpdate(_out, nRow, 6, false);
						
						_out = html = (_rec.taxable) ? 'Yes' : 'No';
						oTable.fnUpdate(_out, nRow, 7, false);
						
						oTable.fnUpdate(_rec.status, nRow, 8, false);
						
						var html = _options.super ? '<button type="button" class="btn btn-blue shiny btn-xs variations tooltip-blue command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Variations"><i class="fa fa-upload"></i></button>' : '';
						html+= '<a href="#" class="btn btn-palegreen shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a><button type="button" class="btn btn-warning shiny btn-xs clone tooltip-warning command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Clone"><i class="fa fa-copy"></i></button><button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>';
						
						oTable.fnUpdate(html, nRow, 9, false);
						oTable.fnDraw();
						$('[data-toggle="tooltip"]',nRow).tooltip();
					} else {
						Notify(response.errorMsg, 'bottom-right', '5000', 'danger', 'fa-times', true);
					}
				}
				
				//save
				_options.saveUpdateHandler(rowData, _callback);
            }

			if (popupMode){
				_options.getListingNewData(function(response){
					var $input = $("#lor-listing-title");
					$input.typeahead({
						source: response.data,
						autoSelect: true
					});
					$input.change(function() {
						var current = $input.typeahead("getActive");
						if (current) {
							if (current.name.toLowerCase() == $input.val().toLowerCase()) {
								$("#lor-listing-id").val(current.id);
							}
						}
					});
				});
			}
			
        }

    };
}();

function _LOAD_DEFAULTS(data){
	if (data.pricingDiscountsQuantityTab) {
		for (prop in data.pricingDiscountsQuantityTab){
			var $element = $("#los-pdq-tab-" + prop);
			if ($element.length){
				var v = data.pricingDiscountsQuantityTab[prop];
				if ($element.attr("type") == "checkbox"){
					$element.prop("checked", v)
				} else {
					$element.val(v);
				}
			}
		}
	}

	if (data.shippingProductInfoTab) {
		for (prop in data.shippingProductInfoTab){
			var $element = $("#los-spi-tab-" + prop);
			if ($element.length){
				var v = data.shippingProductInfoTab[prop];
				if ($element.attr("type") == "checkbox"){
					$element.prop("checked", v)
				} else {
					$element.val(v);
				}
			}
		}
	}
}

function _GET_FORM_DATA(){
	return {
			pricingDiscountsQuantityTab: {
				CurrencyID: $("#los-pdq-tab-CurrencyID").val(),
				ListingPrice: $("#los-pdq-tab-ListingPrice").val(),
				ListingSalePrice: $("#los-pdq-tab-ListingSalePrice").val(),
				ListingMAP: $("#los-pdq-tab-ListingMAP").val(),
				ListingMSRP: $("#los-pdq-tab-ListingMSRP").val(),
				ListingCost: $("#los-pdq-tab-ListingCost").val(),
				ListingOnSale: $("#los-pdq-tab-ListingOnSale")[0].checked,
				ListingDiscountExempt: $("#los-pdq-tab-ListingDiscountExempt")[0].checked,
				ListingWholesaleA: $("#los-pdq-tab-ListingWholesaleA").val(),
				ListingWholesaleB: $("#los-pdq-tab-ListingWholesaleB").val(),
				ListingMinQty: $("#los-pdq-tab-ListingMinQty").val(),
				ListingMaxQty: $("#los-pdq-tab-ListingMaxQty").val(),
				ListingQtyOnHand: $("#los-pdq-tab-ListingQtyOnHand").val()
			},
			shippingProductInfoTab: {
				ListingSKU1: $("#los-spi-tab-ListingSKU1").val(),
				ListingSKU2: $("#los-spi-tab-ListingSKU2").val(),
				ListingSKU3: $("#los-spi-tab-ListingSKU3").val(),
				ListingManufacturerID: $("#los-spi-tab-ListingManufacturerID").val(),
				ListingSupplierID: $("#los-spi-tab-ListingSupplierID").val(),
				ListingBrandID: $("#los-spi-tab-ListingBrandID").val(),
				ListingTaxExempt: $("#los-spi-tab-ListingTaxExempt")[0].checked,
				ListingFreeShipping: $("#los-spi-tab-ListingFreeShipping")[0].checked,
				ListingShipping: $("#los-spi-tab-ListingShipping").val(),
				ListingHandling: $("#los-spi-tab-ListingHandling").val(),
				ListingWeight: $("#los-spi-tab-ListingWeight").val(),
				ListingBuyLink: $("#los-spi-tab-ListingBuyLink").val(),
				UnitID: $("#los-spi-tab-UnitID").val()
			}
		}
}

function doLOValidateTabs(){
	//TAB: Pricing, Discounts, Quantity
	var $currSelect = $("#los-pdq-tab-CurrencyID");
	
	if (!$currSelect.val()) {
		_Error_("The Currency ID is required!");
		$("a.los-pdq-tab").tab('show');
		return false;
	}
	
	var $input = $("#los-pdq-tab-ListingPrice");
	
	if (!$input.val()) {
		_Error_("The Listing Price is required!");
		$("a.los-pdq-tab").tab('show');
		return false;
	}
	
	//TAB: Shipping, Product Info
	var $input = $("#los-spi-tab-ListingSKU1");
	
	if (!$input.val()) {
		_Error_("The Listing SKU1 is required!");
		$("a.los-spi-tab").tab('show');
		return false;
	}	
	
	return true;
}

var initListingOptions = function(options){
	_GLOBAL_OPTIONS = options;
	
	var array;
	
	function renderArray(selectName, array){
		var $select = $("#" + selectName);
		$.each(array, function(i, obj){
			$select.append('<option value="'+obj.id+'">'+obj.name+'</option>')
		});		
	}
	
	//TAB: PRICING, DISCOUNTS, QUANTITY
	array = options.currencyList || [];
	renderArray('los-pdq-tab-CurrencyID', array);
	
	//TAB: SHIPPING, PRODUCT INFO
	array = options.unitList || [];
	renderArray('los-spi-tab-UnitID', array);
	
	//TAB: LISTING OPTIONS
	InitiateLOVariationDataTable.init(options.varOptions, options.popup);
	InitiateLODataTable.init(options.loOptions, options.popup);
	// add new button
	$(".row-continue","#lo-newOpModal").click(function(){
		$(this).fadeOut()
		$(".new-modal-bottom").fadeIn();
	});
	_init_chooser();
	
	$("#lor-lo-tab").on("shown.bs.tab", function(e){
		$("#listingOptionsDT").DataTable().columns.adjust().draw();
	});
		
	//TAB: PAYMENTS & DEPOSITS
	options.loOptions.getDueDataHandler(function(response){
		var $select = $("#lo-pdtab-due-select");
		var arr = response.data || [];					
		$.each(arr, function(i,obj){
			$op = $("<option data-id='"+obj.id+"' value='"+obj.name+"'>"+obj.name+"</option>");
			$select.append($op);
		});
	});
	
	$("#lor-pd-mp-atbooking").change(function(e){
		var $firstP = $("div.lor-dp-mp-firstpayment");
		var _continue = true;
		
		$('input', $firstP).each(function(i, inp){
			_continue = _continue && ($(inp).val() == "");
		});
		
		if (_continue){
			if (this.checked){
				$firstP.addClass("disabled");
				$firstP.find('input').attr("disabled","disabled");
			} else {
				$firstP.removeClass("disabled");
				$firstP.find('input').removeAttr("disabled");
			}
		} else {
			_Error_("You must choose Percentage and Days OR at time of booking");
			this.checked = !this.checked;
		}
	});
	
	//TAB: NOTES, CANCELLATIONS & AGREEMENTS
	
	function setAgreementsTypeView(viewtype){
		var $container = $(".col-lragreement"), html;
		switch (viewtype) {
			case 0 :
				html = '<div class="widget" style="font-size:11px;"><div class="widget-header bordered-bottom bordered-sky"><span class="widget-caption" style="font-size:12px;">You are do not have a Listing Contract selected</span></div><div class="widget-body"><p><a href="#" data-toggle="modal" data-target="#lo-uploaderModal">Click here</a> to upload a Custom Contract. You can also upload a default Contract in your Account Settings.</p></div></div>';
				break;
			case 1 :
				html = '<div class="widget" style="font-size:11px;"><div class="widget-header bordered-bottom bordered-sky"><span class="widget-caption" style="font-size:12px;">You are currently using the Default Contract</span></div><div class="widget-body"><div class="row"><div class="col-xs-12 col-md-12"><div><a style="line-height:22px;display:inline-block;margin-right:20px" href="#">VIEW</a><div class="radio" style="display:inline-block"><label><input name="form-field-radio" type="radio" checked="checked"><span class="text" style="font-size:11px">Default Contract</span></label></div></div><label style="font-size:11px;margin-left:73px;">You have not uploaded an optional custom Contract for this listing. <a href="#" data-toggle="modal" data-target="#lo-uploaderModal">Click here</a> to upload one now.</label></div></div></div></div>';
				break;
			case 2 :	
				html = '<div class="widget" style="font-size:11px;"><div class="widget-header bordered-bottom bordered-sky"><span class="widget-caption" style="font-size:12px;">You are currently using the Default Contract</span></div><div class="widget-body"><div class="row"><div class="col-xs-12 col-md-12"><div><a style="line-height:22px;display:inline-block;margin-right:10px;margin-left:58px" href="#">VIEW</a><div class="radio" style="display:inline-block"><label><input name="lor-nca-radiogroup" type="radio" checked="checked"><span class="text" style="font-size:11px;">Default Contract</span></label></div></div><div><a style="line-height:22px;display:inline-block;margin-right:10px" href="#" data-toggle="modal" data-target="#lo-uploaderModal">REPALCE</a><a style="line-height:22px;display:inline-block;margin-right:10px" href="#">VIEW</a><div class="radio" style="display:inline-block"><label><input name="lor-nca-radiogroup" type="radio"><span class="text" style="font-size:11px;">This listings Custom Contract</span></label></div></div></div></div></div></div>';
				break;
		}
		$container.empty().append(html);
	}
	
	setAgreementsTypeView(options.flag);
	
	
	$("#lo-uploaderModal .dropzone").dropzone().on("success", function(file, response){
		setAgreementsTypeView(response.view);
	});
		
	$("#lo-save-form").click(function(e){
		var isOK = doLOValidateTabs();
		if ((isOK) && (_GLOBAL_OPTIONS) && (_GLOBAL_OPTIONS.saveCallback)){
			var formData = _GET_FORM_DATA();
			_GLOBAL_OPTIONS.saveCallback(formData);
		}
	});
	
	// DEFAULT VALUES
	if (options.defaultValues){
		_LOAD_DEFAULTS(options.defaultValues);
	}
	
}