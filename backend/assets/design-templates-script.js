/**
* Javascript for manage-design-templates.html
* Revision: 11
* Date: 14-07-2015
* Author: Alexander Bulei
*/

var InitiateSiteDataTable = function () {
    return {
        init: function (options) {
			var _options =  options || {};
			var $table = $('#designdatatable');
			
			// check access
			controlActionBtnsAccess($table, _options.access);
			
            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumns" : [
                    { sWidth: '12%' },
                    { sWidth: '12%' },
                    { sWidth: '12%' },
                    { sWidth: '8%' },
                    { sWidth: '25%' }
                ],
                "bAutoWidth": false
            });
            var i = oTable.fnGetData().length + 1;

            //Edit row
            $('#designdatatable').on("click", 'a.edit', function (e) {
                e.preventDefault();

                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }

            });

            $('#designdatatable').on("click", 'button.upload', function (e) {
                e.preventDefault();
                $('#siteModal').modal('show');
                $("form#dropForm").dropzone({ url: "/file/post" });
            });

            //Clone row
            $('#designdatatable').on("click", 'button.clone', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#cloneModal').data('id', id).modal('show');
            });

            $('#clone-btn').click(function () {
                var id = $('#cloneModal').data('id');
                var anSelected = $('#'+id);
                var data=[];
                $(anSelected).find('td').each(function(){data.push($(this).html());});
                var aiNew = oTable.fnAddData( data );
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                isEditing = nRow;
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                $('.publish-sign').closest('td').addClass('sign');
                $('#cloneModal').modal('hide');
            });
            var isEditing = null;

			bindChangeStatusDropDown($('#designdatatable'));
			bindClickActiveModalConfirm();
			bindClickInactiveModalConfirm();
			bindCancelStatusModal();

            //Call Delete Modal passing data-id
            $('#designdatatable').on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteModal').data('id', id).modal('show');
            });

            //Delete Row if Confirmed
            $('#delete-btn').click(function () {
                var id = $('#deleteModal').data('id');
                var nRow = $('#'+id);
                oTable.fnDeleteRow(nRow);
                $('#deleteModal').modal('hide');
            });

            //Cancel Editing or Adding a Row
            $('#designdatatable').on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Save an Editing Row
            $('#designdatatable').on("click", 'a.save', function (e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
                    saveRow(oTable, isEditing);
                    isEditing = null;
                    //Some Code to Highlight Updated Row
                    $('button.activate').on("click", function (e) {
                        e.preventDefault();
                        $(this).hide().next().show();


                    });
                    $('button.deactivate').on("click", function (e) {
                        e.preventDefault();
                        $(this).hide().prev().show();
                    });
                }
            });

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[2] + '">';
				
				// status select
				var $statusSelect = $( $.parseHTML(aData[2])[0] );
				if ($statusSelect.find("select").length == 0){
					$statusSelect = buildStatusDropDown();
					$('option[value="'+aData[2]+'"]',$statusSelect).attr("selected", "selected");
				}				
				
                jqTds[3].innerHTML = $statusSelect[0].outerHTML;
                jqTds[4].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"><i class="fa fa-times"></i> Cancel</a>';

            }
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
            }

            function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
				editRow(oTable, nRow);
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
				var jqSelects = $('select', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate(jqSelects.eq(0).val(), nRow, 3, false);
                oTable.fnUpdate('<button type="button" class="btn btn-blue shiny btn-xs dashboard tooltip-blue command-btn upload" data-toggle="tooltip" data-placement="top" data-original-title="Upload"><i class="fa fa-upload"></i></button><a href="#" class="btn btn-palegreen  shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a><button type="button" class="btn btn-warning shiny btn-xs clone tooltip-warning command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Clone"><i class="fa fa-copy"></i></button><button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>', nRow, 4, false);
                oTable.fnDraw();
            }
        }

    };
}();

$(window).bind("load", function () {
	/*Sets Themed Colors Based on Themes*/
	themeprimary = getThemeColorFromCss('themeprimary');
	themesecondary = getThemeColorFromCss('themesecondary');
	themethirdcolor = getThemeColorFromCss('themethirdcolor');
	themefourthcolor = getThemeColorFromCss('themefourthcolor');
	themefifthcolor = getThemeColorFromCss('themefifthcolor');

	// Start select2 fields
	$("#selectSite").select2();

	//Initialise dataTable
	var opts = {
		access: {
			edit: true,
			delete: true
		}
	}	
	InitiateSiteDataTable.init(opts);
});


/* Select function to show Site fields */
$( "#selectSite" ).change(function() {
	if ($(this).val() != "") {
		$('#siteName').val('Site Name goes here').fadeIn();
		$('#siteDescription').val('Site description goes here').fadeIn();
		$('.site-table').fadeIn();
	} else {
		$('.site-table').fadeOut();
		$('#siteDescription').fadeOut();
		$('#siteName').fadeOut();
	}
});

$("#upload").click(function() {
	if ($( "#selectSite").val() != "") {
		$('#siteModal').modal('show');
		$("form#dropForm").dropzone({ url: "/file/post" });
	}
});
$(".upload").click(function() {
	$('#siteModal').modal('show');
	$("form#dropForm").dropzone({ url: "/file/post" });
});
