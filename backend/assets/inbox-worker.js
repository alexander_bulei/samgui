onmessage = function(e){
	var arr = e.data;
	result = {
		inbox: 0,
		important: 0,
		sent: 0,
		trash: 0,
	}
	for (i = 0; i < arr.length; i++) { 
		if ((arr[i].important) && (arr[i].folder != "trash")){
			result.important = result.important +1;
		}
		
		if (arr[i].folder == 'inbox'){
			result.inbox = result.inbox +1;
		} else if (arr[i].folder == 'sent'){
			result.sent = result.sent+1;
		} else if (arr[i].folder == 'trash'){
			result.trash = result.trash+1;
		}
	}
	
	postMessage(result);
};