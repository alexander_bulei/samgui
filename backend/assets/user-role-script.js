/**
* Javascript for manage-user-role.html
* Revision: 12
* Date: 14-07-2015
* Author: Alexander Bulei
*/

var InitiateUserRoleDataTable = function () {
    return {
        init: function (options) {
			var _options =  options || {};
			var $table = $('#userdatatable');
			
			// check access
			controlActionBtnsAccess($table, _options.access);

            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumns" : [
                    { sWidth: '14%' },
                    { sWidth: '14%' },
                    { sWidth: '12%' },
					{ sWidth: '8%' },
                    { sWidth: '20%' }
                ],
                "bAutoWidth": false
            });

            //Add New Row
            $('#userdatatable_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', buildStatusDropDownHTML(),
                    '<a href="#" class="btn btn-success shiny btn-smll btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;
            });

            //Edit row
            $('#userdatatable').on("click", 'a.edit', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }
            });


            var isEditing = null;

			
			bindChangeStatusDropDown($('#userdatatable'));
			bindClickActiveModalConfirm();
			bindClickInactiveModalConfirm();
			bindCancelStatusModal();

            //Call Delete Modal passing data-id
            $('#userdatatable').on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteModal').data('id', id).modal('show');
            });

            //Delete Row if Confirmed
            $('#delete-btn').click(function () {
                var id = $('#deleteModal').data('id');
                var nRow = $('#'+id);
                oTable.fnDeleteRow(nRow);
                $('#deleteModal').modal('hide');
            });

            //Cancel Editing or Adding a Row
            $('#userdatatable').on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Save an Editing Row
            $('#userdatatable').on("click", 'a.save', function (e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
                    saveRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

				// status select
				var $statusSelect = $( $.parseHTML(aData[3])[0] );
				if ($statusSelect.find("select").length == 0){
					$statusSelect = buildStatusDropDown();
					$('option[value="'+aData[3]+'"]',$statusSelect).attr("selected", "selected");
				}
				
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<select id="siteUserSel" class="form-control input-small" name="selectSite"><option value="Site 1">Site 1</option><option value="Site 2">Site 2</option><option value="Site 3">Site 3</option><option value="Site 4">Site 4</option><option value="Site 5">Site 5</option><option value="Site 6">Site 6</option></select>';
				jqTds[3].innerHTML = $statusSelect[0].outerHTML;
                jqTds[4].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"><i class="fa fa-times"></i> Cancel</a>';
                $("#siteUserSel").val(aData[2]);
            }
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
				$(".task-extension-link", nRow).taskExtension({onlyUserSelect: true});
            }

            function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

				// status select
				var $statusSelect = $( $.parseHTML(aData[3])[0] );
				if ($statusSelect.find("select").length == 0){
					$statusSelect = buildStatusDropDown();
					$('option[value="'+aData[3]+'"]',$statusSelect).attr("selected", "selected");
				}			
				
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<select id="siteUserSel" class="form-control input-small" name="selectSite"><option value="Site 1">Site 1</option><option value="Site 2">Site 2</option><option value="Site 3">Site 3</option><option value="Site 4">Site 4</option><option value="Site 5">Site 5</option><option value="Site 6">Site 6</option></select>';
				jqTds[3].innerHTML = $statusSelect[0].outerHTML;
                jqTds[4].innerHTML = aData[4];
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('.form-control', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
				oTable.fnUpdate($(jqInputs[3]).val(), nRow, 3, false);
                oTable.fnUpdate('<a class="btn btn-blue shiny btn-xs dashboard tooltip-blue command-btn task-extension-link" data-toggle="tooltip" data-placement="top" data-original-title="User" data-taskext-val="{}"><i class="fa fa-user"></i></a><a href="#" class="btn btn-palegreen  shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a><button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>', nRow, 4, false);
                oTable.fnDraw();
				
				$('[data-toggle="tooltip"]',nRow).tooltip();
				$(".task-extension-link", nRow).taskExtension({onlyUserSelect: true});
            }
        }

    };
}();

$(window).bind("load", function () {
	/*Sets Themed Colors Based on Themes*/
	themeprimary = getThemeColorFromCss('themeprimary');
	themesecondary = getThemeColorFromCss('themesecondary');
	themethirdcolor = getThemeColorFromCss('themethirdcolor');
	themefourthcolor = getThemeColorFromCss('themefourthcolor');
	themefifthcolor = getThemeColorFromCss('themefifthcolor');

	//Initialise dataTable
	var opts = {
		access: {
			edit: true,
			delete: true
		}
	}	
	
	InitiateUserRoleDataTable.init(opts);
	//InitiatePhraseDataTable.init();
});

$(function(){
	$(".task-extension-link").taskExtension({onlyUserSelect: true});
});