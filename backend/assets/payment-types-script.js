/**
* Javascript manage-payment-types.html
* Revision: 5
* Date: 14-07-2015
* Author: Alexander Bulei
*/

var _PT_STATUS_ACTIVE = 'Active';
var _PT_STATUS_INACTIVE = 'Inactive';
var _PT_STATUS_PENDING = 'Pending';
var _OPT_DATA_NAME = 'defData';

var InitiatePaymentTypesDataTable = function () {
    return {
        init: function (options) {
			var _options = options || {};
			
			function _check_userPaymentDefault(bool){
				var $alert = $('.payment-type-top-alert');
				if (bool){
					$alert.find('.admin-alert-text').text('You have opted to accept all default payment types.');
				} else {
					$alert.find('.admin-alert-text').text('You are currently only acceptin the payment methods below.');
				}
			}
			
			_check_userPaymentDefault(_options.userPaymentDefault);
			
			$("#payment-type-change-preferences").click(function(e){
				var $modal = $("#userDefaultPaymentModal");
				$modal.find('input[type="checkbox"]')[0].checked = _options.userPaymentDefault || false;
				$modal.modal("show");
			});
			
			
			$("#userDefaultPaymentModal input[type='checkbox']").on("change", function(e){
				var $modal = $("#defaultOpModal");
				if (this.checked){
					$modal.find('.modal-title').text("You are turning on the ability for your listings to use this site's default payment types.  If you previously added a payment type that is included in our defaults, you should remove your manually added payment type or it could cause a conflict.");
				} else {
					$modal.find('.modal-title').text("You are turing off the ability for your listings to use this site's default payment types.  You will need to add all payment types you wish to use manually.");
				}
				$modal.data('checkbox', this);
				$modal.modal('show');
			});

			$("#userDefaultPayment-save").click(function(e){
				var $modal = $("#userDefaultPaymentModal");
				_options.userPaymentDefault = $modal.find('input[type="checkbox"]')[0].checked;				
				_check_userPaymentDefault(_options.userPaymentDefault);
				$modal.modal("hide");
			});
			
			$("#defaultOpModal-btn-cancel").click(function(e){
				var checkbox = $('#defaultOpModal').data("checkbox");
				checkbox.checked = !checkbox.checked;
			});
			
			var $table = $('#payTypesDatatable');
			
			// check access
			controlActionBtnsAccess($table, _options.access);
			
			// check hidden columns
			if ((!_options.showDefaultCol) || (!_options.showUserNameCol)) {
				var _arr = $table.data("cchooserUnchecked") || [];
				if (!_options.showDefaultCol) {
					_arr.push(5);
				}
				if (!_options.showUserNameCol) {
					_arr.push(10);
				}				
				$table.data("cchooserExclude", _arr);
				$table.data("cchooserUnchecked", _arr);
				_init_chooser();
			}
			
            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                }                
            });

			function PT_buildStatusDropDown(){
				var $statusSelect = $('<select class="form-control">');
				$statusSelect.append('<option value="'+_PT_STATUS_ACTIVE+'">'+_PT_STATUS_ACTIVE+'</option>');
				$statusSelect.append('<option value="'+_PT_STATUS_INACTIVE+'">'+_PT_STATUS_INACTIVE+'</option>');
				$statusSelect.append('<option value="'+_PT_STATUS_PENDING+'">'+_PT_STATUS_PENDING+'</option>');
				return $statusSelect;
			}			
			
            //Add New Row
            $('#payTypesDatatable_new').click(function (e) {
                e.preventDefault();
				$("#newPayTypeModal").modal('show');				
            });
			
			$("#insert-paytype-btn").click(function(){
				var $select = $("#scratchSelection");
				var $op =  $select.find(":selected");
				var data = $op.data(_OPT_DATA_NAME);
				newData = [];
				
				newData.push(data.paymentName);
				newData.push(data.ccard ? 'Yes' : 'No');
				newData.push(data.escrow ? 'Yes' : 'No');
				newData.push(data.offline ? 'Yes' : 'No');
				newData.push(data.default ? 'Yes' : 'No');
				newData.push(data.url);
				
				var $viewBtn = $('<a href="#" class="btn btn-blue shiny btn-xs view tooltip-blue command-btn" data-toggle="tooltip" data-placement="top" data-original-title="View"><i class="fa fa-eye"></i></a>');
				var $uploadBtn = $('<a href="#" class="btn btn-maroon shiny btn-xs upload tooltip-maroon command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Upload"><i class="fa fa-upload"></i></a>');
				
				$viewBtn.attr("data-logourl", data.logoUrl);					
				newData.push($viewBtn[0].outerHTML + $uploadBtn[0].outerHTML);
				
				newData.push(data.site);
				newData.push(data.category);
				newData.push(data.username);
				
				var $statusSelect = PT_buildStatusDropDown();
				$('option[value="'+data.status+'"]',$statusSelect).attr("selected", "selected");
				newData.push($statusSelect[0].outerHTML);
				
				newData.push('<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>');
				
				insertNewRow(newData);
			});
			
			$("#new-paytype-btn").on("click", function(e){
				var newData = ['','No','No','No','No','','<a href="#" class="btn btn-blue shiny btn-xs view tooltip-blue command-btn" data-toggle="tooltip" data-placement="top" data-original-title="View"><i class="fa fa-eye"></i></a><a href="#" class="btn btn-maroon shiny btn-xs upload tooltip-maroon command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Upload"><i class="fa fa-upload"></i></a>','','','',PT_buildStatusDropDown()[0].outerHTML,'<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'];
				insertNewRow(newData);
			});			
			
			function insertNewRow(data){
                var aiNew = oTable.fnAddData(data);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;
				$("#newPayTypeModal").modal('hide');			
			}

            var isEditing = null;

			// Bind change event for status dropdown
			$table.find('select.status-select').each(function(i,select){
				var $select = $(select);
				$select.data('lastSel', $select.val());
				
				$select.change(function(e){
					e.preventDefault();
					var $this = $(this);
					var _val = $this.val();
					var id = $this.closest('tr').attr('id');
					switch (_val){
						case _PT_STATUS_ACTIVE: $('#activateModal').data('id', id).modal('show'); break;
						case _PT_STATUS_INACTIVE: $('#modalDeactivate').data('id', id).modal('show'); break;
						case _PT_STATUS_PENDING: $('#modalPending').data('id', id).modal('show'); break;
					}
						
					return false;
				});
			});

			// Bind click event on confrim button - Activate
			$('#activate-btn').click(function () {
				var id = $('#activateModal').data('id');
				var $td = $('#' + id + ' .td-status');
				var $select = $td.find("select");
				var hasSelect = ($select.length > 0);
				
				if (hasSelect) {
					$select.val(_PT_STATUS_ACTIVE); //activate
					$select.data('lastSel', $select.val());
				} else {
					$td.text(_PT_STATUS_ACTIVE);
				}				
				$('#activateModal').modal('hide');
			});
		
			$('#deactivate-btn').click(function () {
				var id = $('#modalDeactivate').data('id');
				var $td = $('#' + id + ' .td-status');
				var $select = $td.find("select");
				var hasSelect = ($select.length > 0);
				
				if (hasSelect) {
					$td.find("select").val(_PT_STATUS_INACTIVE);
					$select.data('lastSel', $select.val());
				} else {
					$td.text(_PT_STATUS_INACTIVE);
				}
				$('#modalDeactivate').modal('hide');
			});
						
			$('#pending-btn').click(function () {
				var id = $('#modalPending').data('id');
				var $td = $('#' + id + ' .td-status');
				var $select = $td.find("select");
				var hasSelect = ($select.length > 0);
				
				if (hasSelect) {
					$td.find("select").val(_PT_STATUS_PENDING);
					$select.data('lastSel', $select.val());
				} else {
					$td.text(_PT_STATUS_PENDING);
				}
				$('#modalPending').modal('hide');
			});			
			
			$('#deactivate-btn-close, #activate-btn-close, #pending-btn-close').click(function(e){
				var $btn = $(this);
				var $modal = $btn.closest('div.modal');
				var id = $modal.data('id');
				var $td = $('#' + id + ' .td-status');
				var $select = $td.find("select");
				var hasSelect = ($select.length > 0);
				
				if (hasSelect) {
					var lastVal = $select.data('lastSel');
					$select.val(lastVal);
				}
				$('#activateModal').modal('hide');
				$('#modalDeactivate').modal('hide');
				$('#modalPending').modal('hide');
			});
			
			
            //Edit row
            $table.on("click", 'a.edit', function (e) {
				e.preventDefault();
                var $row = $(this).parents('tr');
				var nRow = $row.hasClass('child') ? $row.prev()[0] : $row[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }				
            });
			
			$("#new-paytype-btn").on("click", function (e) {
				// save selected categories to db here -->
				$('#assignCategoriesModal').modal('hide');
			});

            //Call Delete Modal passing data-id
            $table.on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteModal').data('id', id).modal('show');
            });

            //
            //Delete Row if Confirmed
            $('#delete-btn').click(function () {
				var id = $('#deleteModal').data('id');
				
				function _callback(response){
					if (!response.errorMsg) {
						var nRow = $('#' + response.id);
						oTable.fnDeleteRow(nRow);
					} else {
						Notify(response.errorMsg, 'bottom-right', '5000', 'danger', 'fa-times', true);
					}
				}
				_options.deleteHandler(id,_callback);
                $('#deleteModal').modal('hide');
            });

            //Cancel Editing or Adding a Row
            $table.on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Save an Editing Row
            $table.on("click", 'a.save', function (e) {
                e.preventDefault();
				if (this.innerHTML.indexOf("Save") >= 0) {
					var $input = $("input.url-field");
					var v = $input.val();
					if ((v.lenght != 0) && (is_valid_url(v))){
						if (v.match(/(^http:\/\/)/) == null) {
							$input.val('http://' + v);
						}
						$input.parent().removeClass('has-error');
						saveRow(oTable, isEditing);
						isEditing = null;
					} else {
						$input.parent().addClass('has-error');
                        Notify('The url is incorrect', 'bottom-right', '5000', 'danger', 'fa-times', true);
					}
				}
            });		
			
			//upload button
            $table.on("click", 'a.upload', function (e) {
                e.preventDefault();				
				var id = $(this).closest('tr').attr('id');
				$("#drop-recordId").val(id);
				$('#uploadModal').data('id', id).modal('show');
            });
			
			//upload button
            $table.on("click", 'a.view', function (e) {
                e.preventDefault();
				var _src = $(this).data("logourl");
				var id = $(this).closest('tr').attr('id');
				var $modal = $('#viewLogoModal');
				$("#viewImageLogo").removeAttr("src");
				if (_src) {
					$("#viewImageLogo").attr("src", _src);
				}
                $modal.data('id', id).modal('show');                
            });			
			
			function _buildInlineSelect(name, fn, callback, siteId){
				var $select = $("<select id='"+name+"' class='form-control full-width'></select>");
				
				if (typeof siteId != "undefined") { 
					$select.attr("data-siteid", siteId);
				}
				
				function _callback(data){					
					var arr = data.data || [];					
					$.each(arr, function(i,obj){
						$op = $("<option data-id='"+obj.id+"' value='"+obj.name+"'>"+obj.name+"</option>");
						$select.append($op);
					});
					callback($select);
				}
				args = [];
				if (typeof siteId != "undefined") { 
					args.push(siteId); 
				}
				args.push(_callback); 
				_options[fn].apply(this,args);
			}			
			
            function editRow(oTable, nRow, isNew) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

				// status select
				var $statusSelect = aData[10].length ? $( $.parseHTML(aData[10])[0] ) : $();
				if ($statusSelect.find("select").length == 0){
					$statusSelect = PT_buildStatusDropDown();
					$('option[value="'+aData[10]+'"]',$statusSelect).attr("selected", "selected");
				}
				
				jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
				
				// credit card
				var checkAttr = (aData[1].toUpperCase() == 'YES') ? ' checked="checked"' : '';
				var $toggleBtn = $('<label><input class="checkbox-slider toggle colored-darkorange yesno" type="checkbox"'+checkAttr+'><span class="text"></span></label>');
				jqTds[1].innerHTML = $toggleBtn[0].outerHTML;

				// Escrow
				var checkAttr = (aData[2].toUpperCase() == 'YES') ? ' checked="checked"' : '';
				var $toggleBtn = $('<label><input class="checkbox-slider toggle colored-palegreen yesno" type="checkbox"'+checkAttr+'><span class="text"></span></label>');
				jqTds[2].innerHTML = $toggleBtn[0].outerHTML;
				
				// offline
				var checkAttr = (aData[3].toUpperCase() == 'YES') ? ' checked="checked"' : '';
				var $toggleBtn = $('<label><input class="checkbox-slider toggle colored-warning yesno" type="checkbox"'+checkAttr+'><span class="text"></span></label>');
				jqTds[3].innerHTML = $toggleBtn[0].outerHTML;
				
				// default
				var checkAttr = (aData[4].toUpperCase() == 'YES') ? ' checked="checked"' : '';
				var $toggleBtn = $('<label><input class="checkbox-slider toggle colored-purple yesno" type="checkbox"'+checkAttr+'><span class="text"></span></label>');
				jqTds[4].innerHTML = $toggleBtn[0].outerHTML;
				
				jqTds[5].innerHTML = '<input type="text" class="form-control input-small url-field" value="' + aData[5] + '">';
				jqTds[6].innerHTML = aData[6];
				
				_buildInlineSelect('site-select', 'getSiteDataHandler', function($select){					
					$('option[value="'+aData[7]+'"]',$select).attr("selected", "selected");
					jqTds[7].innerHTML = $select[0].outerHTML;
					
					$(jqTds[7]).find('select').on("change", function(){
						var idx = $("#site-select")[0].selectedIndex;
						var siteId = $("#site-select option:eq("+idx+")").data("id");
						_buildInlineSelect('category-select', 'getCategoryDataHandler', function($select){
							$('option[value="'+aData[8]+'"]',$select).attr("selected", "selected");
							jqTds[8].innerHTML = $select[0].outerHTML;
						}, siteId);
					}).trigger("change");
					
				});

				
				
				jqTds[9].innerHTML = '<span class="pt-username">'+ aData[9] + '</span>';
				
				jqTds[10].innerHTML = $statusSelect[0].outerHTML;

				jqTds[11].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"'+ ((typeof(isNew) != "undefined") ? ' data-mode="new"' : '')+'><i class="fa fa-times"></i> Cancel</a>';
				
				$('[data-toggle="tooltip"]',nRow).tooltip();
			}
			
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
            }

            function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
				editRow(oTable, nRow, true);
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
				var jqSelects = $('select', nRow);
				var $viewLogo = $('a.view',nRow);

				//build data
				var rowData = {
					"id": nRow.id || -1,
					"paymentName" : jqInputs[0].value,
					"creditCard": jqInputs[1].checked,
					"escrow": jqInputs[2].checked,
					"offline": jqInputs[3].checked,
					"default": jqInputs[4].checked,
					"url": jqInputs[5].value,
					"logo": $viewLogo.data("logourl") || "",
					"site" : jqSelects.eq(0).val(),
					"category" : jqSelects.eq(1).val(),
					"username": $(".pt-username",nRow).text(),
					"status": jqSelects.eq(2).val()
				}				
				
				function _callback(response){
					if (!response.errorMsg){
						var record = response.record;
						oTable.fnUpdate(record.paymentName, nRow, 0, false);
						oTable.fnUpdate(boolToStr(record.creditCard), nRow, 1, false);
						oTable.fnUpdate(boolToStr(record.escrow), nRow, 2, false);
						oTable.fnUpdate(boolToStr(record.offline), nRow, 3, false);
						oTable.fnUpdate(boolToStr(record.default), nRow, 4, false);
						oTable.fnUpdate(record.url, nRow, 5, false);
						oTable.fnUpdate(record.site, nRow, 7, false);
						oTable.fnUpdate(record.category, nRow, 8, false);
						oTable.fnUpdate(record.username, nRow, 9, false);
						oTable.fnUpdate(record.status, nRow, 10, false);
						oTable.fnUpdate('<a href="#" class="btn btn-palegreen shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a><button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>', nRow, 11, false);
						oTable.fnDraw();
						$('[data-toggle="tooltip"]',nRow).tooltip();						
					} else {
						Notify(response.errorMsg, 'bottom-right', '5000', 'danger', 'fa-times', true);
					}
				}				
				_options.saveUpdateHandler(rowData, _callback);
				
				function boolToStr(b){
					return b ? 'Yes' : 'No';
				}			
            }
        }

    };
}();

function addOpToScratchSelection(jsonData){
	var $op = $('<option value="'+jsonData.id+'">'+jsonData.paymentName+'</otpion>');
	$op.data(_OPT_DATA_NAME,jsonData);
	$("#scratchSelection").append($op);
}

$(window).bind("load", function () {
	/*Sets Themed Colors Based on Themes*/
	themeprimary = getThemeColorFromCss('themeprimary');
	themesecondary = getThemeColorFromCss('themesecondary');
	themethirdcolor = getThemeColorFromCss('themethirdcolor');
	themefourthcolor = getThemeColorFromCss('themefourthcolor');
	themefifthcolor = getThemeColorFromCss('themefifthcolor');

	//Initialise dataTable
	var initOptions = {
		access: {
			edit: true,
			delete: true
		},
		showDefaultCol: true,
		showUserNameCol: true,
		saveUpdateHandler: function(data, callback){
			var response = { record: data };
			//var response = { "id" : id, "errorMsg" : "Some Error!"}; // response with error 
			callback(response);
		},
		deleteHandler: function(id, callback){
			var response = { "id" : id };
			//var response = { "errorMsg" : "Some Error!"}; // response with error 
			callback(response);
		},
		getSiteDataHandler : function(callback){
			//var response = { "errorMsg" : "Some Error!"}; // response with error 
			var response = {
				data: [
					{id: -1, name: "Select"},
					{id: 0, name: "NC Tourism"},
					{id: 1, name: "Dummy"}
				]
			};
			callback(response);
		},
		getCategoryDataHandler: function(siteId,callback){
			//var response = { "errorMsg" : "Some Error!"}; // response with error 			
			var response = { data: [] };
			
			//DEMO 
			if (siteId == 0){
				response.data.push({id: -1, name: "Select"});
				response.data.push({id: 0, name: "Vacation Rental"});
				response.data.push({id: 1, name: "Dummy"});
			} else if (siteId == 1){
				response.data.push({id: -1, name: "Select"});
				response.data.push({id: 0, name: "Dummy 1"});
				response.data.push({id: 1, name: "Dummy 2"});
				response.data.push({id: 2, name: "Dummy 3"});
				response.data.push({id: 3, name: "Vacation Rental"});
			}
			
			callback(response);
		}		
	}
	
	InitiatePaymentTypesDataTable.init(initOptions);

	var $select = $("#scratchSelection");
	//demo data	
	var opData = {
		"id" : 1,
		"paymentName": "PayPal",
		"ccard" : false,
		"escrow": true,
		"offline" : false,
		"default" : false,
		"url": "http://www.paypal.com",
		"logoUrl": "http://www.paypal.com/logo.png",
		"site" : "NC Tourism",
		"category": "Dummy",
		"username" : "dynamic",
		"status": _PT_STATUS_ACTIVE
	}
	addOpToScratchSelection(opData);
	opData = {
		"id": 2,
		"paymentName": "Google Checkout",
		"ccard" : true,
		"escrow": true,
		"offline" : true,
		"default" : false,
		"url": "http://www.google.com",
		"logoUrl": "http://www.google.com/google_logo.png",
		"site" : "Dummy",
		"category": "Vacation Rental",
		"username" : "dynamic",		
		"status": _PT_STATUS_ACTIVE
	}
	addOpToScratchSelection(opData);

	$("form#dropForm").on("success", function(res){
		var id = $("#drop-recordId").val();
		var $row = $('#payTypesDatatable').find("#id");
		$('a.view', $row).data("logourl",res);
	});
	
});