/**
* Javascript manage-perm-elements.html.html
* Revision: 2
* Date: 25-11-2015
* Author: Alexander Bulei
*/

var InitiatePermElementsDT = function () {
    return {
        init: function (options) {
			var $table = $('#permElemDT');
			var _options =  options || {};
			
			// check access
			controlActionBtnsAccess($table, _options.access);
		
            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [
                    '',
                    '',
                    '',
                    ''
                ]
            });

            //Add New Row
            $('#permElem_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '', buildStatusDropDownHTML(),
                    '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                editAddedRow(oTable, nRow);
				
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}
				
                isEditing = nRow;                
            });

            //Clone row
            $table.on("click", 'button.clone', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#cloneModal').data('id', id).modal('show');
            });

            $('#clone-btn').click(function () {
                var id = $('#cloneModal').data('id');
                var anSelected = $('#'+id);
                var data=[];
                $(anSelected).find('td').each(function(){data.push($(this).html());});
                var aiNew = oTable.fnAddData( data );
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                isEditing = nRow;
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                $('.publish-sign').closest('td').addClass('sign');
                $('#cloneModal').modal('hide');
            });
            var isEditing = null;

			bindChangeStatusDropDown($table);
			bindClickActiveModalConfirm();
			bindClickInactiveModalConfirm();
			bindCancelStatusModal();
			
            //Edit row
            $table.on("click", 'a.edit', function (e) {
				e.preventDefault();
                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }				
            });
			
            //Call Delete Modal passing data-id
            $table.on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteModal').data('id', id).modal('show');
            });

            //
            //Delete Row if Confirmed
            $('#delete-btn').click(function () {
                var id = $('#deleteModal').data('id');
				
				function _callback(response){
					if (!response.errorMsg) {
						var nRow = $('#' + response.id);
						oTable.fnDeleteRow(nRow);
					} else {
						Notify(response.errorMsg, 'bottom-right', '5000', 'danger', 'fa-times', true);
					}
				}				
				
				_options.deleteHandler(id,_callback);
                $('#deleteModal').modal('hide');				
            });

            //Cancel Editing or Adding a Row
            $table.on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Save an Editing Row
            $table.on("click", 'a.save', function (e) {
                e.preventDefault();
				if (this.innerHTML.indexOf("Save") >= 0) {
					var _canSave = true;
					_check = function(erroMsg){
						var v = $input.is("input") ? $input.val() : $input.find("option:selected").data("id");
						if (( v === "") || (v == null) || (v < 0)) {
							_canSave = _canSave && false;
							$input.addClass("required-error");
							_Error_(erroMsg);
						} else {
							$input.removeClass("required-error");
							_canSave = _canSave && true;
						}						
					}

					$input = $(".inline-module-id");
					_check('The Module is required field!');
					
					$input = $(".inline-section-id");
					_check('The Section is required field!');					
					
					$input = $(".inline-perm-elem");
					_check('The Permissions Element is required field!');					

					$input = $(".inline-perm-avail");
					_check('The Permission Availability is required field!');					
					
					var v = $input.val();
					if ((_canSave) && (v.length != 8)) {
						$input.addClass("required-error");
						_Error_('The Permission Availability must be exactly 8 characters!');
						_canSave = false;
					}
					
					if ((_canSave) && (/[^01]/g.test(v))) {
						$input.addClass("required-error");
						_Error_('Accept 0 and 1 only!');
						_canSave = false;						
					}
						
					if (_canSave){
						saveRow(oTable, isEditing);
						isEditing = null;
					}
				}
            });

			function _buildInlineSelect(classname, name, fn, callback){
				var $select = $("<select id='"+name+"' class='full-width row-ctrl form-control "+classname+"'></select>");
				
				function _callback(data){					
					var arr = data.data || [];					
					$.each(arr, function(i,obj){
						$op = $("<option data-id='"+obj.id+"' value='"+obj.name+"'>"+obj.name+"</option>");
						$select.append($op);
					});
					callback($select);
				}
				
				_options[fn](_callback);				
			}			
			
            function editRow(oTable, nRow, isNew) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
				
				_buildInlineSelect('inline-module-id','perm-module-select','getPermModulesDataHandler', function($select){
					var $td = $(jqTds[0]);
					$('option[value="'+aData[0]+'"]',$select).attr("selected", "selected");
					$td.empty().append($select);
					
					var $selectSection =  $('<select id="perm-section-select" class="full-width row-ctrl form-control inline-section-id"></select>')
					
					$select.on("change", function(e){
						var moduleId = $(this).find('option:selected').data("id");
						var $sectionSel = $(jqTds[1]).find('select').empty();
						
						_options.getPermSectionsDataHandler(function(data){
							var arr = data.data || [];	
							$.each(arr, function(i,obj){
								$op = $("<option data-id='"+obj.id+"' value='"+obj.name+"'>"+obj.name+"</option>");
								$sectionSel.append($op);
							});
							$('option[value="'+aData[1]+'"]',$sectionSel).attr("selected", "selected");						

						}, moduleId);
					});
					jqTds[1].innerHTML = $selectSection[0].outerHTML;
					$select.trigger("change");
				});
				jqTds[2].innerHTML = '<input type="text" class="form-control input-small inline-perm-elem" value="' + aData[2] + '">';
				jqTds[3].innerHTML = '<input type="text" type="number" class="form-control input-small inline-perm-avail" value="' + aData[3] + '">';
				
				// status select
				var $statusSelect = $( $.parseHTML(aData[4])[0] );
				if ($statusSelect.find("select").length == 0){
					$statusSelect = buildStatusDropDown();
					$('option[value="'+aData[4]+'"]',$statusSelect).attr("selected", "selected");
				}
				
                jqTds[4].innerHTML = $statusSelect[0].outerHTML;
				html = (typeof isNew != "undefined") ? ' data-mode="new"' : '';
                jqTds[5].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"'+html+'><i class="fa fa-times"></i> Cancel</a>';

            }
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
            }

            function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
				editRow(oTable, nRow, true);
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
				var jqSelects = $('select', nRow);
				
				var rowData = {
					"id": nRow.id || -1,
					"moduleId": jqSelects.eq(0).val(),
					"sectionId": jqSelects.eq(1).val(),
					"elemName": jqInputs[0].value,
					"elemAvail": jqInputs[1].value,
					"status": jqSelects.eq(2).val()
				}

				function _callback(response){
					if (!response.errorMsg){
						var record = response.record;
						oTable.fnUpdate(record.moduleId, nRow, 0, false);
						oTable.fnUpdate(record.sectionId, nRow, 1, false);
						oTable.fnUpdate(record.elemName, nRow, 2, false);
						oTable.fnUpdate(record.elemAvail, nRow, 3, false);
						oTable.fnUpdate(record.status, nRow, 4, false);
						oTable.fnUpdate('<a href="#" class="btn btn-palegreen  shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a><button type="button" class="btn btn-warning shiny btn-xs clone tooltip-warning command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Clone"><i class="fa fa-copy"></i></button><button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>', nRow, 5, false);
						oTable.fnDraw();
						$('[data-toggle="tooltip"]',nRow).tooltip();
					} else {
						Notify(response.errorMsg, 'bottom-right', '5000', 'danger', 'fa-times', true);
					}
				}
				
				//save
				_options.saveUpdateHandler(rowData, _callback);
            }
			
        }

    };
}();

$(window).bind("load", function () {
	/*Sets Themed Colors Based on Themes*/
	themeprimary = getThemeColorFromCss('themeprimary');
	themesecondary = getThemeColorFromCss('themesecondary');
	themethirdcolor = getThemeColorFromCss('themethirdcolor');
	themefourthcolor = getThemeColorFromCss('themefourthcolor');
	themefifthcolor = getThemeColorFromCss('themefifthcolor');
});

$(function(){
	//Initialise dataTable
	var initOptions = {
		access: {
			edit: true,
			delete: true
		},		
		saveUpdateHandler: function(data, callback){
			var response = { record: data };
			//var response = { "id" : id, "errorMsg" : "Some Error!"}; // response with error 
			callback(response);
		},
		deleteHandler: function(id, callback){
			var response = { "id" : id };
			//var response = { "errorMsg" : "Some Error!"}; // response with error 
			callback(response);
		},
		saveCategoriesHandler: function(data, callback){
			var response = { categories: data };
			callback(response);
		},
		getPermModulesDataHandler: function(callback){
			//var response = { "errorMsg" : "Some Error!"}; // response with error 
			var response = {
				data: [
					{id: -1, name: "Select the module"},
					{id: 1, name: "Module 1"},
					{id: 2, name: "Module 2"},
					{id: 3, name: "Module 3"}
				]
			};
			callback(response);
		},
		getPermSectionsDataHandler: function(callback, moduleId){
			//var response = { "errorMsg" : "Some Error!"}; // response with error
			var response = {
				data: [ {id: -1, name: "Select the section"} ]
			};
			// DEMO ONLY
			if (moduleId == 1) {
				response.data.push({id: 1, name: "Section A"}, {id: 2, name: "Section B"}, {id: 3, name: "Section C"})
			} else if (moduleId == 2) {
				response.data.push({id: 4, name: "Section D"}, {id: 5, name: "Section E"}, {id: 6, name: "Section F"})	
			} else if (moduleId == 3) {
				response.data.push({id: 7, name: "Section X"}, {id: 8, name: "Section Z"}, {id: 9, name: "Section Y"})	
			}

			callback(response);
		}		
	}	
	
	InitiatePermElementsDT.init(initOptions);
});