/**
* Javascript for manage-users-dashboard.html
* Revision: 1
* Author: Alexander Bulei
*/

$(function(){
	$.ajax({
		url: 'tmpl/users-dashboard.html',
		async: false,
		success: function(res){
			var $container = $("#user-dashboard-container");
			$container.append(res);
		}
	});	
});