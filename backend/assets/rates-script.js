/**
* Page: manage-rates-defs.html
* Revision: 2
* Modified Date: 18-01-2016
* Author: Alexander Bulei
*/

var InitiateRatesDT = function () {
    return {
        init: function (options) {
			var _options =  options || {};
			var $table = $('#ratesDT');
			var COLMAN = new COLMANAGER($table, _options.access.cols);
			COLMAN.init();			
			// check access
			controlActionBtnsAccess($table, _options.access);

			makeResponsiveTable($table);
            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "bAutoWidth": false				
            });

			
            //Add New Row
            $('#ratesDT_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '', '', '', buildStatusDropDownHTML(),
                    '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;
				oTable.fnGetPageOfRow(nRow);
            });

            //Clone row
            $table.on("click", 'button.clone', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#cloneModal').data('id', id).modal('show');
            });

            $('#clone-btn').click(function () {
                var id = $('#cloneModal').data('id');
                var anSelected = $('#'+id);
                var data=[];
                $(anSelected).find('td').each(function(){data.push($(this).html());});
                var aiNew = oTable.fnAddData( data );
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                isEditing = nRow;
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                $('.publish-sign').closest('td').addClass('sign');
				oTable.fnGetPageOfRow(nRow);
                $('#cloneModal').modal('hide');
            });
            var isEditing = null;
			
			bindChangeStatusDropDown($table);
			bindClickActiveModalConfirm();
			bindClickInactiveModalConfirm();
			bindCancelStatusModal();			
			
            //Edit row
            $table.on("click", 'a.edit', function (e) {
                e.preventDefault();

                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }
            });

            //Call Delete Modal passing data-id
            $table.on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteModal').data('id', id).modal('show');
            });

            //Delete Row if Confirmed
            $('#delete-btn').click(function () {
				function deleteCallback(response){
					var nRow = $('#' + response.id);
					oTable.fnDeleteRow(nRow);
				}				
				
				var id = $('#deleteModal').data('id');
				_options.deleteHandler(id, deleteCallback);
                $('#deleteModal').modal('hide');
            });

            //Cancel Editing or Adding a Row
            $table.on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });
			
			$table.delegate("a.user-chooser-link","click", function(){
				// USER CHOOSER HERE
			});

            //Save an Editing Row
            $table.on("click", 'a.save', function (e) {
                e.preventDefault();
				if (this.innerHTML.indexOf("Save") >= 0) {
					var _canSave = true;
					_check = function(erroMsg){
						var v = $input.is("input") ? $input.val() : $input.find("option:selected").val();
						if (( v === "") || (v == null) || (v < 0)) {
							_canSave = _canSave && false;
							$input.addClass("required-error");
							_Error_(erroMsg);
						} else {
							$input.removeClass("required-error");
							_canSave = _canSave && true;
						}						
					}

					$input = $("#rate-name-inline");
					_check('The Rate Name is required field!');
					
					$input = $("#rate-unit-inline");
					_check('The Unit is required field!');
					
					$input = $("#rate-increment-inline");
					_check('The Increment is required field!');

					$input = $("#rate-screenlabel-inline");
					_check('The Screen label is required field!');
					

					if (_canSave){
						saveRow(oTable, isEditing);
						isEditing = null;
					}
				}
            });

			function _buildInlineSelect(fnString, callback, param, unique){
				var $select = $("<select id='"+unique+"' class='form-control full-width'></select>");
				$select.append('<option value="-1">Select</option>')
				function _callback(data){					
					var arr = data.data || [];					
					$.each(arr, function(i,obj){
						$op = $("<option value='"+obj.id+"'>"+obj.name+"</option>");
						$select.append($op);
					});
					callback($select);
				}

				_options[fnString](function(response){
					_callback(response);
				}, param);
			}
			
			function populateCategoryDrop(tdSite,tdCategory){
				var $tdSite = $(tdSite), 
					$tdCategory = $(tdCategory),
					siteId = $tdSite.data("value");
				
				_buildInlineSelect('getCategoryDataHandler', function($select){
					var currV = $tdCategory.data("value");
					currV = (typeof currV != "undefined") ? currV : -1;
					
					$select.change(function(e){
						var v = $(this).val();
						$select.parent().data("value", v);
					});
					
					$('option[value="'+currV+'"]',$select).attr("selected", "selected");	
					
					$tdCategory.empty().append($select);
				}, { siteID: siteId }, 'rate-category-inline' );
			}			
			
            function editRow(oTable, nRow, isNew) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
				
				// RATE NAME
				if (COLMAN.haveAccess(jqTds[0])){
					jqTds[0].innerHTML = '<input id="rate-name-inline" style="max-width:120px;" type="text" class="form-control input-small" value="' + aData[0] + '">';
				}			

				// UNIT
				if (COLMAN.haveAccess(jqTds[1])){
					_buildInlineSelect('getUnitsDataHandler', function($select){
						var $td = $(jqTds[1]), currV = $td.data("value");
						currV = (typeof currV != "undefined") ? currV : -1;
						$select.change(function(e){
							var v = $(this).val();
							$select.parent().data("value", v);
						});
						
						$('option[value="'+currV+'"]',$select).attr("selected", "selected");	
						$td.empty().append($select);
					},{},'rate-unit-inline');					
				}	
				
				// INCREMENT 
				if (COLMAN.haveAccess(jqTds[2])){
					jqTds[2].innerHTML = '<input id="rate-increment-inline" style="max-width:80px;" type="number" class="form-control input-small" value="' + aData[2] + '">';
				}	

				// SCREENLABEL 
				if (COLMAN.haveAccess(jqTds[3])){
					jqTds[3].innerHTML = '<input id="rate-screenlabel-inline" type="text" class="form-control input-small" value="' + aData[3] + '">';
				}
				
				//SITE
				if (COLMAN.haveAccess(jqTds[4])){
					_buildInlineSelect('getSiteDataHandler', function($select){
						var $td = $(jqTds[4]), currV = $td.data("value");
						currV = (typeof currV != "undefined") ? currV : -1;
						$select.change(function(e){
							var v = $(this).val();
							$select.parent().data("value", v);
							
							// ATTACH CHANGE EVENT ONLY WHEN CATEGORY IS VISIBLE & EDITABLE
							if (COLMAN.haveAccess(jqTds[5])){
								populateCategoryDrop(jqTds[4],jqTds[5]);
							}
							
						});
						
						$('option[value="'+currV+'"]',$select).attr("selected", "selected");	
						$td.empty().append($select);
					},{},'rate-site-inline');
				}
				
				if (COLMAN.haveAccess(jqTds[5])){
					populateCategoryDrop(jqTds[4],jqTds[5]);
				}
				
				// STATUS 
				if (COLMAN.haveAccess(jqTds[6])){
					// status select
					var $statusSelect = $( $.parseHTML(aData[6])[0] );
					if ($statusSelect.find("select").length == 0){
						$statusSelect = buildStatusDropDown();
						$('option[value="'+aData[6]+'"]',$statusSelect).attr("selected", "selected");
					}
					
					jqTds[6].innerHTML = $statusSelect[0].outerHTML;
				}			

				html = (typeof isNew != "undefined") ? ' data-mode="new"' : '';
                jqTds[7].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"'+html+'><i class="fa fa-times"></i> Cancel</a>';
            }
			
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
				$('[data-toggle="tooltip"]',nRow).tooltip();
            }

            function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
				editRow(oTable, nRow, true);
            }

            function saveRow(oTable, nRow) {
				var jqTds = $('>td', nRow),
					jqSelects = $('select', nRow),
					jqInputs = $('input', nRow);
				
				var rowData = { "id": nRow.id || -1 }
				
				if (COLMAN.haveAccess(jqTds[0])){
					rowData.name = jqInputs[0].value;
				}
				
				if (COLMAN.haveAccess(jqTds[1])){
					rowData.unitId = jqSelects.eq(0).val();
					rowData.unit = jqSelects.eq(0).find('option:selected').text();
				}	
				
				if (COLMAN.haveAccess(jqTds[2])){
					rowData.increment = jqInputs[1].value;
				}
				
				if (COLMAN.haveAccess(jqTds[3])){
					rowData.label = jqInputs[2].value;
				}				
				
				if (COLMAN.haveAccess(jqTds[4])){
					rowData.siteId = jqSelects.eq(1).val();
					rowData.site = (rowData.siteId > -1) ? jqSelects.eq(1).find('option:selected').text() : "";
				}								
				
				if (COLMAN.haveAccess(jqTds[5])){
					rowData.categoryId = jqSelects.eq(2).val()
					rowData.category = (rowData.categoryId > -1) ? jqSelects.eq(2).find('option:selected').text() : "";
				}		

				if (COLMAN.haveAccess(jqTds[6])){
					rowData.status = jqSelects.eq(3).val();
				}
				
				function _callback(response){
					if (response.name) {
						oTable.fnUpdate(response.name, nRow, 0, false);
					}
					if (response.unit) {
						oTable.fnUpdate(response.unit, nRow, 1, false);
						$(jqTds[1]).data("value", response.unitId);
					}
					if (response.increment) {
						oTable.fnUpdate(response.increment, nRow, 2, false);
					}					
					if (response.label) {
						oTable.fnUpdate(response.label, nRow, 3, false);
					}					
					if (response.siteId) {
						oTable.fnUpdate(response.site, nRow, 4, false);
						$(jqTds[4]).data("value", response.siteId);
					}
					if (response.categoryId) {
						oTable.fnUpdate(response.category, nRow, 5, false);
						$(jqTds[5]).data("value", response.categoryId);
					}
					
					if (response.status) {
						oTable.fnUpdate(response.status, nRow, 6, false);
					}

					var actionsHTML = "";
					if (_options.access.actions.edit){
						actionsHTML += '<a href="#" class="btn btn-palegreen shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>';
					}
					if (_options.access.actions.clone){
						actionsHTML += '<button type="button" class="btn btn-warning shiny btn-xs clone tooltip-warning command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Clone"><i class="fa fa-copy"></i></button>';
					}
					if (_options.access.actions.delete){
						actionsHTML += '<button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>';
					}
					
					oTable.fnUpdate(actionsHTML, nRow, 7, false);
					oTable.fnDraw();
					$('[data-toggle="tooltip"]',nRow).tooltip();
				}
				
				_options.saveUpdateHandler(rowData, _callback);
            }
        }

    };
}();

$(window).bind("load", function () {
	var opts = {
		access: {
			actions: { edit: true, delete: true, clone: true, new: true },
			cols: { "rateName": "11", "rateUnit": "11", "rateIncrement": "11", "rateScreenLabel": "11", "rateSite": "11", "rateCategory": "11", "rateStatus": "11", "actions": "1"}
		},
		saveUpdateHandler: function(data, callback){
			var response = data; //DEMO
			console.warn(response);
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}
		},
		deleteHandler: function(id, callback){
			var response = { "id" : id }; //DEMO
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}		
		},
		getUnitsDataHandler: function(callback){
			var response = {
				data: [
					{id: 1, name: "Hour"},
					{id: 2, name: "Day"},
					{id: 3, name: "Week"}
				]
			};
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}	
		},
		getSiteDataHandler : function(callback){
			var response = {
				data: [
					{id: 1, name: "voli.com"},
					{id: 2, name: "lidl.com"},
					{id: 3, name: "t-mobile.com"}
				]
			};
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}	
		},
		getCategoryDataHandler: function(callback, siteId){
			var response = { data: [] };
			
			//DEMO 
			response.data.push({id: 1, name: "e-shop"});
			response.data.push({id: 2, name: "marketsm"});
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}	
		}
	}	
	InitiateRatesDT.init(opts);
});