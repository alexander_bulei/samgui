/**
* Common functions
* Revision: 12
* Date: 14-07-2015
* Author: Alexander Bulei
*/

var _STATUS_ACTIVE = 'Active';
var _STATUS_INACTIVE = 'Inactive';

function checkHiddenCols($table, row){
	var result = $(row);
	var $hiddenTH = $table.find('th:hidden');
	$hiddenTH.each(function(i, th){
		var index = $(th).index();
		result.find('td:eq('+index+')').hide();
	});
	return result[0];
}

// Bind change event for status dropdown
function bindChangeStatusDropDown($table){
	$table.find('select.status-select').each(function(i,select){
		var $select = $(select);
		$select.data('lastSel', $select.val());
		
		$select.change(function(e){
			e.preventDefault();
			var $this = $(this);
			var _val = $this.val();
			var id = $this.closest('tr').attr('id');
			if (_val == _STATUS_ACTIVE) {
				$('#activateModal').data('id', id).modal('show');
			} else {
				$('#modalDeactivate').data('id', id).modal('show');
			}
			return false;
		});
	});
}

// Bind click event on confrim button - Activate
function bindClickActiveModalConfirm(modalId){
	var _$modal = (typeof modalId != "undefined") ? $("#" + modalId) : $('#activateModal');
	
	$('#activate-btn', _$modal).click(function () {
		var id = _$modal.data('id');
		var $td = $('#' + id + ' .td-status');
		var $select = $td.find("select");
		var hasSelect = ($select.length > 0);
		
		if (hasSelect) {
			$select.val(_STATUS_ACTIVE); //activate
			$select.data('lastSel', $select.val());
		} else {
			$td.text(_STATUS_ACTIVE);
		}
		_$modal.modal('hide');
	});
}

// Bind click event on confrim button - Inactive
function bindClickInactiveModalConfirm(){
	var _$modal = (typeof modalId != "undefined") ? $("#" + modalId) : $('#modalDeactivate');
	$('#deactivate-btn',_$modal).click(function () {
		var id = _$modal.data('id');
		var $td = $('#' + id + ' .td-status');
		var $select = $td.find("select");
		var hasSelect = ($select.length > 0);
		
		if (hasSelect) {
			$td.find("select").val(_STATUS_INACTIVE); //Inactivate
			$select.data('lastSel', $select.val());
		} else {
			$td.text(_STATUS_INACTIVE);
		}
		_$modal.modal('hide');
	});
}

// bind click on cancel/close button - Activate/Inactive
function bindCancelStatusModal(modalActivateId, modalDeactivateId){
	var _$modalActivate = (typeof modalActivateId != "undefined") ? $("#" + modalActivateId) : $('#activateModal');
	var _$modalDeactivate = (typeof modalDeactivateId != "undefined") ? $("#" + modalDeactivateId) : $('#modalDeactivate');
	
	function _do(){
		var id = _$modalDeactivate.data('id');
		var $td = $('#' + id + ' .td-status');
		var $select = $td.find("select");
		var hasSelect = ($select.length > 0);
		
		if (hasSelect) {
			var lastVal = $select.data('lastSel');
			$select.val(lastVal);
		}
		
		_$modalActivate.modal('hide');
		_$modalDeactivate.modal('hide');
	}
	
	$('#deactivate-btn-close',_$modalDeactivate).on("click", _do);
	$('#activate-btn-close',_$modalActivate).on("click", _do);
}

// bind click on clone button - show modal prompt
function bindClickCloneShowModal($table){
	$table.on("click", 'button.clone', function (e) {
		e.preventDefault();
		var id = $(this).closest('tr').attr('id');
		$('#cloneModal').data('id', id).modal('show');
	});
}

function bindClickDeleteShowModal($table){
	$table.on("click", 'button.delete', function (e) {
		e.preventDefault();
		var id = $(this).closest('tr').attr('id');
		$('#deleteModal').data('id', id).modal('show');
	});
}

// build status dropdown
function buildStatusDropDown(){
	var $statusSelect = $('<select class="form-control status-drop">');
	$statusSelect.append('<option value="'+_STATUS_ACTIVE+'">'+_STATUS_ACTIVE+'</option>');
	$statusSelect.append('<option value="'+_STATUS_INACTIVE+'">'+_STATUS_INACTIVE+'</option>');
	return $statusSelect;
}

function buildStatusDropDownHTML(){
	var $statusSelect = buildStatusDropDown();
	return $statusSelect[0].outerHTML;
}

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

function is_valid_url(url) {
    return /^(http(s)?:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(url);
}

function _Error_(msg){
	Notify(msg, 'bottom-right', '5000', 'danger', 'fa-times', true);	
}

function loadListingModal(callback){
	$.ajax({
		"url": "tmpl/select-listing.html",
		"success": function(html){
			$("body").append(html);
			callback();
		}
	})
}

function makeResponsiveTable($table){
	$table.removeClass("responsive").addClass('flip-content');
	$table.find('thead').addClass('flip-content');
	$table.wrap('<div class="flip-scroll">');
}

function controlActionBtnsAccess(selector, accessOpts){
	var $selector = $(selector);
	if (!accessOpts) { return false; }
	
	var access = $.extend(true, { edit: true, delete: true, clone: true, new: true }, accessOpts);
	
	// edit command button
	if (!access.edit){
		$('.command-btn.edit', $selector).hide();
	}
	
	// delete command button
	if (!access.delete){
		$('.command-btn.delete', $selector).hide();
	}
	
	// clone command button
	if (!access.clone){
		$('.command-btn.clone', $selector).hide();
	}	
	
	// add new button command button
	if (!access.new){
		var id = $selector.attr('id') + '_new';
		$('#' + id).css("visibility","hidden");
	}	
}
