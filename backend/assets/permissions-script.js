/**
* Page: manage-permissions.html
* Revision: 5
* Modified Date: 21-12-2015
* Author: Alexander Bulei
*/

var InitiatePermissionsDT = function () {
    return {
        init: function (options) {
			var _options =  options || {};
			var $table = $('#permissionDT');
			
			// check access
			controlActionBtnsAccess($table, _options.access);

            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumns" : [
                    { sWidth: '8%' },
                    { sWidth: '8%' },
                    { sWidth: '10%' },
                    { sWidth: '5%' },
                    { sWidth: '9%' },
					{ sWidth: '4%' },
					{ sWidth: '4%' },
					{ sWidth: '3%' },
                    { sWidth: '7%' }
                ],
                "bAutoWidth": false				
            });

			$("thead tr.dt-filters input.grid-input", $table).keyup(function () {
                /* Filter on the column (the index) of this element */
                oTable.fnFilter(this.value, $("thead tr.dt-filters input.grid-input").index(this));
            });			
			
            //Add New Row
            $('#permissionDT_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '<span data-userid="-1" data-username=""></span>', '', '', '',buildStatusDropDownHTML(),
                    '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;
				oTable.fnGetPageOfRow(nRow);
            });

            //Clone row
            $table.on("click", 'button.clone', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#cloneModal').data('id', id).modal('show');
            });

            $('#clone-btn').click(function () {
                var id = $('#cloneModal').data('id');
                var anSelected = $('#'+id);
                var data=[];
                $(anSelected).find('td').each(function(){data.push($(this).html());});
                var aiNew = oTable.fnAddData( data );
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                isEditing = nRow;
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                $('.publish-sign').closest('td').addClass('sign');
				oTable.fnGetPageOfRow(nRow);
                $('#cloneModal').modal('hide');
            });
            var isEditing = null;

			$table.on("click", '.edit-perm-ex', function (e) {
				var $modal = $("#permEditExModal"),
					$tr = $(this).closest('tr'), 
					id = $tr.attr('id');
				$modal.find(".perm-module").text( $.trim( $tr.find('td:eq(0)').text() ) );
				var $tree = $("#permEditExModal-perm-tree").empty(), isFirst;
				_options.getPermEx(id, function(response){
					var array = response || [];
					$.each(array, function(i, section){
						var $li = $('<li><div class="pt-handle"><div class="perm-section-name">'+section.permElemSectionName+'</div></div></li>');
						var $btn = $('<a class="permEditExModal-tree-action" href="#"><i class="fa"></i></a>');
						$li.find('.pt-handle').prepend($btn);
						
						$li.find('.pt-handle').append('<div class="pt-sub-chk-container" style="display:none"><div class="pt-sub-chk-wrapper">View</div><div class="pt-sub-chk-wrapper">Add</div><div class="pt-sub-chk-wrapper">Edit</div><div class="pt-sub-chk-wrapper">Delete</div><div class="pt-sub-chk-wrapper">Publish/Other</div><div class="pt-sub-chk-wrapper">Engage</div><div class="pt-sub-chk-wrapper">Other1</div><div class="pt-sub-chk-wrapper">Other2</div></div>');						
						isFirst = (i === 0);
						if (isFirst){
							$btn.data("state", "expanded").find('i').addClass("fa-minus");
							$li.find(".pt-sub-chk-container").show();
						} else {
							$btn.data("state", "collapsed").find('i').addClass("fa-plus");
						}
						
						var elemArr = section.permElements || [];
						if (elemArr.length) {
							var $sub = $('<ul></ul>').appendTo($li);
							if (!isFirst){
								$sub.hide();
							}
							$.each(elemArr, function(i, element){
								var $subLi = $('<li><div class="pt-handle"><div class="perm-element-name">'+element.name+'</div><div class="pt-sub-chk-container"></div></div></li>');
								$sub.append($subLi);
								var $chkCon = $subLi.find('.pt-sub-chk-container'),
									v = element.value;
								for (var i = 0, len = v.length; i < len; i++) {
									var checked = (v[i] == 1);
									$chkCon.append('<div class="pt-sub-chk-wrapper"><div class="checkbox"><label><input type="checkbox" class="colored-blue" '+ (checked ? 'checked="checked"' : '') +'><span class="text"></span></label></div></div>')
								}								
							});
						}
						$tree.append($li);
					});
				});
				$modal.data("id",id).modal("show");
			});
			
			$("#permEditExModal-save").click(function(){
				var data = [], $modal = $("#permEditExModal"),
					id =  $modal.data("id"), $tree = $("#permEditExModal-perm-tree");
				
				$tree.children('li').each(function(i, li){
					var $li  = $(li);
					var d = { permElemSectionName: $li.find(".perm-section-name").text(), permElements: [] };
					var $sub = $li.children('ul');
					
					if ($sub.length){
						$sub.children('li').each(function(i, subLi){
							var $subLi = $(subLi),
								$chks = $subLi.find('input[type="checkbox"]'),
								v = "";
							$chks.each(function(i, chk){
								v += (chk.checked ? "1" : "0");
							});
							d.permElements.push({
								name: $subLi.find('.perm-element-name').text(),
								value: v
							});
						});
					}
					
					data.push(d);
				});
				
				_options.savePermExHandler(id, data, function(){
					$modal.modal("hide");
				});
			});			
			
            //Edit row
            $table.on("click", 'a.edit', function (e) {
                e.preventDefault();

                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }
            });

            $table.on("click", 'button.view', function () {
                var id = $(this).closest('tr').attr('id');
                $('#editModal').data('id', id).modal('show');
            });

            //Call Delete Modal passing data-id
            $table.on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteModal').data('id', id).modal('show');
            });

            //Delete Row if Confirmed
            $('#delete-btn').click(function () {
                var id = $('#deleteModal').data('id');
                var nRow = $('#'+id);
                oTable.fnDeleteRow(nRow);
                $('#deleteModal').modal('hide');
            });

            //Cancel Editing or Adding a Row
            $table.on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });
			
			$table.delegate("a.user-chooser-link","click", function(){
				// USER CHOOSER HERE
			});

            //Save an Editing Row
            $table.on("click", 'a.save', function (e) {
                e.preventDefault();
				if (this.innerHTML.indexOf("Save") >= 0) {
					var _canSave = true;
					_check = function(erroMsg){
						var v = $input.is("input") ? $input.val() : $input.find("option:selected").data("id");
						if (( v === "") || (v == null) || (v < 0)) {
							_canSave = _canSave && false;
							$input.addClass("required-error");
							_Error_(erroMsg);
						} else {
							$input.removeClass("required-error");
							_canSave = _canSave && true;
						}						
					}

					$input = $("#inline-module-select");
					_check('The Module is required field!');
					
					//check user & user role
					var $userLink = $('a.user-chooser-link');
					$("#inline-userrole-select, a.user-chooser-link").removeClass("required-error");
					if (($userLink.data("userid") < 0) && ($("#inline-userrole-select option:selected").data("id") < 0)){
						_canSave = false;
						if ($userLink.data("userid") < 0) {
							$userLink.addClass("required-error");
						}
						if ($("#inline-userrole-select option:selected").data("id") < 0){
							$("#inline-userrole-select").addClass("required-error");
						}
						_Error_("Select either User or User Role!");
					}
					
					if (_canSave){
						saveRow(oTable, isEditing);
						isEditing = null;
					}
				}
            });

			function _buildInlineSelect(name, fn, callback){
				var $select = $("<select id='"+name+"' class='full-width row-ctrl form-control'></select>");
				
				function _callback(data){					
					var arr = data.data || [];					
					$.each(arr, function(i,obj){
						$op = $("<option data-id='"+obj.id+"' value='"+obj.name+"'>"+obj.name+"</option>");
						$select.append($op);
					});
					callback($select);
				}
				
				_options[fn](_callback);				
			}			
			
            function editRow(oTable, nRow, isNew) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
				
				// module
				_buildInlineSelect('inline-module-select','getPermModulesDataHandler', function($select){
					$('option[value="'+aData[0]+'"]',$select).attr("selected", "selected");
					jqTds[0].innerHTML = $select[0].outerHTML;		
				});
				
				//site
				_buildInlineSelect('inline-site-select','getSiteDataHandler', function($select){
					var $td1 = $(jqTds[1]), $td2 = $(jqTds[2]);
					$('option[value="'+aData[1]+'"]',$select).attr("selected", "selected");
					$td1.empty().append($select);
					
					var $selectCat =  $('<select class="full-width row-ctrl form-control inline-select-categories"></select>')
					$td2.empty().append($selectCat);
					
					$td1.find('select').change(function(e){
						var siteId = $(this).find('option:selected').data('id');
						var $selectCat = $td2.find('select').empty();
						
						if (siteId > -1){
							_options.getCategoryDataHandler(function(data){
								var arr = data.data || [];	
								$.each(arr, function(i,obj){
									$op = $("<option data-id='"+obj.id+"' value='"+obj.name+"'>"+obj.name+"</option>");
									$selectCat.append($op);
								});
								$('option[value="'+aData[2]+'"]',$selectCat).attr("selected", "selected");						

							}, siteId);
						}
					}).trigger("change");
				});
				
				// user
				var $span = $(jqTds[3]).find('span');
				jqTds[3].innerHTML = '<a data-userid="'+$span.data("userid")+'" data-username="'+$span.data("username")+'" class="btn btn-azure shiny btn-xs user-chooser-link" href="#"><i class="fa fa-user"></i></a>';
				
				// userRole
				_buildInlineSelect('inline-userrole-select','getUserRolesDataHandler', function($select){
					$('option[value="'+aData[4]+'"]',$select).attr("selected", "selected");
					jqTds[4].innerHTML = $select[0].outerHTML;		
				});	
				
				// owner flag
				var flag = aData[5].toUpperCase() == 'YES' ? true : false;
				var _chkAttr = flag ? ' checked="checked"' : '';
				jqTds[5].innerHTML = '<label><input class="checkbox-slider toggle yesno row-ctrl" type="checkbox"'+_chkAttr+'><span class="text" style="margin-top: 13px;"></span></label>';
				
				$(jqTds[5]).find('input').change(function(){
					var $s = $("#inline-permstatus-select");
					if (this.checked) {
						$s.multiselect('enable');
					} else {
						$s.multiselect('disable');
						$s.multiselect('deselectAll', false).multiselect('updateButtonText');
					}
				}).trigger("change");
				
				// permissions status
				_buildInlineSelect('inline-permstatus-select','getPermStatusDataHandler', function($select){
					var $td6 = $(jqTds[6]);
					var $span = $td6.find("span"), selStr = $span.data("selected") || false, selArray = selStr ? selStr.split(',') : [];
					$select.attr("multiple", "multiple");
					$.each(selArray, function(i, val){
						$('option[value="'+val+'"]',$select).attr("selected", "selected");
					});
					$td6.empty().append($select);
					var $dropdown = $td6.find("select");
					$dropdown.multiselect({
						buttonText: function(options, select) {
							return options.length + " selected";
						}
					});
					flag ? $dropdown.multiselect('enable') : $dropdown.multiselect('disable');
				});
				
				// status select
				var $statusSelect = $( $.parseHTML(aData[7])[0] );
				if ($statusSelect.find("select").length == 0){
					$statusSelect = buildStatusDropDown();
					$('option[value="'+aData[7]+'"]',$statusSelect).attr("selected", "selected");
				}
				 jqTds[7].innerHTML = $statusSelect[0].outerHTML;
				
				html = (typeof isNew != "undefined") ? ' data-mode="new"' : '';
                jqTds[8].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"'+html+'><i class="fa fa-times"></i> Cancel</a>';
            }
			
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
				$('[data-toggle="tooltip"]',nRow).tooltip();
            }

            function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
				editRow(oTable, nRow, true);
            }

            function saveRow(oTable, nRow) {
				var jqSelects = $('select', nRow),
					$userLink = $('a.user-chooser-link');
				
				
				var record = {
					"id": nRow.id || -1,
					"module": jqSelects.eq(0).val(),
					"site": jqSelects.eq(1).val(),
					"category": jqSelects.eq(2).val(),
					"userId": $userLink.data("userid") || -1,
					"username": $userLink.data("username") || "",
					"userRole": jqSelects.eq(3).val(),
					"owner": $('input[type="checkbox"]', nRow)[0].checked ? 'Yes' : 'No',
					"permStatus": jqSelects.eq(4).val() || [],
					"status": jqSelects.eq(5).val()
				}
				function _callback(response){
					if (!response.errorMsg){
						jqSelects.eq(5).multiselect("destroy");
						var record = response.record;
						oTable.fnUpdate(record.module, nRow, 0, false);
						oTable.fnUpdate(record.site, nRow, 1, false);
						oTable.fnUpdate(record.category, nRow, 2, false);
						oTable.fnUpdate('<span data-userid="'+record.userId+'" data-username="'+record.username+'">'+record.username+'</span>', nRow, 3, false);
						oTable.fnUpdate(record.userRole, nRow, 4, false);
						oTable.fnUpdate(record.owner, nRow, 5, false);
						oTable.fnUpdate('<span data-selected="'+record.permStatus.join(',')+'">'+record.permStatus.length+' selected</span>', nRow, 6, false);
						oTable.fnUpdate(record.status, nRow, 7, false);
						
						oTable.fnUpdate('<button type="button" class="btn btn-blue shiny btn-xs dashboard tooltip-blue command-btn edit-perm-ex" data-toggle="tooltip" data-placement="top" data-original-title="Permission Details"><i class="fa fa-file-o"></i></button><a href="#" class="btn btn-palegreen shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a><button type="button" class="btn btn-warning shiny btn-xs clone tooltip-warning command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Clone"><i class="fa fa-copy"></i></button><button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>', nRow, 8, false);
						oTable.fnDraw();
						$('[data-toggle="tooltip"]',nRow).tooltip();
					} else {
						Notify(response.errorMsg, 'bottom-right', '5000', 'danger', 'fa-times', true);
					}
					oTable.fnGetPageOfRow(nRow);
				}
				
				//save
				_options.saveUpdateHandler(record, _callback);
            }
        }

    };
}();

$(window).bind("load", function () {
	/*Sets Themed Colors Based on Themes*/
	themeprimary = getThemeColorFromCss('themeprimary');
	themesecondary = getThemeColorFromCss('themesecondary');
	themethirdcolor = getThemeColorFromCss('themethirdcolor');
	themefourthcolor = getThemeColorFromCss('themefourthcolor');
	themefifthcolor = getThemeColorFromCss('themefifthcolor');
	
	$("#permEditExModal").delegate("a.permEditExModal-tree-action","click", function(){
		var $this = $(this), $li = $this.closest("li"), state = $this.data("state");
		if (state == "expanded"){
			$li.children("ul").hide();
			$li.find(".pt-sub-chk-container").hide();
			$this.data("state","collapsed").find('i').removeClass("fa-minus").addClass("fa-plus");
		} else {
			$li.children("ul").show();
			$li.find(".pt-sub-chk-container").show();
			$this.data("state","expanded").find('i').removeClass("fa-plus").addClass("fa-minus");
		}
	});

	var opts = {
		access: {
			edit: true,
			delete: true
		},
		saveUpdateHandler: function(data, callback){
			var response = { record: data };
			//var response = { "id" : id, "errorMsg" : "Some Error!"}; // response with error 
			callback(response);
		},
		deleteHandler: function(id, callback){
			var response = { "id" : id };
			//var response = { "errorMsg" : "Some Error!"}; // response with error 
			callback(response);
		},
		getSiteDataHandler : function(callback){
			//var response = { "errorMsg" : "Some Error!"}; // response with error 
			var response = {
				data: [
					{id: -1, name: ""},
					{id: 0, name: "NC Tourism"},
					{id: 1, name: "Dummy"}
				]
			};
			callback(response);
		},
		getCategoryDataHandler: function(callback, siteId){
			//var response = { "errorMsg" : "Some Error!"}; // response with error 			
			var response = { data: [] };
			
			//DEMO 
			if (siteId == 0){
				response.data.push({id: -1, name: ""});
				response.data.push({id: 0, name: "Vacation Rental"});
				response.data.push({id: 1, name: "Dummy"});
			} else if (siteId == 1){
				response.data.push({id: -1, name: ""});
				response.data.push({id: 0, name: "Dummy 1"});
				response.data.push({id: 1, name: "Dummy 2"});
				response.data.push({id: 2, name: "Dummy 3"});
				response.data.push({id: 3, name: "Vacation Rental"});
			}
			callback(response);
		},
		getPermModulesDataHandler: function(callback){
			//var response = { "errorMsg" : "Some Error!"}; // response with error 
			var response = {
				data: [
					{id: -1, name: ""},
					{id: 1, name: "Module 1"},
					{id: 2, name: "Module 2"},
					{id: 3, name: "Module 3"}
				]
			};
			callback(response);
		},
		getUserRolesDataHandler: function(callback){
			//var response = { "errorMsg" : "Some Error!"}; // response with error 
			var response = {
				data: [
					{id: -1, name: ""},
					{id: 1, name: "User Role 1"},
					{id: 2, name: "User Role 2"},
					{id: 3, name: "User Role 3"}
				]
			};
			callback(response);
		},
		getPermStatusDataHandler: function(callback){
			//var response = { "errorMsg" : "Some Error!"}; // response with error 
			var response = {
				data: [
					{id: 1, name: "PS1"},
					{id: 2, name: "PS2"},
					{id: 3, name: "PS3"},
					{id: 3, name: "PS4"},
					{id: 3, name: "PS5"},
					{id: 3, name: "PS6"},
					{id: 3, name: "PS7"},
				]
			};
			callback(response);
		},
		getPermEx: function(id, callback){
			var response = [
					{ permElemSectionName: "User Roles", permElements: [
						{ name: "Element #1", value: "01010101" },
						{ name: "Element #2", value: "00011100" },
						{ name: "Element #3", value: "11001100" },
						{ name: "Element #4", value: "10110001" },
						{ name: "Element #5", value: "11100010" }
					]},
					{ permElemSectionName: "User Manager", permElements: [
						{ name: "Element #1", value: "01010101" },
						{ name: "Element #2", value: "00011100" },
						{ name: "Element #3", value: "11001100" },
						{ name: "Element #4", value: "10110001" },
						{ name: "Element #5", value: "11100010" }
					]},
					{ permElemSectionName: "User Roles", permElements: [
						{ name: "Element #1", value: "01010101" },
						{ name: "Element #2", value: "00011100" },
						{ name: "Element #3", value: "11001100" },
						{ name: "Element #4", value: "10110001" },
						{ name: "Element #5", value: "11100010" }
					]},
					{ permElemSectionName: "User Manager", permElements: [
						{ name: "Element #1", value: "01010101" },
						{ name: "Element #2", value: "00011100" },
						{ name: "Element #3", value: "11001100" },
						{ name: "Element #4", value: "10110001" },
						{ name: "Element #5", value: "11100010" }
					]},
					{ permElemSectionName: "User Roles", permElements: [
						{ name: "Element #1", value: "01010101" },
						{ name: "Element #2", value: "00011100" },
						{ name: "Element #3", value: "11001100" },
						{ name: "Element #4", value: "10110001" },
						{ name: "Element #5", value: "11100010" }
					]},
					{ permElemSectionName: "User Manager", permElements: [
						{ name: "Element #1", value: "01010101" },
						{ name: "Element #2", value: "00011100" },
						{ name: "Element #3", value: "11001100" },
						{ name: "Element #4", value: "10110001" },
						{ name: "Element #5", value: "11100010" }
					]}					
				];
			callback(response);			
		},
		savePermExHandler: function(id,data,callback){
			var response = { "response" : "OK" }; // response with error 
			if ($.isFunction(callback)){
				callback(response);
			}
		}
	}	
	InitiatePermissionsDT.init(opts);
});