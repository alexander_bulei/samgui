/**
* Javascript for manage-site-profiles.html
* Revision: 17
* Date: 17-12-2015
* Author: Alexander Bulei
*/

function _buildInlineSelect(realFn, callback, param){
	var $select = $("<select class='form-control full-width'></select>");
	
	function _callback(data){					
		var arr = data || [];					
		$.each(arr, function(i,obj){
			$op = $('<option data-text="'+obj.name+'" value="'+obj.id+'">'+obj.name+'</option>');
			$select.append($op);
		});
		callback($select);
	}

	realFn(function(response){
		_callback(response);
	}, param);
}

function selectByText($select, text){
	$select.find('option[data-text="'+text+'"]').attr("selected","selected");
}

var InitMainDataTable = function () {
    return {
        init: function (options) {
			var _options =  options || {};
			var $table = $('#sitetable');
			
			// column manager
			var COLMAN = new COLMANAGER($table, _options.access.cols);
			COLMAN.init();
			
			// check access
			controlActionBtnsAccess($table, _options.access);
			
            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumns": [
                    null,
                    null,
                    null,
                    { "bSortable": false }
                ]
            });
            var i = oTable.fnGetData().length + 1;

            // EDIT MAIN DIALOG
            $table.on("click", 'a.edit', function () {
                var id = $(this).closest('tr').attr('id');
                $('#mainModal').data('id', id).modal('show');
            });

            var isEditing = null;

            //Add New Row
            $('#editabledatatable_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '<i class="fa fa-check publish-sign"></i>',
                    '<a href="#" class="btn btn-success btn-smll shiny btn-xs save" data-mode="new"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning btn-smll shiny btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;
				oTable.fnGetPageOfRow(nRow);
            });

            //Clone row
            $('#sitetable').on("click", 'button.clone', function (e) {
                e.preventDefault();
				var id = $(this).closest('tr').attr('id');
				$('#cloneModal').data('id', id).modal('show');
            });

            $('#clone-btn').click(function () {
                var id = $('#cloneModal').data('id');
                var anSelected = $('#'+id);
                var data=[];
                $(anSelected).find('td').each(function(){data.push($(this).html());});
                var aiNew = oTable.fnAddData( data );
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                isEditing = nRow;
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
				
				oTable.fnGetPageOfRow(nRow);
                $('#cloneModal').modal('hide');
            });

            var isEditing = null;

			bindChangeStatusDropDown($('#sitetable'));
			bindClickActiveModalConfirm();
			bindClickInactiveModalConfirm();
			bindCancelStatusModal();
			
            //Call Delete Modal passing data-id
            $('#sitetable').on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteSiteModal').data('id', id).modal('show');
            });
			
            //Delete Row if Confirmed
            $('#deleteSite-btn').click(function () {
				
				function deleteCallback(response){
					var nRow = $('#' + response.id);
					oTable.fnDeleteRow(nRow);
				}				
				
				var id = $('#deleteSiteModal').data('id');
				_options.deleteHandler(id, deleteCallback);
                $('#deleteSiteModal').modal('hide');
            });

            //Cancel Editing or Adding a Row
            $('#sitetable').on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Save an Editing Row
            $('#sitetable').on("click", 'a.save', function (e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
                    saveRow(oTable, isEditing);
                    isEditing = null;
                    //Some Code to Highlight Updated Row
                }
            });


            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
				$('[data-toggle="tooltip"]',nRow).tooltip();
            }

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
				
				if (COLMAN.haveAccess(jqTds[0])){
					jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
				}
				
				if (COLMAN.haveAccess(jqTds[1])){
					jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
				}
				
				if (COLMAN.haveAccess(jqTds[2])){
					// status select
					var $statusSelect = $( $.parseHTML(aData[2])[0] );
					if ($statusSelect.find("select").length == 0){
						$statusSelect = buildStatusDropDown();
						$('option[value="'+aData[2]+'"]',$statusSelect).attr("selected", "selected");
					}					
					jqTds[2].innerHTML = $statusSelect[0].outerHTML;
				}
				
				jqTds[3].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"><i class="fa fa-times"></i> Cancel</a>';
            }

            function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
				
				if (COLMAN.haveAccess(jqTds[0])){
					jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
				}
				
				if (COLMAN.haveAccess(jqTds[1])){
					jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
				}
				
				if (COLMAN.haveAccess(jqTds[2])){
					// status select
					var $statusSelect = $( $.parseHTML(aData[2])[0] );
					if ($statusSelect.find("select").length == 0){
						$statusSelect = buildStatusDropDown();
						$('option[value="'+aData[2]+'"]',$statusSelect).attr("selected", "selected");
					}					
					jqTds[2].innerHTML = $statusSelect[0].outerHTML;
				}
				
                jqTds[3].innerHTML = aData[3];
            }

            function saveRow(oTable, nRow) {
				var jqTds = $('>td', nRow);
                var jqInputs = $('input', nRow);
				var jqSelects = $('select', nRow);
				
				var rowData = { "id": nRow.id || -1 }
				
				if (COLMAN.haveAccess(jqTds[0])){
					rowData.siteName = jqInputs[0].value;
				}
				
				if (COLMAN.haveAccess(jqTds[1])){
					rowData.siteDesc = jqInputs[1].value;
				}				
				
				if (COLMAN.haveAccess(jqTds[2])){
					rowData.status = jqSelects.eq(0).val();
				}
				
				function _callback(response){
					if (response.siteName) {
						oTable.fnUpdate(response.siteName, nRow, 0, false);
					}
					
					if (response.siteDesc) {
						oTable.fnUpdate(response.siteDesc, nRow, 1, false);
					}
					
					if (response.status) {
						oTable.fnUpdate(response.status, nRow, 2, false);
					}

					var actionsHTML = "";
					if (_options.access.actions.open){
						actionsHTML += '<button type="button" class="btn btn-blue shiny btn-xs dashboard tooltip-blue command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Open"><i class="fa fa-external-link"></i></button>';
					}
					if (_options.access.actions.edit){
						actionsHTML += '<a href="#" class="btn btn-palegreen  shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>';
					}
					if (_options.access.actions.clone){
						actionsHTML += '<button type="button" class="btn btn-warning shiny btn-xs clone tooltip-warning command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Clone"><i class="fa fa-copy"></i></button>';
					}
					if (_options.access.actions.delete){
						actionsHTML += '<button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>';
					}
					
					oTable.fnUpdate(actionsHTML, nRow, 3, false);
					oTable.fnDraw();
					$('[data-toggle="tooltip"]',nRow).tooltip();
				}
				
				_options.saveUpdateHandler(rowData, _callback);				
            }
        }
    };
}();

var InitiateEmailDataTable = function () {
    return {
        init: function (options) {
			var _options =  options || {};
			var $table = $('#emaildatatable');
			
			// column manager
			var COLMAN = new COLMANAGER($table, _options.access.cols);
			COLMAN.init();
			
			// check access
			controlActionBtnsAccess($table, _options.access);
			
            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumns" : [
                    { sWidth: '12%' },
                    { sWidth: '12%' },
                    { sWidth: '12%' },
                    { sWidth: '8%' },
                ],
                "bAutoWidth": false
            });

            var isEditing = null;

            //Add New Row
            $('#emaildatatable_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '',
                    '<a href="#" class="btn btn-success shiny btn-xs btn-smll save" data-mode="new"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn shiny btn-warning btn-xs btn-smll cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;

            });

            //Call Delete Modal passing data-id
            $('#emaildatatable').on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteEmailModal').data('id', id).modal('show');
            });
			
            //Delete Row if Confirmed
            $('#delete-btn').click(function () {
				
				function deleteCallback(response){
					var nRow = $('#' + response.id);
					oTable.fnDeleteRow(nRow);
				}				
				
				var id = $('#deleteEmailModal').data('id');
				_options.deleteHandler(id, deleteCallback);
                $('#deleteEmailModal').modal('hide');				
				
            });

            //Cancel Editing or Adding a Row
            $('#emaildatatable').on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Edit A Row
            $('#emaildatatable').on("click", 'a.edit', function (e) {
                e.preventDefault();

                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }
            });

            //Save an Editing Row
            $('#emaildatatable').on("click", 'a.save', function (e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
					$('.val-email').parent().removeClass('has-error');
					$('.val-url').parent().removeClass('has-error');
                    var string = $('.val-email').val();
                    if (string.lenght != 0) {
                        if (isValidEmailAddress(string)) {                            
                            var string = $('.val-url').val();
                            if (string.lenght != 0) {
                                if (is_valid_url(string)) {
                                    if (string.match(/(^http:\/\/)/) == null) {
                                        $('.val-url').val('http://' + string);
                                    }                                    
                                    saveRow(oTable, isEditing);
                                    isEditing = null;
                                } else {
                                    $('.val-url').parent().addClass('has-error');
                                    Notify('The url is incorrect', 'bottom-right', '5000', 'danger', 'fa-times', true);

                                }
                            }
                        } else {
                            $('.val-email').parent().addClass('has-error');
                            Notify('The email is incorrect', 'bottom-right', '5000', 'danger', 'fa-times', true);
                            if (is_valid_url(string)) {
                                $('.val-url').parent().removeClass('has-error');
                            } else {
                                $('.val-url').parent().addClass('has-error');
                                Notify('The url is incorrect', 'bottom-right', '5000', 'danger', 'fa-times', true);

                            }
                        }
                    }
                }
            });


            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
				$('[data-toggle="tooltip"]',nRow).tooltip();
            }

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<div class="input-group"><input type="text" class="form-control input-small val-email" value="' + aData[0] + '"></div>';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<div class="input-group"><input type="url" class="form-control input-small url-fill val-url" value="' + aData[2] + '"></div>';
                jqTds[3].innerHTML = '<a href="#" class="btn btn-success shiny btn-smll btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"><i class="fa fa-times"></i> Cancel</a>';
            }

            function editAddedRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<div class="input-group"><input type="text" class="form-control input-small val-email" value="' + aData[0] + '"></div>';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<div class="input-group"><input type="url" class="form-control input-small url-fill val-url" value="' + aData[2] + '"></div>';
                jqTds[3].innerHTML = aData[3];
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
				
				var rowData = { 
					id: nRow.id || -1,
					email: jqInputs[0].value,
					pw: jqInputs[1].value,
					host: jqInputs[2].value
				}
				
				function _callback(response){
					
					var actionsHTML = "";
					if (_options.access.actions.edit){
						actionsHTML += '<a href="#" class="btn btn-palegreen shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>';
					}
					if (_options.access.actions.delete){
						actionsHTML += '<button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>';
					}					
					
					oTable.fnUpdate(response.email, nRow, 0, false);
					oTable.fnUpdate(response.pw, nRow, 1, false);
					oTable.fnUpdate(response.host, nRow, 2, false);
					oTable.fnUpdate(actionsHTML, nRow, 3, false);
					oTable.fnDraw();
					$('[data-toggle="tooltip"]',nRow).tooltip();					
				}
				_options.saveUpdateHandler(rowData, _callback);
            }
        }

    };


}();
var InitiatePhoneDataTable = function () {
    return {
        init: function (options) {
			var _options =  options || {};
			var $table = $('#phonedatatable');
			
			// column manager
			var COLMAN = new COLMANAGER($table, _options.access.cols);
			COLMAN.init();			
			
			// check access
			controlActionBtnsAccess($table, _options.access);
			
            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumns" : [
                    { sWidth: '12%' },
                    { sWidth: '12%' },
                    { sWidth: '12%' },
                    { sWidth: '8%' },
                ],
                "bAutoWidth": false
            });

            var isEditing = null;

            //Add New Row
            $('#phonedatatable_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '',
                    '<a href="#" class="btn btn-success shiny btn-smll btn-xs save" data-mode="new"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn shiny btn-warning btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;

            });

            //Call Delete Modal passing data-id
            $('#phonedatatable').on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deletePhoneModal').data('id', id).modal('show');
            });
			
            //Delete Row if Confirmed
            $('#delete-phone-btn').click(function () {
				function deleteCallback(response){
					var nRow = $('#' + response.id);
					oTable.fnDeleteRow(nRow);
				}				
				var id = $('#deletePhoneModal').data('id');
				_options.deleteHandler(id, deleteCallback);
                $('#deletePhoneModal').modal('hide');				
            });

            //Cancel Editing or Adding a Row
            $('#phonedatatable').on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Edit A Row
            $('#phonedatatable').on("click", 'a.edit', function (e) {
                e.preventDefault();

                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }
            });

            //Save an Editing Row
            $('#phonedatatable').on("click", 'a.save', function (e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
                    saveRow(oTable, isEditing);
                    isEditing = null;
                    //Some Code to Highlight Updated Row
                }
            });


            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
				$('[data-toggle="tooltip"]',nRow).tooltip();
            }

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input type="text" class="form-control input-small phone_us" value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<a href="#" class="btn btn-success shiny btn-xs btn-smll save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn shiny btn-warning btn-xs cancel"><i class="fa fa-times"></i> Cancel</a>';
            }

            function editAddedRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input type="text" class="form-control input-small phone_us" value="' + aData[2] + '">';
                jqTds[3].innerHTML = aData[3];
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
				
				var rowData = { 
					id: nRow.id || -1,
					accId: jqInputs[0].value,
					token: jqInputs[1].value,
					phone: jqInputs[2].value
				}
				
				function _callback(response){
					
					var actionsHTML = "";
					if (_options.access.actions.edit){
						actionsHTML += '<a href="#" class="btn btn-palegreen shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>';
					}
					if (_options.access.actions.delete){
						actionsHTML += '<button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>';
					}					
					
					oTable.fnUpdate(response.accId, nRow, 0, false);
					oTable.fnUpdate(response.token, nRow, 1, false);
					oTable.fnUpdate(response.phone, nRow, 2, false);
					oTable.fnUpdate(actionsHTML, nRow, 3, false);
					oTable.fnDraw();
					$('[data-toggle="tooltip"]',nRow).tooltip();					
				}
				_options.saveUpdateHandler(rowData, _callback);				
            }
        }

    };
}();

var InitiateMediaDataTable = function () {
    return {
        init: function (options) {
			var _options =  options || {};
			var $table = $('#mediadatatable');
			
			// column manager
			var COLMAN = new COLMANAGER($table, _options.access.cols);
			COLMAN.init();			
			
			// check access
			controlActionBtnsAccess($table, _options.access);
			
            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },

                "bAutoWidth": false
            });

            var isEditing = null;

            //Add New Row
            $('#mediadatatable_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '',
                    '<a href="#" class="btn btn-success shiny btn-xs btn-smll save" data-mode="new"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn shiny btn-warning btn-xs btn-smll cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;

            });

            //Call Delete Modal passing data-id
            $('#mediadatatable').on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteMediaModal').data('id', id).modal('show');
            });
			
            //Delete Row if Confirmed
            $('#delete-media-btn').click(function () {
				function deleteCallback(response){
					var nRow = $('#' + response.id);
					oTable.fnDeleteRow(nRow);
				}				
				var id = $('#deleteMediaModal').data('id');
				_options.deleteHandler(id, deleteCallback);
                $('#deleteMediaModal').modal('hide');				
				
            });

            //Cancel Editing or Adding a Row
            $('#mediadatatable').on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Edit A Row
            $('#mediadatatable').on("click", 'a.edit', function (e) {
                e.preventDefault();

                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;

                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }
            });

            //Save an Editing Row
            $('#mediadatatable').on("click", 'a.save', function (e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
                    var string = $('.val-url').val();
                    if (string.lenght != 0) {
                        if (is_valid_url(string)) {
                            if (string.match(/(^http:\/\/)/) == null) {
                                $('.val-url').val('http://' + string);
                            }
                            $('.val-url').parent().removeClass('has-error');
                            saveRow(oTable, isEditing);
                            isEditing = null;
                        } else {
                            $('.val-url').parent().addClass('has-error');
                            Notify('The url is incorrect', 'bottom-right', '5000', 'danger', 'fa-times', true);
                        }
                    }
                }
            });


            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
				$('[data-toggle="tooltip"]',nRow).tooltip();
            }

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<div class="input-group"><input type="url" class="form-control input-small url-fill val-url" value="' + aData[4] + '"></div>';
                jqTds[5].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"><i class="fa fa-times"></i> Cancel</a>';

            }

            function editAddedRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<div class="input-group"><input type="url" class="form-control input-small url-fill val-url" value="' + aData[4] + '"></div>';
                jqTds[5].innerHTML = aData[5];

            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
				
				var rowData = { 
					id: nRow.id || -1,
					name: jqInputs[0].value,
					user: jqInputs[1].value,
					pw: jqInputs[2].value,
					authkey: jqInputs[3].value,
					url: jqInputs[4].value
				}
				
				function _callback(response){
					
					var actionsHTML = "";
					if (_options.access.actions.edit){
						actionsHTML += '<a href="#" class="btn btn-palegreen shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>';
					}
					if (_options.access.actions.delete){
						actionsHTML += '<button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>';
					}					
					
					oTable.fnUpdate(response.name, nRow, 0, false);
					oTable.fnUpdate(response.user, nRow, 1, false);
					oTable.fnUpdate(response.pw, nRow, 2, false);
					oTable.fnUpdate(response.authkey, nRow, 3, false);
					oTable.fnUpdate(response.url, nRow, 4, false);
					oTable.fnUpdate(actionsHTML, nRow, 5, false);
					oTable.fnDraw();
					$('[data-toggle="tooltip"]',nRow).tooltip();					
				}
				_options.saveUpdateHandler(rowData, _callback);
            }
        }

    };
}();

var InitiateShareGeoDT = function () {
    return {
        init: function (options) {
			var _options =  options || {};
			var $table = $('#shareGeoDT');
			
			makeResponsiveTable($table);
			
			// column manager
			var COLMAN = new COLMANAGER($table, _options.access.cols);
			COLMAN.init();			
			
			// check access
			controlActionBtnsAccess($table, _options.access);
			
            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },

                "bAutoWidth": false
            });

            var isEditing = null;

            //Add New Row
            $('#shareGeoDT_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '', '', '', '', '', '', '', '', buildStatusDropDownHTML(),
                    '<a href="#" class="btn btn-success shiny btn-xs btn-smll save" data-mode="new"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn shiny btn-warning btn-xs btn-smll cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;});

            //Call Delete Modal passing data-id
            $table.on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteShareGeoModal').data('id', id).modal('show');
            });
			
            //Delete Row if Confirmed
            $('#delete-shareGeo-btn').click(function () {
				function deleteCallback(response){
					var nRow = $('#' + response.id, $table);
					oTable.fnDeleteRow(nRow);
				}				
				var id = $('#deleteShareGeoModal').data('id');
				_options.deleteHandler(id, deleteCallback);
                $('#deleteShareGeoModal').modal('hide');
            });
			
			$("#close-shareGeo").click(function(){
				$('#deleteShareGeoModal').modal('hide');
			});

            //Clone row
            $table.on("click", 'button.clone', function (e) {
                e.preventDefault();
				var id = $(this).closest('tr').attr('id');
				$('#cloneShareGeoModal').data('id', id).modal('show');
            });

            $('#ShareGeo-clone-btn').click(function () {
                var id = $('#cloneShareGeoModal').data('id');
                var anSelected = $('#'+id, $table);
                var data=[];
                $(anSelected).find('td').each(function(){data.push($(this).html());});
                var aiNew = oTable.fnAddData( data );
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                isEditing = nRow;
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
				oTable.fnGetPageOfRow(nRow);
                $('#cloneShareGeoModal').modal('hide');
            });
			
			$("#ShareGeo-clone-close").click(function(){
				$('#cloneShareGeoModal').modal('hide');
			});
			
            //Cancel Editing or Adding a Row
            $table.on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Edit A Row
            $table.on("click", 'a.edit', function (e) {
                e.preventDefault();

                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;

                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }
            });
			
			$table.on('click','a.dates', function(e){
				e.preventDefault();
				var $modal = $("#shareDatesModal"), id = $(this).closest('tr').attr('id');
				
				_options.getDatesInfo(id, function(response){
					var $tr = $('<tr></tr>');

					function insert(prop){
						if (response.hasOwnProperty(prop)){
							$tr.append('<td>'+response[prop]+'</td>');
						} else {
							$tr.append('<td></td>');
						}
					}
					
					insert('KSByGeoDateCreated');
					insert('KSByGeoDateAccepted');
					insert('KSByGeoDateRevoked');
					insert('KSByGeoRevokedBy');
					insert('KSByGeoDateLastFeeChanged');
					insert('KSByGeoDateLastFeeChangeAccepted');

					$modal.find('table tbody').empty().append($tr);
					$modal.modal("show");
				});
				
			});
			
			
			$table.on('click','a.activeshares', function(e){
				e.preventDefault();
				var $modal = $("#activeSharesModal");
				
				$modal.modal("show");
			});
			

            //Save an Editing Row
            $table.on("click", 'a.save', function (e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
					var _canSave = true;
					_check = function(erroMsg){
						var v = $input.is("input") ? $input.val() : $input.find("option:selected").val();
						if (( v === "") || (v == null) || (v < 0)) {
							_canSave = _canSave && false;
							$input.addClass("required-error");
							_Error_(erroMsg);
						} else {
							$input.removeClass("required-error");
							_canSave = _canSave && true;
						}						
					}
					
					$input = $("#sharegeo-name-inline");
					_check('The GEO name is required field!');
					
					$input = $("#sharegeo-category-inline");
					_check('The category is required field!');
					
					$input = $("#sharegeo-lat-inline");
					_check('The Latitude is required field!');
					
					$input = $("#sharegeo-lon-inline");
					_check('The Longitude is required field!');
					
					$input = $("#sharegeo-distance-inline");
					_check('The Distance is required field and can\'t be negative value!');

					$input = $("#sharegeo-fixedfee-inline");
					var $curr = $("#sharegeo-currency-inline").removeClass("required-error");
					if (($input.val().length) && ($curr.val() < 0)){
						_canSave = _canSave && false;
						$curr.addClass("required-error");
						_Error_('The Currency is required field!');
					}
					
					$input = $("#sharegeo-sharefee-inline").removeClass("required-error");
					if (($input.val().length) && (($input.val() < 0) || ($input.val() > 100))){
						_canSave = _canSave && false;
						$input.addClass("required-error");
						_Error_('The Share Fee should be between 0 and 100!');
					}
					
					$input = $("#sharegeo-sharecomm-inline").removeClass("required-error");
					if (($input.val().length) && (($input.val() < 0) || ($input.val() > 100))){
						_canSave = _canSave && false;
						$input.addClass("required-error");
						_Error_('The Share Commission should be between 0 and 100!');
					}					
					
					if (_canSave){
						saveRow(oTable, isEditing);
						isEditing = null;
					}					
					
                }
            });

            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
				$('[data-toggle="tooltip"]',nRow).tooltip();
            }

            function editRow(oTable, nRow, isNew) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
				
				if (COLMAN.haveAccess(jqTds[0])){
					jqTds[0].innerHTML = '<input id="sharegeo-name-inline" style="width:100px;padding:4px;" type="text" class="form-control input-small" value="' + aData[0] + '">';
				}
				
				if (COLMAN.haveAccess(jqTds[1])){
					_buildInlineSelect(_options.getCategories, function($select){
						selectByText($select, aData[1]);
						$select.attr({
							'style': 'width:120px!important; padding:4px;',
							'id': 'sharegeo-category-inline'
						});
						jqTds[1].innerHTML = $select[0].outerHTML;
					});					
				}				
				
				if (COLMAN.haveAccess(jqTds[2])){
					jqTds[2].innerHTML = '<input id="sharegeo-lat-inline" style="width:100px;padding:4px;" type="text" class="form-control input-small" value="' + aData[2] + '">';
				}
				
				if (COLMAN.haveAccess(jqTds[3])){
					jqTds[3].innerHTML = '<input id="sharegeo-lon-inline" style="width:100px;padding:4px;" type="text" class="form-control input-small" value="' + aData[3] + '">';
				}				
				
				//DISTANCE
				if (COLMAN.haveAccess(jqTds[4])){
					jqTds[4].innerHTML = '<input id="sharegeo-distance-inline" style="width:50px;padding:4px;" type="number" class="form-control input-small" value="' + aData[4] + '">';
				}				
				
				//UNIT
				if (COLMAN.haveAccess(jqTds[5])){
					_buildInlineSelect(_options.getUnits, function($select){
						selectByText($select, aData[5]);
						$select.attr('style','width:55px!important; padding:4px;');
						jqTds[5].innerHTML = $select[0].outerHTML;
					});
				}				
				
				if (COLMAN.haveAccess(jqTds[6])){
					jqTds[6].innerHTML = '<input id="sharegeo-fixedfee-inline" style="width:100px" type="text" class="form-control input-small" value="' + aData[6] + '">';
				}				
				
				//CURRENCY
				if (COLMAN.haveAccess(jqTds[7])){
					_buildInlineSelect(_options.getCurrencies, function($select){
						selectByText($select, aData[7]);
						$select.attr({
							'id': 'sharegeo-currency-inline',
							'style': 'width:65px!important; padding:4px;'
						});
						jqTds[7].innerHTML = $select[0].outerHTML;
					});					
				}				
				
				//SHARE FEE
				if (COLMAN.haveAccess(jqTds[8])){
					var html = $(jqTds[8]).html(),
						v = (html.length) ? parseFloat(html) : 0;
					jqTds[8].innerHTML = '<div class="input-group input-group-small"><input id="sharegeo-sharefee-inline" style="width:40px;text-align:right;" type="text" class="form-control" value="' + v + '"><span class="input-group-addon">%</span></div>';
				}				
				
				//SHARE COMMISSION
				if (COLMAN.haveAccess(jqTds[9])){
					var html = $(jqTds[9]).html(),
						v = (html.length) ? parseFloat(html) : 0;
					jqTds[9].innerHTML = '<div class="input-group input-group-small"><input id="sharegeo-sharecomm-inline" style="width:40px;text-align:right;" type="text" class="form-control" value="' + v + '"><span class="input-group-addon">%</span></div>';
				}				
				
				//ACTION DATE - READONLY
				
				//STATUS
				if (COLMAN.haveAccess(jqTds[11])){
					// status select
					var $statusSelect = $( $.parseHTML(aData[11])[0] );
					if ($statusSelect.find("select").length == 0){
						$statusSelect = buildStatusDropDown();
						$statusSelect.attr('style','width:90px!important; padding:4px;');
						$('option[value="'+aData[11]+'"]',$statusSelect).attr("selected", "selected");
					}
					jqTds[11].innerHTML = $statusSelect[0].outerHTML;
				}
                
				html = (typeof isNew != "undefined") ? ' data-mode="new"' : '';
                jqTds[12].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"'+html+'><i class="fa fa-times"></i> Cancel</a>';
            }

			function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
				editRow(oTable, nRow, true);
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
				var jqSelects = $('select', nRow);
				var jqTds = $('>td', nRow);
				
				var rowData = { KSByGeoID: nRow.id || -1 };
				
				if (COLMAN.haveAccess(jqTds[0])){
					rowData.KSByGeoName = jqInputs[0].value;
				}
				
				if (COLMAN.haveAccess(jqTds[1])){
					rowData.KSRecvCategoryID = jqSelects.eq(0).val();
					rowData.KSRecvCategoryIDOutput = jqSelects.eq(0).find('option:selected').data("text");
				}
				
				if (COLMAN.haveAccess(jqTds[2])){
					rowData.KSByGeoLat = jqInputs[1].value;
				}
				
				if (COLMAN.haveAccess(jqTds[3])){
					rowData.KSByGeoLong = jqInputs[2].value;
				}
				
				if (COLMAN.haveAccess(jqTds[4])){
					rowData.KSByGeoDistance = jqInputs[3].value;
				}				
				
				if (COLMAN.haveAccess(jqTds[5])){
					rowData.KSByGeoUnit = jqSelects.eq(1).val();
					rowData.KSByGeoUnitOutput = jqSelects.eq(1).find('option:selected').data("text");
				}
				
				if (COLMAN.haveAccess(jqTds[6])){
					rowData.KSByGeoFixedFee = jqInputs[4].value || 0;
				}
				
				if (COLMAN.haveAccess(jqTds[7])){
					rowData.KSByGeoFixedCurrency = jqSelects.eq(2).val();
					rowData.KSByGeoFixedCurrencyOutput = jqSelects.eq(2).find('option:selected').data("text");
				}				
				
				if (COLMAN.haveAccess(jqTds[8])){
					rowData.KSByGeoFeeShareFixed = jqInputs[5].value || 0;
				}					
				
				if (COLMAN.haveAccess(jqTds[9])){
					rowData.KSByGeoFeeSharePercentage = jqInputs[6].value || 0;
				}
				
				if (COLMAN.haveAccess(jqTds[11])){
					rowData.KSByGeoStatus = jqSelects.eq(3).val();
				}				
				
				
				function _callback(response){
					
					if (rowData.hasOwnProperty('KSByGeoName')){
						oTable.fnUpdate(response.KSByGeoName, nRow, 0, false);
					}

					if (rowData.hasOwnProperty('KSRecvCategoryID')){
						oTable.fnUpdate(response.KSRecvCategoryIDOutput, nRow, 1, false);
					}
					
					if (rowData.hasOwnProperty('KSByGeoLat')){
						oTable.fnUpdate(response.KSByGeoLat, nRow, 2, false);
					}
					
					if (rowData.hasOwnProperty('KSByGeoLong')){
						oTable.fnUpdate(response.KSByGeoLong, nRow, 3, false);
					}					
					
					if (rowData.hasOwnProperty('KSByGeoDistance')){
						oTable.fnUpdate(response.KSByGeoDistance, nRow, 4, false);
					}					
					
					if (rowData.hasOwnProperty('KSByGeoUnit')){
						oTable.fnUpdate(response.KSByGeoUnitOutput, nRow, 5, false);
					}					
					
					if (rowData.hasOwnProperty('KSByGeoFixedFee')){
						oTable.fnUpdate(response.KSByGeoFixedFee, nRow, 6, false);
					}					

					if (rowData.hasOwnProperty('KSByGeoFixedCurrency')){
						oTable.fnUpdate(response.KSByGeoFixedCurrencyOutput, nRow, 7, false);
					}									

					if (rowData.hasOwnProperty('KSByGeoFeeShareFixed')){
						oTable.fnUpdate(response.KSByGeoFeeShareFixed + '%', nRow, 8, false);
					}
					
					if (rowData.hasOwnProperty('KSByGeoFeeSharePercentage')){
						oTable.fnUpdate(response.KSByGeoFeeSharePercentage + '%', nRow, 9, false);
					}
					
					if (rowData.hasOwnProperty('KSByGeoStatus')){
						oTable.fnUpdate(response.KSByGeoStatus, nRow, 11, false);
					}					
					
					var actionsHTML = "";
					if (_options.access.actions.share){
						actionsHTML += '<a href="#" class="btn btn-blue shiny btn-xs activeshares tooltip-blue command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Active Shares"><i class="fa fa-share-alt"></i></a>';
					}
					
					if (_options.access.actions.dates){
						actionsHTML += '<a href="#" class="btn btn-maroon shiny btn-xs dates tooltip-maroon command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Dates"><i class="fa fa-calendar"></i></a>';
					}
					
					if (_options.access.actions.edit){
						actionsHTML += '<a href="#" class="btn btn-palegreen shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>';
					}
					
					if (_options.access.actions.clone){
						actionsHTML += '<button type="button" class="btn btn-warning shiny btn-xs clone tooltip-warning command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Clone"><i class="fa fa-copy"></i></button>';
					}
					
					if (_options.access.actions.delete){
						actionsHTML += '<button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>';
					}					
					oTable.fnUpdate(actionsHTML, nRow, 12, false);
					oTable.fnDraw();
					$('[data-toggle="tooltip"]',nRow).tooltip();					
				}
				_options.saveUpdateHandler(rowData, _callback);
            }
        }

    };
}();


var InitiateShareLocDT = function () {
    return {
        init: function (options) {
			var _options =  options || {};
			var $table = $('#shareLocDT');
			
			makeResponsiveTable($table);
			
			// column manager
			var COLMAN = new COLMANAGER($table, _options.access.cols);
			COLMAN.init();			
			
			// check access
			controlActionBtnsAccess($table, _options.access);
			
            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },

                "bAutoWidth": false
            });

            var isEditing = null;

            //Add New Row
            $('#shareLocDT_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '', '', '', '', '', '', buildStatusDropDownHTML(),
                    '<a href="#" class="btn btn-success shiny btn-xs btn-smll save" data-mode="new"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn shiny btn-warning btn-xs btn-smll cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;});

            //Call Delete Modal passing data-id
            $table.on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteShareLocModal').data('id', id).modal('show');
            });
			
            //Delete Row if Confirmed
            $('#delete-shareLoc-btn').click(function () {
				function deleteCallback(response){
					var nRow = $('#' + response.id, $table);
					oTable.fnDeleteRow(nRow);
				}				
				var id = $('#deleteShareLocModal').data('id');
				_options.deleteHandler(id, deleteCallback);
                $('#deleteShareLocModal').modal('hide');
            });
			
			$("#close-shareLoc").click(function(){
				$('#deleteShareLocModal').modal('hide');
			});

            //Clone row
            $table.on("click", 'button.clone', function (e) {
                e.preventDefault();
				var id = $(this).closest('tr').attr('id');
				$('#cloneShareLocModal').data('id', id).modal('show');
            });

            $('#shareLoc-clone-btn').click(function () {
                var id = $('#cloneShareLocModal').data('id');
                var anSelected = $('#'+id, $table);
                var data=[];
                $(anSelected).find('td').each(function(){data.push($(this).html());});
                var aiNew = oTable.fnAddData( data );
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                isEditing = nRow;
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
				oTable.fnGetPageOfRow(nRow);
                $('#cloneShareLocModal').modal('hide');
            });
			
			$("#shareLoc-clone-close").click(function(){
				$('#cloneShareLocModal').modal('hide');
			});
			
			var timer = 0;
			$("#sharesLocationsModal-live").keyup(function(){
				var v = $(this).val().toLowerCase();
				clearTimeout(timer);
				timer = setTimeout(function(){
					var $modal = $("#sharesLocationsModal"), $list = $modal.find('#sharesLocationsModal-list');
					
					$list.find('li').removeClass('invi');
					$list.find('li').filter(function(i, li){
						var match = $(li).find('span.text').text().toLowerCase();
						if (match.indexOf(v) == -1){
							$(li).addClass('invi');
						}
					});
					
				}, 50);
			});
			
			$("#sharesLocationsModal-cancel").click(function(){
				$("#sharesLocationsModal").modal("hide");
			});
			
            //Cancel Editing or Adding a Row
            $table.on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Edit A Row
            $table.on("click", 'a.edit', function (e) {
                e.preventDefault();

                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;

                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }
            });
			
			$table.delegate('a.locations','click', function(e){
				e.preventDefault();
				var $this = $(this), array = $this.data("array") || [], $modal = $("#sharesLocationsModal"), $tr = $this.closest('tr');
				var modalLocType = $tr.find("#shareloc-loctype-inline option:selected").data("text");
				
				$modal.find('.modal-title span').text(modalLocType);
				$modal.find('.modal-body ul').empty();
				
				_options.getAllLocations(function(response){
					var arr = response || [];
					$.each(arr, function(i, location){
						var $li = $('<li><div class="checkbox"><label><input data-id="'+location.id+'" type="checkbox" class="colored-blue"><span class="text">'+location.text+'</span></label></div></li>');
						if ($.inArray(location.id, array) > -1){
							$li.find('input').attr("checked","checked");
						}
						$modal.find('.modal-body ul').append($li);
					});
					
				});
				
				$modal.find("#sharesLocationsModal-OK").one('click', function(){
					var $list = $("#sharesLocationsModal").find('#sharesLocationsModal-list');
					var array = [];
					$list.find('input[type="checkbox"]:checked').each(function(i, checkbox){
						array.push($(checkbox).data("id"));
					});
					$this.data("array", array);
					$modal.modal("hide");
				});				
				
				$modal.modal("show");
			});
			
			$table.on('click','a.dates', function(e){
				e.preventDefault();
				var $modal = $("#shareDatesModal"), id = $(this).closest('tr').attr('id');
				
				_options.getDatesInfo(id, function(response){
					var $tr = $('<tr></tr>');

					function insert(prop){
						if (response.hasOwnProperty(prop)){
							$tr.append('<td>'+response[prop]+'</td>');
						} else {
							$tr.append('<td></td>');
						}
					}
					
					insert('KSByLocDateCreated');
					insert('KSByLocDateAccepted');
					insert('KSByLocDateRevoked');
					insert('KSByLocRevokedBy');
					insert('KSByLocLastFeeChanged');
					insert('KSByLocDateLastFeeChangeAccepted');

					$modal.find('table tbody').empty().append($tr);
					$modal.modal("show");
				});
				
			});
			
			
			$table.on('click','a.activeshares', function(e){
				e.preventDefault();
				var $modal = $("#activeSharesModal");
				
				$modal.modal("show");
			});
			

            //Save an Editing Row
            $table.on("click", 'a.save', function (e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
					var _canSave = true;
					_check = function(erroMsg){
						var v = $input.is("input") ? $input.val() : $input.find("option:selected").val();
						if (( v === "") || (v == null) || (v < 0)) {
							_canSave = _canSave && false;
							$input.addClass("required-error");
							_Error_(erroMsg);
						} else {
							$input.removeClass("required-error");
							_canSave = _canSave && true;
						}						
					}
					
					$input = $("#shareloc-name-inline");
					_check('The GEO name is required field!');
					
					$input = $("#shareloc-category-inline");
					_check('The category is required field!');

					$input = $("#shareloc-fixedfee-inline");
					var $curr = $("#shareloc-currency-inline").removeClass("required-error");
					if (($input.val().length) && ($curr.val() < 0)){
						_canSave = _canSave && false;
						$curr.addClass("required-error");
						_Error_('The Currency is required field!');
					}
					
					$input = $("#shareloc-sharefee-inline").removeClass("required-error");
					if (($input.val().length) && (($input.val() < 0) || ($input.val() > 100))){
						_canSave = _canSave && false;
						$input.addClass("required-error");
						_Error_('The Share Fee should be between 0 and 100!');
					}
					
					$input = $("#shareloc-sharecomm-inline").removeClass("required-error");
					if (($input.val().length) && (($input.val() < 0) || ($input.val() > 100))){
						_canSave = _canSave && false;
						$input.addClass("required-error");
						_Error_('The Share Commission should be between 0 and 100!');
					}					
					
					if (_canSave){
						saveRow(oTable, isEditing);
						isEditing = null;
					}					
					
                }
            });

            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
				$('[data-toggle="tooltip"]',nRow).tooltip();
            }

            function editRow(oTable, nRow, isNew) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
				
				//LOC NAME
				if (COLMAN.haveAccess(jqTds[0])){
					jqTds[0].innerHTML = '<input id="shareloc-name-inline" style="width:100px;padding:4px;" type="text" class="form-control input-small" value="' + aData[0] + '">';
				}
				
				//CATEGORY ID
				if (COLMAN.haveAccess(jqTds[1])){
					_buildInlineSelect(_options.getCategories, function($select){
						selectByText($select, aData[1]);
						$select.attr({
							'style': 'width:120px!important; padding:4px;',
							'id': 'shareloc-category-inline'
						});
						jqTds[1].innerHTML = $select[0].outerHTML;
					});					
				}				
				
				//LOCATION TYPE
				if (COLMAN.haveAccess(jqTds[2])){
					_buildInlineSelect(_options.getLocTypes, function($select){
						selectByText($select, aData[2]);
						$select.attr({
							'id': 'shareloc-loctype-inline',
							'style': 'width:90px!important; padding:4px;'
						});
						jqTds[2].innerHTML = $select[0].outerHTML;
					});						
				}
				
				// LOCATIONS
				if (COLMAN.haveAccess(jqTds[3])){
					jqTds[3].innerHTML = '<a href="#" data-array="'+aData[3]+'" class="btn btn-azure shiny btn-xs locations tooltip-azure command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Locations"><i class="fa fa-map-marker"></i></a>';
					$(jqTds[3]).css("text-align","center");					
				}				
				
				//FIXED FEE
				if (COLMAN.haveAccess(jqTds[4])){
					jqTds[4].innerHTML = '<input id="shareloc-fixedfee-inline" style="width:100px" type="text" class="form-control input-small" value="' + aData[4] + '">';
				}				
				
				//CURRENCY
				if (COLMAN.haveAccess(jqTds[5])){
					_buildInlineSelect(_options.getCurrencies, function($select){
						selectByText($select, aData[5]);
						$select.attr({
							'id': 'shareloc-currency-inline',
							'style': 'width:65px!important; padding:4px;'
						});
						jqTds[5].innerHTML = $select[0].outerHTML;
					});					
				}				
				
				//SHARE FEE
				if (COLMAN.haveAccess(jqTds[6])){
					var html = $(jqTds[6]).html(),
						v = (html.length) ? parseFloat(html) : 0;
					jqTds[6].innerHTML = '<div class="input-group input-group-small"><input id="shareloc-sharefee-inline" style="width:40px;text-align:right;" type="text" class="form-control" value="' + v + '"><span class="input-group-addon">%</span></div>';
				}				
				
				//SHARE COMMISSION
				if (COLMAN.haveAccess(jqTds[7])){
					var html = $(jqTds[7]).html(),
						v = (html.length) ? parseFloat(html) : 0;
					jqTds[7].innerHTML = '<div class="input-group input-group-small"><input id="shareloc-sharecomm-inline" style="width:40px;text-align:right;" type="text" class="form-control" value="' + v + '"><span class="input-group-addon">%</span></div>';
				}				
				
				//ACTION DATE - READONLY
				
				//STATUS
				if (COLMAN.haveAccess(jqTds[9])){
					// status select
					var $statusSelect = $( $.parseHTML(aData[9])[0] );
					if ($statusSelect.find("select").length == 0){
						$statusSelect = buildStatusDropDown();
						$statusSelect.attr('style','width:90px!important; padding:4px;');
						$('option[value="'+aData[9]+'"]',$statusSelect).attr("selected", "selected");
					}
					jqTds[9].innerHTML = $statusSelect[0].outerHTML;
				}
                
				html = (typeof isNew != "undefined") ? ' data-mode="new"' : '';
                jqTds[10].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"'+html+'><i class="fa fa-times"></i> Cancel</a>';
				
				$(nRow).find('[data-toggle="tooltip"]').tooltip();
            }

			function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
				editRow(oTable, nRow, true);
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
				var jqSelects = $('select', nRow);
				var jqTds = $('>td', nRow);
				
				var rowData = { KSByLocID: nRow.id || -1 };
				
				if (COLMAN.haveAccess(jqTds[0])){
					rowData.KSByLocName = jqInputs[0].value; /*FIELD NOT EXISTS IN TABLE ? */
				}
				
				if (COLMAN.haveAccess(jqTds[1])){
					rowData.KSRecvCategoryID = jqSelects.eq(0).val();
					rowData.KSRecvCategoryIDOutput = jqSelects.eq(0).find('option:selected').data("text");
				}
				
				if (COLMAN.haveAccess(jqTds[2])){
					rowData.KSLocTypeID = jqSelects.eq(1).val();
					rowData.KSLocTypeIDOutput = jqSelects.eq(1).find('option:selected').data("text");
				}
				
				// LOCATIONS
				if (COLMAN.haveAccess(jqTds[3])){
					var $link = $('a.locations', nRow);
					rowData.KSArrayLocID = $link.data("array");
				}
				
				if (COLMAN.haveAccess(jqTds[4])){					
					rowData.KSByLocRecvFixedFee = jqInputs[1].value || 0;
				}
				
				if (COLMAN.haveAccess(jqTds[5])){
					rowData.KSByLocFixedCurrency = jqSelects.eq(2).val();
					rowData.KSByLocFixedCurrencyOutput = jqSelects.eq(2).find('option:selected').data("text");
				}				
				
				if (COLMAN.haveAccess(jqTds[6])){
					rowData.KSByLocFeeShareFixed = jqInputs[2].value || 0;
				}					
				
				if (COLMAN.haveAccess(jqTds[7])){
					rowData.KSByLocFeeSharePercentage = jqInputs[3].value || 0;
				}
				
				if (COLMAN.haveAccess(jqTds[9])){
					rowData.KSByLocStatus = jqSelects.eq(3).val();
				}				
				
				
				function _callback(response){
					
					if (rowData.hasOwnProperty('KSByLocName')){
						oTable.fnUpdate(response.KSByLocName, nRow, 0, false);
					}

					if (rowData.hasOwnProperty('KSRecvCategoryID')){
						oTable.fnUpdate(response.KSRecvCategoryIDOutput, nRow, 1, false);
					}
					
					if (rowData.hasOwnProperty('KSLocTypeID')){
						oTable.fnUpdate(response.KSLocTypeIDOutput, nRow, 2, false);
					}
					
					if (rowData.hasOwnProperty('KSArrayLocID')){
						var str = '[' + response.KSArrayLocID.join(',') + ']';
						oTable.fnUpdate(str, nRow, 3, false);
					}					
					
					if (rowData.hasOwnProperty('KSByLocRecvFixedFee')){
						oTable.fnUpdate(response.KSByLocRecvFixedFee, nRow, 4, false);
					}					
					
					if (rowData.hasOwnProperty('KSByLocFixedCurrency')){
						oTable.fnUpdate(response.KSByLocFixedCurrencyOutput, nRow,5, false);
					}					
					
					if (rowData.hasOwnProperty('KSByLocFeeShareFixed')){
						oTable.fnUpdate(response.KSByLocFeeShareFixed + '%', nRow, 6, false);
					}
					
					if (rowData.hasOwnProperty('KSByLocFeeSharePercentage')){
						oTable.fnUpdate(response.KSByLocFeeSharePercentage + '%', nRow, 7, false);
					}
					
					if (rowData.hasOwnProperty('KSByLocStatus')){
						oTable.fnUpdate(response.KSByLocStatus, nRow, 9, false);
					}					
					
					var actionsHTML = "";
					if (_options.access.actions.share){
						actionsHTML += '<a href="#" class="btn btn-blue shiny btn-xs activeshares tooltip-blue command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Active Shares"><i class="fa fa-share-alt"></i></a>';
					}
					
					if (_options.access.actions.dates){
						actionsHTML += '<a href="#" class="btn btn-maroon shiny btn-xs dates tooltip-maroon command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Dates"><i class="fa fa-calendar"></i></a>';
					}
					
					if (_options.access.actions.edit){
						actionsHTML += '<a href="#" class="btn btn-palegreen shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>';
					}
					
					if (_options.access.actions.clone){
						actionsHTML += '<button type="button" class="btn btn-warning shiny btn-xs clone tooltip-warning command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Clone"><i class="fa fa-copy"></i></button>';
					}
					
					if (_options.access.actions.delete){
						actionsHTML += '<button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>';
					}					
					oTable.fnUpdate(actionsHTML, nRow, 10, false);
					oTable.fnDraw();
					$('[data-toggle="tooltip"]',nRow).tooltip();					
				}
				_options.saveUpdateHandler(rowData, _callback);
            }
        }

    };
}();

var InitiateShareAgDT = function () {
    return {
        init: function (options) {
			var _options =  options || {};
			var $table = $('#shareAggDT');
			
			makeResponsiveTable($table);
			
			// column manager
			var COLMAN = new COLMANAGER($table, _options.access.cols);
			COLMAN.init();			
			
			// check access
			controlActionBtnsAccess($table, _options.access);
			
            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },

                "bAutoWidth": false
            });

            var isEditing = null;

            //Add New Row
            $('#shareAggDT_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '', '', '', '', '', '', '', '', '', buildStatusDropDownHTML(),
                    '<a href="#" class="btn btn-success shiny btn-xs btn-smll save" data-mode="new"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn shiny btn-warning btn-xs btn-smll cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;});

            //Call Delete Modal passing data-id
            $table.on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteShareAgModal').data('id', id).modal('show');
            });
			
            //Delete Row if Confirmed
            $('#delete-shareAg-btn').click(function () {
				function deleteCallback(response){
					var nRow = $('#' + response.id, $table);
					oTable.fnDeleteRow(nRow);
				}				
				var id = $('#deleteShareAgModal').data('id');
				_options.deleteHandler(id, deleteCallback);
                $('#deleteShareAgModal').modal('hide');
            });
			
			$("#close-shareAg").click(function(){
				$('#deleteShareAgModal').modal('hide');
			});

            //Clone row
            $table.on("click", 'button.clone', function (e) {
                e.preventDefault();
				var id = $(this).closest('tr').attr('id');
				$('#cloneShareAgModal').data('id', id).modal('show');
            });

            $('#shareAg-clone-btn').click(function () {
                var id = $('#cloneShareAgModal').data('id');
                var anSelected = $('#'+id, $table);
                var data=[];
                $(anSelected).find('td').each(function(){data.push($(this).html());});
                var aiNew = oTable.fnAddData( data );
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                isEditing = nRow;
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
				oTable.fnGetPageOfRow(nRow);
                $('#cloneShareAgModal').modal('hide');
            });
			
			$("#shareAg-clone-close").click(function(){
				$('#cloneShareAgModal').modal('hide');
			});
			
            //Cancel Editing or Adding a Row
            $table.on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Edit A Row
            $table.on("click", 'a.edit', function (e) {
                e.preventDefault();

                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;

                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }
            });
			
			$table.on('click','a.dates', function(e){
				e.preventDefault();
				var $modal = $("#shareDatesModal"), id = $(this).closest('tr').attr('id');
				
				_options.getDatesInfo(id, function(response){
					var $tr = $('<tr></tr>');

					function insert(prop){
						if (response.hasOwnProperty(prop)){
							$tr.append('<td>'+response[prop]+'</td>');
						} else {
							$tr.append('<td></td>');
						}
					}
					
					insert('KSShareDateCreated');
					insert('KSShareDateAccepted');
					insert('KSShareDateRevoked');
					insert('KSShareRevokedBy');
					insert('KSShareDateLastFeeChanged');
					insert('KSShareDateLastFeeChangeAccepted');

					$modal.find('table tbody').empty().append($tr);
					$modal.modal("show");
				});
				
			});
			
			
			$table.on('click','a.activeshares', function(e){
				e.preventDefault();
				var $modal = $("#activeSharesModal");
				
				$modal.modal("show");
			});
			

            //Save an Editing Row
            $table.on("click", 'a.save', function (e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
					var _canSave = true;
					_check = function(erroMsg){
						var v = $input.is("input") ? $input.val() : $input.find("option:selected").val();
						if (( v === "") || (v == null) || (v < 0)) {
							_canSave = _canSave && false;
							$input.addClass("required-error");
							_Error_(erroMsg);
						} else {
							$input.removeClass("required-error");
							_canSave = _canSave && true;
						}						
					}
					
					/**VALIDATTIONS HERE**/
					$input = $("#shareag-origsharefee-inline").removeClass("required-error");
					if (($input.val().length) && (($input.val() < 0) || ($input.val() > 100))){
						_canSave = _canSave && false;
						$input.addClass("required-error");
						_Error_('The Originating Percentage Fee should be between 0 and 100!');
					}
					
					$input = $("#shareag-sharefeeperc-inline").removeClass("required-error");
					if (($input.val().length) && (($input.val() < 0) || ($input.val() > 100))){
						_canSave = _canSave && false;
						$input.addClass("required-error");
						_Error_('The Fee Share Percentage should be between 0 and 100!');
					}	
					
					if (_canSave){
						saveRow(oTable, isEditing);
						isEditing = null;
					}					
					
                }
            });

            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
				$('[data-toggle="tooltip"]',nRow).tooltip();
            }

			function _buildInlineSelect(fnString, callback, param, unique){
				var $select = $("<select id='"+unique+"' class='form-control full-width'></select>");
				$select.append('<option value="-1">Select</option>')
				function _callback(data){					
					var arr = data.data || [];					
					$.each(arr, function(i,obj){
						$op = $("<option value='"+obj.id+"'>"+obj.name+"</option>");
						$select.append($op);
					});
					callback($select);
				}

				_options[fnString](function(response){
					_callback(response);
				}, param);
			}
			
			function populateCategoryDrop(tdSite, tdCategory, unique){
				var $tdSite = $(tdSite), 
					$tdCategory = $(tdCategory),
					siteId = $tdSite.data("value");
				
				_buildInlineSelect('getCategoriesDataHandler', function($select){
					var currV = $tdCategory.data("value");
					currV = (typeof currV != "undefined") ? currV : -1;
					
					$select.change(function(e){
						var v = $(this).val();
						$select.parent().data("value", v);
					});
					
					$('option[value="'+currV+'"]',$select).attr("selected", "selected");	
					
					$tdCategory.empty().append($select);
				}, { siteID: siteId }, unique);
			}
			
            function editRow(oTable, nRow, isNew) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
				
				//Originating Server
				if (COLMAN.haveAccess(jqTds[0])){
					jqTds[0].innerHTML = '<input style="width:100px;padding:4px;" type="text" class="form-control input-small" value="' + aData[0] + '">';
				}
				
				//Receiving Server
				if (COLMAN.haveAccess(jqTds[1])){
					jqTds[1].innerHTML = '<input style="width:100px;padding:4px;" type="text" class="form-control input-small" value="' + aData[1] + '">';
				}
				
				//Originating Site
				if (COLMAN.haveAccess(jqTds[2])){
					_buildInlineSelect('getSiteDataHandler', function($select){
						var $td = $(jqTds[2]), currV = $td.data("value");
						currV = (typeof currV != "undefined") ? currV : -1;
						$select.change(function(e){
							var v = $(this).val();
							$select.parent().data("value", v);
							
							// ATTACH CHANGE EVENT ONLY WHEN CATEGORY IS VISIBLE & EDITABLE
							if (COLMAN.haveAccess(jqTds[4])){
								populateCategoryDrop(jqTds[2],jqTds[4], 'shareAgg-orig-cat-inline');
							}
							
						});
						
						$('option[value="'+currV+'"]',$select).attr("selected", "selected");	
						$td.empty().append($select);
					},{},'shareAgg-orig-site-inline');					
				}			
				
				//Receiving Site
				if (COLMAN.haveAccess(jqTds[3])){
					_buildInlineSelect('getSiteDataHandler', function($select){
						var $td = $(jqTds[3]), currV = $td.data("value");
						currV = (typeof currV != "undefined") ? currV : -1;
						$select.change(function(e){
							var v = $(this).val();
							$select.parent().data("value", v);
							
							// ATTACH CHANGE EVENT ONLY WHEN CATEGORY IS VISIBLE & EDITABLE
							if (COLMAN.haveAccess(jqTds[5])){
								populateCategoryDrop(jqTds[3],jqTds[5], 'shareAgg-receiving-cat-inline');
							}
							
						});
						
						$('option[value="'+currV+'"]',$select).attr("selected", "selected");	
						$td.empty().append($select);
					},{},'shareAgg-receiving-site-inline');						
				}	

				//Originating Category
				if (COLMAN.haveAccess(jqTds[4])){
					populateCategoryDrop(jqTds[2],jqTds[4]);
				}					
				
				//Receiving Category
				if (COLMAN.haveAccess(jqTds[5])){
					populateCategoryDrop(jqTds[3],jqTds[5]);
				}	
				
				//VRID
				if (COLMAN.haveAccess(jqTds[6])){
					jqTds[6].innerHTML = '<input style="width:100px;padding:4px;" type="text" class="form-control input-small" value="' + aData[6] + '">';
				}					

				//Originating Fixed Fee
				if (COLMAN.haveAccess(jqTds[7])){
					jqTds[7].innerHTML = '<input style="width:100px;padding:4px;" type="number" min="0" class="form-control input-small" value="' + aData[7] + '">';
				}					
				
				//Originating Percentage Fee
				if (COLMAN.haveAccess(jqTds[8])){
					var html = $(jqTds[8]).html(),
						v = (html.length) ? parseFloat(html) : 0;
					jqTds[8].innerHTML = '<div class="input-group input-group-small" style="width:65px"><input id="shareag-origsharefee-inline" style="width:40px;text-align:right;" type="text" class="form-control" value="' + v + '"><span class="input-group-addon">%</span></div>';
				}		

				//Fee Share Fixed
				if (COLMAN.haveAccess(jqTds[9])){
					jqTds[9].innerHTML = '<input style="width:100px" min="0" type="number" class="form-control input-small" value="' + aData[9] + '">';
				}					

				//Fee Share Percentage
				if (COLMAN.haveAccess(jqTds[10])){
					var html = $(jqTds[10]).html(),
						v = (html.length) ? parseFloat(html) : 0;
					jqTds[10].innerHTML = '<div class="input-group input-group-small" style="width:65px"><input id="shareag-sharefeeperc-inline" style="width:40px;text-align:right;" type="text" class="form-control" value="' + v + '"><span class="input-group-addon">%</span></div>';
				}		
				
				//Receiving Fixed Fee
				if (COLMAN.haveAccess(jqTds[11])){
					jqTds[11].innerHTML = '<input style="width:100px" min="0" type="number" class="form-control input-small" value="' + aData[11] + '">';
				}					
				
				//STATUS
				if (COLMAN.haveAccess(jqTds[12])){
					// status select
					var $statusSelect = $( $.parseHTML(aData[12])[0] );
					if ($statusSelect.find("select").length == 0){
						$statusSelect = buildStatusDropDown();
						$statusSelect.attr('style','width:90px!important; padding:4px;');
						$('option[value="'+aData[12]+'"]',$statusSelect).attr("selected", "selected");
					}
					jqTds[12].innerHTML = $statusSelect[0].outerHTML;
				}
                
				html = (typeof isNew != "undefined") ? ' data-mode="new"' : '';
                jqTds[13].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"'+html+'><i class="fa fa-times"></i> Cancel</a>';
				
				$(nRow).find('[data-toggle="tooltip"]').tooltip();
            }

			function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
				editRow(oTable, nRow, true);
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
				var jqSelects = $('select', nRow);
				var jqTds = $('>td', nRow);
				
				var rowData = { KSShareID: nRow.id || -1 };
				
				if (COLMAN.haveAccess(jqTds[0])){
					rowData.KSShareOrigServerID = jqInputs[0].value; /*FIELD NOT EXISTS IN TABLE ? */
				}
				
				if (COLMAN.haveAccess(jqTds[1])){
					rowData.KSShareRecvServerID = jqInputs[1].value;
				}
				
				if (COLMAN.haveAccess(jqTds[2])){
					rowData.KSShareOrigSiteID = jqInputs[2].value;
				}
				
				if (COLMAN.haveAccess(jqTds[3])){
					rowData.KSShareRecvSiteID = jqInputs[3].value;
				}
				
				if (COLMAN.haveAccess(jqTds[4])){
					rowData.KSShareOrigCategoryID = jqInputs[4].value;
				}			

				if (COLMAN.haveAccess(jqTds[5])){
					rowData.KSShareRecvCategoryID = jqInputs[5].value;
				}	
				
				if (COLMAN.haveAccess(jqTds[6])){
					rowData.KSShareVRID = jqInputs[6].value;
				}			

				if (COLMAN.haveAccess(jqTds[7])){
					rowData.KSShareOrigFixedFee = jqInputs[7].value || 0;
				}	
				
				if (COLMAN.haveAccess(jqTds[8])){
					rowData.KSShareOrigPercentageFee = jqInputs[8].value || 0;
				}				
				
				if (COLMAN.haveAccess(jqTds[9])){
					rowData.KSShareFeeShareFixed = jqInputs[9].value || 0;
				}				
				
				if (COLMAN.haveAccess(jqTds[10])){
					rowData.KSShareFeeSharePercentage = jqInputs[10].value || 0;
				}				
				
				if (COLMAN.haveAccess(jqTds[11])){
					rowData.KSShareRecvFixedFee = jqInputs[1].value || 0;
				}								

				if (COLMAN.haveAccess(jqTds[12])){
					rowData.KSShareActive = jqSelects.eq(0).val();
				}					
				
				function _callback(response){
					
					if (rowData.hasOwnProperty('KSShareOrigServerID')){
						oTable.fnUpdate(response.KSShareOrigServerID, nRow, 0, false);
					}

					if (rowData.hasOwnProperty('KSShareRecvServerID')){
						oTable.fnUpdate(response.KSShareRecvServerID, nRow, 1, false);
					}
					
					if (rowData.hasOwnProperty('KSShareOrigSiteID')){
						oTable.fnUpdate(response.KSShareOrigSiteID, nRow, 2, false);
					}
					
					if (rowData.hasOwnProperty('KSShareRecvSiteID')){
						oTable.fnUpdate(response.KSShareRecvSiteID, nRow, 3, false);
					}					
					
					if (rowData.hasOwnProperty('KSShareOrigCategoryID')){
						oTable.fnUpdate(response.KSShareOrigCategoryID, nRow, 4, false);
					}					
					
					if (rowData.hasOwnProperty('KSShareRecvCategoryID')){
						oTable.fnUpdate(response.KSShareRecvCategoryID, nRow,5, false);
					}					
					
					if (rowData.hasOwnProperty('KSShareVRID')){
						oTable.fnUpdate(response.KSShareVRID, nRow, 6, false);
					}
					
					if (rowData.hasOwnProperty('KSShareOrigFixedFee')){
						oTable.fnUpdate(response.KSShareOrigFixedFee, nRow, 7, false);
					}
					
					if (rowData.hasOwnProperty('KSShareOrigPercentageFee')){
						oTable.fnUpdate(response.KSShareOrigPercentageFee + '%', nRow, 8, false);
					}
					
					if (rowData.hasOwnProperty('KSShareFeeShareFixed')){
						oTable.fnUpdate(response.KSShareFeeShareFixed, nRow, 9, false);
					}
					
					if (rowData.hasOwnProperty('KSShareFeeSharePercentage')){
						oTable.fnUpdate(response.KSShareFeeSharePercentage + '%', nRow, 10, false);
					}					
					
					if (rowData.hasOwnProperty('KSShareRecvFixedFee')){
						oTable.fnUpdate(response.KSShareRecvFixedFee, nRow, 11, false);
					}

					if (rowData.hasOwnProperty('KSShareActive')){
						oTable.fnUpdate(response.KSShareActive, nRow, 12, false);
					}
					
					var actionsHTML = "";
					if (_options.access.actions.share){
						actionsHTML += '<a href="#" class="btn btn-blue shiny btn-xs activeshares tooltip-blue command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Active Shares"><i class="fa fa-share-alt"></i></a>';
					}
					
					if (_options.access.actions.dates){
						actionsHTML += '<a href="#" class="btn btn-maroon shiny btn-xs dates tooltip-maroon command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Dates"><i class="fa fa-calendar"></i></a>';
					}
					
					if (_options.access.actions.edit){
						actionsHTML += '<a href="#" class="btn btn-palegreen shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>';
					}
					
					if (_options.access.actions.clone){
						actionsHTML += '<button type="button" class="btn btn-warning shiny btn-xs clone tooltip-warning command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Clone"><i class="fa fa-copy"></i></button>';
					}
					
					if (_options.access.actions.delete){
						actionsHTML += '<button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>';
					}					
					oTable.fnUpdate(actionsHTML, nRow, 13, false);
					oTable.fnDraw();
					$('[data-toggle="tooltip"]',nRow).tooltip();					
				}
				_options.saveUpdateHandler(rowData, _callback);
            }
        }

    };
}();

$(document).ready(function() {

	// MAIN
	var mainOptions = {
		access: {
			actions: { open: true, edit: true, clone: true, delete: true, new: true },
			cols: { "name": "11", "description": "11", "status": "11", "actions": "1" }
		},
		saveUpdateHandler: function(data, callback){
			var response = data; //DEMO
			console.warn(response);
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}
		},
		deleteHandler: function(id, callback){
			var response = { "id" : id }; //DEMO
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}			
		}		
	}
	InitMainDataTable.init(mainOptions);
	
	// EMAIL TABLE
	var emailOptions = {
		access: {
			actions: { edit: true, delete: true, new: true },
			cols: { "email": "11", "pw": "11", "host": "11", "actions": "1" }
		},		
		saveUpdateHandler: function(data, callback){
			var response = data; //DEMO
			console.warn(response);
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}
		},
		deleteHandler: function(id, callback){
			var response = { "id" : id }; //DEMO
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}			
		}		
	}
	InitiateEmailDataTable.init(emailOptions);
	
	// PHONE TABLE
	var phoneOptions = {
		access: {
			actions: { edit: true, delete: true, new: true },
			cols: { "id": "11", "token": "11", "phone": "11", "actions": "1" }
		},		
		saveUpdateHandler: function(data, callback){
			var response = data; //DEMO
			console.warn(response);
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}
		},
		deleteHandler: function(id, callback){
			var response = { "id" : id }; //DEMO
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}			
		}		
	}	
	InitiatePhoneDataTable.init(phoneOptions);
	
	// MEDIA TABLE
	var mediaOptions = {
		access: {
			actions: { edit: true, delete: true, new: true },
			cols: { "name": "11", "user": "11", "pw": "11", "authkey": "11", "url": "11", "actions": "1" }
		},		
		saveUpdateHandler: function(data, callback){
			var response = data; //DEMO
			console.warn(response);
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}
		},
		deleteHandler: function(id, callback){
			var response = { "id" : id }; //DEMO
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}			
		}		
	}		
	InitiateMediaDataTable.init(mediaOptions);

	// SHARE GEO
	var shareGeoOptions = {
		access: {
			actions: {share: true, dates: true, edit: true, clone: true, delete: true, new: true },
			cols: { 
				"geoname": "11", 
				"category": "11", 
				"latitude": "11", 
				"longitude": "11", 
				"distance": "11", 
				"unit": "11", 
				"fixedFee": "11", 
				"curr": "11", 
				"shareFee": "11", 
				"shareCommission": "11", 
				"actionDate": "11", 
				"status": "11", 
				"actions": "1" 
			}
		},		
		saveUpdateHandler: function(data, callback){
			var response = data; //DEMO
			console.warn(response);
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}
		},
		deleteHandler: function(id, callback){
			var response = { "id" : id }; //DEMO
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}			
		},
		getCategories: function(callback){
			var response = [{id: -1, name: 'Select'}, {id: 1, name: 'Category A'}, {id: 2, name: 'Category B'}]; //DEMO
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}			
		},
		getUnits: function(callback){
			var response = [{id: 1, name: 'km'}, {id: 2, name: 'mi'}]; //DEMO
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}			
		},
		getCurrencies: function(callback){
			var response = [{id: -1, name: '-'}, {id: 1, name: 'EUR'}, {id: 2, name: 'USD'}]; //DEMO
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}
		},
		getDatesInfo: function(id, callback){
			var response = {
				KSByGeoDateLastFeeChanged: "12/15/2015",
				KSByGeoDateLastFeeChangeAccepted: "12/15/2015",
				KSByGeoDateCreated: "12/15/2015",
				KSByGeoDateAccepted: "12/15/2015",
				KSByGeoDateRevoked: "12/15/2015",
				KSByGeoRevokedBy: "Alex"
			}; //DEMO
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}			
		}
	}		
	InitiateShareGeoDT.init(shareGeoOptions);
	
	// SHARE LOCATIONS
	var shareLocOptions = {
		access: {
			actions: {share: true, dates: true, edit: true, clone: true, delete: true, new: true },
			cols: { 
				"locname": "11", 
				"categoryId": "11", 
				"loctype": "11", 
				"locations": "11", 
				"fixedFee": "11", 
				"curr": "11", 
				"shareFee": "11", 
				"shareCommission": "11", 
				"actionDate": "11", 
				"status": "11", 
				"actions": "1" 
			}
		},		
		saveUpdateHandler: function(data, callback){
			var response = data; //DEMO
			console.warn(response);
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}
		},
		deleteHandler: function(id, callback){
			var response = { "id" : id }; //DEMO
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}			
		},
		getCategories: function(callback){
			var response = [{id: -1, name: 'Select'}, {id: 1, name: 'Category A'}, {id: 2, name: 'Category B'}]; //DEMO
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}			
		},
		getCurrencies: function(callback){
			var response = [{id: -1, name: '-'}, {id: 1, name: 'EUR'}, {id: 2, name: 'USD'}]; //DEMO
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}
		},
		getLocTypes: function(callback){
			var response = [{KSLocTypeID: 1, KSLocTable: 'WorldRegion'}, {KSLocTypeID: 2, KSLocTable: 'Country'}, {KSLocTypeID: 3, KSLocTable: 'CountryRegion'}]; //DEMO
			
			var mappedArr = $.map(response, function(obj){
				return { id: obj.KSLocTypeID, name: obj.KSLocTable }
			});
			
			var result = $.merge([{id: -1, name: 'Select'}], mappedArr);
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(result);
			}			
		},		
		getDatesInfo: function(id, callback){
			var response = {
				KSByLocLastFeeChanged: "12/15/2015",
				KSByLocDateLastFeeChangeAccepted: "12/15/2015",
				KSByLocDateCreated: "12/15/2015",
				KSByLocDateAccepted: "12/15/2015",
				KSByLocDateRevoked: "12/15/2015",
				KSByLocRevokedBy: "Alex"
			}; //DEMO
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}			
		},
		getAllLocations: function(callback){
			var response = [
				{ id: 1, text: "Location A"},
				{ id: 2, text: "Location B"},
				{ id: 3, text: "Location C"},
				{ id: 4, text: "Location D"},
				{ id: 5, text: "Location E"},
				{ id: 6, text: "AA"},
				{ id: 7, text: "XX"},
				{ id: 8, text: "ZZZ"},
				{ id: 9, text: "AAXX"}
			]; //DEMO
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}				
		}
	}		
	InitiateShareLocDT.init(shareLocOptions);

	// SHARE AGREEMENTS
	var shareAgOptions = {
		access: {
			actions: {share: true, dates: true, edit: true, clone: true, delete: true, new: true },
			cols: {
				"origServer": "11", 
				"recServer": "11", 
				"origSite": "11", 
				"recSite": "11", 
				"origCat": "11", 
				"recCat": "11", 
				"vrid": "11", 
				"origFixedFee": "11", 
				"origPercFee": "11", 
				"feeSharedFixed": "11", 
				"feeSharedPerc": "11", 
				"recFixedFee": "11", 
				"status": "11", 
				"actions": "1" 
			}
		},		
		saveUpdateHandler: function(data, callback){
			var response = data; //DEMO
			console.warn(response);
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}
		},
		deleteHandler: function(id, callback){
			var response = { "id" : id }; //DEMO
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}			
		},
		getDatesInfo: function(id, callback){
			var response = {
				KSShareDateLastFeeChanged: "12/15/2015",
				KSShareDateLastFeeChangeAccepted: "12/15/2015",
				KSShareDateCreated: "12/15/2015",
				KSShareDateAccepted: "12/15/2015",
				KSShareDateRevoked: "12/15/2015",
				KSShareRevokedBy: "Alex"
			}; //DEMO
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}			
		},
		getCategoriesDataHandler: function(callback){
			var response = {
				data: [
					{id: 1, name: "Category ZXC"},
					{id: 2, name: "Category ABC"},
					{id: 3, name: "Category KOP"}
				]
			};
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}	
		},
		getSiteDataHandler : function(callback){
			var response = {
				data: [
					{id: 1, name: "voli.com"},
					{id: 2, name: "lidl.com"},
					{id: 3, name: "t-mobile.com"}
				]
			};
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}	
		},		
	}		
	InitiateShareAgDT.init(shareAgOptions);	
	
	$("#editForm").bootstrapValidator();
	$('.phone').mask('+9 (999) 999 99 99');
	$('.phone_us').mask('(999) 999-9999');

	/* change tel if country == US */
	$(document).on('change', 'select[name="country"]', function() {
		var $country = $(this).val();
		if ($country === "US") {
			$('.phone').removeClass('phone').addClass('phone_us');
		}
		else {
			$('.phone_us').removeClass('phone_us').addClass('phone');
		}
	});
	$('#Email').keyup(function() {
		var email = $(this).val();
		if (email.length != 0) {
			if (isValidEmailAddress(email)) {
				$("#group-email").addClass("has-success has-feedback");
				$("#group-email").removeClass("has-error");
				$("#email-ok-ic").removeClass("hidden");
				$("#email-bad-ic").addClass("hidden");
			} else {
				$("#group-email").addClass("has-error has-feedback");
				$("#group-email").removeClass("has-success");
				$("#email-ok-ic").addClass("hidden");
				$("#email-bad-ic").removeClass("hidden");
			}
		} else {
			$("#group-email").addClass("has-error has-feedback");
			$("#group-email").removeClass("has-success");
			$("#email-ok-ic").addClass("hidden");
			$("#email-bad-ic").removeClass("hidden");

		}
	});

	$('#ConsoleUrl').keyup(function() {
		var email = $(this).val();
		if (email.length != 0) {
			if (is_valid_url(email)) {
				$("#group-url").addClass("has-success has-feedback");
				$("#group-url").removeClass("has-error");
				$("#url-ok-ic").removeClass("hidden");
				$("#url-bad-ic").addClass("hidden");
			} else {
				$("#group-url").addClass("has-error has-feedback");
				$("#group-url").removeClass("has-success");
				$("#url-ok-ic").addClass("hidden");
				$("#url-bad-ic").removeClass("hidden");
			}
		} else {
			$("#group-url").addClass("has-error has-feedback");
			$("#group-url").removeClass("has-success");
			$("#url-ok-ic").addClass("hidden");
			$("#url-bad-ic").removeClass("hidden");

		}
	});

	$('#close-email').click(function() {
		$('#deleteEmailModal').modal('hide');
	});
	$('#close-phone').click(function() {
		$('#deletePhoneModal').modal('hide');
	});
	$('#close-media').click(function() {
		$('#deleteMediaModal').modal('hide');
	});


});


// If you want to draw your charts with Theme colors you must run initiating charts after that current skin is loaded
$(window).bind("load", function () {
	/*Sets Themed Colors Based on Themes*/
	themeprimary = getThemeColorFromCss('themeprimary');
	themesecondary = getThemeColorFromCss('themesecondary');
	themethirdcolor = getThemeColorFromCss('themethirdcolor');
	themefourthcolor = getThemeColorFromCss('themefourthcolor');
	themefifthcolor = getThemeColorFromCss('themefifthcolor');
});