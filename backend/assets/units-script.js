/**
* Javascript manage-units.html
* Revision: 2
* Date: 04-05-2015
* Author: Alexander Bulei
*/

var InitiateUnitsDataTable = function () {
    return {
        init: function (options) {
			var $table = $('#unitsdatatable');
			var _options =  options || {};
			
			// check access
			controlActionBtnsAccess($table, _options.access);
		
            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [
                    '',
                    '',
                    '',
                    ''
                ]
            });

            //Add New Row
            $('#unitsdatatable_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '', buildStatusDropDownHTML(),
                    '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                editAddedRow(oTable, nRow);
				
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}
				
                isEditing = nRow;                
            });

            //Clone row
            $table.on("click", 'button.clone', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#cloneModal').data('id', id).modal('show');
            });

            $('#clone-btn').click(function () {
                var id = $('#cloneModal').data('id');
                var anSelected = $('#'+id);
                var data=[];
                $(anSelected).find('td').each(function(){data.push($(this).html());});
                var aiNew = oTable.fnAddData( data );
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                isEditing = nRow;
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                $('.publish-sign').closest('td').addClass('sign');
                $('#cloneModal').modal('hide');
            });
            var isEditing = null;

			bindChangeStatusDropDown($table);
			bindClickActiveModalConfirm();
			bindClickInactiveModalConfirm();
			bindCancelStatusModal();
			
            //Edit row
            $table.on("click", 'a.edit', function (e) {
				e.preventDefault();
                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }				
            });

			
			var _SORT_AVAL_INST;
			var _SORT_SEL_INST;
			
			function _sortList(inst){
				var order = inst.toArray();
				order.sort();
				inst.sort(order);
			}
			
			$("button.cmd-select-all, button.cmd-deselect-all").on("click", function(e){
				var $btn = $(this);
				var $avalList = $("#available-cat-list");
				var $selList = $("#selected-cat-list");
				
				function selToAvail(){
					$.each($selList.find('li.cat-list-item'), function(i, li){
						$(li).appendTo($avalList);						
					});					
				}
				
				function availToSel(){
					$.each($avalList.find('li.cat-list-item'), function(i, li){
						$(li).appendTo($selList);						
					});					
				}				
				
				if ($btn.hasClass('cmd-select-all')){
					availToSel();
					_sortList(_SORT_AVAL_INST);
					_sortList(_SORT_SEL_INST);
					$btn.addClass('disabled');
					$("button.cmd-deselect-all").removeClass('disabled');
				} else {
					selToAvail();
					_sortList(_SORT_AVAL_INST);
					_sortList(_SORT_SEL_INST);
					$btn.addClass('disabled');
					$("button.cmd-select-all").removeClass('disabled');
				}
			});
			
			// assign categories modal
			$table.on("click", 'button.categories', function () {
                var id = $(this).closest('tr').attr('id');
				var $modal = $('#assignCategoriesModal');
				var $catRow = $('.categories-row',$modal);

				function _fillLists(response){
					var $row = $('.categories-row',$('#assignCategoriesModal'));
					var $avalList = $("#available-cat-list");
					var $selList = $("#selected-cat-list");
					
					if (response.errorMsg){
						Notify(response.errorMsg, 'bottom-right', '5000', 'danger', 'fa-times', true);
						return false;
					}
					
					$avalList.empty();
					$selList.empty();
					
					/*HEADERS*/
					$avalList.append('<li class="list-group-item"><span class="unit-categ-span header">Site ID</span><span class="unit-categ-span header">Category ID</span></li>');
					
					$selList.append('<li class="list-group-item"><span class="unit-categ-span header">Site ID</span><span class="unit-categ-span header">Category ID</span></li>');
					/*/HEADERS*/
					
					var _data = response.data || {};
					
					function _insert_item($list, obj){
						var $li = $('<li class="list-group-item cat-list-item"></li>');
						$li.attr("data-record", JSON.stringify(obj));
						var $spanSite = $('<span class="unit-categ-span">').text(obj.site.name);
						var $spanCateg = $('<span class="unit-categ-span">').text(obj.name);
						$li.append($spanSite).append($spanCateg);
						$list.append($li);					
					}
					
					if (_data.available) {
						$.each(_data.available, function(i,obj){
							_insert_item($avalList,obj);
						});
					}
					
					if (_data.selected) {
						$.each(_data.selected, function(i,obj){
							_insert_item($selList,obj);							
						});
					}
					
					//make draggable - init plugin
					var options = {
						group: "categories-group",
						draggable: '.cat-list-item',
						onAdd: function (evt) {
							_sortList(_SORT_AVAL_INST);
							_sortList(_SORT_SEL_INST);
						}					
					}
					
					_SORT_AVAL_INST = Sortable.create($avalList[0], options);
					_SORT_SEL_INST = Sortable.create($selList[0], options);
					
					_sortList(_SORT_AVAL_INST);
					_sortList(_SORT_SEL_INST);						
					
					$row.show();
					
				}
				
				_options.loadCategoriesLists(_fillLists);
				
                $modal.data('id', id).modal('show');
            });
			
			$("#save-categories-btn").on("click", function (e) {				
				var $selList = $("#selected-cat-list").find('li.cat-list-item');
				var data = [];
				
				$.each($selList, function(i, li){
					var catId = $(li).data('record');
					data.push(catId);
				});
				
				function _callback(response){
					if (response.errorMsg) {
						Notify(response.errorMsg, 'bottom-right', '5000', 'danger', 'fa-times', true);
					}
				}
				
				_options.saveCategoriesHandler(data,_callback);
				$('#assignCategoriesModal').modal('hide');
			});

            //Call Delete Modal passing data-id
            $table.on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteModal').data('id', id).modal('show');
            });

            //
            //Delete Row if Confirmed
            $('#delete-btn').click(function () {
                var id = $('#deleteModal').data('id');
				
				function _callback(response){
					if (!response.errorMsg) {
						var nRow = $('#' + response.id);
						oTable.fnDeleteRow(nRow);
					} else {
						Notify(response.errorMsg, 'bottom-right', '5000', 'danger', 'fa-times', true);
					}
				}				
				
				_options.deleteHandler(id,_callback);
                $('#deleteModal').modal('hide');				
            });

            //Cancel Editing or Adding a Row
            $table.on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Save an Editing Row
            $table.on("click", 'a.save', function (e) {
                e.preventDefault();
				saveRow(oTable, isEditing);
				isEditing = null;
            });

            function editRow(oTable, nRow, isNew) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
				
				// status select
				var $statusSelect = $( $.parseHTML(aData[4])[0] );
				if ($statusSelect.find("select").length == 0){
					$statusSelect = buildStatusDropDown();
					$('option[value="'+aData[4]+'"]',$statusSelect).attr("selected", "selected");
				}
				
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
				var html = aData[2].toUpperCase() == 'YES' ? ' checked="checked"' : '';
				var $toggle = $('<label><input class="checkbox-slider toggle yesno row-ctrl" type="checkbox"'+html+'><span class="text" style="margin-top: 13px;"></span></label>');
				
				$(jqTds[2]).empty().append($toggle);
				
				$toggle.find('input').on('change', function(e){
					var $input = $(jqTds[3]).find('input');
					if (this.checked) {
						$input.removeAttr("disabled")
					} else {
						$input.attr("disabled","disabled");
					}
				});
				
				html = aData[2].toUpperCase() == 'NO' ? ' disabled="disabled"' : '';
				jqTds[3].innerHTML = '<input type="number" min="0" class="form-control input-small" value="' + aData[3] + '" '+html+'>';
                jqTds[4].innerHTML = $statusSelect[0].outerHTML;
				html = (typeof isNew != "undefined") ? ' data-mode="new"' : '';
                jqTds[5].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"'+html+'><i class="fa fa-times"></i> Cancel</a>';

            }
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
            }

            function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
				editRow(oTable, nRow, true);
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
				var jqSelects = $('select', nRow);
				
				
				var rowData = {
					"id": nRow.id || -1,
					"unitName": jqInputs[0].value,
					"unitAbb": jqInputs[1].value,
					"isTime": jqInputs[2].checked,
					"minutes": jqInputs[3].value,
					"status": jqSelects.eq(0).val()
				}

				function _callback(response){
					if (!response.errorMsg){
						var record = response.record;
						oTable.fnUpdate(record.unitName, nRow, 0, false);
						oTable.fnUpdate(record.unitAbb, nRow, 1, false);
						oTable.fnUpdate((record.isTime ? "Yes" : "No"), nRow, 2, false);
						oTable.fnUpdate(record.isTime ? record.minutes : "", nRow, 3, false);
						oTable.fnUpdate(record.status, nRow, 4, false);
						oTable.fnUpdate('<button type="button" class="btn btn-blue shiny btn-xs dashboard tooltip-blue command-btn categories" data-toggle="tooltip" data-placement="top" data-original-title="Categories"><i class="fa fa-external-link"></i></button><a href="#" class="btn btn-palegreen  shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a><button type="button" class="btn btn-warning shiny btn-xs clone tooltip-warning command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Clone"><i class="fa fa-copy"></i></button><button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>', nRow, 5, false);
						oTable.fnDraw();
						$('[data-toggle="tooltip"]',nRow).tooltip();
					} else {
						Notify(response.errorMsg, 'bottom-right', '5000', 'danger', 'fa-times', true);
					}
				}
				
				//save
				_options.saveUpdateHandler(rowData, _callback);
            }
			
        }

    };
}();

$(window).bind("load", function () {
	/*Sets Themed Colors Based on Themes*/
	themeprimary = getThemeColorFromCss('themeprimary');
	themesecondary = getThemeColorFromCss('themesecondary');
	themethirdcolor = getThemeColorFromCss('themethirdcolor');
	themefourthcolor = getThemeColorFromCss('themefourthcolor');
	themefifthcolor = getThemeColorFromCss('themefifthcolor');
});

$(function(){
	//Initialise dataTable
	var initOptions = {
		access: {
			edit: true,
			delete: true
		},		
		saveUpdateHandler: function(data, callback){
			var response = { record: data };
			//var response = { "id" : id, "errorMsg" : "Some Error!"}; // response with error 
			callback(response);
		},
		deleteHandler: function(id, callback){
			var response = { "id" : id };
			//var response = { "errorMsg" : "Some Error!"}; // response with error 
			callback(response);
		},
		saveCategoriesHandler: function(data, callback){
			var response = { categories: data };
			callback(response);
		},
		loadCategoriesLists: function(callback){
			//var response = { "errorMsg" : "Some Error!"}; // response with error 
			var response = {
				data: {
					available: [
						{id: 6, site:{ id: 1, name: 'Site 1'}, name: "Category 6"},
						{id: 5, site:{ id: 2, name: 'Site 2'}, name: "Category 5"},
						{id: 3, site:{ id: 3, name: 'Site 3'}, name: "Category 3"},
						{id: 7, site:{ id: 1, name: 'Site 1'}, name: "Category 7"},
						{id: 8, site:{ id: 6, name: 'Site 6'}, name: "Category 8"},
						{id: 9, site:{ id: 2, name: 'Site 2'}, name: "Category 9"}
					],
					selected: [
						{id: 2, site:{ id: 3, name: 'Site 3'}, name: "Category 2"},
						{id: 1, site:{ id: 5, name: 'Site 5'}, name: "Category 1"},
						{id: 4, site:{ id: 7, name: 'Site 7'}, name: "Category 4"}
					]
				}
			}
			callback(response);
		}
	}	
	
	InitiateUnitsDataTable.init(initOptions);
});