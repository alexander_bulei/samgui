/**
* Columns Permissons Manager
* Author: Alexander Bulei
* 
* CANVIEW CANEDIT
*	1		1
*
*/

var COLMANAGER = function($table,r){
	this.$table = $table;
	this.colRules = r;
};

COLMANAGER.prototype = {
	_colProps: ['view','edit','post'],
	cols: {},
	init: function(){
		this.make();
		this.proccessVisibility();
		this.createCChooser();
	},
	make: function(){
		var that = this;
		$.each(this.colRules, function(n,v){
			if (!that.cols[n]) that.cols[n] = {};
			if (v.length > 0){
				v = (v.length > 2) ? v.substring(0,2) : v;
				for (i = 0, len = v.length; i < len; i++) { 
					that.cols[n][ that._colProps[i] ] = (parseInt(v[i]) === 1);
				}
			}
		});
	},
	proccessVisibility: function(){
		var that = this;
		$.each(this.cols, function(n,obj){
			if ((typeof obj.view == "boolean") && (!obj.view)){
				var idx = that.$table.find('th[data-name="'+n+'"]').index()+1;
				that.$table.find('th:nth-child('+idx+')').addClass('cm-invi').hide();
				that.$table.find('td:nth-child('+ idx +')').hide();
			}
		});
	},
	getByTd: function(td){
		var $td = $(td), idx = $td.index()+1,
			$th = this.$table.find('th:nth-child('+idx+')'),
			col = $th.data("name");
		return this.cols[col];
	},
	haveAccess: function(td){
		var co = this.getByTd(td);
		return co.edit && co.view;
	},
	createCChooser: function(){
		var that = this;
		var $select = $( that.$table.data("cchooserSelector") );
		
		var proccessCol = function($table,index,show){
			if (show) {
				$table.find('td:nth-child('+ index +'),th:nth-child('+index+')').show();
			} else {
				$table.find('td:nth-child('+ index +'),th:nth-child('+index+')').hide();
			}
		}
		
		if ($select.length){
			$select.empty().attr("multiple","multiple");
			var uncheckedArr = that.$table.data("cchooserUnchecked") || [];
			var excludeArr = that.$table.data("cchooserExclude") || [];
			
			var $ths = that.$table.find("thead tr:not(.search_inputs)").find("th:not(.cm-invi)");
			$.each($ths, function(i,th){
				var $th = $(th);
				var $op = $("<option>");
				var _text = $th.text();
				var thIndex = parseInt($th.index())+1;
				if (_text) {
					if ($.inArray(thIndex,excludeArr) == -1) {
						$op.val($th.index()).text(_text).appendTo($select);
					}
					if ($.inArray(thIndex,uncheckedArr) == -1){
						$op.attr("selected","selected");
					} else {
						proccessCol(that.$table,thIndex,false);
					}
				}
			});
			
			$select.data("tableControl", that.$table);
			
			if ($select.data("multiselect")){
				$select.multiselect('destroy');
			}
			
			$select.multiselect({
				onChange: function(option, checked) {
					var _$select = this.$select;
					var _$table =  $(_$select.data("tableControl"));
					var colIndex = parseInt(option.val())+1;
					proccessCol(_$table,colIndex,checked);
				},
				buttonText: function(options) {
					//return options.length + ' columns';
					return 'Show/Hide Columns';
                }
            });
		}		
	}
}