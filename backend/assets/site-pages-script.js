/**
* Javascript for manage-site-pages.html
* Revision: 2
* Date: 14-07-2015
* Author: Alexander Bulei
*/

var InitiateSitePagesDataTable = function () {
    return {
        init: function (options) {
			var _options =  options || {};
			var $table = $('#pagesdatatable');
			
			// check access
			controlActionBtnsAccess($table, _options.access);
			
			
			var $siteSelect = $("#selectSite");
            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
				responsive: true
            });
            var i = oTable.fnGetData().length + 1;

            //Add New Row
            $('#sitePagesdatatable_new').click(function (e) {
                e.preventDefault();
				var tmplDDHTML, sysDDHTML;
				// template page dropdown
				buildTmplPageDropdown(function($select){
					tmplDDHTML = $select[0].outerHTML;
				});
				// system page dropdown
				buildSysPageDropdown(function($select){
					sysDDHTML = $select[0].outerHTML;
					
					var aiNew = oTable.fnAddData(['', tmplDDHTML, '', sysDDHTML, buildStatusDropDownHTML(),
						'<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
					]);
					var nRow = oTable.fnGetNodes(aiNew[0]);
					var n = (oTable.fnGetData().length + 1);
					nRow.id = n;
					editAddedRow(oTable, nRow);
					if (isEditing !== null && isEditing != nRow) {
						restoreRow(oTable, isEditing);
					}				
					isEditing = nRow;
				})								
            });

            //Edit row
            $table.on("click", 'a.edit', function (e) {
                e.preventDefault();

                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }

            });

            var isEditing = null;

			bindChangeStatusDropDown($table);
			bindClickActiveModalConfirm();
			bindClickInactiveModalConfirm();
			bindCancelStatusModal();

            //Call Delete Modal passing data-id
            $table.on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteModal').data('id', id).modal('show');
            });

            //Delete Row if Confirmed
            $('#delete-btn').click(function () {
				
                var id = $('#deleteModal').data('id');
				
				function _callback(response){
					if (!response.errorMsg) {
						var nRow = $('#' + response.id);
						oTable.fnDeleteRow(nRow);
					} else {
						Notify(response.errorMsg, 'bottom-right', '5000', 'danger', 'fa-times', true);
					}
				}				

				_options.deleteHandler(id,_callback);
				$('#deleteModal').modal('hide');
            });

            //Cancel Editing or Adding a Row
            $table.on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Save an Editing Row
            $table.on("click", 'a.save', function (e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
                    saveRow(oTable, isEditing);
                    isEditing = null;
                }
            });
			
			function buildTmplPageDropdown(afterBuild){
				var id = $siteSelect.val();
				var $select = $("<select></select>");
				
				function _callback(data){					
					var arr = data.data || [];					
					$.each(arr, function(i,obj){
						$op = $("<option data-id='"+obj.id+"' value='"+obj.name+"'>"+obj.name+"|"+obj.version+"</option>");
						$select.append($op);
					});
					afterBuild($select);
				}
				
				_options.loadTmplSelectHandler(id, _callback);
			}
			
			function buildSysPageDropdown(afterBuild){
				var id = $siteSelect.val();
				var $select = $("<select></select>");
				
				function _callback(data){
					var arr = data.data || [];
					$.each(arr, function(i,obj){
						$op = $("<option data-id='"+obj.id+"' value='"+obj.name+"'>"+obj.name+"</option>");
						$select.append($op);
					});
					afterBuild($select);
				}
				
				_options.loadSysSelectHandler(id, _callback);
			}

            function editRow(oTable, nRow, isNew) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
				
				// template page dropdown
				buildTmplPageDropdown(function($select){
					$('option[value="'+aData[1]+'"]',$select).attr("selected", "selected");
					jqTds[1].innerHTML = $select[0].outerHTML;
				});

                jqTds[2].innerHTML = aData[2];
				
				// system page dropdown
				buildSysPageDropdown(function($select){
					$('option[value="'+aData[3]+'"]',$select).attr("selected", "selected");
					jqTds[3].innerHTML = $select[0].outerHTML;
				})
				
				// status select
				var $statusSelect = $( $.parseHTML(aData[4])[0] );
				if ($statusSelect.find("select").length == 0){
					$statusSelect = buildStatusDropDown();
					$('option[value="'+aData[4]+'"]',$statusSelect).attr("selected", "selected");
				}				
				
                jqTds[4].innerHTML = $statusSelect[0].outerHTML;
				var str = (typeof isNew != "undefined") ? ' data-mode="new"' : "";
                jqTds[5].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" '+str+'><i class="fa fa-times"></i> Cancel</a>';

            }
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
            }

            function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
				editRow(oTable, nRow, true);
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
				var jqSelects = $('select', nRow);
				
				var rowData = {
					"id": nRow.id || -1,
					"sitePageName": jqInputs[0].value,
					"templatePageName": jqSelects.eq(0).val(),
					"systemPageName": jqSelects.eq(1).val(),
					"status": jqSelects.eq(2).val()
				}

				function _callback(response){
					if (!response.errorMsg){
						// site page
						oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
						// tempate page
						oTable.fnUpdate(jqSelects.eq(0).val(), nRow, 1, false);
						// verison
						var ver = jqSelects.eq(0).find('option:selected').text().split('|')[1];
						oTable.fnUpdate(ver, nRow, 2, false);
						// system page
						oTable.fnUpdate(jqSelects.eq(1).val(), nRow, 3, false);
						// status select
						oTable.fnUpdate(jqSelects.eq(2).val(), nRow, 4, false);
						// command buttons
						oTable.fnUpdate('<a href="#" class="btn btn-palegreen shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a><button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>', nRow, 5, false);
						oTable.fnDraw();
						$('[data-toggle="tooltip"]',nRow).tooltip();
					} else {
						Notify(response.errorMsg, 'bottom-right', '5000', 'danger', 'fa-times', true);
					}
				}
				
				//save
				_options.saveUpdateHandler(rowData, _callback);
            }
        }

    };
}();

$(window).bind("load", function () {
	/*Sets Themed Colors Based on Themes*/
	themeprimary = getThemeColorFromCss('themeprimary');
	themesecondary = getThemeColorFromCss('themesecondary');
	themethirdcolor = getThemeColorFromCss('themethirdcolor');
	themefourthcolor = getThemeColorFromCss('themefourthcolor');
	themefifthcolor = getThemeColorFromCss('themefifthcolor');

	// Start select2 fields
	$("#selectSite").select2();

	//Initialise dataTable
	var initOp = {
		access: {
			edit: true,
			delete: true
		},		
		loadTmplSelectHandler : function(id, callback){
			//var response = { "errorMsg" : "Some Error!"}; // response with error 
			var response = {
				data: [
					{id: 0, name: "Home", version: "1.1"},
					{id: 1, name: "Listing rent", version: "2.0"},
					{id: 2, name: "Listing sale", version: "1.0"}
				]
			};
			callback(response);
		},
		loadSysSelectHandler: function(id, callback){
			//var response = { "errorMsg" : "Some Error!"}; // response with error 
			var response = {
				data: [
					{id: 0, name: "Home"},
					{id: 1, name: "Listing"}
				]
			};
			callback(response);
		},
		deleteHandler: function(id, callback){
			var response = { "id" : id };
			//var response = { "errorMsg" : "Some Error!"}; // response with error 
			callback(response);
		},
		saveUpdateHandler: function(data, callback){
			var response = { categories: data };
			callback(response);
		}		
	}
	InitiateSitePagesDataTable.init(initOp);
});

/* Select function to show Site fields */
$( "#selectSite" ).change(function() {
	if ($(this).val() != "") {
		$('#siteName').val('Site Name goes here').fadeIn();
		$('#siteDescription').val('Site description goes here').fadeIn();
		$('.site-table').fadeIn(400, function(e){
			$( $.fn.dataTable.tables(true) ).DataTable().responsive.recalc();
		});
	} else {
		$('.site-table').fadeOut();
		$('#siteDescription').fadeOut();
		$('#siteName').fadeOut();
	}
});
