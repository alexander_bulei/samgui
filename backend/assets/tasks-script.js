/**
* Javascript for manage-tasks.html
* Revision: 13
* Date: 14-07-2015
* Author: Alexander Bulei
*/

var InitiateTasksDataTable = function () {
    return {
        init: function (options) {
			var _options =  options || {};
			var $table = $('#tasksdatatable');
			
			// check access
			controlActionBtnsAccess($table, _options.access);

            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
				"aoColumns" : [
                    { sWidth: '6%' },
                    { sWidth: '6%' },
                    { sWidth: '6%' },
                    { sWidth: '6%' },
                    { sWidth: '6%' },
					{ sWidth: '6%' },
                    { sWidth: '6%' },
					{ sWidth: '4%' },
					{ sWidth: '4%' },
					{ sWidth: '4%' },
					{ sWidth: '6%' },
					{ sWidth: '6%' },
					{ sWidth: '6%' },
					{ sWidth: '6%' },
					{ sWidth: '12%' }
                ],
                "bAutoWidth": false
            });

			/*15*/
			
            //Add New Row
            $('#tasksdatatable_new').click(function (e) {
                e.preventDefault();
				
                var aiNew = oTable.fnAddData(['', '', '', '', '', '', '', '', '', '', '<a class="task-extension-link" href="#" data-taskext-val="{}" data-taskext-type="assign">Assign</a>', '<a class="task-extension-link" href="#" data-taskext-val="{}" data-taskext-type="escalate">Escalate</a>', '<a class="task-extension-link" href="#" data-taskext-val="{}" data-taskext-type="notify">Notify</a>', buildStatusDropDownHTML(),
                    '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;
				$(".task-extension-link", nRow).taskExtension();
            });

            var isEditing = null;

			var $table = $('#tasksdatatable');
			bindChangeStatusDropDown($table);
			bindClickActiveModalConfirm();
			bindClickInactiveModalConfirm();
			bindCancelStatusModal();
			bindClickCloneShowModal($table);
			bindClickDeleteShowModal($table);
			
			$('#clone-btn').click(function () {
                var id = $('#cloneModal').data('id');
                var anSelected = $('#'+id);
                var data=[];
                $(anSelected).find('td').each(function(){data.push($(this).html());});
                var aiNew = oTable.fnAddData( data );
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                isEditing = nRow;
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                $('#cloneModal').modal('hide');
				$(".task-extension-link", nRow).taskExtension();
            });			

            //Edit row
            $('#tasksdatatable').on("click", 'a.edit', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }
            });
			
            //Call Delete Modal passing data-id
            $('#tasksdatatable').on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteModal').data('id', id).modal('show');
            });

            //
            //Delete Row if Confirmed
            $('#delete-btn').click(function () {
                var id = $('#deleteModal').data('id');
                var nRow = $('#'+id);
                oTable.fnDeleteRow(nRow);
                $('#deleteModal').modal('hide');
            });

            //Cancel Editing or Adding a Row
            $('#tasksdatatable').on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Save an Editing Row
            $('#tasksdatatable').on("click", 'a.save', function (e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
					var canSave = true;
					$(".number-validation").each(function(i,input){
						var $input = $(input);
						var val = $input.val();
						$input.parent().removeClass('has-error');
						if ((val.lenght != 0) && (!$.isNumeric(val))) {							
							$input.parent().addClass('has-error');
							canSave = false;
                            Notify('The numeric only!', 'bottom-right', '5000', 'danger', 'fa-times', true);							
						}
					});
					if (canSave) {
						saveRow(oTable, isEditing);
						isEditing = null;
					}
                }
            });

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
				
				// status select
				var $statusSelect = $( $.parseHTML(aData[13])[0] );
				if ($statusSelect.find("select").length == 0){
					$statusSelect = buildStatusDropDown();
					$('option[value="'+aData[13]+'"]',$statusSelect).attr("selected", "selected");
				}
				
                jqTds[0].innerHTML = '<input style="width: 100% !important" type="text" class="row-ctrl form-control input-small" value="' + aData[0] + '">';
				jqTds[1].innerHTML = '<input style="width: 100% !important" type="text" class="row-ctrl form-control input-small" value="' + aData[1] + '">';
				jqTds[2].innerHTML = '<input style="width: 100% !important" type="text" class="row-ctrl form-control input-small number-validation" value="' + aData[2] + '">';
				jqTds[3].innerHTML = '<input style="width: 100% !important" type="text" class="row-ctrl form-control input-small number-validation" value="' + aData[3] + '">';
				var _chkAttr = aData[4].toUpperCase() == 'YES' ? ' checked="checked"' : '';
				jqTds[4].innerHTML = '<label><input class="checkbox-slider toggle yesno row-ctrl" type="checkbox"'+_chkAttr+'><span class="text" style="margin-top: 13px;"></span></label>';
				var _chkAttr = aData[5].toUpperCase() == 'YES' ? ' checked="checked"' : '';
				jqTds[5].innerHTML = '<label><input class="checkbox-slider toggle yesno row-ctrl" type="checkbox"'+_chkAttr+'><span class="text" style="margin-top: 13px;"></span></label>';
				
				
				$taskTable = $('<select style="width: 100% !important; padding: 6px 0;" class="row-ctrl">');
				$taskTable.append('<option value="none">None</option>');
				$taskTable.append('<option value="Listing">Listing</option>');
				$taskTable.append('<option value="Another">Another</option>');
				$('option[value="'+aData[6]+'"]',$taskTable).attr("selected", "selected");
				
				jqTds[6].innerHTML = $taskTable[0].outerHTML;
				
				$emailSelect = $('<select style="width: 100% !important; padding: 6px 0;" class="row-ctrl">');
				$emailSelect.append('<option value="E01">E01 | A new Listing has been created that needs approval</option>');
				$emailSelect.append('<option value="E02">E02 | Description X</option>');
				$emailSelect.append('<option value="E03">E03 | Description XX</option>');
				$emailSelect.append('<option value="E04">E04 | Description XXX</option>');
				$emailSelect.append('<option value="E05">E05 | Thank you for your reservation</option>');
				$emailSelect.append('<option value="N02">N02 | The attached listing needs your attention</option>');
				$('option[value="'+aData[7]+'"]',$emailSelect).attr("selected", "selected");
				
				jqTds[7].innerHTML = $emailSelect[0].outerHTML;
				
				$('option[value="'+aData[8]+'"]',$emailSelect).attr("selected", "selected");
				jqTds[8].innerHTML = $emailSelect[0].outerHTML;
				
				$('option[value="'+aData[9]+'"]',$emailSelect).attr("selected", "selected");
				jqTds[9].innerHTML = $emailSelect[0].outerHTML;
				
				jqTds[10].innerHTML = aData[10];
				jqTds[11].innerHTML = aData[11];
				jqTds[12].innerHTML = aData[12];
				jqTds[13].innerHTML = $statusSelect[0].outerHTML;
                jqTds[14].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"><i class="fa fa-times"></i> Cancel</a>';

            }
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
            }

            function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
				editRow(oTable, nRow);
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('.row-ctrl', nRow);				
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
				var html = (jqInputs[4].checked) ? 'Yes' : 'No';
				oTable.fnUpdate(html, nRow, 4, false);
				var html = (jqInputs[5].checked) ? 'Yes' : 'No';				
				oTable.fnUpdate(html, nRow, 5, false);
				oTable.fnUpdate($(jqInputs[6]).val(), nRow, 6, false);				
				oTable.fnUpdate(jqInputs[7].value, nRow, 7, false);
				oTable.fnUpdate(jqInputs[8].value, nRow, 8, false);
				oTable.fnUpdate(jqInputs[9].value, nRow, 9, false);
				oTable.fnUpdate($(jqInputs[13]).val(), nRow, 13, false);                
                oTable.fnUpdate('<a href="#" class="btn btn-palegreen shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a><button type="button" class="btn btn-warning shiny btn-xs clone tooltip-warning command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Clone"><i class="fa fa-copy"></i></button><button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>', nRow, 14, false);
                oTable.fnDraw();
				$('[data-toggle="tooltip"]',nRow).tooltip();
				$(".task-extension-link", nRow).taskExtension();
            }
        }

    };
}();


$(window).bind("load", function () {
	/*Sets Themed Colors Based on Themes*/
	themeprimary = getThemeColorFromCss('themeprimary');
	themesecondary = getThemeColorFromCss('themesecondary');
	themethirdcolor = getThemeColorFromCss('themethirdcolor');
	themefourthcolor = getThemeColorFromCss('themefourthcolor');
	themefifthcolor = getThemeColorFromCss('themefifthcolor');

	//Initialise dataTable
	var opts = {
		access: {
			edit: true,
			delete: true
		}
	}	
	
	InitiateTasksDataTable.init(opts);
});

$(function(){
	$(".task-extension-link").taskExtension();
});
