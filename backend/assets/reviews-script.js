/**
* Javascript for manage-reviews.html
* Revision: 1
* Modified Date: 27-07-2015
* Author: Alexander Bulei
*/

var InitiateReviewsDataTable = function () {
    return {
        init: function (options) {
			var _options = options || {};
			var $table = $('#reviewsdatatable');
			
			_init_chooser();
			
			function initRating($where, readonly){
				var b = (typeof readonly != "undefined" ) ? readonly : true;
				$where.find('input.rating-input').rating({
					size : 'xxs',					
					showCaption: true,
					showClear: false,
					readonly: b,
					step: 1,
					starCaptions: function(v){
						return v;
					}
				});
			}
			
			//init rating
			initRating($table, true);
			
            $table.find('thead tr.search_inputs input').keyup(function () {
                /* Filter on the column (the index) of this element */
                oTable.fnFilter(this.value, $table.find('thead tr.search_inputs input').index(this));
            });			
			
			// check access
			controlActionBtnsAccess($table, _options.access);
			
            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
				"columns": [
					{ "width": "10%" },
					{ "width": "10%" },
					{ "width": "13%" },
					{ "width": "13%" },
					{ "width": "10%" },
					{ "width": "10%" },
					{ "width": "10%" },
					{ "width": "10%" },
					{ "width": "14%" }
				]                
            });
			
			var isEditing = null;

			bindChangeStatusDropDown($table);
			bindClickActiveModalConfirm();
			bindClickInactiveModalConfirm();
			bindCancelStatusModal();
			
            //Add New Row
            $('#reviewsdatatable_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '', buildStatusDropDownHTML(),
                    '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;
            });
			
            //Edit row
            $table.on("click", 'a.edit', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }
            });

            //Call Delete Modal passing data-id
            $table.on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteModal').data('id', id).modal('show');
            });

            //Delete Row if Confirmed
            $('#delete-btn').click(function () {
				var id = $('#deleteModal').data('id');
				
				function _callback(response){
					if (!response.errorMsg) {
						var nRow = $('#' + response.id);
						oTable.fnDeleteRow(nRow);
					} else {
						Notify(response.errorMsg, 'bottom-right', '5000', 'danger', 'fa-times', true);
					}
				}
				_options.deleteHandler(id,_callback);
                $('#deleteModal').modal('hide');
            });

            //Cancel Editing or Adding a Row
            $table.on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Save an Editing Row
            $table.on("click", 'a.save', function (e) {
                e.preventDefault();
				if (this.innerHTML.indexOf("Save") >= 0) {
					saveRow(oTable, isEditing);
					isEditing = null;
				}
            });		
			
            //Clone row
            $table.on("click", 'button.clone', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#cloneModal').data('id', id).modal('show');
            });

            $('#clone-btn').click(function () {
                var id = $('#cloneModal').data('id');
                var anSelected = $('#'+id);
                var data=[];
                $(anSelected).find('td').each(function(){data.push($(this).html());});
                var aiNew = oTable.fnAddData( data );
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                isEditing = nRow;
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                $('#cloneModal').modal('hide');
            });
			
			//contact reviewer
            $table.on("click", 'button.contact-reviewer', function (e) {
                e.preventDefault();				
				var id = $(this).closest('tr').attr('id');
				//$('#').data('id', id).modal('show');
            });

			//request moderation
            $table.on("click", 'button.request-moderation', function (e) {
                e.preventDefault();				
				var id = $(this).closest('tr').attr('id');
				//$('#').data('id', id).modal('show');
            });
			
            function editRow(oTable, nRow, isNew) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
				
				// Listing
				jqTds[0].innerHTML = '<input type="text" class="form-control input-small full-width" value="' + aData[0] + '">';
				
				// Name
				var _name = aData[1];
				var _arr_names = aData[1].split(' ');
				jqTds[1].innerHTML = '<input style="margin-bottom:3px !important;" type="text" class="form-control input-small full-width" value="' + _arr_names[0] + '"><input type="text" class="form-control input-small full-width" value="' + _arr_names[1] + '">';

				// Review
				jqTds[2].innerHTML = '<textarea rows="3" type="text" class="form-control input-small full-width">'+aData[2]+'</textarea>';

				// Response
				jqTds[3].innerHTML = '<textarea rows="3" type="text" class="form-control input-small full-width">'+aData[3]+'</textarea>';
				
				// Date submitted
				jqTds[4].innerHTML = '<input type="text" class="form-control input-small date-picker-date-only full-width" value="'+aData[4]+'"></input>';
				
				// Reservation
				var $element = $(aData[5]);
				jqTds[5].innerHTML = '<div style="position:relative;"><input style="margin-bottom:3px !important;" type="text" class="form-control input-small full-width date-picker" value="'+$element.eq(0).text()+'" /><input type="text" class="form-control input-small full-width date-picker" value="'+$element.eq(1).text()+'" /></div>';

				// Rating
				var $element = $(aData[6]);
				var v =  $element.is('input') ? $element.val() : $("input",$element).val();
				jqTds[6].innerHTML = '<input type="number" value="'+v+'" class="rating-input" />';
				
				
				// status select
				var $statusSelect = aData[7].length ? $( $.parseHTML(aData[7])[0] ) : $();
				if ($statusSelect.find("select").length == 0){
					$statusSelect = buildStatusDropDown();
					$('option[value="'+aData[7]+'"]',$statusSelect).attr("selected", "selected");
				}
				jqTds[7].innerHTML = $statusSelect[0].outerHTML;
				
				jqTds[8].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"'+ ((typeof(isNew) != "undefined") ? ' data-mode="new"' : '')+'><i class="fa fa-times"></i> Cancel</a>';
				
				$("input.date-picker-date-only", jqTds).datepicker({ format: 'mm/dd/yyyy' });
				$("input.date-picker", jqTds).datetimepicker({ format: 'HH:mm MM/DD/YYYY' });
				
				
				initRating(jqTds, false);
				
				$('[data-toggle="tooltip"]',nRow).tooltip();
			}
			
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
            }

            function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
				editRow(oTable, nRow, true);
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
				var jqTextarea = $('textarea', nRow);
				var jqSelects = $('select', nRow);

				//build data
				var rowData = {
					"id": nRow.id || -1,
					"listing" : jqInputs[0].value,
					"author_name": jqInputs[1].value + ' ' + jqInputs[2].value,
					"review": jqTextarea[0].value,
					"response": jqTextarea[1].value,
					"submit_date": jqInputs[3].value,
					"reservation": [jqInputs[4].value, jqInputs[5].value],
					"rating": jqInputs[6].value,
					"status": jqSelects.eq(0).val()
				}				
				
				function _callback(response){
					if (!response.errorMsg){
						var record = response.record;
						oTable.fnUpdate(record.listing, nRow, 0, false);
						oTable.fnUpdate(record.author_name, nRow, 1, false);
						oTable.fnUpdate(record.review, nRow, 2, false);
						oTable.fnUpdate(record.response, nRow, 3, false);
						oTable.fnUpdate(record.submit_date, nRow, 4, false);
						oTable.fnUpdate('<label class="reservation-start">'+record.reservation[0]+'</label><label class="reservation-end">'+record.reservation[1]+'</label>', nRow, 5, false);
						oTable.fnUpdate('<input type="number" value="'+record.rating+'" class="rating-input" />', nRow, 6, false);
						oTable.fnUpdate(record.status, nRow, 7, false);
						
						oTable.fnUpdate('<button type="button" class="btn btn-blue shiny btn-xs contact-reviewer tooltip-blue command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Contact Reviewer"><i class="fa fa-envelope-o"></i></button><button type="button" class="btn btn-purple shiny btn-xs request-moderation tooltip-blue command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Request Moderation"><i class="fa fa-asterisk"></i></button><a href="#" class="btn btn-palegreen shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a><button type="button" class="btn btn-warning shiny btn-xs clone tooltip-warning command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Clone"><i class="fa fa-copy"></i></button><button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>', nRow, 8, false);
						oTable.fnDraw();
						
						initRating(oTable, true);
						$('[data-toggle="tooltip"]',nRow).tooltip();						
					} else {
						Notify(response.errorMsg, 'bottom-right', '5000', 'danger', 'fa-times', true);
					}
				}				
				_options.saveUpdateHandler(rowData, _callback);
				
				function boolToStr(b){
					return b ? 'Yes' : 'No';
				}			
            }
        }

    };
	
	
}();

$(window).bind("load", function () {
	//Initialise dataTable
	var initOptions = {
		access: {
			edit: true,
			delete: true
		},		
		saveUpdateHandler: function(data, callback){
			var response = { record: data };
			//var response = { "id" : id, "errorMsg" : "Some Error!"}; // response with error 
			callback(response);
		},
		deleteHandler: function(id, callback){
			var response = { "id" : id };
			//var response = { "errorMsg" : "Some Error!"}; // response with error 
			callback(response);
		}
	}
	InitiateReviewsDataTable.init(initOptions);
});

function initReviewsPage(options){
	InitiateReviewsDataTable.init(options.reviewsTableOp);
}