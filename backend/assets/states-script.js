/**
* Javascript for manage-states.html
* Revision: 12
* Date: 10-08-2015
* Author: Alexander Bulei
*/

var InitiateStatesDataTable = function () {
    return {
        init: function (options) {
			var _options =  options || {};
			var $table = $('#statesdatatable');
			
			// check access
			controlActionBtnsAccess($table, _options.access);

            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumns" : [
                    { sWidth: '11%' },
                    { sWidth: '11%' },
                    { sWidth: '8%' },
                    { sWidth: '8%' },
                    { sWidth: '8%' },
                    { sWidth: '4%' },
                    { sWidth: '14%' }
                ],
                "bAutoWidth": false
            });

            //Add New Row
            $('#statesdatatable_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '', '', buildStatusDropDownHTML(),
                    '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;
                $('.publish-sign').closest('td').addClass('sign');
            });

            var isEditing = null;

			bindChangeStatusDropDown($('#statesdatatable'));
			bindClickActiveModalConfirm();
			bindClickInactiveModalConfirm();
			bindCancelStatusModal();

            //Edit row
            $('#statesdatatable').on("click", 'a.edit', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }
            });


            //Call Delete Modal passing data-id
            $('#statesdatatable').on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteModal').data('id', id).modal('show');
            });

            //
            //Delete Row if Confirmed
            $('#delete-btn').click(function () {
                var id = $('#deleteModal').data('id');
                var nRow = $('#'+id);
                oTable.fnDeleteRow(nRow);
                $('#deleteModal').modal('hide');
            });

            //Cancel Editing or Adding a Row
            $('#statesdatatable').on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Save an Editing Row
            $('#statesdatatable').on("click", 'a.save', function (e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
					
					var _canSave = true, $input;
					_check = function(erroMsg){
						var v = $input.val();
						if (( v == "") || (v == null)) {
							_canSave = _canSave && false;
							$input.addClass("required-error");
							_Error_(erroMsg);
						} else {
							$input.removeClass("required-error");
							_canSave = _canSave && true;
						}						
					}
					
					$input = $(".inline-state-country");
					_check('The Country Name is required field!');
					
					$input = $(".inline-state-name");
					_check('The State Name is required field!');

					$input = $(".inline-state-code");
					_check('The State Code is required field!');
					
					$input = $(".inline-state-tax");
					_check('The State Tax is required field!');

					$input = $(".inline-state-ship-code");
					_check('The State Ship Zone is required field!');
					
					if (_canSave){
						saveRow(oTable, isEditing);
						isEditing = null;
					}
                }
            });

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

				// status select
				var $statusSelect = $( $.parseHTML(aData[5])[0] );
				if ($statusSelect.find("select").length == 0){
					$statusSelect = buildStatusDropDown();
					$('option[value="'+aData[5]+'"]',$statusSelect).attr("selected", "selected");
				}				
				
                jqTds[0].innerHTML = '<select  class="form-control full-width inline-state-country" id="country" name="country" data-bv-field="country"><option value="">Select a country</option><option value="France">France</option> <option value="Germany">Germany</option> <option value="Italy">Italy</option> <option value="Japan">Japan</option><option value="Russian">Russian</option> <option value="United States">United States</option><option value="United Kingdom">United Kingdom</option><option value="other">Other</option></select>';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small full-width inline-state-name" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input type="text" class="form-control input-small full-width inline-state-code" value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<input type="text" class="form-control input-small full-width inline-state-tax" value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<input type="text" class="form-control input-small full-width inline-state-ship-code" value="' + aData[4] + '">';
                jqTds[5].innerHTML = $statusSelect[0].outerHTML;
                jqTds[6].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"><i class="fa fa-times"></i> Cancel</a>';
                $("#country").val(aData[0]);
            }
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
            }

            function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
				
				// status select
				var $statusSelect = $( $.parseHTML(aData[5])[0] );
				if ($statusSelect.find("select").length == 0){
					$statusSelect = buildStatusDropDown();
					$('option[value="'+aData[5]+'"]',$statusSelect).attr("selected", "selected");
				}
				
                jqTds[0].innerHTML = '<select style="width: 108px !important;" class="form-control extra-smll-2 inline-state-country" id="country" name="country" data-bv-field="country" tabindex="6"><option value="">Select a country</option><option value="France">France</option> <option value="Germany">Germany</option> <option value="Italy">Italy</option> <option value="Japan">Japan</option><option value="Russian">Russian</option> <option value="United States">United States</option><option value="United Kingdom">United Kingdom</option><option value="Other">Other</option></select>';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small extra-smll inline-state-name" style="width: 108px !important;" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input type="text" class="form-control input-small extra-smll-2 inline-state-code" style="width: 74px !important;" value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<input type="text" class="form-control input-small extra-smll-2 inline-state-tax" style="width: 74px !important;" value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<input type="text" class="form-control input-small extra-smll-2 inline-state-ship-code" style="width: 74px !important;" value="' + aData[4] + '">';
                jqTds[5].innerHTML =  $statusSelect[0].outerHTML;
                jqTds[6].innerHTML = aData[6];
                $("#country").val(aData[0]);
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('.form-control', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                oTable.fnUpdate(jqInputs[4].value, nRow, 4, false);
                oTable.fnUpdate($(jqInputs[5]).val(), nRow, 5, false);
                oTable.fnUpdate('<a href="#" class="btn btn-palegreen shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a><button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>', nRow, 6, false);
                oTable.fnDraw();
				$('[data-toggle="tooltip"]',nRow).tooltip();
            }
        }

    };
}();

$(window).bind("load", function () {
	/*Sets Themed Colors Based on Themes*/
	themeprimary = getThemeColorFromCss('themeprimary');
	themesecondary = getThemeColorFromCss('themesecondary');
	themethirdcolor = getThemeColorFromCss('themethirdcolor');
	themefourthcolor = getThemeColorFromCss('themefourthcolor');
	themefifthcolor = getThemeColorFromCss('themefifthcolor');

	//Initialise dataTable
	var opts = {
		access: {
			edit: true,
			delete: true
		}
	}	
	InitiateStatesDataTable.init(opts);
});
