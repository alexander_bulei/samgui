/**
* Javascript for manage-workflows.html
* Revision: 14
* Last Modified: 03-12-2015
* Author: Alexander Bulei
*/

var InitiateWorkflowDataTable = function () {
    return {
        init: function (options) {
			var _options =  options || {};
			var $table = $('#wfdatatable');
			var COLMAN = new COLMANAGER($table, _options.access.cols);
			COLMAN.init();
			
			// check access
			controlActionBtnsAccess($table, _options.access.actions);

            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                }                
            });

            //Add New Row
            $('#wfdatatable_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '', '', buildStatusDropDownHTML(),
                    '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;
			});

            //Clone row
            $('#wfdatatable').on("click", 'button.clone', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#cloneModal').data('id', id).modal('show');
            });

            $('#clone-btn').click(function () {
                var id = $('#cloneModal').data('id');
                var anSelected = $('#'+id);
                var data=[];
                $(anSelected).find('td').each(function(){data.push($(this).html());});
                var aiNew = oTable.fnAddData( data );
				COLMAN.proccessVisibility();
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                isEditing = nRow;
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                $('#cloneModal').modal('hide');
            });
            var isEditing = null;

			bindChangeStatusDropDown($('#wfdatatable'));
			bindClickActiveModalConfirm();
			bindClickInactiveModalConfirm();
			bindCancelStatusModal();

            //Edit row
            $('#wfdatatable').on("click", 'a.edit', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }
            });

            //Call Delete Modal passing data-id
            $('#wfdatatable').on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteModal').data('id', id).modal('show');
            });

            //
            //Delete Row if Confirmed
            $('#delete-btn').click(function () {

				function deleteCallback(response){
					var nRow = $('#' + response.id);
					oTable.fnDeleteRow(nRow);
				}				
				
				var id = $('#deleteModal').data('id');
				_options.deleteHandler(id, deleteCallback);
                $('#deleteModal').modal('hide');
            });

            //Cancel Editing or Adding a Row
            $('#wfdatatable').on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Save an Editing Row
            $('#wfdatatable').on("click", 'a.save', function (e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
                    if ($('#file').val() != "") {
                        saveRow(oTable, isEditing);
                        isEditing = null;
                    } else {
                        Notify('File Extension cannot be blank', 'bottom-right', '5000', 'danger', 'fa-times', true);
                    }
                }
            });
			
			function _buildInlineSelect(fnString, callback, param){
				var $select = $("<select class='form-control full-width'></select>");
				
				function _callback(data){					
					var arr = data.data || [];					
					$.each(arr, function(i,obj){
						$op = $("<option value='"+obj.id+"'>"+obj.name+"</option>");
						$select.append($op);
					});
					callback($select);
				}

				_options[fnString](function(response){
					_callback(response);
				}, param);
			}			
			
			function populateCategoryDrop(tdSite,tdCategory){
				var $tdSite = $(tdSite), 
					$tdCategory = $(tdCategory),
					siteId = $tdSite.data("value");
				
				_buildInlineSelect('getCategoryDataHandler', function($select){
					var currV = $tdCategory.data("value");
					
					$select.change(function(e){
						var v = $(this).val();
						$select.parent().data("value", v);
					});
					
					$('option[value="'+currV+'"]',$select).attr("selected", "selected");	
					
					$tdCategory.empty().append($select);
				}, { siteID: siteId } );
			}
			
            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
				
				if (COLMAN.haveAccess(jqTds[0])){
					jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
				}
				
				if (COLMAN.haveAccess(jqTds[1])){
					jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
				}
				
				if (COLMAN.haveAccess(jqTds[2])){
					_buildInlineSelect('getSiteDataHandler', function($select){
						var $td = $(jqTds[2]), currV = $td.data("value");
						$select.change(function(e){
							var v = $(this).val();
							$select.parent().data("value", v);
							
							// ATTACH CHANGE EVENT ONLY WHEN CATEGORY IS VISIBLE & EDITABLE
							if (COLMAN.haveAccess(jqTds[3])){
								populateCategoryDrop(jqTds[2],jqTds[3]);
							}
							
						});
						
						$('option[value="'+currV+'"]',$select).attr("selected", "selected");	
						$td.empty().append($select);
					});
				}
				
				if (COLMAN.haveAccess(jqTds[3])){
					populateCategoryDrop(jqTds[2],jqTds[3]);
				}
				
				if (COLMAN.haveAccess(jqTds[4])){
					var json = (aData[4].length) ? {users : [ { 'id' : $(aData[4]).data("val"), "name": $(aData[4]).text() } ]} : {};
					jqTds[4].innerHTML = "<a class='btn btn-azure shiny btn-xs task-extension-link' href='#' data-taskext-val='"+JSON.stringify(json)+"'><i class='fa fa-user'></i></a>";
				}
				
				if (COLMAN.haveAccess(jqTds[5])){
					// status select
					var $statusSelect = $( $.parseHTML(aData[5])[0] );
					if ($statusSelect.find("select").length == 0){
						$statusSelect = buildStatusDropDown();
						$('option[value="'+aData[5]+'"]',$statusSelect).attr("selected", "selected");
					}
					
					jqTds[5].innerHTML = $statusSelect[0].outerHTML;
				}

                jqTds[6].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"><i class="fa fa-times"></i> Cancel</a>';
								
				$(".task-extension-link", nRow).taskExtension({onlyUserSelect: true, singleUserSelect: true});
            }
			
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
            }

            function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
				
				if (COLMAN.haveAccess(jqTds[0])){
					jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
				}
				
				if (COLMAN.haveAccess(jqTds[1])){
					jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
				}
				
				if (COLMAN.haveAccess(jqTds[2])){
					_buildInlineSelect('getSiteDataHandler', function($select){
						var $td = $(jqTds[2]), currV = $td.data("value");
						$select.change(function(e){
							var v = $(this).val();
							$select.parent().data("value", v);
							
							// ATTACH CHANGE EVENT ONLY WHEN CATEGORY IS VISIBLE & EDITABLE
							if (COLMAN.haveAccess(jqTds[3])){
								populateCategoryDrop(jqTds[2],jqTds[3]);
							}
							
						});
						
						$('option[value="'+currV+'"]',$select).attr("selected", "selected");	
						$td.empty().append($select);
					});
				}
				
				if (COLMAN.haveAccess(jqTds[3])){
					populateCategoryDrop(jqTds[2],jqTds[3]);
				}
				
				if (COLMAN.haveAccess(jqTds[4])){
					jqTds[4].innerHTML = "<a class='btn btn-azure shiny btn-xs task-extension-link' href='#' data-taskext-val='{}'><i class='fa fa-user'></i></a>";
				}
				
				if (COLMAN.haveAccess(jqTds[5])){
					// status select
					var $statusSelect = $( $.parseHTML(aData[5])[0] );
					if ($statusSelect.find("select").length == 0){
						$statusSelect = buildStatusDropDown();
						$('option[value="'+aData[5]+'"]',$statusSelect).attr("selected", "selected");
					}
					jqTds[5].innerHTML = $statusSelect[0].outerHTML;
				}
				
				if (COLMAN.haveAccess(jqTds[6])){
					jqTds[6].innerHTML = aData[6];
				}
				
				$(".task-extension-link", nRow).taskExtension({onlyUserSelect: true, singleUserSelect: true});
            }

            function saveRow(oTable, nRow) {
				var jqTds = $('>td', nRow);
                var jqInputs = $('input', nRow);
				var jqSelect = $('select', nRow);
				var jqUser = $('.task-extension-link', nRow);
				
				var rowData = { "id": nRow.id || -1 }
				
				if (COLMAN.haveAccess(jqTds[0])){
					rowData.wfName = jqInputs[0].value;
				}
				
				if (COLMAN.haveAccess(jqTds[1])){
					rowData.wfCode = jqInputs[1].value;
				}
				
				if (COLMAN.haveAccess(jqTds[2])){
					rowData.siteId = jqSelect.eq(0).val();
					rowData.siteOutput = jqSelect.eq(0).find('option:selected').text();
				}
				
				if (COLMAN.haveAccess(jqTds[3])){
					rowData.categoryId = jqSelect.eq(1).val();
					rowData.categoryOutput = jqSelect.eq(1).find('option:selected').text();
				}
				
				rowData.user = { id: -1, name: "-" };
				if (COLMAN.haveAccess(jqTds[4])){
					var userData = jqUser.data('taskextVal');
					if ((userData) && (!$.isEmptyObject(userData))){
						var userId, userName;
						rowData.user.id = jqUser.data('taskextVal').users[0].id;
						rowData.user.name = jqUser.data('taskextVal').users[0].name;
					}
				}
				
				if (COLMAN.haveAccess(jqTds[5])){
					rowData.status = jqSelect.eq(2).val();
				}
				
				function _callback(response){
					if (response.wfName) {
						oTable.fnUpdate(response.wfName, nRow, 0, false);
					}
					if (response.wfCode) {
						oTable.fnUpdate(response.wfCode, nRow, 1, false);
					}
					if (response.siteId) {
						oTable.fnUpdate(response.siteOutput, nRow, 2, false);
						$(jqTds[2]).data("value", response.siteId);
					}
					if (response.categoryId) {
						oTable.fnUpdate(response.categoryOutput, nRow, 3, false);
						$(jqTds[3]).data("value", response.categoryId);
					}
					
					oTable.fnUpdate('<label data-val="'+response.user.id+'">'+response.user.name+'</label>', nRow, 4, false);
					
					if (response.status) {
						oTable.fnUpdate(response.status, nRow, 5, false);
					}

					var actionsHTML = "";
					if (_options.access.actions.task){
						actionsHTML += '<a href="manage-tasks.html" class="btn btn-blue shiny btn-xs tasks tooltip-blue command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Tasks"><i class="fa fa-upload"></i></a>';
					}
					if (_options.access.actions.edit){
						actionsHTML += '<a href="#" class="btn btn-palegreen shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>';
					}
					if (_options.access.actions.clone){
						actionsHTML += '<button type="button" class="btn btn-warning shiny btn-xs clone tooltip-warning command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Clone"><i class="fa fa-copy"></i></button>';
					}
					if (_options.access.actions.delete){
						actionsHTML += '<button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>';
					}
					
					oTable.fnUpdate(actionsHTML, nRow, 6, false);
					oTable.fnDraw();
					$('[data-toggle="tooltip"]',nRow).tooltip();
				}
				
				_options.saveUpdateHandler(rowData, _callback);
            }
        }

    };
}();

$(window).bind("load", function () {
	/*Sets Themed Colors Based on Themes*/
	themeprimary = getThemeColorFromCss('themeprimary');
	themesecondary = getThemeColorFromCss('themesecondary');
	themethirdcolor = getThemeColorFromCss('themethirdcolor');
	themefourthcolor = getThemeColorFromCss('themefourthcolor');
	themefifthcolor = getThemeColorFromCss('themefifthcolor');

	//Initialise dataTable
	var opts = {
		access: {
			actions: { edit: true, delete: true, clone: true, task: true, new: true },
			cols: { "name": "11", "code": "00", "site": "11", "category": "11", "user": "11", "status": "11", "actions": "1"}
		},
		saveUpdateHandler: function(data, callback){
			var response = data; //DEMO
			console.warn(response);
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}
		},
		deleteHandler: function(id, callback){
			var response = { "id" : id }; //DEMO
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}			
		},
		getSiteDataHandler : function(callback){
			//var response = { "errorMsg" : "Some Error!"}; // response with error 
			var response = {
				data: [
					{id: 1, name: "voli.com"},
					{id: 2, name: "lidl.com"},
					{id: 3, name: "t-mobile.com"}
				]
			};
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}			
		},
		getCategoryDataHandler: function(callback, siteId){
			//var response = { "errorMsg" : "Some Error!"}; // response with error 			
			var response = { data: [] };
			
			//DEMO 
			response.data.push({id: 1, name: "e-shop"});
			response.data.push({id: 2, name: "marketsm"});
			
			if (response.status == 'error'){
				_Error_(response.message);
			} else {
				callback(response);
			}	
		}		
	}	
	InitiateWorkflowDataTable.init(opts);
});

$('.last-input').focusout(function() {
	$('.first-input').focus();
});
$('button.close').click(function() {
	if ($("#editModal #fileExtension").val() == "") {
		Notify('File Extension cannot be blank', 'bottom-right', '5000', 'danger', 'fa-times', true);
	} else {
		$('#editModal').modal('hide');
	}
});
