/**
* Javascript for manage-languages.html
* Revision: 15
* Modified Date: 14-07-2015
* Author: Alexander Bulei
*/

var InitiateLanguagesDataTable = function () {
    return {
        init: function (options) {
			var _options =  options || {};
			var $table = $('#landatatable');
			
			// check access
			controlActionBtnsAccess($table, _options.access);

            //Datatable Initiating
            var oTable = $table.dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                }
            });

            //Add New Row
            $('#landatatable_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', buildStatusDropDownHTML(),
                    '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                editAddedRow(oTable, nRow);
				if (isEditing !== null && isEditing != nRow) {
					restoreRow(oTable, isEditing);
				}				
                isEditing = nRow;
                $('.publish-sign').closest('td').addClass('sign');
            });

            //Clone row
            $('#landatatable').on("click", 'button.clone', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#cloneModal').data('id', id).modal('show');
            });

            $('#clone-btn').click(function () {
                var id = $('#cloneModal').data('id');
                var anSelected = $('#'+id);
                var data=[];
                $(anSelected).find('td').each(function(){data.push($(this).html());});
                var aiNew = oTable.fnAddData( data );
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                isEditing = nRow;
                var n = (oTable.fnGetData().length + 1);
                nRow.id = n;
                $('.publish-sign').closest('td').addClass('sign');
                $('#cloneModal').modal('hide');
            });
            var isEditing = null;

			bindChangeStatusDropDown($('#landatatable'));
			bindClickActiveModalConfirm();
			bindClickInactiveModalConfirm();
			bindCancelStatusModal();
			
            //Edit row
            $('#landatatable').on("click", 'a.edit', function (e) {
                e.preventDefault();

                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }
            });

            $('#landatatable').on("click", 'button.view', function () {
                var id = $(this).closest('tr').attr('id');
                $('#editModal').data('id', id).modal('show');
            });

            //Call Delete Modal passing data-id
            $('#landatatable').on("click", 'button.delete', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#deleteModal').data('id', id).modal('show');
            });

            //
            //Delete Row if Confirmed
            $('#delete-btn').click(function () {
                var id = $('#deleteModal').data('id');
                var nRow = $('#'+id);
                oTable.fnDeleteRow(nRow);
                $('#deleteModal').modal('hide');
            });

            //Cancel Editing or Adding a Row
            $('#landatatable').on("click", 'a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Save an Editing Row
            $('#landatatable').on("click", 'a.save', function (e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
                    if ($('#file').val() != "") {
                        saveRow(oTable, isEditing);
                        isEditing = null;
                    } else {
                        Notify('File Extension cannot be blank', 'bottom-right', '5000', 'danger', 'fa-times', true);
                    }
                }
            });

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
				
				// status select
				var $statusSelect = $( $.parseHTML(aData[1])[0] );
				if ($statusSelect.find("select").length == 0){
					$statusSelect = buildStatusDropDown();
					$('option[value="'+aData[1]+'"]',$statusSelect).attr("selected", "selected");
				}
				
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
                jqTds[1].innerHTML = $statusSelect[0].outerHTML;
                jqTds[2].innerHTML = '<a href="#" class="btn btn-success btn-smll shiny btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning shiny btn-smll btn-xs cancel"><i class="fa fa-times"></i> Cancel</a>';

            }
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
				$('[data-toggle="tooltip"]',nRow).tooltip();
            }

            function editAddedRow(oTable, nRow) {
				nRow = checkHiddenCols(oTable,nRow);
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
				
				// status select
				var $statusSelect = $( $.parseHTML(aData[2])[0] );
				if ($statusSelect.find("select").length == 0){
					$statusSelect = buildStatusDropDown();
					$('option[value="'+aData[2]+'"]',$statusSelect).attr("selected", "selected");
				}
				
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
                jqTds[1].innerHTML = $statusSelect[0].outerHTML;
                jqTds[2].innerHTML = aData[3];
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
				var jqSelects = $('select', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqSelects.eq(0).val(), nRow, 1, false);
                oTable.fnUpdate('<button type="button" class="btn btn-blue shiny btn-xs dashboard tooltip-blue command-btn view" data-toggle="tooltip" data-placement="top" data-original-title="View"><i class="fa fa-external-link"></i></button><a href="#" class="btn btn-palegreen  shiny btn-xs edit tooltip-success command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a><button type="button" class="btn btn-warning shiny btn-xs clone tooltip-warning command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Clone"><i class="fa fa-copy"></i></button><button type="button" class="btn btn-danger delete btn-smll shiny btn-xs tooltip-danger command-btn" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>', nRow, 2, false);
                oTable.fnDraw();
				$('[data-toggle="tooltip"]',nRow).tooltip();
            }
        }

    };
}();

var InitiatePhraseDataTable = function () {
    return {
        init: function (options) {
			var _options =  options || {};
			$table = $("#phrasedatatable");
			
			// check access
			controlActionBtnsAccess($table, _options.access);
			
			function _buildInlineSelect(fn, callback, siteId){
				var $select = $("<select class='form-control full-width'></select>");
				
				if (typeof siteId != "undefined") { 
					$select.attr("data-siteid", siteId);
				}
				
				function _callback(data){					
					var arr = data.data || [];					
					$.each(arr, function(i,obj){
						$op = $("<option data-id='"+obj.id+"' value='"+obj.name+"'>"+obj.name+"</option>");
						$select.append($op);
					});
					callback($select);
				}
				
				args = [];
				if (typeof siteId != "undefined") { 
					args.push(siteId);
				}
				args.push(_callback);
				_options[fn].apply(this,args);
			}
			
			$table.on("click", 'button.clone', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $('#clonePhraseModal').data('id', id).modal('show');
            });

            $('#clone-extended-btn').click(function () {
                var id = $('#clonePhraseModal').data('id');
				var $tr = $("#" + id);
				var $newTr = $tr.clone(true);
				var siteVal = $tr.find(".site-select select").val();
				var categoryVal = $tr.find(".category-select select").val();
				$newTr.attr("id", (new Date().getTime()));
				$table.prepend($newTr);
				$newTr.find(".site-select select").find("option[value='"+siteVal+"']").attr("selected","selected");
				$newTr.find(".category-select select").find("option[value='"+categoryVal+"']").attr("selected","selected");
                $('#clonePhraseModal').modal('hide');
            });		

			_buildInlineSelect('getSiteDataHandler', function($select){						
				var tds = $(".site-select");
				$(".site-select").each(function(i, td){
					var $td = $(td);
					var selVal = $td.data("selected");
					$td.append($select[0].outerHTML);
					if (selVal) {
						$select = $td.find('select');
						$select.find('option[value="'+selVal+'"]').attr("selected","selected").trigger("change");
					}
				});
				
				$(".site-select").find('select').on("change", function(){
					var $this = $(this);
					var idx = this.selectedIndex;
					var siteId = $this.find("option:eq("+idx+")").data("id");
					_buildInlineSelect('getCategoryDataHandler', function($select){
						var $catTd = $this.closest('td.site-select').next();
						$catTd.empty().append($select[0].outerHTML);
						var selVal = $catTd.data("selected");
						if (selVal) {
							$select = $catTd.find('select');
							$select.find('option[value="'+selVal+'"]').attr("selected","selected");
						}						
					}, siteId);
				}).trigger("change");
				
			});

		}
	}
}();

$(window).bind("load", function () {
	/*Sets Themed Colors Based on Themes*/
	themeprimary = getThemeColorFromCss('themeprimary');
	themesecondary = getThemeColorFromCss('themesecondary');
	themethirdcolor = getThemeColorFromCss('themethirdcolor');
	themefourthcolor = getThemeColorFromCss('themefourthcolor');
	themefifthcolor = getThemeColorFromCss('themefifthcolor');

	var opts = {
		access: {
			edit: true,
			delete: true
		}
	}	
	InitiateLanguagesDataTable.init(opts);
	
	//Initialise dataTable
	var initOptions = {
		access: {
			edit: true,
			delete: true
		},		
		saveUpdateHandler: function(data, callback){
			var response = { record: data };
			//var response = { "id" : id, "errorMsg" : "Some Error!"}; // response with error 
			callback(response);
		},
		deleteHandler: function(id, callback){
			var response = { "id" : id };
			//var response = { "errorMsg" : "Some Error!"}; // response with error 
			callback(response);
		},
		getSiteDataHandler : function(callback){
			//var response = { "errorMsg" : "Some Error!"}; // response with error 
			var response = {
				data: [
					{id: -1, name: "Select"},
					{id: 0, name: "NC Tourism"},
					{id: 1, name: "Dummy"}
				]
			};
			callback(response);
		},
		getCategoryDataHandler: function(siteId,callback){
			//var response = { "errorMsg" : "Some Error!"}; // response with error 			
			var response = { data: [] };
			
			//DEMO 
			if (siteId == 0){
				response.data.push({id: -1, name: "Select"});
				response.data.push({id: 0, name: "Vacation Rental"});
				response.data.push({id: 1, name: "Dummy"});
			} else if (siteId == 1){
				response.data.push({id: -1, name: "Select"});
				response.data.push({id: 0, name: "Dummy 1"});
				response.data.push({id: 1, name: "Dummy 2"});
				response.data.push({id: 2, name: "Dummy 3"});
				response.data.push({id: 3, name: "Vacation Rental"});
			}
			callback(response);
		}
	}	
	InitiatePhraseDataTable.init(initOptions);
});

$('.last-input').focusout(function() {
	$('.first-input').focus();
});