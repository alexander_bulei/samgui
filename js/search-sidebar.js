/**
 * Project: NCTourism
 * Description: Search Sidebar Slider 
 * Last Modified: 12-06-2015
 * Author: Alexander Bulei (alexander.bulei@gmail.com)
**/
 
var _timer;
function initSidebar(onFilterCallback){
	
	function _internalFilterCallback(){
		var filterCallback = $.isFunction(onFilterCallback) ? onFilterCallback : $.noop;
		
		function stringToArrayInt(string){
			var temp = string.split(',');
			for (a in temp ) {
				temp[a] = parseInt(temp[a], 10);
			}
			return temp;
		}
		
		var bedroomsVal = $(".slider-input-bedrooms").val();
		var bathroomsVal = $(".slider-input-bathrooms").val();
		var sleepsVal = $(".slider-input-sleeps").val();
		
		var filter = { 
			cities: [],
			bedrooms: stringToArrayInt(bedroomsVal),
			bathrooms: stringToArrayInt(bathroomsVal),
			sleeps: stringToArrayInt(sleepsVal),
			amentities: []
		};
		
		$(".search-sidebar-cities-towns").find('input[type="checkbox"]:checked:not([data-isfilter="false"])').each(function(i, check){
			filter.cities.push( $(check).data("id") );
		});
		
		$("#amenitiesTab").find('input[type="checkbox"]:checked').each(function(i, check){
			filter.amentities.push( $(check).data("id") );
		});		
		
		filterCallback(filter);
	}
	
	nc.get('demo/cities-towns.json', true, function(result){
		var $body = $(".search-sidebar-cities-towns");
		var $ul = $('<ul>');
		
		$.each(result, function(i, obj){
			var li = '<li data-search="'+obj.name+'"><div class="checkbox checkbox-success"><input data-id="'+obj.id+'" id="search-sidebar-cities-towns-chk-'+obj.id+'" type="checkbox" data-place="'+obj.name+'"><label for="search-sidebar-cities-towns-chk-'+obj.id+'"> '+obj.name+'</label></div></li>';
			$ul.append(li);
		});		
		$body.empty().append($ul);
		
		// search panel
		var html = '<div class="row no-margins"><input type="text" class="form-control search-sidebar-cities-towns-filter input-sm" placeholder="Enter a city name or partial name"/><div class="checkbox checkbox-info" style="display:inline-block;margin:7px 10px;"><input data-isfilter="false" type="checkbox" id="search-sidebar-cities-towns-csens"><label for="search-sidebar-cities-towns-csens">Case-sensitive</label></div></div><div class="row no-margins"><a href="javascript:void(0)" class="search-sidebar-cities-towns-filter-clear">Clear all city filters</a></div>';
		$body.append(html);
		
		var $filterInput = $(".search-sidebar-cities-towns-filter", $body);
		var $list = $body.find("ul");
		var $caseCheck =  $("#search-sidebar-cities-towns-csens");
		
		$caseCheck.on("change", _doFilter);
		
		function _doFilter(){
			var csensitive = $caseCheck[0].checked;
			var v = $filterInput.val();
			var searchFor = csensitive ? v : v.toLowerCase();
			
			$list.find("li").each(function(i,li){
				var $li = $(li);
				var str = csensitive ? $li.data("search") : $li.data("search").toLowerCase();
				if (str.indexOf(searchFor) > -1) $li.show(); else $li.hide();
			});			
		}
		
		$filterInput.on("change keyup", function(e){
			_doFilter();
		});
		
		$('.search-sidebar').delegate('input[type="checkbox"]:not([data-isfilter="false"])','change', function(){
			_internalFilterCallback();
		});
		
		$body.find(".search-sidebar-cities-towns-filter-clear").click(function(e){
			$list.find('li input[type="checkbox"]').each(function(i,checkbox){
				checkbox.checked = false;
			});
			$filterInput.val("").trigger("change");
			_internalFilterCallback();
		});
	});
	
	//bedrooms
	$(".slider-input-bedrooms").jRange({
		from: 0,
		to: 20,
		step: 1,
		format: '%s',
		width: 440,
		showLabels: false,
		isRange : true,
		theme: 'theme-blue',
		onDragEnd: function(){
			_internalFilterCallback();	
		},
		onstatechange: function(v){
			var arr = v.split(',');
			$(".label-for-bedrooms").text(arr[0] + ' - ' + arr[1]);			
		}
	});
	//bathrooms
	$(".slider-input-bathrooms").jRange({
		from: 0,
		to: 10,
		step: 1,
		format: '%s',
		width: 440,
		showLabels: false,
		isRange : true,
		theme: 'theme-blue',
		onDragEnd: function(){
			_internalFilterCallback();	
		},
		onstatechange: function(v){
			var arr = v.split(',');
			$(".label-for-bathrooms").text(arr[0] + ' - ' + arr[1]);
		}
	});	
	//sleeps
	//bathrooms
	$(".slider-input-sleeps").jRange({
		from: 1,
		to: 36,
		step: 1,
		format: '%s',
		width: 440,
		showLabels: false,
		isRange : true,
		theme: 'theme-blue',
		onDragEnd: function(){
			_internalFilterCallback();	
		},
		onstatechange: function(v){
			var arr = v.split(',');
			$(".label-for-sleeps").text(arr[0] + ' - ' + arr[1]);
		}
	});

	$(".sb-clearall").on("click", function(e){
		$(".search-sidebar input[type='checkbox']").each(function(i, input){
			$(input).removeAttr("checked");
		});
		$(".slider-input-bedrooms").jRange("setValue","1,20");
		$(".slider-input-bathrooms").jRange("setValue","1,10");
		$(".slider-input-sleeps").jRange("setValue","1,36");
		_internalFilterCallback();
	});
	
	//init hidden
	var $wrapper = $(".filter-container");
	$(".filter-content",$wrapper).css("visibility","hidden");
	$wrapper.css("left", "-454px");
	$(".close-btn i").removeClass("fa-times").addClass("fa-search");

	$wrapper.on("mouseleave", function(e){
		if ($wrapper.hasClass('openned')){
			_timer = setTimeout(function(){
				$wrapper.animate({
					left: "-=454"
				}, 700, function() {
					$(".filter-content",$wrapper).css("visibility","hidden");
					$(".right-invoker",$wrapper).show();
					$(".close-btn").find("i").addClass("fa-search").removeClass("fa-times");
					$(".search-sidebar").css("background", "transparent");
				});
				$wrapper.removeClass('openned');
			},2000);
		}
	}).on("mouseenter", function(e){		
		if (typeof _timer != "undefined"){
			clearTimeout(_timer);
		}
	});

	function _open(e){
		var $close = $("a.close-btn",".search-sidebar");
		var $body = $(".search-sidebar");
		if ($wrapper.hasClass("openned")){			
			$wrapper.stop().animate({
				left: "-=454"
			}, 700, function() {
				$(".filter-content",$wrapper).css("visibility","hidden");
				$(".right-invoker",$wrapper).show();
				$close.find("i").addClass("fa-search").removeClass("fa-times");
				$body.css("background", "transparent");
			});
			$wrapper.removeClass('openned');
		} else {			
			$body.css("background", "#FFF");
			$(".filter-content",$wrapper).css("visibility","visible");
			$(".right-invoker",$wrapper).hide();
			
			$close.find("i").removeClass("fa-search").addClass("fa-times");
			$wrapper.stop().animate({
				left: "+=454"
			}, 700, function() {
				
			});
			$wrapper.addClass('openned');			
		}
	}
	
	$(".right-invoker").on("mouseover", _open);
	$("a.close-btn",".search-sidebar").on("click", _open);
	
}