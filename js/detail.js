/**
 * Project: NCTourism
 * Page: detail.html
 * VERSION: 08062016.1
 * Author: Alexander Bulei (alexander.bulei@gmail.com)
**/

$(function(){
  $('[data-toggle="tooltip"]').tooltip();

  SmoothScroll({ stepSize: 100 });

  $(window).on('ncmmenu.beforeInit', function(event, mmInst){
    mmInst._settings.secondaryPages = false;
  });

  $(window).on('ncmmenu.afterInit', function(event, mmInst){
    mmInst.dom.$mainContainer.addClass('nc-megamenu-gradient');
  });
    
  // includes
  nc.include('tmpl/header.html','begin');
  nc.include('tmpl/footer.html');
  nc.include('tmpl/login.html');
    
  if (__PAGE.slideshow){
    var $slider = $("#detail-main-slider");
    $.each(__PAGE.slideshow, function(i, image){
        $slider.append('<li><div data-thumb="'+image+'" style="background-image:url('+image+')"></div></li>');
    });
  }
  
  $('.convert-svg').shapeSvgConvert();
    
  $('#detail-main-slider').responsiveSlides({
      auto:true,
      pager: true,
      thumb: 'bg',
      thumbSrc: 'thumb'
  });

  $(".nct-flex-slider").flexslider({
      maxItems: 5,
      itemWidth: 200,
      animation: "slide",
      slideshow: false,
      customDirectionNav: $("#detail-slider-nav a"),
      start: function(){
        $(".nct-flex-slider").css("visibility","visible");
      }
  }); 
    
  $('.detail-main-middle-tab-nav-list .detail-main-middle-tab-nav').click(function(e){
      e.preventDefault();
      var $this = $(this), $tab = $( $this.data('tab') );
      $('.detail-main-middle-tab-nav-list a.active').removeClass('active');
      $this.addClass('active');
      $('.detail-main-middle-tab-content .detail-main-middle-tab.active').removeClass('active');
      $tab.addClass('active');
  });
  
  function adjustSliderResponsive(){
    var wW = $(window).width();
    var sliderInstance = $(".nct-flex-slider").data('flexslider');
    var $container = $(".detail-main-highlight-slider");
    if (wW < 500){
      sliderInstance.vars.maxItems = 1;
      sliderInstance.vars.itemWidth = 0;
      sliderInstance.vars.itemMargin = 0;
      sliderInstance.vars.controlNav = false;
    } else {
      var max = Math.floor($container.width() / 210);
      sliderInstance.vars.maxItems = (max < 5 ? max : 5);
      sliderInstance.vars.itemWidth = 200;
      sliderInstance.vars.itemMargin = 10;
      sliderInstance.vars.controlNav = true;
    }
    
    sliderInstance.resize();
  }
  adjustSliderResponsive();
  
  function winResizeEnd(){
    adjustSliderResponsive();
  }
  
  var resizeTimer;
  $(window).on("resize", function(e){
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(winResizeEnd, 100);    
  });
  
    /** MAP **/
    var $map = $('#detail-main-map'), $mapFilter = $("#detail-main-map-filter");
    var mapOps = { mapTypeId:google.maps.MapTypeId.ROADMAP, streetViewControl:false, zoom: 11 };
        
    if (__PAGE.map && __PAGE.map.center){
        mapOps.center = new google.maps.LatLng(__PAGE.map.center[0],__PAGE.map.center[1]);
    }
        
    var mapInstance = new google.maps.Map($map[0],mapOps), _MARKERS = [];
  var infowindow = new google.maps.InfoWindow();
    
    if (__PAGE.map && __PAGE.map.data){
        $.each(__PAGE.map.data, function(i, catObj){
            $mapFilter.append('<option value="'+i+'" selected>'+catObj.filterName+'</option>');
            
            if (catObj.markers){
                $.each(catObj.markers, function(j, markerObj){
                    
                    var _marker = new google.maps.Marker({
                        position: new google.maps.LatLng(markerObj.latLng[0],markerObj.latLng[1]),
                        map: mapInstance,
                        category: i,
                        icon : catObj.marker,
            data: {
              imageUrl: markerObj.imgUrl,
              title: markerObj.title || "",
              placeId: markerObj.placeId,
              placeInTrip: markerObj.placeInTrip || false,
              placeFav: markerObj.placeFav || false
            }
                    });           
          
          google.maps.event.addListener(_marker, 'click', function() {
            var content = '<div class="iw-body"><div class="iw-body-image-wrapper"><div style="background-image:url('+_marker.data.imageUrl+')"></div></div><h5>'+_marker.data.title+'</h5><div class="iw-body-footer"><a data-placeid="'+_marker.data.placeId+'" class="iw-body-fav" href="#">';
            if (_marker.data.placeFav){
              content += '<i class="fa fa-heart"></i> Remove from Favorites</a>';
            } else {
              content += '<i class="fa fa-heart-o"></i> Add to Favorites</a>';
            }
            
            if (_marker.data.placeInTrip){
              content += '<a class="iw-body-suitcase iw-body-suitcase-in" href="#" data-placeid="'+_marker.data.placeId+'"><i class="fa fa-suitcase"></i> Already in Your Trip</a>';
            } else {
              content += '<a class="iw-body-suitcase" href="#" data-placeid="'+_marker.data.placeId+'"><i class="fa fa-suitcase"></i> Add to Your Trip</a>';
            }           
            content += '</div>';
            
            infowindow.setContent(content);
            infowindow.open(mapInstance, this);
          });         

                    _MARKERS.push(_marker);
                });
            }

        });
    }
    
    $mapFilter.change(function(){
        var category = $(this).val()

        if (!_MARKERS.length){
            return false;
        }
        for (i = 0; i < _MARKERS.length; i++) {
            marker = _MARKERS[i];
            if ($.inArray(marker.category.toString(), category) > -1) {
                marker.setVisible(true);
            }
            else {
                marker.setVisible(false);
            }
        }        
    }).multiselect();
    
  if (__PAGE.ticketPricing){
    var $place = $(".detail-main-middle-tab-tickets-body").empty();
    $.each(__PAGE.ticketPricing, function(i){
      var $element = $('<div class="detail-main-middle2-line"><label>'+this.title+'&nbsp;:&nbsp;</label><span>'+this.price+'</span></div>');
      if (this.note){
        $element.append('<div class="detail-main-middle2-note2">Note: '+this.note+'</div>');
      }
      $place.append($element);
    });
  }
  $("#detail-main-add").click(function(e){e.preventDefault();})
  $("#detail-main-add").ncTripAdder({
    title: "Choose Your List(s)",
    serverUrl: 'demo/trip.adder.lists.json',
    saveCallback: function(data){
      console.log(data);
    }
  });
  
});    