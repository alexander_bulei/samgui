/**
 * Project: NCTourism
 * Page: blank.html
 * Last Modified: 24-12-2015
 * Author: Alexander Bulei (alexander.bulei@gmail.com)
**/
 
$(function(){
	// includes
	nc.include('tmpl/header.html','begin');
	nc.include('tmpl/footer.html');
	nc.include('tmpl/login.html');
})
