var _LI_DATA_PROP = 'imagedata';
var _LI_DATA_ATTR_PROP = 'data-imagedata';
$(function(){
	
	if (typeof NCIM == "undefined"){
		
		NCIM = {
			_cropper: null,
			_options: {
				uploadUrl: "uploader-url-here.php",
				additionalParamsOnUpload: [],
				buttons: {
					enable: true,
					remove: true,
					edit: true,
					link: true,
					caption: true
				},
				onready: function(){}
			},
			_$container: null,
			reinit: function(data){
				this._$container.find(".ncim-list").remove();
				this._$container.find(".ncim-upload-row").remove();
				this.buildLayout(data);
				this.binds();
			},
			error: function(type){
				var msg;
				switch (type){
					case 1 : 
						msg =  'Images must be at least 400px by 300px.';
						break;
					case 2 : 
						msg =  'This file is not in the proper format.  It must be jpg or png format.';
						break;
					case 3 : 
						msg =  'The maximum file size for an image is 5 MB.  Please reduce your file size and try again.';
						break;
						
				}
				toastr.options = {
				  "closeButton": true,
				  "debug": false,
				  "newestOnTop": false,
				  "progressBar": false,
				  "positionClass": "toast-bottom-right",
				  "preventDuplicates": false,
				  "onclick": null,
				  "showDuration": "300",
				  "hideDuration": "1000",
				  "timeOut": "5000",
				  "extendedTimeOut": "1000",
				  "showEasing": "swing",
				  "hideEasing": "linear",
				  "showMethod": "fadeIn",
				  "hideMethod": "fadeOut"
				}				
				toastr["error"](msg);
			},
			init: function($container, data, options){
				this._options = $.extend(true, this._options, options || {});
				this._$container = $container;
				var self = this;
				nc.include("tmpl/image-manager.html", null, function(){
					self.buildLayout(data);
					self.binds();
					self.initDz();
					self._options.onready();
				});
			},
			destroy: function(){
				this._$container.find(".ncim-list").remove();
				this._$container.find(".ncim-upload-row").remove();				
			},
			previewImage: function($img){
				var $modal = $("#ncimPreviewPicModal");
				$modal.find(".ncimPreviewImage").attr("src",$img.attr("src"));
				$modal.modal("show");
			},
			insertElement: function(imageData, $ul){
				var self = this;
				var _$ul = $ul ? $ul : $(".ncim-list");
				imageData = nc.mapListingImagesToFEModel(imageData);
				var $li = $("<li>").attr(_LI_DATA_ATTR_PROP, JSON.stringify(imageData));
				var $divCtrl = $("<div class='ncim-controls'>");

				if (self._options.buttons.enable){
					var $btnView = $("<button class='btn btn-view'><i class='fa fa-check-circle-o'></i></button>").attr({
							"data-toggle":"tooltip",
							"data-placement":"right" 							
						});
					if (imageData.disabled){
						$btnView.attr("title","Enable this image").addClass("btn-danger");
					} else {						
						$btnView.attr("title","Disable this image").addClass("btn-success");
					}
					$btnView.on("click", function(){
						var $this = $(this);
						if ($this.hasClass('btn-danger')){
							$this.removeClass('btn-danger').addClass('btn-success');
						}else{
							$this.addClass('btn-danger').removeClass('btn-success');
						}
					});
					$divCtrl.append($btnView);					
				}

				if (self._options.buttons.edit){
					var $btnEdit = $("<button class='btn btn-im-image-btn btn-primary'><i class='fa fa-pencil-square-o'></i>").attr({
							"data-toggle":"tooltip",
							"data-placement":"right",
							"title": "Edit image"
						});
					$btnEdit.on("click", function(e){						
						var $modal = $("#imageUploadCropModal");
						var $li = $(this).closest("li");
						var data = $li.data(_LI_DATA_PROP);
						data['$li'] = $li;
						$modal.data("mode", "edit");
						$modal.data(_LI_DATA_PROP, data);						
						$modal.modal("show");
					});

					$divCtrl.append($btnEdit);						
				}					
				
				if (self._options.buttons.link){
					var $btnUrl= $("<button class='btn btn-im-image-btn btn-success'><i class='fa fa-link'></i>").attr({
							"data-toggle":"tooltip",
							"data-placement":"right",
							"title": "Manage Link"
						});
					$btnUrl.on("click", function(e){
						var $modal = $("#ncimSimpleEditorModal");
						$modal.find(".modal-title").text("Add | Edit Picture url");
						$modal.find("div.form-group.url").show();
						$modal.find("div.form-group.caption").hide();
						var $li = $(this).closest("li");
						var data = $li.data(_LI_DATA_PROP);
						$("#ncim-imgurl").val(data.imgurl || "");
						data['$li'] = $li;
						$modal.data(_LI_DATA_PROP, data);
						$modal.data("mode","url");
						$modal.modal("show");
					});

					$divCtrl.append($btnUrl);						
				}
				
				if (self._options.buttons.caption){
					var $btnCaption = $("<button class='btn btn-img-caption btn-im-image-btn btn-info'><i class='fa fa-font'></i>").attr({
							"data-toggle":"tooltip",
							"data-placement":"right",
							"title": "Manage Caption"
						});
					$btnCaption.on("click", function(e){
						var $modal = $("#ncimSimpleEditorModal");
						$modal.find(".modal-title").text("Add | Edit Picture Caption");
						$modal.find("div.form-group.url").hide();
						$modal.find("div.form-group.caption").show();
						var $li = $(this).closest("li");
						var data = $li.data(_LI_DATA_PROP);
						$("#ncim-img-caption").val(data.imgcaption || "");
						data['$li'] = $li;
						$modal.data(_LI_DATA_PROP, data);
						$modal.data("mode","caption");
						$modal.modal("show");
					});

					$divCtrl.append($btnCaption);						
				}				
				
				if (self._options.buttons.remove){
					var $btnRemove = $("<button class='btn btn-remove btn-im-image-btn btn-danger'><i class='fa fa-times'></i>").attr({
							"data-toggle":"tooltip",
							"data-placement":"right",
							"title": "Remove this image"
						});
						
					$btnRemove.on("click", function(){
						$(this).closest("li").remove();
					});
					$divCtrl.append($btnRemove);
				}				
								
				$divCtrl.appendTo($li);
				
				var $a = $("<a>").attr({
					"href": "javascript:void(0)",
					"data-imgid" : imageData.id,					
					"class" : imageData.disabled ? "ncim-disabled" : ""
				}).on("click", function(){
					var $img = $(this).find("img");
					self.previewImage($img);
				});
				var $img = $("<img/>").attr({
						"src": imageData.src,
						"class" : "aspect-fit"
				}).appendTo($a);
				$li.append($a);
				_$ul.append($li);
				$img.parent().imgLiquid({fill:true});
				return $li;
			},
			buildLayout: function(data){
				var self = this;
				var $ul = $("<ul class='ncim-list'>");
				var _arr = data || [];
				$.each(_arr, function(i, imgData){
					self.insertElement(imgData,$ul);
				});
				self._$container.append(
					$("<div class='ncim-upload-row'><button class='btn btn-info ncim-upload-btn'>Upload new picture</button></div>")
				);
				self._$container.append($ul);
				$('[data-toggle="tooltip"]',self._$container).tooltip();
				var sortable = Sortable.create($ul[0]);
			},
			binds: function(){
				var self = this;
				var $modal = $("#imageUploadCropModal");
				$(".ncim-upload-btn").on("click", function(){
					$modal.data("mode", "new");
					$modal.modal("show");
				});
				
				$modal.on("show.bs.modal", function(){
					var mode = $modal.data("mode");
					if (mode == 'new'){
						$(".imageUploadCrop-dz").show();
						$(".cropper-container").remove();
					} else {
						$(".imageUploadCrop-dz").hide();
					}
				});
				
				$modal.on("shown.bs.modal", function(){
					var mode = $modal.data("mode");
					if (mode != 'new'){
						var _data = $modal.data(_LI_DATA_PROP);
						self.initCropper(_data, true);
					}
				});
				
				if (self._options.buttons.link){
					$("#ncim-urlsave").click(function(e){						
						var $modal = $("#ncimSimpleEditorModal");
						var mode = $modal.data("mode");
						var data = $modal.data(_LI_DATA_PROP);
						var canClose = true;
						if (mode == 'url'){
							v = $("#ncim-imgurl").val();
							
							function addhttp(url) {
							   if (!/^(f|ht)tps?:\/\//i.test(url)) {
								  url = "http://" + url;
							   }
							   return url;
							}							
							
							data.imgurl = addhttp(v);
							data.$li.data("imgurl", data);
						} else {
							v = $("#ncim-img-caption").val();
							data.imgcaption = v;
							data.$li.data("imgcaption", data);
						}
						
						if (canClose) {
							$modal.modal("hide");
						}
					});
				}
				
				$(".save-btn", $modal).on("click", function(){
					var mode = $modal.data("mode");					
					var canvas = self._cropper.cropper("getCroppedCanvas");
					var saveData = {
						originSrc: self._cropper.attr("src"),
						src: canvas.toDataURL(),
						cropdata: self._cropper.cropper("getData")
					}
					
					if (self._options.saveCallback) {

						self._options.saveCallback(saveData, function(response){
							var $li, _data_;
							if (mode == 'new'){
								_data_ = response;
								$li = self.insertElement(response);
								_data_.$li = $li;
							} else {
								_data_ = $.extend(true, $modal.data(_LI_DATA_PROP), response);
								var $img = _data_.$li.find('img')
								$img.attr("src", response.src);
								$img.parent().imgLiquid({fill:true});
							}
							_data_.$li.data(_LI_DATA_PROP, _data_);
						});
					}					
					$modal.modal("hide");
				});
			},
			initDz: function(){
				var self = this;
				var $modal = $("#imageUploadCropModal");
				var $form = $modal.find('.imageUploadCrop-dz form');
				$form.attr("action", self._options.uploadUrl);
				var myDropzone = new Dropzone("#imageUploadCrop-dzone",{
					maxFiles: 1
				});
				
				myDropzone.on("maxfilesexceeded", function(file){
					this.removeFile(file);
				});				
				
				myDropzone.on('sending', function(file, xhr, formData){
					var _array = self._options.additionalParamsOnUpload;
					$.each(_array, function(i, data){
						formData.append(data.name, data.value);
					});
				});				
				myDropzone.on("success", function(file, response) {
					$(".imageUploadCrop-dz").hide();
					response = JSON.parse(response);
					self.initCropper(response.filePath); // url to new picture
				});				
				
			},
			buildCropperControls: function(){
				var html = '<div class="row row-cropper-controls"><div class="col-xs-4 col-md-4 col-lg-4"><div class="btn-group cropper-controls" role="group"><button type="button" class="btn btn-primary cropper-zoom-in"><i class="fa fa-search-plus"></i> Zoom In</button><button type="button" class="btn btn-primary cropper-zoom-out"><i class="fa fa-search-minus"></i> Zoom Out</button><button type="button" class="btn btn-primary cropper-reset"><i class="fa fa-refresh"></i> Reset</button></div></div><div class="col-xs-3 col-md-3 col-lg-3"><div class="input-group"><input type="text" class="form-control cropper-rotate-input" placeholder="Degree" value="45"><span class="input-group-btn"><button class="btn btn-primary cropper-rotate-btn" type="button">Rotate</button></span></div></div></div>';
				var $modal = $("#imageUploadCropModal");
				var $controls = $(".row-cropper-controls", $modal);
				if (!$controls.length){
					$modal.find(".modal-body").prepend(html);
				}
			},
			initCropper: function(data, editing){
				var self = this;
				var _data = $.isPlainObject(data) ? data : { src: data };
				var _src = (editing) ? _data.originSrc : _data.src;
				var html = '<div class="row crop-row"><div class="col-md-12 col-lg-12"><div class="cropper-container"><img src="'+_src+'"></div></div></div>';
				var $modal = $("#imageUploadCropModal");
				$modal.find(".modal-body").find(".crop-row").remove();
				$modal.find(".modal-body").append(html);
				var cropper_opts = {
					aspectRatio: 16 / 9,					
					resizable: false
				}
				if ((editing) && (_data.cropdata)){
					cropper_opts['built'] = function(){
						self._cropper.cropper("setData", _data.cropdata);
					}
				}

				this._cropper = $('.cropper-container > img', $modal).cropper(cropper_opts);
				self._cropper.cropper("setData", _data.cropdata);
				
				// bind cropper controls
				self.buildCropperControls();
				var $controls = $(".cropper-controls",$modal);
				$("button.cropper-zoom-in",$controls).click(function(e){
					self._cropper.cropper("zoom", 0.1);
				});
				$("button.cropper-zoom-out",$controls).click(function(e){
					self._cropper.cropper("zoom", -0.1);
				});
				$("button.cropper-reset",$controls).click(function(e){
					self._cropper.cropper("reset");
				});
				$("button.cropper-rotate-btn",$modal).click(function(e){
					var $input = $(".cropper-rotate-input",$modal);
					var v = $input.val();
					if (v == "") { return; }
					if (v > 360) v = 360;
					if (v < -360) v = -360;					
					self._cropper.cropper("rotate", v);
				});				
			}
		}
		
	}
	
})