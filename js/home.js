/**
 * Project: NCTourism
 * Page: index.html
 * Version: 20052016.1
 * Author: Alexander Bulei (alexander.bulei@gmail.com)
**/

function responsiveAdjust(){
  var winHeight = $(window).height();
  $('header').css({
    "height":  winHeight - $("#topbar").height()
  });
}
 
 $(function(){
  var ABIMGLOADER = new ABPRELOADER({debug: true});
 
  SmoothScroll({ stepSize: 100 });
    
	nc.megamenu.create({
		suitcaseCounter: 15,
		$place: $("#megamenu-place"),
		json: "demo/megamenu.json"
	});
	
	schemeManeger.loadColorScheme("demo/fronend-color-scheme.json");
	$(".admin-top-navbar").show();
	
	responsiveAdjust();
	$(window).on("resize", function(e){
		responsiveAdjust();
	});

	$("#headerBackSlider").responsiveSlides();
	
	/* INIT DATEPICKERS */	
	initDatePickers($('input.datepicker.checkin'), $('input.datepicker.checkout'));
	
	$('input.datepicker.checkin').on('dp.show', function(e){
		$(".container-header").addClass("on-top");
	});
	
	$('input.datepicker.checkout').on('dp.show', function(e){
		$(".container-header").addClass("on-top");
	});
	
	$('input.datepicker.checkin').on('dp.hide', function(e){
		$(".container-header").removeClass("on-top");
	});
	
	$('input.datepicker.checkout').on('dp.hide', function(e){
		$(".container-header").removeClass("on-top");
	});	
	
	var sharedOptions = {
		loop:true,
		items: 5,
		lazyLoad : true,
		autoHeight: false,		
		autoHeightClass: 'owl-height',
		nav: true,
		navText: [
			"<div class='owl-nav-layout small'><i class='fa fa-chevron-left'></i></div>",
			"<div class='owl-nav-layout small'><i class='fa fa-chevron-right'></i></div>"
		],		
		responsive: {
			0: { items: 3 },
			600:  { items: 5 },
			1000: { items: 6 },
			1400: { items: 7 },
			1800: { items: 8 }
		}
	}
	
	function addFeaturedItemsToSlider($slider, obj, itemCss){
		var _class = (typeof itemCss != "undefined") ? itemCss : "";
		
		$.each(obj, function(i, item){
			
			var dataTmpl = {
				roomPrice: item.roomPrice,
				period: item.period,
				hotelName: item.hotelName,
				location: item.location,
				bedroom: item.bedRooms,
				guests: item.guests,
				bathrooms: item.bathRooms
			}
				
			var $html = $.tmpl("featuredTmpl", dataTmpl);
			
			$slider.append($html);
			
			var $li = $slider.find('.single-slide:last').parent();
			var $thumbSlider = $li.find('.thumb-slider')
			if ($.isArray(item.images) && (item.images.length) ){
				$.each(item.images, function(i, imageObj){
          
          var $slide = $('<li><a></a></li>');
          
          $slide.find('a').attr({
            style: "height:150px",
            class: "bg-centered",
            title: imageObj.alt || "",
            href: "javascript:void(0)"
          });
          
          if (i == 0){
            $slide.find('a').css({"background-image": "url("+imageObj.src+")" }).addClass("img-loaded");
          } else {
            ABIMGLOADER.addSourceOnly(imageObj.src, function(source){
              $slide.find('a').css({"background-image": "url("+source+")" }).addClass("img-loaded");
            });
          }
         $thumbSlider.append($slide);
				});
			}
			
			var liData = {
				"contentLoaded": false,
				"rating": item.rate
			}
			$li.attr('data-featured', JSON.stringify(liData) );
		});
		
    ABIMGLOADER.process();
    
		var $thumbSliders = $slider.find('.thumb-slider');
		
		$thumbSliders.each(function(i, slider){
			var _$slider = $(slider);			
			_$slider.responsiveSlides({
				speed: 800,
				pause: true,
				defaultPaused: true,
				timeout: 3000
			});		
		})
	}
	
	function addItemsToSlider($slider, obj, itemCss){
		var _class = (typeof itemCss != "undefined") ? itemCss : "";
		$.each(obj, function(i, item){
      var $slide = $('<div class="item '+_class+'"><a></a></div>');
      
      $slide.find('a').attr({
        style: "height:150px",
        class: "bg-centered",
        title: item.alt || "",
        href: item.url || "javascript:void(0)"
      });
      
      if (i == 0){
        $slide.find('a').css({"background-image": "url("+item.src+")" }).addClass("img-loaded");
      } else {
        ABIMGLOADER.addSourceOnly(item.src, function(source){
          $slide.find('a').css({"background-image": "url("+source+")" }).addClass("img-loaded");
        });
      }
      $slider.append($slide);
		});
    ABIMGLOADER.process();
	}	
  
	//featured slider
	nc.get('demo/data.json', true, function(data){
		var _arr = (data) ? data : [];
		var $slider = $(".featured-slider");
		addFeaturedItemsToSlider($slider,_arr, 'featured-item');
		$slider.owlCarousel({
			itemsMobile: false,
			autoplay:true,
			autoplayTimeout:4000,
			autoplayHoverPause:true,
			stagePadding: 50,
			loop:true,
			items: 5,
			nav: true,
			navText: [
				  "<div class='owl-nav-layout'><i class='fa fa-chevron-left'></i></div>",
				  "<div class='owl-nav-layout'><i class='fa fa-chevron-right'></i></div>"
			],
			responsive: {
				0: { items: 1 },
				600: { items: 5 },
				1000: { items: 6 },
				1400: { items: 7 },
				1800: { items: 8 }
			}
		});
	});
	
    function enlarge() {
		var $owlItem = $(this);
		var $li = $owlItem.children('li:first');
		var data = $li.data('featured');
		
		if (!$owlItem.hasClass("hover")){
			$(".owl-prev,.owl-next").css("height",$li.outerHeight());
			$(".header-feature-row").css({
				"bottom": $li.outerHeight(),
				"position": "absolute"
			});
		}
		
		if (!data.contentLoaded){
			var $inputRating = $owlItem.find('input.rating');
			
			$inputRating.val(parseFloat(data.rating));
			$inputRating.rating({
				size : 'xxs',
				showCaption: false,
				showClear: false,
				readonly: true
			});
		}
		
		 $li.find('.featured-detail').removeClass('hidden');
		
        $(this).addClass("hover");
    }

	
    function reset_enlarged() {			
      var $this = $(this);
      $(".owl-prev,.owl-next").css("height","100%");
      $(".header-feature-row").css({
        "bottom": "auto",
        "position": "relative"
      })		
      $this.removeClass("hover").find('.featured-detail').addClass('hidden');
    }
	

    $(".featured-slider").hoverIntent({
        over: enlarge,
        out: reset_enlarged,
        selector: ".owl-item"
    });
	
	$(".thumb-slider").responsiveSlides({
        speed: 800,
        pause: true,
        DefaultPaused: true,
        timeout: 3000
    });
	
	// get items & init "places to stay" slider
	nc.get('demo/places.json', true, function(data){
		var _arr = (data && data.items) ? data.items : [];
		var $slider = $("#placesToStay-slider");
		addItemsToSlider($slider,_arr);
		$slider.owlCarousel( $.extend(true,sharedOptions,{autoplayTimeout:1000}) );		
	});
	
	// get items & init "where to eat" slider
	nc.get('demo/places.json', true, function(data){
		var _arr = (data && data.items) ? data.items : [];
		var $slider = $("#whereToEat-slider");
		addItemsToSlider($slider,_arr);
		$slider.owlCarousel( $.extend(true,sharedOptions,{autoplayTimeout:1200}) );
	});	

	// get items & init "things to do" slider
	nc.get('demo/places.json', true, function(data){
		var _arr = (data && data.items) ? data.items : [];
		var $slider = $("#thingsToDo-slider");
		addItemsToSlider($slider,_arr);
		$slider.owlCarousel( $.extend(true,sharedOptions,{autoplayTimeout:1400}) );
	});	
	
	// get items & init "cities & regions" slider
	nc.get('demo/places.json', true, function(data){
		var _arr = (data && data.items) ? data.items : [];
		var $slider = $("#citiesRegions-slider");
		addItemsToSlider($slider,_arr);
		$slider.owlCarousel( $.extend(true,sharedOptions,{autoplayTimeout:1600}) );
	});	
	
	wall = new freewall("#latest-news-events");
	wall.reset({
		selector: '.news-item',
		cellW: 200,
		cellH: 'auto',
		onResize: function() {
			wall.fitHeight();
			execDelay(function(){
				wall.fitWidth();
			},100);
		}
	});
	
	// includes	
	nc.include('tmpl/login.html');

	// define template for featured item
	var _TMPL_DEFAULT = '<li><div class="single-slide"><ul class="thumb-slider"></ul><div class="price-block featured-detail hidden"><strong>${roomPrice}</strong> ${period}</div><div class="row no-margins row-location featured-detail hidden"><div class="col-xs-6 col-md-6 col-lg-6 text-left"><label class="text-blue" style="font-size:14px;text-transform:uppercase;"><strong>${hotelName}</strong></label></div><div class="col-xs-6 col-md-6 col-lg-6 text-right"><label style="line-height: 20px;"><i class="fa fa-map-marker"></i> ${location}</label></div></div><div class="row no-margins row-info featured-detail hidden"><div class="col-xs-4 col-md-4 col-lg-4 col-rating"><input type="number" class="rating"/></div><div class="col-xs-8 col-md-8 col-lg-8 no-padding text-right"><div class="info-div"><i class="fa fa-bed"></i> ${bedroom} Bedroom</div><div class="info-div"><i class="fa fa-users"></i> ${guests} Guests</div><div class="info-div"><i><img src="img/shower.png"></i> ${bathrooms} Bathrooms</div></div></div></div></li>';
	
	
	$.template( "featuredTmpl" , _TMPL_DEFAULT );	
	
	execDelay(function(){
		$(window).trigger('resize');
	},100);
		
	$("input.header-input.vacation").on("focus", function(e){
		$(".search-submenu").slideDown();
	}).on("blur", function(){
		$(".search-submenu").slideUp();
	});

	function _hideWrapper(){
		$("#home-all-area-expand-panel-wrapper").hide(1, function(){
			$('body').off("click.close-all-area-panel");
		});	
	}
	
	$(".btn-featured-dd").click(function(){
		var $this = $(this);
		var $panel = $("#home-all-area-expand-panel");
		var $wrapper = $("#home-all-area-expand-panel-wrapper");

		$wrapper.css({
			top: $this.offset().top - 200,
			left: $this.offset().left
		}).show(1, function(){
			$('body').on("click.close-all-area-panel", function(e){
				var $target = $(e.target);
				if ($target.closest('#home-all-area-expand-panel').length == 0){
					_hideWrapper();
				}
			})
		});
	});
	
	var $input = $("#home-all-area-ga");
	var ALL_AREA_GA = new google.maps.places.Autocomplete($input[0],{ types: [] });
	
	google.maps.event.addListener(ALL_AREA_GA, 'place_changed', function() {
		var place = ALL_AREA_GA.getPlace();
		if (place){
			$("#home-all-area-show-all").closest('.row').show();
			$('.btn-featured-dd').html('Selected Areas <span class="caret"></span>');
		}
	});
	
	$("#home-all-area-show-all").click(function(){
		$("#home-all-area-ga").val("");
		$(this).closest('.row').hide();
		$('.btn-featured-dd').html('All Areas <span class="caret"></span>');
		_hideWrapper();
	});

 });
 
 var wall;