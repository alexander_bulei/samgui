/**
 * Project: NCTourism
 * Page: Front-End Room Editor for property-single.html
 * Last Modified: 03-05-2016
 * Version: 2.3
 * Author: Alexander Bulei (alexander.bulei@gmail.com)
**/
var RECSSCLASS = 'room-editor-element';

function initOnce(){
	var $modal = $("#addEditRoomModal");
	
	/*GROUP-ROOM-VIEW-MODE*/
	var _TMPL_GROUP = '<div class="room-container"><div class="title-color room-name">${name}</div><div class="title-color room-type">Type: ${room_type}</div><ul class="amentities-list"></ul><div class="room-note">${note}</div></div>';
	$.template("view_group", _TMPL_GROUP);
	
	/*GROUP-ROOM-EDIT-MODE*/
	_TMPL_GROUP = '<div class="room-container"><div class="title-color room-name">${name} <a class="room-edit-btn" href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i></a> <a href="javascript:void(0)" class="room-delete-btn"><i class="fa fa-times"></i></a></div><div class="title-color room-type">Type: ${room_type}</div><div class="amentities-list"></div><div class="room-note">${note}</div></div>';
	$.template("edit_group", _TMPL_GROUP);
	
	/*RECORD*/
	_TMPL_ = '<div data-object="${obj}" class="re-record-row" data-record="${id}"><div class="row row-panel-title padding15"><div class="col-xs-12 col-md-12 col-lg-12 no-padding"><div class="inline-block sub-title title-color">${title}</div><div class="inline-block pull-right room-editor-edit-btn-div"><a href="javascript:void(0)" class="btn btn-xs only-for-users btn-roomeditor-edit-btn" data-type="group" data-mode="view"><i class="fa fa-pencil-square-o"></i> Edit</a></div></div><span class="arrow-bottom"></span></div><div class="row padding15 roomeditor-body"></div></div>';
	$.template("record", _TMPL_);

	$("#aerm-save",$modal).click(function(e){
		var obj = $modal.data("NCROOMEDITOR");
		obj.save();
	});

	$("#aerm-delete",$modal).click(function(e){
		var workdata = $modal.data("workdata");
		var obj = $modal.data("NCROOMEDITOR");
		obj.showConfirm('Are you sure you want to delete this area?', function(e){
			if (workdata.main.type == "single"){
				obj.removeSingle(workdata)
			} else {
				obj.removeGroupItem(workdata);
			}			
			$modal.modal("hide");
		},$.noop);		
		
	});

	$('body').on('click', function (e) {
		$('a.room-delete-btn').each(function () {
			if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
				$(this).popover('hide');
			}
		});
	});
	
	var prev_val;
	$('#aerm-room-type', $modal).focus(function(){
		prev_val = $(this).val();
	}).change(function(e){
		var self = $(this);
		self.blur();
		var obj = $modal.data("NCROOMEDITOR");
		var mode = $modal.data("mode");
		
		function _process(){
			
			var data = $modal.data("workdata");
			
			var amentityType = self.val();
			obj.prepareModal(amentityType,'edit', function(){
				obj.fillModal(mode, data);	
			});
		}
		
		if (mode == "edit"){
			obj.showConfirm('If you change the Amenity type, you will loose the available amenities below you have already selected.  Are you sure you want to continue?', function(e){
				_process();
			},function(){
				self.val(prev_val);
			});
		} else {
			_process();
		}
	});
	
}

(function ( $ ) {
	initOnce();
	$.fn.NCROOMEDITOR = function(options){
		var $element = $(this).addClass(RECSSCLASS), obj = { $element: $element };
		var defaults = {
			admin: false,
			mode: "view",
			onInit: $.noop,
			onReady: $.noop,
			saveCallback: $.noop,
			deleteCallback: $.noop
		}
		var _opts = obj.options = $.extend({},defaults,options);
		obj.$modal = $("#addEditRoomModal");
		
		obj.setSingleMode = function($section, mode){
			obj.mode = mode;
			var $editBtn = $(".btn-roomeditor-edit-btn", obj.$element);
			var $newBtn = $(".btn-roomeditor-addnew-btn", obj.$element);
			if (mode == "edit"){
				$editBtn.data("mode","edit").addClass("btn-success").html('<i class="fa fa-floppy-o"></i> Save');
				$newBtn.show();
				$(".room-name", obj.$element).each(function(i,title){
					var $title = $(title);
					if (!$title.find('.room-edit-btn').length){
						var $a = $('<a class="room-edit-btn" href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i></a> <a href="javascript:void(0)" class="room-delete-btn"><i class="fa fa-times"></i></a>');
						$title.append($a);
					}
				});				
			} else {
				$newBtn.hide();
				$editBtn.data("mode","view").removeClass("btn-success").html('<i class="fa fa-pencil-square-o"></i> Edit');
				$("#aerm-room-name",obj.$modal).val("");
				$("#aerm-room-note",obj.$modal).val("");
				obj.restore();
			}
		}
		
		obj.setGroupMode = function($section, mode){
			obj.mode = mode;
			var $editBtn = $(".btn-roomeditor-edit-btn",$section);
			var $section = $editBtn.closest('.re-record-row');
			var $el = $section.find('.btn-roomeditor-addnew-btn');
			if (mode == "edit"){
				if (!$el.length){
					$section.find('.sub-title').empty().prepend('<button class="btn btn-nc-blue btn-xs only-for-users btn-roomeditor-addnew-btn" data-type="group"><i class="fa fa-plus"></i> Add New</button>');
				}
				
				$editBtn.data("mode","edit").addClass("btn-success").html('<i class="fa fa-floppy-o"></i> Save');
				$(".room-name", $section).each(function(i,title){
					var $title = $(title);
					if (!$title.find('.room-edit-btn').length){
						var $a = $('<a class="room-edit-btn" href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i></a> <a href="javascript:void(0)" class="room-delete-btn"><i class="fa fa-times"></i></a>');
						$title.append($a);
					}
				});
			} else {
				$el.remove();
				$editBtn.data("mode","view").removeClass("btn-success").html('<i class="fa fa-pencil-square-o"></i> Edit');
				$("#aerm-room-name",obj.$modal).val("");
				$("#aerm-room-note",obj.$modal).val("");
				obj.restore();
			}
		}
		
		obj.updateModal = function(mode, title){
			var caption;
			if (mode == "edit"){
				caption = "Edit ";
				obj.$modal.find("#aerm-delete").show();
			} else {
				caption = "Add ";
				obj.$modal.find("#aerm-delete").hide();
			}
			caption += title;						
			obj.$modal.find("h4.modal-title").text(caption);
		}
		
		obj.buildGroup = function(data, $roomList, editmode){
			var $li = $('<li class="re-room-item">').attr("data-roomid", data.id);
			var roomData = {
				name: data.name,
				note: data.note,
				room_type : data.amentityType
			}
			var _editmode = editmode ? editmode : false;
			var html;
			if (_editmode){
				html = $.tmpl("edit_group", roomData);
			}else{
				html = $.tmpl("view_group", roomData);
			}
			
			var $list = html.find(".amentities-list");
			var _amentities = data.amentities || [];
			$.each(_amentities, function(i,amentity){
				$div = $("<li>").attr({
					"data-id" : amentity.id,
					"class": "room-amentity-row ncae-collapsed",
					"data-qtd": amentity.qtd || 1
				});
				var _text = amentity.name + ((amentity.qtd > 1) ? " (" + amentity.qtd + ")" : "");
				$div.append('<div>'+_text+'</div>');
				if (amentity.description){
					var $desc = $('<div class="ncae-amentity-desc"></div>');
					$desc.append(amentity.description);
					$div.append($desc);
					$div.append('<a class="ncae-amentity-desc-lessmore" data-mode="collapsed" style="display:none;" href="javascript:void(0)">...more</a>');
				}
				$list.append($div);
			});
			$li.append(html);
			$li.data("roomdata", data);

			if ($roomList) {
				$roomList.append($li);
			} else {
				return $li;
			}
		}
		
		obj.buildSingle = function(data, $list){
			$list.closest(".re-record-row").find('.room-name').text(data.name);
			var _amentities = data.amentities || [];
			$.each(_amentities, function(i,amentity){
				var $li = $("<li>").attr({
					"data-id" : amentity.id,
					"class": "room-amentity-row ncae-collapsed",
					"data-qtd": amentity.qtd || 1
				});
				var _text = amentity.name + ((amentity.qtd > 1) ? " (" + amentity.qtd + ")" : "");
				$li.append('<div>'+_text+'</div>');
				if (amentity.description){
					var $desc = $('<div class="ncae-amentity-desc"></div>');
					$desc.append(amentity.description);
					$li.append($desc);
					$li.append('<a class="ncae-amentity-desc-lessmore" data-mode="collapsed" style="display:none;" href="javascript:void(0)">...more</a>');
				}
				$list.append($li);
			});
			$list.data("roomdata",data);
		}		
		
		obj.restore = function(){
			$(".room-name", obj.$element).each(function(i,title){
				var $title = $(title);
				$title.find('a.room-edit-btn').remove();
				$title.find('a.room-delete-btn').remove();
			});			
		}
		
		obj.removeSingle = function(object){
			obj.$element.find("*[data-record='"+object.main.id+"']").remove();
			obj.options.deleteCallback(object);
		}
		
		obj.removeGroupItem = function(object){
			obj.$element.find("li[data-roomid='"+object.item.id+"']").remove();
			obj.options.deleteCallback(object);
		}

		obj.save =  function(){
			var workdata =  obj.$modal.data("workdata");
			var mode = obj.$modal.data("mode");
			var _name = ((workdata.main.type == "single") && (mode == "new")) ?  $.trim($("#aerm-room-type option:selected", obj.$modal).text()) : $.trim($("#aerm-room-name", obj.$modal).val());
			workdata.item.name = _name;
			workdata.item.amentityType = $.trim($("#aerm-room-type", obj.$modal).val());
			workdata.item.note = $.trim($("#aerm-room-note", obj.$modal).val());
			workdata.item.amentities = [];
			
			$lis = $(".aerm-amentities-list li",obj.$modal);
			var _arr = [];
			$.each($lis, function(i,li){
				var $li = $(this);
				if ($li.find("input[type='checkbox']")[0].checked){
					var qtd = $li.find("input[type='text']").val();
					var _d = {
						id : $li.data("amentity"),
						qtd: (qtd) ? qtd : 1,
						name: $li.find("label.aerm-amentity-label").text(),
						description: $li.find(".aerm-desc-wrapper input").val()
					}
					workdata.item.amentities.push(_d);
				}
			});
			
			if (workdata.main.type == "group"){
				var $li = obj.buildGroup(workdata.item,null,true);
				if (mode == 'edit'){
					workdata.$section.replaceWith($li);
					obj.checkAmentitiesDesc();
				} else {
					workdata.$section.find('.rooms-list').append($li);
				}
				obj.setGroupMode(obj.$element, 'edit');
			} else {
				if (mode == 'edit'){
					var $list = workdata.$section.find('.amentities-list');
					$list.empty();
					obj.buildSingle(workdata.item, $list);
				} else {
					obj.insertSingle(workdata.item);
				}
				obj.setSingleMode(workdata.$section,'edit');
			}
			
			var saveObj = {
				groupType : workdata.main.type,
				itemId: workdata.item.id,
				amentityType: workdata.item.amentityType,
				name: workdata.item.name,
				amentities: workdata.item.amentities
			}
			
			if (saveObj.groupType == "single") {
				saveObj.groupId = workdata.main.id;
			} else {
				saveObj.note = workdata.item.note;
			}
			
			obj.options.saveCallback(saveObj);
			
		}
		
		obj.showEditor = function(data, mode){
			mode = mode ? mode : 'edit';
			obj.updateModal(mode, data.main.title);
			obj.$modal.data("NCROOMEDITOR", obj);
			obj.prepareModal(data.item.amentityType, mode, function(){
				obj.fillModal(mode, data);
				
				if (data.main.type == "single"){
					function _checkInput(){
						var maxChars = 200
							remaining = maxChars - $(this).val().length,
							$span = $(this).next();
						$span.text('('+ remaining +' remaining characters)');
					}
					$(".aerm-desc-wrapper input", obj.$modal).on("change keyup", _checkInput).trigger("change");						
					
				}
				
				var $top = obj.$modal.find('.row-aerm-header');
				$top.show();
				if (data.main.type == "single"){
					if (mode == "edit") {
						$top.hide();
					} else {
						$top.find('div:first').hide();
						$top.find('div:first').next().removeClass("col-md-6 col-lg-6").addClass("col-md-12 col-lg-12");
					}
					
				} else {
					$top.find('div:first').next().removeClass("col-md-12 col-lg-12").addClass("col-md-6 col-lg-6");
					$top.find('div:first').show();
				}
				
				var $roomType = $('#aerm-room-type').empty();
				nc.get(obj.options.amentityTypesUrl, true, function(result){
					$.each(result, function(i, record){
						$roomType.append('<option value="'+record.value+'">'+record.name+'</option>');
					});
					if (mode == 'edit'){
						$roomType.find('option[value="'+data.item.amentityType+'"]').attr("selected","selected");
					} else	{
						if (data.main.type == "single"){
							data.$section.find("div.re-record-row").each(function(i, divRow){
								var aType = $(divRow).data("object").amentityType;
								var $element = $roomType.find('option[value="'+aType+'"]');
								if ($element.length){
									$element.attr("disabled","disabled");
								}
							});
						}
					}
				});
				
				obj.$modal.data("workdata", data);
				obj.$modal.data("mode", mode);
				obj.$modal.modal("show");				
				
			});
		}
			
		obj.checkAmentitiesDesc = function(){
			$(".ncae-amentity-desc:visible", obj.$element).each(function(i, div){
				if (div.offsetHeight < div.scrollHeight || div.offsetWidth < div.scrollWidth) {
					$(div).parent().find(".ncae-amentity-desc-lessmore").show();
				}										
			});				
		}
		
		obj.insertRow = function(data){
			var temp = {id: data.id, title: data.title, type: data.type};
			
			if (data.type == "single"){
				temp.amentityType = data.amentityType;
			}
			
			var data = { 
				id: data.id, 
				title: data.title,
				obj: JSON.stringify(temp)
			};
			var $html = $.tmpl("record", data);
			
			if (obj.options.single){
				$html.find('div.room-editor-edit-btn-div').remove();
			}
			
			return {
				$html: $html,
				$container: $html.find(".roomeditor-body"),
				$roomList: $('<ul class="rooms-list">') 
			}
		}
		
		obj.insertGroup = function(data){
			var object = obj.insertRow(data);
			
			$.each(data.items, function(i,room){
				obj.buildGroup(room,object.$roomList);
			});
			object.$container.append(object.$roomList);
			
			obj.$element.append(object.$html);			
			obj.$element.find('.sub-title').empty();
			if (!obj.options.admin){
				object.$html.find('.only-for-users').remove();
			}
		}
		
		obj.insertSingle = function(data){
			var object = obj.insertRow(data);
			var $list = $('<ul class="amentities-list"></ul>');
			object.$container.append($list);
			object.$html.find('.sub-title').addClass("room-name");
			object.$container.addClass("er-view-type2");
			obj.$element.append(object.$html);
			
			//HIDE: TYPE & NOTES
			object.$html.find(".room-type").hide();
			object.$html.find(".room-note").hide();
			
			obj.buildSingle(data,$list);
			
			if (!obj.options.admin){
				object.$html.find('.only-for-users').remove();
			}			
		}		
		
		obj.showConfirm = function(msg, confirmFn, cancelFn){
			var $modal = $("#confirmRoomModal");
			$modal.find('.modal-body').html(msg);
			$modal.find("#confirmRoomModal-yes").off('click.btnConfirm').on('click.btnConfirm', confirmFn);
			var _cancelFn = function(){
				cancelFn();
				$modal.modal("hide");
			}
			$modal.find("#confirmRoomModal-no").off('click.btnCancel').on('click.btnCancel', _cancelFn);
			$modal.modal("show");
		}
		
		obj.binds = function(){
			obj.$element.delegate("a.room-edit-btn", "click", function(e){
				var $this = $(this);
				var $recordRow = $this.closest(".re-record-row");
				var $item = $this.closest("li.re-room-item");
				var json = $recordRow.data("object");
				var o = {
					main: json,
					$section: (json.type == "group") ? $item : $recordRow,
					item: (json.type == "group") ? $item.data("roomdata") : $recordRow.find(".amentities-list").data("roomdata")
				};
				obj.showEditor(o);
			});
			
			$('body').delegate('button.ncae-delete-prompt-cancel', 'click', function(e){
				$(this).closest(".popover").data("bs.popover").$element.popover("hide");
			});
			
			$('body').delegate('button.ncae-delete-prompt-confirm', 'click', function(e){
				var $this = $(this), $popover = $this.closest(".popover");
				$popover.data("bs.popover").$element.popover("hide");
				var $row = $popover.data("bs.popover").$row;
				var main = $row.data("object");
				var _data_ = { main: main};
				if (main.type == "single"){
					obj.removeSingle(_data_)
				} else {
					var d = $popover.data("bs.popover").$item.data("roomdata");
					_data_.item = d;
					obj.removeGroupItem(_data_);
				}
			});					
			
			obj.$element.delegate("a.room-delete-btn", "click", function(e){
				var $this = $(this);
				// close all
				var $popover = $this.popover({
					html : true, 
					title: "",
					container: "body",
					content: function() {
						var $html = $("<div>"),$div, $button;
						$("<label>Do you really want delete this item?</label>").appendTo($html);
						
						$div = $("<div>").css("text-align", "right").addClass("ncae-delete-prompt-btns")
						
						$button = $("<button class='btn btn-default btn-xs ncae-popover ncae-delete-prompt-cancel'>Cancel</button>").appendTo($div);
						$button = $("<button class='btn btn-danger btn-xs ncae-popover ncae-delete-prompt-confirm'>Delete</button>").appendTo($div);
						
						$html.append($div);
						return $html.html();
					},
					
				}).data('bs.popover').tip();
				
				$this.data('bs.popover').$row = $this.closest(".re-record-row");
				$this.data('bs.popover').$item = $this.closest(".re-room-item");
				$popover.addClass('ncae-delete-prompt');
				$this.popover('show');
				
				/*
				var $li = $(this).closest("li");
				$li.remove();
				*/						
			});
			
			obj.$element.delegate("a.ncae-amentity-desc-lessmore", "click", function(e){
				var $this = $(this);
				var mode = $this.data("mode");
				if (mode == 'collapsed') {
					$this.closest('.room-amentity-row').removeClass("ncae-collapsed").addClass("ncae-expanded");
					$this.data("mode", "expended");
					$this.text('...less');
				}else{
					$this.closest('.room-amentity-row').removeClass("ncae-expanded").addClass("ncae-collapsed");
					$this.data("mode", "collapsed");
					$this.text('...more');
				}
			});
			
			obj.$element.delegate("a.btn-roomeditor-edit-btn", "click", function(e){
				var $this = $(this), mode = $this.data("mode"),
					sectionType = $this.data("type"), nMode = (mode != 'edit') ? 'edit' : 'view',
					$section = $this.closest('.re-record-row'), json = $section.data("object");
				
				if (sectionType == "single"){
					obj.setSingleMode($section,nMode);
				} else {
					obj.setGroupMode($section, nMode);
				}
				
					/*
					var o = {
						main: json,
						item : $section.find(".amentities-list").data("roomdata"),
						$section: $section
					}					
					obj.showEditor(o);
					*/				
			});
			
			obj.$element.delegate(".btn-roomeditor-addnew-btn", "click", function(e){
				var $this = $(this);
				var type = $this.data("type");

				var itemData = { 
					id: new Date().getTime(), 
					name: "",
					amentityType: "",
					note: "",
					amentities: []
				}
				
				var o = { item : itemData }
				
				obj.$modal.find("#aerm-room-name").val("");
				obj.$modal.find("#aerm-room-type").val("");
				obj.$modal.find(".aerm-room-amentities-container ul").remove();
				
				if (type == "group") {
					var $section = $this.closest('.re-record-row');
					var json = $section.data("object");
					itemData.id = json.id + '-' + itemData.id;
					o.main = $section.data("object");
					o.$section = $section;
				} else {
					o.$section = $this.closest('.room-editor-element');
					o.item.type = "single";
					o.main = {
						title: "Room/Area",
						type: "single"
					};
				}
				obj.showEditor(o, 'new');
			});

		}
		
		obj.prepareModal = function(aType, mode, callback){
			var params = aType ? { type: aType } : {};
			var $body = obj.$modal.find(".aerm-room-amentities-container");
			$body.find("ul").remove();
			var fn = callback ? callback : $.noop;
			if (mode != "new"){
				nc.get(obj.options.amentitiesUrl, true, function(response){
					var arr = response.amentities || [];
					var $list = $("<ul class='aerm-amentities-list'>"), $li, $checkbox, $spinner, $label;
					$.each(arr, function(i, amentity){
						$li = $("<li>").attr("data-amentity", amentity.id);
						var _id = "checkbox_" + amentity.id;
						$labelWrapper = $("<div class='aerm-amentity-label-wrapper'><div><label class='aerm-amentity-label'></label></div></div>");
						$checkbox = $('<div class="aerm-checkbox-wrapper"><div class="checkbox checkbox-success"><input type="checkbox"><label></label></div></div>');
						$spinner = $('<div class="aerm-spinner-wrapper"><div class="input-group spinner"><input type="text" value="0" class="form-control"><div class="input-group-btn-vertical"><button class="btn btn-success" type="button"><i class="fa fa-caret-up"></i></button><button class="btn btn-danger" type="button"><i class="fa fa-caret-down"></i></button></div></div></div>');
						$checkbox.find("input").attr({ "name": _id, "id": _id});
						
						$labelWrapper.find("label").text(amentity.name).attr("for", _id);
						$description = '<div class="aerm-desc-wrapper"><input class="form-control" /><span>(200 remaining characters)</span><div>';
						$li.append($checkbox);						
						$li.append($spinner);
						$li.append($labelWrapper);
						$li.append($description);
						
						$list.append($li);
						
						$checkbox.find('input[type="checkbox"]').on("change", function(e){
							var $input = $(this).closest('li').find('.aerm-spinner-wrapper input');
							if (!this.checked) {
								$input.val("0");
							} else {
								$input.val("1")
							}
						});
					});
					$body.append($list);
					fn();
				}, params);				
			} else {
				fn();
			}
		}
		
		obj.fillModal = function(mode,data){
			if (mode == 'edit'){
				var $container = data.$section, v;
				
				//title
				v = $.trim($container.find(".room-name").text());
				$("#aerm-room-name").val(v);
				
				//amentities
				$.each(data.item.amentities, function(i, amentity){
					var $li = $("li[data-amentity='" + amentity.id + "']", obj.$modal);
					$li.find("input[type='checkbox']").attr("checked","checked");
					$li.find("input[type='text']").val(amentity.qtd || 1);
					if ((data.main.type == "single") && (amentity.description)){
						var $input = $li.find(".aerm-desc-wrapper input");
						$input.val(amentity.description);
					}
				});
				
				//note
				if (data.main.type == "group"){
					v = $container.find(".room-note").text();
					$("#aerm-room-note").val(v);
				}
			}
	
			if (data.main.type == "single"){			
				$(".row-aerm-room-note", obj.$modal).hide();
				$(".aerm-desc-wrapper", obj.$modal).show();
			} else {
				$(".row-aerm-room-note", obj.$modal).show();
				$(".aerm-desc-wrapper", obj.$modal).hide();
			}
		}
		
		obj.init = function(){
		
			if (obj.options.single){
				var $singleActionBar = '<div class="col-xs-12 col-md-12 col-lg-12 no-padding" style="margin-bottom:10px;"><button class="btn btn-nc-blue btn-xs only-for-users btn-roomeditor-addnew-btn" data-type="single" style="display:none"><i class="fa fa-plus"></i> Add New</button><a href="javascript:void(0)" class="btn btn-xs only-for-users btn-roomeditor-edit-btn pull-right" data-mode="view" data-type="single"><i class="fa fa-pencil-square-o"></i> Edit</a></div>';
				obj.$element.prepend($singleActionBar);
			}
		
			// load rooms
			var p  = obj.options.params ? obj.options.params : {};
			nc.get(obj.options.data, true, function(response){
				var _arr = response || [];
				
				$.each(_arr, function(i,record){
					if (record.type == 'group'){
						obj.insertGroup(record);
					} else if (record.type == 'single'){
						obj.insertSingle(record);
					} else {
						console.error("Front-End Room Editor: Unknown room type");
					}
				});
				obj.options.onReady();
			}, p);
			
			
		}
		
		obj.options.onInit();
		obj.init();
		obj.binds();
		obj.checkAmentitiesDesc();
		
		//save object in element
		$element.data("roomEditor", obj);
	}
}( jQuery ));