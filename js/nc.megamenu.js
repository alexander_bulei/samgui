/**
 * Project: NCTourism
 * Description: MegaMenu v4
 * Last Modified: 03062016
 * Author: Alexander Bulei (alexander.bulei@gmail.com)
**/

var nc = nc || {};

$(function(){
    nc = $.extend(true, nc, {
            megamenu: {
                /** PRIVATE **/
                _settings: {},
                _checkResponsive: function(){
                  var self = this;
                  var wW = $(window).width();
                  if (wW <= 992){
                    self._settings.$place.addClass("nc-mobile-megamenu");
                  } else {
                    self._settings.$place.removeClass("nc-mobile-megamenu");
                  }
                },
                _renderULs: function(array){
                    var self = this; array = [], $ul, $li;
                    
                    $ul = $('<ul/>');
                    $.each(data.items, function(i, item){
                        $li = '<li><a>'+item.title+'</a></li>';
                        if (item.items){
                           $li.find('a').attr("href","#");
                        } else {
                           $li.find('a').attr("href", item.href); 
                        }
                        $ul.append($li);
                    });
                  
                },
                _renderHorizData: function($li){
                    var $dataViewer = $('.nc-megamenu-horizontal-data-container').empty().removeClass("ncmm-2-tail ncmm-4-tail").append('<ul></ul>');
                    var data = $li.data("data");
                    if (data){
                        $dataViewer.addClass( data.length < 3 ? "ncmm-2-tail" : "ncmm-4-tail" );
                        $(".nc-megamenu-horizontal").removeClass("ncmm-2-tail ncmm-4-tail");
                        $.each(data, function(i, obj){
                            var _link  = obj.href ? obj.href : "#";
                            var _alt = obj.imageTitle ? obj.imageTitle : "";
                            $dataViewer.find('ul').append('<li><div class="nc-megamenu-horizontal-dc-image-wrapper"><a title="'+_alt+'" href="'+_link+'" style="background-image:url('+obj.image+')"></a></div><a class="nc-megamenu-horizontal-dc-text" href="'+_link+'">'+obj.text+'</a></li>');
                        });
                    }                    
                },
                _hoverHoriz: function($link){
                    var self = this, $this = $link, $li = $this.parent(), id = $li.attr("id"), $ul = $li.parent(), $nextUL = $ul.next('ul'), c = 0, $auto;
                    $ul.find('li.ncm-hover').removeClass("ncm-hover");
                    $li.addClass('ncm-hover');
                    $ul.nextAll('ul').hide();

                    if ($this.hasClass("submenu")){
                        $ul.next('ul').find('li').each(function(i, li){
                            var $loopLi = $(li);
                            if ($loopLi.data("for") == id){
                                if (c == 0){ //FIRST
                                    $nextUL.find('li.ncm-submenu-title').remove();
                                    $('<li class="ncm-submenu-title">'+$this.text()+'</li>').insertBefore(li);
                                    self._renderHorizData( $loopLi );
                                    $auto = $loopLi;
                                }
                                c++;
                                 $loopLi.show();
                            } else {
                                 $loopLi.hide();   
                            }
                        });
                        
                        if (c > 0){
                            $nextUL.find('li.ncm-hover').removeClass("ncm-hover");
                            $nextUL.show();
                        }
                        
                        if ($auto && $auto.length){
                            $auto.addClass("ncm-hover");
                        }
                    } else {
                        self._renderHorizData($li);
                    }
                },
                renderHorizonal: function(data){
                    var self = this;
                    if (!data.items) { return; }
                    
                    var $col = $('<div class="col-xs-12 col-md-12 col-lg-12 no-padding"><div class="nc-megamenu-horizontal"><ul class="nc-megamenu-first"></ul><ul class="nc-megamenu-second"></ul><ul class="nc-megamenu-third"></ul><div class="nc-megamenu-horizontal-data-container"></div></div></div>'),
                        $result = $('<div class="row" style="margin:0"></div>').append($col), sublevel = 0, gArray = []
                        
                    var $ul1 = $col.find('ul.nc-megamenu-first'), $ul2 = $col.find('ul.nc-megamenu-second').hide(), $ul3 = $col.find('ul.nc-megamenu-third').hide();
                    
                    function _CREATE_ITEM($ul, item, forId, u, hidden){
                        var id = new Date().getTime()+u, $li = $('<li id="'+id+'"><a>'+item.title+'</a></li>'), $link = $li.find('a');
                        if (forId){
                            $li.attr("data-for", forId);
                        }
                        
                        if (item.items){
                            $link.attr("href","#").addClass("submenu").append('<div class="global-nav__right-arrow"></div>');
                        }
                        
                        if (hidden){
                            $ul.hide();
                            $li.hide();
                        }
                        
                        $link.hover(function(e){
                            self._hoverHoriz( $(this) );
                        });
                        
                        if (item.data){
                            $li.data("data", item.data);
                        }
                        
                        $ul.append($li);
                        return id;
                    }
                    
                    $.each(data.items, function(i, item){
                        var forId1 = _CREATE_ITEM($ul1, item, false, i);
                        if (item.items){
                            $.each(item.items, function(j, subitem){
                                var forId2 = _CREATE_ITEM($ul2, subitem, forId1, j, true);
                                if (subitem.items){
                                    $.each(subitem.items, function(l, subsubitem){
                                        _CREATE_ITEM($ul3, subsubitem, forId2, l, true);
                                    });
                                }
                            });
                        }                        
                    });

                    return $result;
                },
                _createViewMorePanel: function(){
                    var id = new Date().getTime();
                    return $('<div id="'+id+'" style="display:none;" class="col-xs-12 col-md-12 col-lg-12 col-nc-megamenu-viewmore-item"><h5 style="margin-top:0;"></h5><div class="nc-megamenu-viewmore-items"><ul></ul></div><div class="nc-megamenu-viewmore-items-close"><a href="#">Back to main list</a><div></div>');
                },
                _createMega: function(item){
                    if (!item.megamenu) { return; }
                    var self = this, $div = $('<div style="display:none;" data-forid="'+item.unique+'"></div>'), $content;
                    
                    function bindHorizMouseleave($c){
                        $c.find('.nc-megamenu-horizontal').on("mouseleave", function(){
                           self.reset(); 
                        });                        
                    }
                    
                    $content = self.renderHorizonal(item);
                    bindHorizMouseleave($content);
                    
                    $div.append($content);
                    self.dom.$body.append($div);
                },
                _renderTopLevelItem: function(data){
                    var self = this;
                    var _href = data.url ? data.url : "#";
                    var tooltip = data.tooltip ? data.tooltip : "";
                    var $li = $('<li><a data-toggle="tooltip" data-container="body" data-placement="bottom" title="'+tooltip+'" data-itemid="'+data.unique+'" href="'+_href+'">'+data.title+'</a></li>');
                    
                    if (data.cssClass){
                        $li.addClass(data.cssClass + '-li');
                        $li.find('a').addClass(data.cssClass);
                    }
                    
                    if (data.dropdown || data.megamenu ){
                        $li.find('a').addClass("nc-megamenu-dropdown");
                    }
                    
                    if (data.dropdown && data.dropItems && data.dropItems.length){
                        var $ul = $('<ul class="nc-megamenu-sub-dropdown"></ul>');
                        
                        $ul.on("mouseleave", function(e){
                           self.reset();
                        });
                        
                        if (data.dropClass){
                            $ul.addClass(data.dropClass);
                        }
                        
                        function _CREATE_ITEM($list, d){
                            var href = d.url ? d.url : "#";
                            var tooltip = d.tooltip ? d.tooltip : "";
                            var $subLi = $('<li><a title="'+tooltip+'" href="'+href+'">'+d.title+'</a></li>');
                            if (d.subItems){
                                $subLi.find('a').addClass('nc-megamenu-dropdown-sub');
                                var $subList = $('<ul></ul>');
                                $.each(d.subItems, function(i, subitem){
                                    _CREATE_ITEM($subList, subitem);
                                });
                                $subLi.append($subList);
                            }
                            $list.append($subLi);
                        }
                        
                        $.each(data.dropItems, function(i, item){
                            _CREATE_ITEM($ul, item);
                        });
                        
                        var $div = $('<div data-forid="'+data.unique+'"></div>').append($ul);
                        self.dom.$body.append($div);
                    }
                    
                    this.dom.$topLevelMenu.append($li);
                    
                    if (data.right){
                        $li.css("float","right");
                        if (data.suitcaseCounter){
                            $li.addClass("nc-megamenu-top-level-suitcase").find('a').append('<span class="mm-badge">'+data.suitcaseCounter+'</span>');
                        }                        
                    }
                    
                    return $li;
                },
                build: function(){
                    var self = this;
                    nc.get(self._settings.json, true, function(response){
                        var array = response || [];
                        var $template = $('<div style="margin:0" class="row megamenu-main-container"><div class="nc-megamenu-bg"></div><div class="col-xs-12 col-md-12 col-lg-10 col-lg-offset-1 nc-megamenu-top-level-col"><div class="row nc-megamenu-bg-row" style="margin:0"><div class="col-nc-megamenu-logo"><a style="display:inline-block;" href="/"><img class="nc-megamenu-logo nc-megamenu-logo-white" src="img/nc-logo1.png"/><img class="nc-megamenu-logo nc-megamenu-logo-colored" src="img/nc-logo-colored.png" style="height:51px;"/></a></div><ul class="nc-megamenu-top-level"></ul><a class="megamenu-responsive-caller" href="javascript:void(0);"></a><a class="megamenu-responsive-back" href="javascript:void(0);"></a></div><div class="row" style="margin:0"><div class="col-xs-12 col-md-12 col-lg-12 nc-megamenu-body" style="display:none"></div></div>');
                        
                        $(window).trigger("ncmmenu.beforeInit", [self]);
                        
                        self._settings.$place.append($template);
                        
                        self.dom.$bg = self._settings.$place.find('div.nc-megamenu-bg');
                        self.dom.$topLevelMenu = self._settings.$place.find('ul.nc-megamenu-top-level');
                        self.dom.$mainContainer = self._settings.$place.find('.megamenu-main-container');
                        self.dom.$body = self._settings.$place.find('.nc-megamenu-body');
                        self.dom.$overlay = $('<div style="display:none;" class="nc-megamenu-overlay"></div>');
                        
                        if (self.isFF){
                            self.dom.$body.addClass("firefox");
                        }
                       
                        $('body').append(self.dom.$overlay);
                        
                        var zIndexOverlay = parseInt(self._settings.$place.css("z-index"));
                        zIndexOverlay--;
                        self.dom.$overlay.css("z-index", zIndexOverlay);
                        
                        //RENDER ITEMS FROM JSON
                        $.each(array, function(i, item){
                            item = $.extend(true, { megamenu: true, unique: new Date().getTime() +'-'+i }, item);
                            self._renderTopLevelItem(item);
                            self._createMega(item);
                        });
                        
                        self.dom.$mainContainer.find('[data-toggle="tooltip"]').tooltip();
                        
                        self.binds();
                        $(window).trigger("ncmmenu.afterInit", [self]);
                        $(window).trigger("scroll.mmenu");                        
                    });
                },
                reset: function(){
                    this.dom.$overlay.hide();
                    this.dom.$body.find('[data-forid]').hide();
                    this.dom.$body.find('.col-nc-megamenu-viewmore-item').hide();
                    this.dom.$body.find('.nc-megamenu-destination-list-container').scrollTop(0);
                    this.dom.$body.hide();                  
                },
                binds: function(){
                    var self = this;
                    $(window).on("scroll.mmenu", function(){
                        var wW = $(window).width();
                        if (wW > 992) {
                          self.reset();
                        }                      
                        // if (self._settings.ignoreResizing){ return; }
                        var winScroll = $(window).scrollTop(), placeOffset = self._settings.$place.offset().top;
                        if (winScroll > placeOffset){
                            self.dom.$mainContainer.addClass('nc-megamenu-docked');
                        } else {
                            self.dom.$mainContainer.removeClass('nc-megamenu-docked');
                        }
                    });
                    
                    function __showPanel($elem){
                      var $this = $elem, itemId = $this.data("itemid"), $panel = self.dom.$body.find('[data-forid="'+itemId+'"]');
                        self.reset();
                        if ($panel.length){
                            self.dom.$overlay.show();
                            $panel.show();
                            self.dom.$body.show();
                            var $firstLink = $panel.find('ul.nc-megamenu-first li:first a');
                            self._hoverHoriz($firstLink);
                        }
                    }
                    
                    self.dom.$topLevelMenu.find('li > a').hover(function(e){
                        e.preventDefault();
                        var wW = $(window).width();
                        if (wW > 992){
                          __showPanel($(this));
                        }
                    },function(e){
                        var wW = $(window).width();
                        if (wW <= 992) {
                          return false;
                        }
                        var $elem = self.isFF ? $(e.relatedTarget) : $(e.toElement);
                        var valid = (($elem.closest('.nc-megamenu-horizontal').length > 0) || ($elem.closest('.nc-megamenu-sub-dropdown').length > 0));
                        if (!valid){
                            self.reset();
                        }                        
                    }).click(function(e){
                      e.preventDefault();
                      __showPanel($(this));($(this));
                    });
                    
                    self.dom.$overlay.on('click', function(e){
                        self.reset();
                    });
                    
                    self.dom.$body.find('a.nc-megamenu-viewmore').click(function(e){
                        e.preventDefault();
                        var $this = $(this), $mainList = $(this).closest('ul'), moreList = $mainList.data("more");
                        if ($this.hasClass("nc-megamenu-viewmore-destinations")){
                            if (moreList && moreList.length){
                                $.each(moreList, function(i, item){
                                    $mainList.append('<li><a href="'+item.url+'"><div class="nc-megamenu-destination-item"><img src="'+item.image+'"><label>'+item.title+'</label><div>'+item.description+'</div></div></a></li>');
                                });
                                $this.remove();
                            }
                        } else {
                            var viewPanelId = $(this).data("target"), $viewPanel = $('#' + viewPanelId), title = $(this).closest('.nc-megamenu-items-group').find('h5').text();
                            if (moreList && moreList.length){
                                $viewPanel.find('h5').html(title);
                                var $newUl = $viewPanel.find('ul');
                                $newUl.empty();
                                
                                $.each(moreList, function(i, item){
                                    $newUl.append('<li><a href="'+item.url+'">'+item.title+'</a></li>');
                                });
                                
                                $viewPanel.show();
                            }
                        }
                    });
                    
                    self.dom.$body.find('.nc-megamenu-viewmore-items-close a').click(function(e){
                        e.preventDefault();
                        $(this).closest('.col-nc-megamenu-viewmore-item').hide();
                    });
                    
                    self._settings.$place.find('.megamenu-responsive-caller').click(function(e){
                      e.preventDefault();
                      self.reset();
                      var showMenu = !self._settings.$place.hasClass('showmenu');
                      
                      if (showMenu){
                        self._settings.$place.addClass('showmenu');
                        self.dom.$mainContainer.removeClass('nc-megamenu-docked');
                      } else {
                        self._settings.$place.removeClass('showmenu');
                        $(window).trigger("scroll.mmenu");
                      }
                      
                    });                    
                    
                },
                /** PUBLIC **/
                create: function(settings){
                    var self = this;
                    self._settings = settings;
                    self.isFF = (window.mozInnerScreenX != null);
                    self.dom = {};
                    self.build();
                    
                    self._checkResponsive();
                    $(window).on("resize", function(){
                      self._checkResponsive();
                    });
                    
                }
            }
    });
    
});