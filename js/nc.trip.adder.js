﻿/**
 * Project: NCTourism
 * Description: Trip Adder
 * VERSION: 17052016.1
 * Author: Alexander Bulei (alexander.bulei@gmail.com)
**/

$(function(){
  $.fn.ncTripAdder = function(options){
    var that = this, availLists = [];
    
    var settings = $.extend(true,{
      title: "",
      simpleListAdder: false,
      checkbox: true,
      serverUrl: 'demo/trip.adder.lists.json',
      saveCallback: $.noop
    }, options);
    
    function buildUI(element){
      var $element = $(element);
      
      function _createInsertRow(){
          var $li = $('<li><input type="text" class="form-control input-sm trip-adder-live-input" placeholder="Enter a name for your list..." /></li>');
          if (settings.checkbox){
            $li.append('<div class="checkbox checkbox-primary checkbox-single"><input type="checkbox" value="'+(new Date().getTime())+'"><label></label></div>');
          }
          if (settings.simpleListAdder){
            $li.find('input').css("width", "100%");
          }
          return $li;
      }
      
      var getContent = function(){
        var $result = $('<div />');
        
        var $list = $('<ul class="tripadder-list"></ul>');
        
        if (availLists.length && !settings.simpleListAdder){
          $.each(availLists, function(i){
            var $li = $('<li><span>'+this+'</span></li>');
            if (settings.checkbox){
              $li.append('<div class="checkbox checkbox-primary checkbox-single"><input type="checkbox" value="'+i+'"><label></label></div>');
            }
            $list.append($li);
          });          
        }
        
        $result.append($list);
        var html = '<div class="trip-adder-footer">';
        if (!settings.simpleListAdder){
          html += '<a href="#" class="create-button"><i class="fa fa-plus-circle" aria-hidden="true"></i> Create new list</a>';
        }
        html += '<a href="#" class="save-button"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</a></div>';
        $result.append(html);
        
        if (!settings.simpleListAdder){
          $result.find('.trip-adder-footer .create-button').click(function(e){
            e.preventDefault();
            var $li = _createInsertRow();
            $list.append($li);
            $li.find('input[type="text"]').focus();
          });
        }
        
        $result.find('.trip-adder-footer .save-button').click(function(e){
          e.preventDefault();
          var $this = $(this), result = settings.simpleListAdder ? {} :[];
          var $tmpList = [];
          
          if (settings.simpleListAdder){
            var $in = $list.find('li:first input'), v = $in.val();
            $in.val('');
            result = { listName: v }
          } else {
            if (settings.checkbox){
              $tmpList = $this.closest('.popover-content').find('ul.tripadder-list input[type="checkbox"]:checked');
            } else {
              $tmpList = $this.closest('.popover-content').find('ul.tripadder-list li');
            }
            
            $tmpList.each(function(){
              var $li = $(this).closest('li');
              var $input = $li.find('input[type="text"]');
              if ($input.length){
                result.push( $input.val() );
              } else {
                result.push( $li.find('span').text() );
              }
            });
          }
          settings.saveCallback(result);
          $element.popover("hide");
        });
        
        if (settings.simpleListAdder){
            var $li = _createInsertRow();
            $list.append($li);
            $li.find('input[type="text"]').focus();
        }        
        
        return $result;
      }
      
      var popover = $element.popover({
        container: 'body',
        html: true,
        placement: 'bottom',
        template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title trip-adder-title"></h3><div class="popover-content"></div></div>',
        title: settings.title + ' <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>',
        trigger: 'click',
        content: getContent
      }).data("bs.popover");
      
      popover.tip().on('click','.trip-adder-title button.close',function(){
        $element.popover("hide");
      });
      
      if (settings.simpleListAdder){
        $element.on('shown.bs.popover', function(){
          $(this).data("bs.popover").$tip.find('input.trip-adder-live-input').focus();
        });
      }
      
    }
    
    if (settings.serverUrl && !settings.simpleListAdder){
      $.ajax({
        url: settings.serverUrl,
        success: function(data){
          availLists = data;
          return that.each(function() {
              buildUI(this);
          });
        }
      });
    } else {
      return that.each(function() {
          buildUI(this);
      });      
    }
    

  }
});