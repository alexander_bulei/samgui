/**
 * Project: NCTourism
 * Page: destinations2.html
 * Version: 08062016-1
 * Author: Alexander Bulei (alexander.bulei@gmail.com)
**/

var _TOP_ITEM_WIDTH = 164;
var _DEFAULT_BG = 'img/paper_hatched_bg_tile.jpg';
var _SCROLL_CHECK = true;

var SS = {
    init: function(){
        SmoothScroll({ stepSize: 100 });
    },
    destroy: function(){
        SmoothScroll.destroy();
    }
}

function sectionResponsive(){
    var wh = $(window).height();
    $("#nct-fullpage .section").height(wh);
}

function adjustTopSlider(){
    var $topWrapper, twW, count, bestItemW, $list, marginDiff;
    
    $topWrapper = $('.main-section-image-list-wrapper');
    twW = $topWrapper.width();
    count = Math.floor(twW/_TOP_ITEM_WIDTH);
    marginDiff = (count-1) * 3;
    bestItemW = Math.round((twW-marginDiff)/count);    
    
    $list = $("#main-section-image-list");
    $list.find('li > a').width(bestItemW);
    
    var $topGallery = $("#main-section-image-list"),
        itemCount = $topGallery.find('li').length;
    var listW = (itemCount * bestItemW) + (itemCount * 3);
    $topGallery.width(listW); 

    return { wrapperWidth: twW, listWidth: listW };
}

$(function(){
    var ua = window.navigator.userAgent;
    if (ua.indexOf('MSIE ') > 0 || ua.indexOf('Trident/') > 0 || ua.indexOf('Edge/') > 0){
      $(".svgmap-container").addClass('ie-map-fix');
    }
  
    $('[data-toggle="tooltip"]').tooltip();
    
    /** CHECK BACKGROUND  **/
    function setBgSection($section, propName){
        var $slider = $section.find(".section-bg-slider").empty();
        
        if (__PAGE && __PAGE[propName] && __PAGE[propName].length) {
            $.each(__PAGE[propName], function(i, imgSrc){
                $slider.append('<li><div style="background-image:url('+imgSrc+')"></div></li>');
            });
            
            $slider.responsiveSlides({
                pager: true
            });
            
        } else {
            $slider.append('<li><div class="default-bg" style="background-image:url('+_DEFAULT_BG+')"></div></li>');
        }
        
    }
    
    var $sec;
    // SECTION 2
    $sec = $("#section2");
    setBgSection($sec,'section2');
    
    // SECTION 4
    $sec = $("#section4");
    setBgSection($sec,'section4');
    
    $sec = $("#section6");
    setBgSection($sec,'section6');
    
    /** END CHECK BACKGROUND  **/
    
    SS.init();
    
	// includes
	nc.include('tmpl/header.html','begin');
	nc.include('tmpl/footer.html');
	nc.include('tmpl/login.html');

    sectionResponsive();
    
    var sliderOptions = {
        maxItems: 4,
        itemWidth: 200,
        itemMargin: 10,
        animation: "slide",
        slideshow: false
    }, newOpts;
    
    var obj = adjustTopSlider();
    
    function slideNav(way){
        var $topWrapper = $('.main-section-image-list-wrapper');
        var itemWidth = $topWrapper.find('li:first a').width();
        var sL = $topWrapper.scrollLeft(), newSL;
        
        if (way == 'next'){
            newSL = sL + itemWidth;
        } else {
            newSL = sL - itemWidth;
        }
        
        $topWrapper.stop(true,true).animate({
            scrollLeft: newSL
        }, 100);
    }
    
    if (obj.listWidth > obj.wrapperWidth){
        var $topWrapper = $('.main-section-image-list-wrapper');
        var $prev = $('<a class="main-section-slider-nav left" href="#"><i class="fa fa-angle-left"></i></a>');
        var $next = $('<a class="main-section-slider-nav right" href="#"><i class="fa fa-angle-right"></i></a>');
        
        $topWrapper.prepend($prev).append($next);
        
        $next.on('click', function(e){
           slideNav('next');
        });
        
        $prev.on('click', function(e){
           slideNav('prev');
        });        
        
    }
    
    // MAIN SLIDER
    newOpts = $.extend({},sliderOptions, { customDirectionNav: $("#main-slider-nav a") });
    $("#nct-main-slider").flexslider(newOpts);
    
    newOpts = $.extend({},sliderOptions, { customDirectionNav: $("#city-section-slider-nav a") });
    $("#nct-city-slider").flexslider(newOpts);
    
    newOpts = $.extend({},sliderOptions, { customDirectionNav: $("#section4-slider-nav a") });
    $("#nct-section4-slider").flexslider(newOpts);

    newOpts = $.extend({},sliderOptions, { customDirectionNav: $("#section5-slider-nav a") });
    $("#nct-section5-slider").flexslider(newOpts);

    newOpts = $.extend({},sliderOptions, { customDirectionNav: $("#section6-slider-nav a") });
    $("#nct-section6-slider").flexslider(newOpts);
    
    //SHOW SLIDERS
    $(".nct-flex-slider").css("visibility","visible")
   
    function adjustSliderResponsive(){
      var $sliders = $(".nct-flex-slider");

      var wW = $(window).width();
      $sliders.each(function(){
        var $parent = $(this).parent();
        var sliderInstance = $(this).data('flexslider')
        if (wW < 500){
          sliderInstance.vars.maxItems = 1;
          sliderInstance.vars.itemWidth = 0;
          sliderInstance.vars.itemMargin = 0;
          sliderInstance.vars.controlNav = false;
        } else {
          var max = Math.floor($parent.width() / 210);
          sliderInstance.vars.maxItems = (max < 4 ? max : 4);
          sliderInstance.vars.itemWidth = 200;
          sliderInstance.vars.itemMargin = 10;
          sliderInstance.vars.controlNav = true;
        }
        sliderInstance.resize();      
      });
    }
   
    function winResizeEnd(){
      adjustSliderResponsive();
      sectionResponsive();
      adjustTopSlider();
    }
   
    var resizeTimer;
    adjustSliderResponsive();
    $(window).on("resize", function(e){
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(winResizeEnd, 100);    
    });
    
    $(".page-navigation a, .nc-map-svg-marker").click(function(e){
        e.preventDefault();
        _SCROLL_CHECK = false;
        var $section = $("#" + $(this).data("section") );
        $(this).closest(".page-navigation").find('a.active').removeClass("active");
        $(this).addClass("active");
        
        
        SS.destroy();
        $('body, html').stop(true, true).animate({
            scrollTop: $section.offset().top - 51
        },2000, function(){
            _SCROLL_CHECK = true;
            SS.init();
        });
        
    });
    
    function setBulletNav(id){
        var $list = $('.page-navigation ul');
        $list.find('a.active').removeClass('active');
        $list.find('a[data-section="'+id+'"]').addClass('active');
    }
    
    $(window).on("scroll", function(){
        if (!_SCROLL_CHECK) {
            return;
        }
        var $section = $("._page-section:in-viewport(50)").eq(0);
        if ($section.length){
            var id = $section.attr("id");
            setBulletNav(id);
        }
    });
    
    $('.nc-joined-images div.nc-joined-image').click(function(e){
       var $clicked = $(this), $parent = $clicked.closest('.nc-joined-images'),
           $left = $parent.find('.nc-joined-image-left'), $middle = $parent.find('.nc-joined-image-middle'),
           $right = $parent.find('.nc-joined-image-right');
       
       if ($clicked.hasClass('nc-joined-image-left')){
           $clicked.toggleClass('nc-joined-image-left nc-joined-image-middle');
           $middle.toggleClass('nc-joined-image-middle nc-joined-image-right');
           $right.toggleClass('nc-joined-image-right nc-joined-image-left');
       } else if ($clicked.hasClass('nc-joined-image-right')){
           $clicked.toggleClass('nc-joined-image-right nc-joined-image-middle');
           $middle.toggleClass('nc-joined-image-middle nc-joined-image-left');
           $left.toggleClass('nc-joined-image-right nc-joined-image-left');
       }
       $parent.find('.nc-joined-image-left').addClass('hover-content-disabled');
       $parent.find('.nc-joined-image-right').addClass('hover-content-disabled');
       $parent.find('.nc-joined-image-middle').removeClass('hover-content-disabled');
    });
    
    $('#nc-map-svg path[data-title]').qtip({
        content: {
            text: function(event, obj){
                return $(this).data("title");
            }
        },
        style: {
            classes: 'qtip-light qtip2-custom'
        },
        position: {
            target: 'mouse', // Track the mouse as the positioning target
            adjust: { x: 15, y: 20 } // Offset it slightly from under the mouse
        }
     }); 

    $('#nc-map-svg path[data-url]').click(function(e){
      e.preventDefault();
      var url = $(this).data("url");
      window.location.href = url;
    });
     
});    