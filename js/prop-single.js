/**
 * Project: NCTourism
 * Page: property-single.html
 * Modified Version: 12052015.1
 * Author: Alexander Bulei (alexander.bulei@gmail.com)
**/
 
 var _LOGGED = true;
 var _DATES_OK = false;
 var _DQ_DATES_OK = false;
 var _PROP_MAP_INST;
 var _PROP_GEOAC_INST;
 var _PROP_MAP_MARKER_INST;
 var INIT_PROPERTY;
 
 var ABIMGLOADER = new ABPRELOADER({debug: true});
 
function PROPERTY(){
	return $$PROPERTY || {};
}

function initState(){
	$(".pick-date-notify").show();
	$(".main-rates").hide();
	$(".more-detail-quote-notify").hide();
	$(".row-get-detail-quote").show();
}

function datesFilled(){
	$(".pick-date-notify").hide();
	$(".main-rates").show();
	$(".more-detail-quote-notify").show();
	$(".row-get-detail-quote").hide();
}

function publishPanel(command, error){
	var $panel = $("#publishPanel");
	if (command == 'show'){
		$panel.show().removeClass().addClass('flipInX animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
			$(this).removeClass();
		});
	} else {
		$panel.removeClass().addClass('flipOutX animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
			$panel.hide();
		});			
	}
}

function checkDates(checkIn,checkOut){
	return ((typeof checkIn != "undefined") && (typeof checkIn != null) && (typeof checkOut != "undefined") && (typeof checkOut != null) && (checkOut > checkIn));
}

function initSlider(){
	nc.get('demo/images.json', true, function(result){
		var $ul = $("#main-slider");
		var arr = result;
		$.each(arr, function(i, data){
			var _data = nc.mapListingImagesToFEModel(data);
			
			// only not disabled images
			if (!_data.disabled){
        
        var $li = $('<li><a></a></li>');
        
        $li.find('a').attr({
          class: "bg-centered",
          title: _data.alt || "",
          href: "javascript:void(0)",
          "data-thumb": _data.src
        });
        
        if (i == 0){
          $li.find('a').css({"background-image": "url("+_data.src+")" }).addClass("img-loaded");
        } else {
          ABIMGLOADER.addSourceOnly(_data.src, function(source){
            $li.find('a').css({"background-image": "url("+source+")" }).addClass("img-loaded");
          });
        }
        $ul.append($li);
			}
		});
    
		ABIMGLOADER.process();
    
		$ul.responsiveSlides({        
			speed: 800,
			pause: true,
			defaultPaused: true,
			timeout: 3000,
			auto : true,
			pager : true,
			thumb : "img",
      thumbSrc: "thumb",
			navContainer: ".thumb-container",
			nav: true		
		});
		
		//thumbnail control
		var html = '<div class="main-slider-thumbcontrol"><div class="up" style="visibility: hidden"><i class="fa fa-chevron-up"></i></div><div class="down"><i class="fa fa-chevron-down"></i></div></div>';
		var $thumbnailsCnt = $(".thumb-container");
		var $thumb = $(".rslides1_tabs");
		$thumbnailsCnt.append(html);
		
		$thumbnailsCnt.find("img").addClass("aspect-fit");
		
		$("a.rslides_nav.prev", $thumbnailsCnt).appendTo(".nav-container");
		$("a.rslides_nav.next", $thumbnailsCnt).appendTo(".nav-container");

		$thumbnailsCnt.find('div.up,div.down').on("click", function(e){
			var $this = $(this);
			var sT = $thumb.scrollTop(), newST;
			if ($this.hasClass('up')){
				newST = sT - 65;
			} else {
				newST = sT + 65;
			}
			
			(newST <= 0) ? $("div.up").css("visibility","hidden") : $("div.up").css("visibility","visible");
			$thumb.animate({ scrollTop: newST }, 600);
		});
		
		$(".aspect-fit").parent().imgLiquid({fill:true});
		
	});	
}
  
$(function(){
	if (!_LOGGED){
		$(".only-for-users").remove();
		$(".row-prop-info div.disabled").parent().hide();
	}
		
	initState();
	
	// includes	
	nc.include('tmpl/header.html','begin', function(){
		initializeAdminBar();
	});
	nc.include('tmpl/footer.html');
	nc.include('tmpl/login.html');
	nc.include('tmpl/modals.html');
	
	backToSearchFilters();
	
	//SNAPSHOT OF ORIGINAL DATA - WITHOUT REFERENCE!!
	INIT_PROPERTY = $.extend( {}, PROPERTY() );
	watch(PROPERTY(), function(){
		var cmd = (JSON.stringify(INIT_PROPERTY) != JSON.stringify(PROPERTY())) ? 'show' : 'hide';
		publishPanel(cmd);
	});
	
	$("#publishPanel .publishPanel-buttons button").click(function(){
		var $button = $(this), action = $button.data("action");
		if (action == 'publish') {
			obj.send("url/to/service", PROPERTY(), function(e){
				publishPanel('hide');
			})
		} else {
			publishPanel('hide');
		}
	})
		
	$('div.panel-collapse').on('show.bs.collapse', function () {		
		var $panel = $(this);
		var $header = $panel.prev();
		$header.addClass('active');
		$header.find('span.pull-right i').removeClass('fa-chevron-right').addClass('fa-chevron-down');
		$header.find("img").attr("src","img/shower.png");
		
	}).on('hidden.bs.collapse', function () {
		var $panel = $(this);
		var $header = $panel.prev();
		$header.removeClass('active');
		$header.find('span.pull-right i').removeClass('fa-chevron-down').addClass('fa-chevron-right');
		$header.find("img").attr("src","img/shower_acbfce.png");
	});	
	
	$('div.panel-collapse').on('shown.bs.collapse', function () {	
		var $panel = $(this);
		var isloaded = $panel.data("loaded");
		//google map
		if (($panel.attr("id") == "locationTab") && (!_PROP_MAP_INST)){
			var $map = $(".prop-map");
			_PROP_MAP_INST = new google.maps.Map($map[0],{
				center: new google.maps.LatLng(PROPERTY().latLng[0],PROPERTY().latLng[1]),
				zoom:11,
				mapTypeId:google.maps.MapTypeId.ROADMAP,
				streetViewControl:false,
				scrollwheel:false
			});
			_PROP_MAP_MARKER_INST = new google.maps.Marker({
				position: new google.maps.LatLng(PROPERTY().latLng[0],PROPERTY().latLng[1]),
				map: _PROP_MAP_INST,
				title: PROPERTY().mapTitle,
				icon : 'img/marker.png'				
			});			
			
			if (!_PROP_GEOAC_INST){
				var $input = $("#geoac-locationTab");
				_PROP_GEOAC_INST = new google.maps.places.Autocomplete($input[0],{ types: [] });
				google.maps.event.addListener(_PROP_GEOAC_INST, 'place_changed', function() {
					var place = _PROP_GEOAC_INST.getPlace();
					if ((place) && (_PROP_MAP_MARKER_INST)){
						_PROP_MAP_INST.setCenter(place.geometry.location);
						_PROP_MAP_MARKER_INST.setPosition(place.geometry.location);
						PROPERTY().latLng = [place.geometry.location.lat(), place.geometry.location.lng()];
					}
				});
			}
		} else if ($panel.attr("id") == "bedroomsTab") {
			if (!isloaded){
				$("#bedroomsTab .panel-body").NCROOMEDITOR({
					admin: true,
					data: nc.config.pages.propertySingle.requests.bedrooms,
					params: { type : "bedrooms" },
					amentitiesUrl: nc.config.pages.propertySingle.requests.bedroomAmentities,
					amentityTypesUrl: nc.config.pages.propertySingle.requests.bedroomTypes,
					onInit: function(){
						$panel.find('.panel-body').prepend('<div class="loader" style="text-align:center;padding:40px;color:#c6be9a;"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>');
					},
					onReady: function(){
						$panel.find('.loader').remove();
						$panel.find('.row-panel-title:first').addClass("first");
						$(".room-editor-element",$panel).each(function(i, element){
							reObj = $(element).data('roomEditor');
							reObj.checkAmentitiesDesc();
						});
						$panel.data("loaded", true);
					},
					saveCallback: function(data){
						console.log(data);
					}
				});
			}			
		} else if ($panel.attr("id") == "bathroomsTab") {
			if (!isloaded){
				$("#bathroomsTab .panel-body").NCROOMEDITOR({
					admin: true,
					data: nc.config.pages.propertySingle.requests.bathrooms,
					params: { type : "bathrooms" },
					amentitiesUrl: nc.config.pages.propertySingle.requests.bathroomAmentities,
					amentityTypesUrl: nc.config.pages.propertySingle.requests.bathroomTypes,
					onInit: function(){
						$panel.find('.panel-body').prepend('<div class="loader" style="text-align:center;padding:40px;color:#c6be9a;"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>');
					},
					onReady: function(){
						$panel.find('.loader').remove();
						$panel.find('.row-panel-title:first').addClass("first");
						$(".room-editor-element",$panel).each(function(i, element){
							reObj = $(element).data('roomEditor');
							reObj.checkAmentitiesDesc();
						});
						$panel.data("loaded", true);
					},
					saveCallback: function(data){
						console.log(data);
					}
				});
			}			
		} else if ($panel.attr("id") == "amentitiesTab") {
			if (!isloaded){
				$("#amentitiesTab .panel-body").NCROOMEDITOR({
					admin: true,
					data: nc.config.pages.propertySingle.requests.amentities,
					params: { type : "amentities" },
					amentityTypesUrl: nc.config.pages.propertySingle.requests.amentitiesTypes,					
					amentitiesUrl: nc.config.pages.propertySingle.requests.amentityAmentities,
					onInit: function(){
						$panel.find('.panel-body').prepend('<div class="loader" style="text-align:center;padding:40px;color:#c6be9a;"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>');
					},
					onReady: function(){
						$panel.find('.loader').remove();
						$panel.find('.row-panel-title:first').addClass("first");
						$(".room-editor-element",$panel).each(function(i, element){
							reObj = $(element).data('roomEditor');
							reObj.checkAmentitiesDesc();
						});
						$panel.data("loaded", true);
					},
					saveCallback: function(data){
						console.log(data);
					}
				});
			}			
		}	
	});
	
	var html_review = '<div class="row row-review" data-id="${id}"><div class="col-xs-1 col-md-1 col-lg-1 col-image"><div><img src="${avatar}" width="50" height="50" class="img-circle"></div><div class="text-center person">${name}</div></div><div class="col-xs-11 col-md-11 col-lg-11 no-padding" style="width:99%"><div class="popover right popover-review"><div class="arrow"></div><div class="popover-content"><div class="row-comment"><div style="padding-right:10px;">${comment}</div><div class="col-xs-3 col-md-3 col-lg-3 text-center col-rating border-left" style="width:100px"><input class="rating-input" value="${rate}" type="number"></div></div></div></div></div></div><div class="row row-review-answer" data-id="${id}"><div class="col-xs-1 col-md-1 col-lg-1"></div><div class="col-xs-11 col-md-11 col-lg-11 no-padding"><div class="review-answer"><strong>Owners Response: </strong>${answer}</div></div></div><div class="row row-who-review" data-id="${id}"><div class="col-xs-12 col-md-12 col-lg-12 no-padding text-right">${locationdate}</div></div>';
	$.template("review", html_review);
	
	nc.get('demo/reviews.json', true, function(result){
		var data = nc.mapReviewsToFEModel(result);
		
		var $div = $("#reviewsTab-summary-rate");
		
		$div.find("label.label-score").text(data.rating.avg);
		
		$div.find("input").val(data.rating.avg).rating({
			size : 'xxs',
			showCaption: false,
			showClear: false,
			readonly: true
		});
		
		$div.find("label.label-reviews").text(data.total + ' reviews');
		
		//build rate filters
		var $tmpl, prec;
		var $container = $("#reviewsTab-filter-container");
		
		//5*
		prec = Math.round((data.rating.five * 100) / data.total);  
		$tmpl = '<div><span style="width:20px;display: inline-block;">' + data.rating.five + '</span><div class="checkbox checkbox-default"><input type="checkbox"><label></label></div><input type="number" value="5"/><div class="progress" style="margin-left:10px;width:150px;margin-bottom:-1px;height:10px;"><div class="progress-bar" role="progressbar" aria-valuenow="'+prec+'" aria-valuemin="0" aria-valuemax="100" style="width: '+prec+'%;"></div></div></div>';
		$container.append($tmpl);
		
		//4*
		prec = Math.round((data.rating.four * 100) / data.total);  
		$tmpl = '<div><span style="width:20px;display: inline-block;">' + data.rating.four + '</span><div class="checkbox checkbox-default"><input type="checkbox"><label></label></div><input type="number" value="4"/><div class="progress" style="margin-left:10px;width:150px;margin-bottom:-1px;height:10px;"><div class="progress-bar" role="progressbar" aria-valuenow="'+prec+'" aria-valuemin="0" aria-valuemax="100" style="width: '+prec+'%;"></div></div></div>';
		$container.append($tmpl);
		
		//3*
		prec = Math.round((data.rating.three * 100) / data.total);  
		$tmpl = '<div><span style="width:20px;display: inline-block;">' + data.rating.three + '</span><div class="checkbox checkbox-default"><input type="checkbox"><label></label></div><input type="number" value="3"/><div class="progress" style="margin-left:10px;width:150px;margin-bottom:-1px;height:10px;"><div class="progress-bar" role="progressbar" aria-valuenow="'+prec+'" aria-valuemin="0" aria-valuemax="100" style="width: '+prec+'%;"></div></div></div>';
		$container.append($tmpl);
		
		//2*
		prec = Math.round((data.rating.two * 100) / data.total);  
		$tmpl = '<div><span style="width:20px;display: inline-block;">' + data.rating.two + '</span><div class="checkbox checkbox-default"><input type="checkbox"><label></label></div><input type="number" value="2"/><div class="progress" style="margin-left:10px;width:150px;margin-bottom:-1px;height:10px;"><div class="progress-bar" role="progressbar" aria-valuenow="'+prec+'" aria-valuemin="0" aria-valuemax="100" style="width: '+prec+'%;"></div></div></div>';
		$container.append($tmpl);
		
		//1*
		prec = Math.round((data.rating.one * 100) / data.total);  
		$tmpl = '<div><span style="width:20px;display: inline-block;">' + data.rating.one + '</span><div class="checkbox checkbox-default"><input type="checkbox"><label></label></div><input type="number" value="1"/><div class="progress" style="margin-left:10px;width:150px;margin-bottom:-1px;height:10px;"><div class="progress-bar" role="progressbar" aria-valuenow="'+prec+'" aria-valuemin="0" aria-valuemax="100" style="width: '+prec+'%;"></div></div></div>';
		$container.append($tmpl);
		
		
		$container.find('input[type="number"]').rating({
			size : 'xxs',
			showCaption: false,
			showClear: false,
			readonly: true
		});
		
		var _arr = data.reviews || [];
		var $body = $(".reviews-body")
		$body.empty();
		$.each(_arr, function(i, obj){
			var _html = $.tmpl("review", obj);
			
			if ((i % 2) == 1){
				_html.addClass("row-review-offset")
			}
			
			$body.append(_html);
			
			if (!obj.answer){
				$body.find(".row-review-answer[data-id='"+obj.id+"']").hide();
			}
			
		});
		$("#btn-load-more").data("recordsShown", _arr.length);
		initRating();
	});
	
	function initRating(){
		$("#reviewRating, .rating-input").rating({
			size : 'xxs',
			showCaption: false,
			showClear: false,
			readonly: true
		});		
	}
	
	initRating();
	
	$("#guestSpinner").on('change input', function(){
		var $this = $(this), v = $this.val();
		var $input = $( $this.data("result") ).find("span.info-value");
		PROPERTY().guests = v;
		$input.text(v);
	});
	
	/* init datepickers */
	var $checkoutInput = $('#checkoutDate');
	var $dqCheckoutInput = $('#dq-checkout');
	initDatePickers( $('input.datepicker.checkin','.row-top-header'), $('input.datepicker.checkout','.row-top-header') );
	initDatePickers( $('#checkinDate'), $checkoutInput );
	initDatePickers( $('#dq-checkin'), $dqCheckoutInput );
	
	$checkoutInput.on('dp.change', function(){
    var checkOutObj = $(this).data("DateTimePicker").date();
    var checkInObj = $('#checkinDate').data("DateTimePicker").date();
    if (checkOutObj && checkInObj){
      var checkOut = checkOutObj._d;
      var checkIn = checkInObj._d;
      _DATES_OK = checkDates(checkIn,checkOut);
    } else {
      _DATES_OK = false;
    }
		
		if (_DATES_OK) {
			datesFilled()
		} else {
			initState();
		}
	});
	
	$dqCheckoutInput.on('dp.change', function(){
		var checkOut = $(this).data("DateTimePicker").date()._d;
		var checkIn = $('#dq-checkin').data("DateTimePicker").date()._d;
		_DQ_DATES_OK = checkDates(checkIn,checkOut);
		if (_DQ_DATES_OK) {
			$(".body-secondary", "#detailQuoteModal").show();
		} else {
			$(".body-secondary", "#detailQuoteModal").hide();
		}
	});	
	
	$(".btn-booknow").on("click", function(e){
		if (!_DATES_OK){
			$(".nc-alert-yellow").fadeOut(500).fadeIn(500).fadeOut(500).fadeIn(500);
		}
	});	
	
	$(".btn-get-detailed-quote, .link-get-detailed-quote").on("click", function(e){
		var $modal = $("#detailQuoteModal");
		
		if (!_DATES_OK){
			$(".body-secondary", $modal).hide();
		} else {			
			var checkIn = $('#checkinDate').data("DateTimePicker").date()._d;
			var checkOut = $checkoutInput.data("DateTimePicker").date()._d;
			$("#dq-checkin").data("DateTimePicker").date(checkIn);
			$("#dq-checkout").data("DateTimePicker").date(checkOut);
		}
		
		$modal.modal("show");
	});
	
	//init slider
	initSlider();
		
	//bind fav icon
	$("i.fav-icon").parent().on("click", function(e){
		var $i = $(this).find('i');
		if ($i.hasClass("fa-heart-o")){
			$i.removeClass('fa-heart-o').addClass("fa-heart");
		}else{
			$i.addClass('fa-heart-o').removeClass("fa-heart");
		}
	});
	
	//bind toggle buttons
	$("i.btn-toggle-check").parent().on("click", function(e){
		var $i = $(this).find('i');
		var $btn = $("." + $i.data("btn"));
		var $main = $btn.parent();
		var code = $main.data("propertyAttr");
		if ($i.hasClass("btn-toggle-check-on")){
			$i.removeClass('btn-toggle-check-on fa-check-square').addClass("fa-minus-square btn-toggle-check-off");
			$main.addClass("disabled");
			PROPERTY().attributes[code] = false;
		}else{
			$i.addClass('btn-toggle-check-on fa-check-square').removeClass("fa-minus-square btn-toggle-check-off");
			$main.removeClass("disabled");
			PROPERTY().attributes[code] = true;
		}		
	});
	
	//spinner
	$(document).delegate('.spinner .btn:first-of-type', 'click', function(){
		var $input = $(this).closest(".spinner").find("input");
		var v = $input.val();
		$input.val( parseInt(v, 10) + 1).trigger("change");
	});
	
	$(document).delegate('.spinner .btn:last-of-type', 'click', function(){
		var $input = $(this).closest(".spinner").find("input");
		var v = $input.val(), newVal;		
		newVal = parseInt(v, 10) - 1;
		newVal = (newVal <= 0) ? 0 : newVal;
		$input.val(newVal).trigger("change");		
	});
	
	function editImages(){
		$("#main-slider").empty().hide();
		$(".thumb-container").empty().hide();
		$(".nav-container").empty().hide();
		$("#edit-images").addClass("pull-right");
		nc.get('demo/images.json', true, function(result){
			var $c = $(".main-slider-wrapper");
			var opts = {
				uploadUrl: 'upload.php',
				saveCallback: function(data, callback){
					var response = $.extend(true, data, {
						id: Math.floor((Math.random() * 100) + 1),
						disabled: false
					});
					callback(response);
				},
				onready: function(){
					$('body').scrollTop( $(".ncim-upload-btn").offset().top );
				}
			}
			NCIM.init($c,result,opts);			
		});
	}
	
	function restoreImages(){
		$("#edit-images").removeClass("pull-right");
		NCIM.destroy();
		$("#main-slider").show();
		$(".thumb-container").show();
		$(".nav-container").show();
		initSlider();		
	}
	
	$("#edit-images").on("click", function(){
		nc_editmode($(this),editImages,restoreImages);
	});	

	var TEMP_GA_INST;
	function editTitleLoc(){
		// TITLE
		var $input = $("<input class='form-control prop-single-title-input' type='text'>").val( PROPERTY().headLine );
		$(".prop-single-title").replaceWith($input);
		
		//LOCATION
		$label = $(".prop-single-location");
		var oldV = PROPERTY().locationTitle;
		$input = $("<input class='form-control prop-single-location-input' type='text'>").val(oldV);
		$label.replaceWith($input);
		$input.data("oldValue",oldV);
		
		TEMP_GA_INST = new google.maps.places.Autocomplete($input[0],{ types: [] });
	}
	
	function restoreTitleLoc(){
		var $input = $(".prop-single-title-input");
		PROPERTY().headLine = $input.val();
		
		var $label = $("<label class='prop-single-title'>" + PROPERTY().headLine + "</label>");
		$input.replaceWith($label);
		
		//LOCATION
		$input = $(".prop-single-location-input");		
		var place = TEMP_GA_INST.getPlace();
		var v;
		if (place){
			v = place.formatted_address;
			var decomposed = nc.utils.convertGoogleAddressComponents(place.address_components);
			
			PROPERTY().address = decomposed.street_number + ' ' + decomposed.route,
			PROPERTY().city = decomposed.locality,
			PROPERTY().state = decomposed.administrative_area_level_1,
			PROPERTY().country = decomposed.country,
			PROPERTY().zip = decomposed.postal_code
			
			PROPERTY().latLng = [place.geometry.location.lat(),place.geometry.location.lng()];
			
		} else if ($input.val() == "") {
			v = $input.data("oldValue");
		}else {
			v = $input.val();			
		}
		PROPERTY().locationTitle = v;
		$label = $("<label class='prop-single-location' style='display:block;'><i style='display:inline-block;' class='fa fa-map-marker'></i> "+v+"</label>");
		$input.replaceWith($label);
	}
	
	$(".edit-title-loc").on("click", function(){
		nc_editmode($(this),editTitleLoc,restoreTitleLoc);
	});
	
	function editDescriptionTab(){
		var $tab = $("#descriptionTab");
		var $element, $input;
		
		//title
		$input = $("<input class='form-control ps-desctab-title-input input-sm' type='text'>").val( PROPERTY().title );
		$(".sub-title",$tab).replaceWith($input);
		
		//description
		$input = $("<textarea class='form-control ps-desctab-description-input input-sm' rows='5'>").text( PROPERTY().description );
		$(".ps-desctab-description",$tab).replaceWith($input);
		
		//table
		function createSelectElement(array, defaultValue){
			var $select = $("<select class='form-control table-inline-edit input-sm'></select>");
			$.each(array, function(i, obj){
				$select.append('<option data-id="'+obj.id+'" value="'+obj.name+'">'+obj.name+'</option>');
			});
			$select.find('option[value="'+ defaultValue +'"]').attr("selected","selected");
			return $select;
		}
		
		function createInputElement(defautlValue){
			return $('<input class="form-control table-inline-edit input-sm" type="number" min="0" value="'+defautlValue+'" />');
		}
		
		$("#panel-desc-table").find("tr").each(function(e,tr){
			var $tr = $(tr),
				prop = $tr.data("property"),
				$editTD = $tr.find("td.row-value"),
				value = PROPERTY()[prop];
			
			if (prop == "propType"){
				/** DEMO DATA **/
				var DEMODATA = [
					{ id: 1, name: "Dummy Record #1" },
					{ id: 2, name: "Single Family Home" },
					{ id: 3, name: "Dummy Record #3" }
				];
				/** END DEMO DATA **/
				var $select = createSelectElement(DEMODATA, value);
				$editTD.empty().append($select);
				
			} else if (prop == "propLocation"){
				/** DEMO DATA **/
				var DEMODATA = [
					{ id: 1, name: "Dummy Record #1" },
					{ id: 2, name: "Ocean First Row" },
					{ id: 3, name: "Ocean Second Row" }
				];
				/** END DEMO DATA **/
				var $select = createSelectElement(DEMODATA, value);
				$editTD.empty().append($select);
			} else if (prop == "propDialyHousekeeping") {
				/** DEMO DATA **/
				var DEMODATA = [
					{ id: 1, name: "Ask Owner" },
					{ id: 2, name: "Housekeeping Included" },
					{ id: 3, name: "Housekeeping Optional" }
				];
				/** END DEMO DATA **/				
				var $select = createSelectElement(DEMODATA, value);
				$editTD.empty().append($select);
			} else if ((prop == "propFloors") || (prop == "propSleeps")) {
				var $input = createInputElement(value);
				$editTD.empty().append($input);
			} else if (prop == "propLivingSpace") {
				/** DEMO DATA **/
				var DEMODATA = [
					{ id: 1, name: "sq ft" },
					{ id: 2, name: "sq meters" }
				];
				/** END DEMO DATA **/
				var $select = createSelectElement(DEMODATA, PROPERTY().propLivingSpaceUnit);
				var $input = createInputElement(value);
				$select.attr("style","width:120px!important;float:left");
				$input.css({
					"width": "200px",
					"text-align": "right",
					"float": "left",
					"margin-right": "5px"
				});
				$editTD.empty().append($input).append($select);
			}
		});
	}
	
	function restoreDescriptionTab(){
		var $tab = $("#descriptionTab");
		var $element, $input;

		//title
		$input = $(".ps-desctab-title-input",$tab);
		PROPERTY().title = $input.val();
		$element = $("<div class='inline-block sub-title title-color'>"+PROPERTY().title+"</div>");
		$input.replaceWith($element);
		
		//description
		$input = $(".ps-desctab-description-input",$tab);
		PROPERTY().description = $input.val();
		$element = $("<div class='ps-desctab-description'>"+PROPERTY().description+"</div>");
		$input.replaceWith($element);
		
		//table
		$("#panel-desc-table").find("tr").each(function(e,tr){
			var $tr = $(tr),
				prop = $tr.data("property"),
				$editTD = $tr.find("td.row-value");
				
			if (prop == "propLivingSpace"){
				PROPERTY()[prop] = parseFloat($editTD.find("input").val());
				PROPERTY().propLivingSpaceUnit = $editTD.find("select").val();
				
				$editTD.empty().append(PROPERTY()[prop] + ' <span class="munit">'+PROPERTY().propLivingSpaceUnit+'<span>');
			}else{
				var $element = $editTD.find("input");
				var newVal;
				if (!$element.length){
					$element = $editTD.find("select");
					newVal = $element.val();
				} else {
					newVal = parseFloat($element.val());
				}
				PROPERTY()[prop] = newVal;
				$editTD.empty().text(PROPERTY()[prop]);
			}
		});
	}
	
	
	$(".edit-description-tab").on("click", function(){
		nc_editmode($(this),editDescriptionTab,restoreDescriptionTab);
	});
	
	/*EDIT LOCATION TAB*/
	
	function editLocationTab(){
		var $map = $(".prop-map");
		var g_latlng = new google.maps.LatLng(PROPERTY().latLng[0],PROPERTY().latLng[1]);
		_PROP_MAP_INST.setCenter(g_latlng);
		_PROP_MAP_MARKER_INST.setPosition(g_latlng);			
		$("#geoac-locationTab").val("");
		$("#geoac-locationTab").show();
	}
	
	function restoreLocationTab(){
		var $map = $(".prop-map");
		$("#geoac-locationTab").hide();
	}	
	
	$("#edit-location").on("click", function(){
		nc_editmode($(this),editLocationTab,restoreLocationTab);
	});	
	
	// LOAD LISTING OPTION ADMIN PAGE
	$("#editRatesFeesModalIFrame").load(function(e){
		var iframewindow = this.contentWindow ? this.contentWindow : this.contentDocument.defaultView;
		
		$(iframewindow.document.body).on("dragover", function(e){
			e.preventDefault();
		}).on("drop", function(e){
			e.preventDefault();
		});		
		
		var _opts = {
			loOptions: {
				super: true,
				saveUpdateHandler: function(data, callback){
					var response = { record: data };
					//var response = { "id" : id, "errorMsg" : "Some Error!"}; // response with error 
					callback(response);
				},
				deleteHandler: function(id, callback){
					var response = { "id" : id };
					//var response = { "errorMsg" : "Some Error!"}; // response with error 
					callback(response);
				},
				saveCategoriesHandler: function(data, callback){
					var response = { categories: data };
					callback(response);
				},
				getDueDataHandler: function(callback){
					//var response = { "errorMsg" : "Some Error!"}; // response with error 
					var response = {
						data: [
							{id: 0, name: "In full at time of transaction"},
							{id: 1, name: "In full at payment 1"},
							{id: 2, name: "In full at payment 2"},
							{id: 3, name: "In full at payment 3"},
							{id: 4, name: "In full at payment 4"},
							{id: 5, name: "In full at payment 5"}
						]
					};
					
					callback(response);
				},
				getUnitDataHandler: function(callback){
					//var response = { "errorMsg" : "Some Error!"}; // response with error 
					var response = {
						data: [
							{id: 0, name: "Hour"},
							{id: 1, name: "Day"},
							{id: 2, name: "Week"},
							{id: 3, name: "Month"},
							{id: 3, name: "Year"}
						]
					};
					
					callback(response);
				},		
				getOptionsNewData: function(callback){
					//var response = { "errorMsg" : "Some Error!"}; // response with error 
					var response = {
						data: [
							{id: -1, name: "Select option"},
							{id: 0, name: "Option 1"},
							{id: 1, name: "Option 2"},
							{id: 2, name: "Option 3"},
							{id: 3, name: "Option 4"},
							{id: 4, name: "Option 5"},
							{id: 5, name: "Option 6"}
						]
					}
					callback(response);
				},
				getFavNewData: function(callback){
					//var response = { "errorMsg" : "Some Error!"}; // response with error 
					var response = {
						data: [
							{id: -1, name: "Select favorite"},
							{id: 0, name: "Fav Option 1"},
							{id: 1, name: "Fav Option 2"},
							{id: 2, name: "Fav Option 3"},
							{id: 3, name: "Fav Option 4"},
							{id: 4, name: "Fav Option 5"},
							{id: 5, name: "Fav Option 6"}
						]
					}
					callback(response);
				},
				getListingNewData: function(callback){
					//var response = { "errorMsg" : "Some Error!"}; // response with error 
					var response = {
						data: [
							{id: 0, name: "Beach House 1"},
							{id: 1, name: "Beach House 2"},
							{id: 2, name: "Beach House 3"},
							{id: 3, name: "Beach House 4"},
							{id: 4, name: "Beach House 5"},
							{id: 5, name: "Beach House 6"}
						]
					}
					callback(response);
				}			
			},
			varOptions: {
				saveUpdateHandler: function(data, callback){
					var response = { record: data };
					//var response = { "id" : id, "errorMsg" : "Some Error!"}; // response with error 
					callback(response);
				},
				deleteHandler: function(id, callback){
					var response = { "id" : id };
					//var response = { "errorMsg" : "Some Error!"}; // response with error 
					callback(response);
				}					
			},
			ratesOptions: {
				saveUpdateHandler: function(data, callback){
					var response = { record: data };
					//var response = { "id" : id, "errorMsg" : "Some Error!"}; // response with error 
					callback(response);
				},
				deleteHandler: function(id, callback){
					var response = { "id" : id };
					//var response = { "errorMsg" : "Some Error!"}; // response with error 
					callback(response);
				}	
			},
			popup : true,
			flag : 0
		}
		
		iframewindow.initListingOptions(_opts);			
		$('[data-toggle="tooltip"]', this).tooltip();
		
		iframewindow.loadListingModal(function(){
			_opts.loOptions.getListingNewData(function(response){
				var $select = $("#slm-listing-select");
				var $slm = $("#selectListingModal");
				var arr = response.data || [];					
				$select.empty();
				$.each(arr, function(i,obj){
					$op = $("<option value='"+obj.id+"'>"+obj.name+"</option>");
					$select.append($op);
				});
				$select.select2();
				
				$(".select-listing-btn", $slm).click(function(){
					var v = $("#slm-listing-select").select2("val");
					if (v > -1){
						var callback = $("#selectListingModal").data("callback");
						var _d = {
							id : v,
							text : $("#slm-listing-select").find("option:selected").text()
						}
						callback(_d);
						$slm.modal("hide");
					}
				})
			});
		});			
		
	});
	
	$("#editRatesFeesModalIFrame").attr("src","backend/manage-listing-frame.html");
	
	$("#rate-editmode").on("click", function(e){
		$("#editRatesFeesModal").modal({
		  backdrop: 'static',
		  keyboard: true
		}).modal("show");		
	});
	
	function _createMediaLinkRow(text, link){
		return '<tr><td><input type="text" class="form-control input-sm" value="'+text+'"></td><td><input type="text" class="form-control input-sm" value="'+link+'"></td><td><button class="btn btn-danger btn-remove-medialink btn-xs" style="margin-top:4px;">Remove</button></td></tr>';
	}	
	
	function showMediaLinksEditor(){
		var $modal = $("#editMediaLinksModal"),
			$body = $modal.find(".modal-editor-body"),
			$where = $(".col-media-links"),
			$rows = $(".media-row", $where);
			
		$body.empty();
		
		function _insertSection($where, header){
			var $panel = $('<div class="panel panel-info"><div class="panel-heading"><h3 class="panel-title"</h3></div><div class="panel-body"></div></div>');
			$panel.find('.panel-title').append(header);
			$table = $("<table class='table table-condensed'></table>");
			$table.append('<thead><tr style="background:#EEE"><th>Text</th><th>Link</th><th>Actions</th></tr></thead><tbody></tbody>');
			
			$panel.find('.panel-body').append('<div style="margin-bottom:10px;"><button class="btn btn-success btn-xs btn-add-medialink"><i class="fa fa-plus"></i> Add Link</button></div>');
			$panel.find('.panel-body').append($table);
			$where.append($panel);
			$('[data-toggle="tooltip"]', $where).tooltip();
			return $panel;
		}
		
		$(".editor-medialinks-addsection").off('click').on('click', function(e){
			var $body = $(".modal-editor-body");
			var $wrapper = $("<div class='editmode-row-medialinks'>");
			var $panel = _insertSection($wrapper, 'New Section');
			$body.prepend($wrapper);
			$wrapper.find('.panel-title input').focus();
		});		
		
		$rows.each(function(i, row){			
			var $row = $(row);
			var $wrapper = $("<div class='editmode-row-medialinks'>");
			var $header = $row.find("h6");

			var $panel = _insertSection($wrapper, $header.text());

			$tbody = $panel.find("tbody");
			var $links = $row.find("a");
			$.each($links, function(i, link){
				var t = $(link).text(), l = $(link).attr("href");
				row = _createMediaLinkRow(t,l);
				$tbody.append(row);
			});
			
			$panel.find('.panel-body').append($table);
			
			$wrapper.append($panel);
			$body.append($wrapper);
			//$element.replaceWith($input);
		})		
		
		$modal.modal("show");
	}
	
	$("#editMediaLinksModal").delegate(".btn-remove-medialink", "click", function(e){
		var $tr = $(this).closest("tr");
		$tr.remove();
	});
	
	$("#editMediaLinksModal").delegate(".btn-add-medialink", "click" ,function(e){
		var $table = $(this).parent().next();
		row = _createMediaLinkRow("","");
		$table.append(row);
	});
	
	
	$("#editMediaLinksModal").delegate(".btn-remove-section-medialink", "click" ,function(e){
		var $panel = $(this).closest('div.panel.panel-info');
		$panel.remove();
	});
	
	$("#edit-media-links").on("click", function(){
		showMediaLinksEditor();
	});	
	
	$("#editor-medialinks-save").click(function(e){
		var $modal = $("#editMediaLinksModal");
		var $where = $(".col-media-links");
		$where.empty();

		$("div.panel.panel-info", $modal).each(function(i, panel){
			var $panel = $(this);
			var header = $panel.find('.panel-title input').val();
			var $new = $('<div class="media-row">');
			$new.append("<h6>" + header + "</h6>");
			
			$panel.find('.panel-body tbody tr').each(function(i, tr){
				$inputs = $(tr).find('input');

				function addhttp(url) {
				   if (!/^(f|ht)tps?:\/\//i.test(url)) {
					  url = "http://" + url;
				   }
				   return url;
				}				
				
				var httpVal = addhttp($inputs.eq(1).val());
				$a = '<a href="'+httpVal+'">'+$inputs.eq(0).val()+'</a>'
				$new.append($a);
			});			
			$where.append($new);
		});
	});
	
	$("#btn-load-more").click(function(){
		var $this = $(this);
		var params = {
			start: $this.data("recordsShown") || 0,
			max: nc.config.pages.propertySingle.reviewsMoreNumRecords
		}
		nc.get('demo/reviews.json', true, function(result){
			var _arr = result ? nc.mapReviewsToFEModel(result) : [];
			var $body = $(".reviews-body");
			var _count = $body.find(".row-review").length || 0;
			$.each(_arr.reviews, function(i, obj){
				var _html = $.tmpl("review", obj);
				if (((_count+i) % 2) == 1){
					_html.addClass("row-review-offset")
				}
				$body.append(_html);
				
				if (!obj.answer){
					$body.find(".row-review-answer[data-id='"+obj.id+"']").hide();
				}				
			});
			$this.data("recordsShown", params.start + params.max);
			initRating();
		}, params);		
	});
	
	$("#share-panel").jsSocials({
		shares: ["email", "twitter", "facebook", "googleplus", "pinterest"]
    });	
	
	$("#editReviewsModalIFrame").attr("src","backend/manage-reviews-frame.html");
	
	function _responsiveModal($modal){
		var minHeight = Math.round($(window).height() - ($(window).height() / 5));
		$modal.find('iframe').css({"min-height": minHeight + 'px'});
	}
	
	$("#edit-reviews").on("click", function(e){
		var $modal = $("#editReviewsModal");
		_responsiveModal($modal);
		$modal.modal({
		  backdrop: 'static',
		  keyboard: true
		}).modal("show");		
	});	
	
	$(window).on("resize", function(){
		_responsiveModal( $("#editReviewsModal") );
	});
	
	$('[data-toggle="tooltip"]').tooltip();

	$(".btn-contact-manager").click(function(e){
		$("#contactOwnerModal").modal("show");
	});
	
	$(".btn-contact-us").click(function(){
		$("#contactGeneralModal").modal("show")
	})
});