
function sortByPopularityDesc(a,b){
	return parseFloat(b.popularity) - parseFloat(a.popularity);
}

function sortByPopularityAsc(a,b){
	return parseFloat(a.popularity) - parseFloat(b.popularity);
}

function sortByRatingDesc(a,b){
	return parseFloat(b.rate) - parseFloat(a.rate);
}

function sortByRatingAsc(a,b){
	return parseFloat(a.rate) - parseFloat(b.rate);
}

function sortByLocationDesc(a,b){
    if(a.location > b.location) return -1;
    if(a.location < b.location) return 1;
    return 0;	
}

function sortByLocationAsc(a,b){
    if(a.location < b.location) return -1;
    if(a.location > b.location) return 1;
    return 0;	
}

function sortByCategoryDesc(a,b){
    if(a.category < b.category) return -1;
    if(a.category > b.category) return 1;
    return 0;	
}

function sortByCategoryAsc(a,b){
    if(a.category > b.category) return -1;
    if(a.category < b.category) return 1;
    return 0;	
}

onmessage = function(e){
	var arr = e.data.array;
	
	if ( e.data.sortby === "popularity" ) {
		if (e.data.order == "asc"){
			arr.sort(sortByPopularityAsc);
		} else {
			arr.sort(sortByPopularityDesc);
		}
		
	} else if ( e.data.sortby === "location" ) {
		if (e.data.order == "asc"){
			arr.sort(sortByLocationAsc);
		} else {
			arr.sort(sortByLocationDesc);
		}		
	} else if ( e.data.sortby === "category" ) {
		if (e.data.order == "asc"){
			arr.sort(sortByCategoryDesc);
		} else {
			arr.sort(sortByCategoryAsc);
		}
	} else if ( e.data.sortby === "rating" ) {
		if (e.data.order == "asc"){
			arr.sort(sortByRatingAsc);
		} else {
			arr.sort(sortByRatingDesc);
		}
	}
	
	//done - send back to main thread
	postMessage(arr);
};