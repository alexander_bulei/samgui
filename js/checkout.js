/**
 * Project: NCTourism
 * Page: checkout.html
 * Last Modified: 13-10-2015
 * Internal Version: 7.7.3
 * Author: Alexander Bulei (alexander.bulei@gmail.com)
**/

function checkResponsive(){
	var wWidth = $(window).width();
	var $rightBar = $("#rightBar");
	var $rowBody = $(".row-body");
	if (wWidth <= 992){
		$rightBar.addClass("col-xs-offset-1").prependTo($rowBody);
	} else {
		$rightBar.removeClass("col-xs-offset-1").appendTo($rowBody);
	}
	
	var iframeHeight = $(window).height() - 150;
	$("#checkout-termsConditions-iframe").css("height", iframeHeight);
}

$(function(){
	// includes
	nc.include('tmpl/header.html','begin');
	nc.include('tmpl/footer.html');
	nc.include('tmpl/login.html');
	
	$('div.panel-collapse','#accordion').on('show.bs.collapse', function () {		
		var $panel = $(this);
		var $header = $panel.prev();
		$header.addClass('active');
		$header.find('span.pull-right i').removeClass('fa-chevron-right').addClass('fa-chevron-down');
	}).on('hidden.bs.collapse', function () {
		var $panel = $(this);
		var $header = $panel.prev();
		$header.removeClass('active');
		$header.find('span.pull-right i').removeClass('fa-chevron-down').addClass('fa-chevron-right');
	});
	
	nc.get('demo/countries.json', true, function(result){
		var $select = $("#b-country");
		$select.empty();
		$select.append('<option value="-1">Select country</option>');
		$.each(result, function(i,country){
			$select.append('<option value="'+country.code+'">'+ country.name + '</option>');
		});	
	})
	
	$("#b-country").select2({
		placeholder: "Select country"
	});
	
	$("#b-states").select2({
		placeholder: "Select state"
	});
	
	$("#b-country").data("select2").$container.addClass("checkout-select");
	$("#b-states").data("select2").$container.addClass("checkout-select");

	$("#rating").rating({
		size : 'xxs',
		showCaption: false,
		showClear: false,
		readonly: true
	});
	
	checkResponsive();
	$(window).on("resize", function(){
		checkResponsive();
	});

	$("#sCode-popover").popover({
        html : true, 
        content: function() {
          return $('#sCode-popover-content').html();
        },
        title: "Security code on card"
	});

	function validateCardExpiryDate(expiryDate) {
		var re = /^(0[1-9]|1[0-2])\/[\/-]?[0-9]{2}$/mg; 
		return re.test(expiryDate);
	}
		
	function _validate_($input)	{
		var val = $input.val();
		var result = false;
		var isvalid, isselect = false, $elem = $input;
		if ($input.length > 1) {
			$input.each(function(i, elem){
				if (!isvalid) {
					isvalid = elem.checked;
				}
			});
			$elem = $input.parent();
		}else{
			if ($input.attr('required')){
				if ($input.data("select2")) {
					isvalid = ($input.select2("val") != '-1');
				} else {
					isvalid = !(val == '');
				}
			}
			
			if ($input.data('ccardexpdate')){
				isvalid = validateCardExpiryDate(val);
			}
			
			
			if ($input.attr("type")) {
				
				if ($input.attr("type").toLowerCase() == 'number'){
					isvalid = $.isNumeric(val);
				}
				
				if ($input.attr("type").toLowerCase() == 'email'){
					isvalid = nc.utils.isValidEmailAddress(val);
				}
			
			} else {
				if ($input.data("select2")) {
					$elem = $input.data("select2").$selection;
					isselect = true;
				}
			}
		}
		
		
		if (!isvalid){
			$elem.addClass("required-error");
			if (isselect) {
				$elem.attr("style", "border-color: #E7BEBE!important");
			}
			result = false;
		} else {
			$elem.removeClass("required-error");
			if (isselect) {
				$elem.attr("style", "border-color: rgba(51,153,204,.3)!important");
			}
			result = true;
		}
		return result;
	}
		
		
	function validateAboutTab(focus){
		var result = true,r, v, $input;
		r = _validate_( $("#fistname-input") );
		result = result && r;
		
		r = _validate_( $("#lastname-email") );
		result = result && r;

		r = _validate_( $("#email-input") );
		result = result && r;

		r = _validate_( $("#phone-input") );
		result = result && r;
		
		if ((!result) && (focus)) {
			var $tab = $('#aboutTab');
			$tab.collapse("show");
			$(window).scrollTop( $tab.offset().top );
		}
		
		return result;
	}
	
	function validatePoliciesTermsTab(focus){
		var result = true, r;
		r = _validate_( $('input[name="coverCancel"]') );
		result = result && r;
		
		r = _validate_( $('input[name="coverDamage"]') );
		result = result && r;
		
		if ((!result) && (focus)) {
			var $tab = $('#policiesTermsTab');
			$tab.collapse("show");
			$(window).scrollTop( $tab.offset().top );
		}
		
		return result;
	}
	
	function validateConfidenceTab(focus){
		var result = true, r;
		r = _validate_( $('input[name="protectPayment"]') );
		result = result && r;
		if ((!result) && (focus)) {
			var $tab = $('#confidenceTab');
			$tab.collapse("show");
			$(window).scrollTop( $tab.offset().top );
		}
		return result;		
	}
	
	function validatePaymentTab(){
		var result = true, r;
		r = _validate_( $('#b-cardnumber-input') );
		result = result && r;		

		r = _validate_( $('#b-expdate-input') );
		result = result && r;
		
		r = _validate_( $('#b-seccode-input') );
		result = result && r;

		r = _validate_( $('#b-fistname-input') );
		result = result && r;

		r = _validate_( $('#b-lastname-input') );
		result = result && r;

		r = _validate_( $('#b-street-input') );
		result = result && r;
		
		r = _validate_( $('#b-country') );
		result = result && r;

		r = _validate_( $('#b-city-input') );
		result = result && r;

		/*
		r = _validate_( $('#b-states') );
		result = result && r;
		*/
		
		r = _validate_( $('#b-postalcode-input') );
		result = result && r;
		
		var $tab = $('#paymentTab');
		$tab.collapse("show");
		$(window).scrollTop( $tab.offset().top );	
		
		return result;
	}
	
	$("button.checkout-next-btn").click(function(e){
		var $tab = $(this).closest('.panel-collapse');
		var r = false;
		switch ($tab.attr("id")) {
			case "aboutTab": 
				r = validateAboutTab();
				break;
			case "policiesTermsTab": 
				r = validatePoliciesTermsTab();
				break;
			case "confidenceTab": 
				r = validateConfidenceTab();
				break;
		}
		if (r) {
			$tab.collapse("hide");
			$tab.closest(".panel").next().find(".panel-collapse").collapse("show");			
		}
	});
	
	$("button.checkout-prev-btn").click(function(e){
		var $tab = $(this).closest('.panel-collapse');
		$tab.collapse("hide");
		$tab.closest(".panel").prev().find(".panel-collapse").collapse("show");
	});
	
	$('button.btn-submit-request').click(function(e){
		var res;
		res = validateAboutTab(true);
		if (res){
			res = validatePoliciesTermsTab(true);
		}
		if (res) {
			res = validateConfidenceTab(true);
		}
		if (res) {
			validatePaymentTab()
		}
	});
	
	$("#view-terms-conditions").click(function(e){
		var $modal = $("#checkout-termsConditions");
		var $iframe = $modal.find("iframe");
		var h = $(window).height() - 150;
		$iframe.css("height", h).attr("src", $(this).data("url") );
		$modal.modal("show");
	});
	
})