/**
 * Description: GEO FILTRERS
 * Version: 17052016.1
 * Requirements: jQuery, nc-core.js, geo-filters-template.html
 * Author: Alexander Bulei (alexander.bulei@gmail.com)
**/

(function ( $ ) {

	$.fn.geoFilter = function(options){
		var $element = $(this), obj = { $element: $element, options: options || {}  };
		obj.breadcrumbs = [];
		obj.userGeoFilters = [];
		
		obj.updateTop = function(full){
			obj.$breadcrumbs.empty();
			var len = obj.breadcrumbs.length;
			
			if (!full){
				obj.$breadcrumbs.append('<i class="fa fa-angle-double-right nc-filter-breadcrumbs-sep"></i>');
			}
			
			$.each(obj.breadcrumbs, function(i, breadcrumb){
				var $html;
				if (breadcrumb.haveSubLevels){
					$html = $('<a data-index="'+i+'" class="geo-filter-breadcrumb nc-filter-breadcrumb" data-path="'+breadcrumb.path+'" href="#">' + breadcrumb.text + '</a>');
				} else {
					$html = $('<span>' + breadcrumb.text + '</span>')
				}

				obj.$breadcrumbs.append($html);
				
				if (i < (len-1)){
					obj.$breadcrumbs.append('<i class="fa fa-angle-double-right nc-filter-breadcrumbs-sep"></i>');
				}				
			});
			
			
			obj.$title.text("");
			var tooltip = obj.options.rootTooltip;
			if (len > 0){
				// update title
				var prev = obj.breadcrumbs[len-2];
				if (!prev) {
					obj.$title.html('<a class="geo-filter-back-link" data-path="" href="#"><i style="margin-right:10px" class="fa fa-angle-double-left"></i>'+obj.options.rootTitle+'</a>');
				} else {
					var _object = obj.getObjectByPath(prev.path);
					obj.$title.html('<a class="geo-filter-back-link" data-path="'+prev.path+'" href="#"><i style="margin-right:10px" class="fa fa-angle-double-left"></i>'+_object.subLevelGroupName+'</a>');				
				}
				
				var last = obj.breadcrumbs[len-1];
				if (!last){
					tooltip = obj.options.rootTooltip;
				} else {
					var _object = obj.getObjectByPath(last.path);
					tooltip = _object.subLevelTooltip;
				}
				
				//distance filter
				if (obj.breadcrumbs[len-1].showDistanceFilter){
					obj.$distanceFilter.show();
				} else {
					obj.$distanceFilter.hide();
					obj.$distanceFilter.find('input').removeAttr("checked");
					obj.$distanceFilter.find('select').val(25);
				}				
			} else {
				tooltip = obj.options.rootTooltip;
				obj.$distanceFilter.hide();
				obj.$distanceFilter.find('input').removeAttr("checked");
				obj.$distanceFilter.find('select').val(25);
			}
			
			obj.$list.find('a').attr({
				"data-toggle": "tooltip",
				"data-placement": "top",
				"title": tooltip,
				"data-container": "body" 
			});
			obj.$list.find('[data-toggle="tooltip"]').tooltip();			
		}
		
		obj.insertFilter = function(data, path){
			var $li = $('<li data-path="'+path+'" data-filter-value="'+data.filterValue+'"><div class="checkbox checkbox-success"><input type="checkbox"><label><a class="nc-filter-item" data-id="'+data.id+'" href="#" data-filter-value="'+data.filterValue+'">'+data.name+'</a></label></div></li>');

			obj.$list.append($li);
			
			if (data.subLevelCanFilterByDistance){
				$li.data("canFilterByDistance", true);
			}
			
			if ((!data.subLevel) || (data.subLevel.length == 0)){
				$li.data("nomore", true);
			}
			
			$li.data("showDistanceFilter", data.subLevelCanFilterByDistance || false);
			$li.data("subLevelTooltip", data.subLevelTooltip || false);
			
		}
		
		obj.requestData = function(){
      
      function _processData(data){
				obj.geoFilters = data;
				obj.proccessSubLevels(data, '');
				
				obj.$list.find('a').attr({
					"data-toggle": "tooltip",
					"data-placement": "top",
					"title": obj.options.rootTooltip,
					"data-container": "body" 
				});
				obj.$list.find('[data-toggle="tooltip"]').tooltip();
      }
      
      if (obj.options.hasOwnProperty('dataUrl')){
        nc.get(obj.options.dataUrl, true, function(response){
          _processData(response);
        });
      } else {
        _processData(obj.options.data);
      }
		}
		
		obj.openPanel = function(){
			obj.$toggle.hide();
			obj.$body.slideDown();
		}
		
		obj.closePanel = function(speed){
			if (speed == 0){
				obj.$body.hide();
			} else {
				obj.$body.slideUp( (speed ? speed : 400), function(){
					obj.$toggle.show();
				});
			}
		}
		
		obj.goToPath = function(path){
			if (path.length){
				var subLevels = obj.getObjectByPath(path).subLevel || [];
				obj.proccessSubLevels(subLevels,  path);
			} else {
				obj.proccessSubLevels(obj.geoFilters, '');
			}
		}
		
		obj.delegates = function(){
			
			obj.$breadcrumbs.delegate('a.geo-filter-breadcrumb','click', function(event){
				event.preventDefault();
				var $this = $(this);
				var path = $this.data("path");
				var index = $this.data("index");
				obj.goToPath(path);
				var len = obj.breadcrumbs.length;
				obj.breadcrumbs.splice(index+1,len-index);
				obj.updateTop();
			});
			
			obj.$element.delegate('#geo-filter-reset','click', function(event){
				event.preventDefault();
				obj.proccessSubLevels(obj.geoFilters, '');
				obj.breadcrumbs = [];
				obj.updateTop(true);
				obj.$distanceFilter.hide();
				obj.$distanceFilter.find('input').removeAttr("checked");
				obj.$distanceFilter.find('select').val(25);
			});
			
			obj.$element.delegate('a.geo-filter-back-link','click', function(event){
				event.preventDefault();
				var $this = $(this);
				var path = $this.data("path");
				obj.goToPath(path);
				var len = obj.breadcrumbs.length;
				obj.breadcrumbs.splice(len-1,1);
				obj.updateTop();				
			});
			
			obj.$element.delegate('a.geo-filter-toggle','click',function(event){
				event.preventDefault();
				obj.openPanel();
			});
			
			function _doFilter(){
				var csensitive = obj.$colLiveInput.find('input.nc-filter-case-sensitive')[0].checked;
				var v = obj.$colLiveInput.find('input.nc-filter-live-input').val();
				var searchFor = csensitive ? v : v.toLowerCase();
				
				obj.$list.find("li").each(function(i,li){
					var $li = $(li);
					var match = $li.data("filterValue");
					var str = csensitive ? match : match.toLowerCase();
					if (str.indexOf(searchFor) > -1) $li.show(); else $li.hide();
				});					
			}
			
			obj.$colLiveInput.find('input.nc-filter-case-sensitive').click(function(){
				_doFilter();
			});
			
			obj.$colLiveInput.find('input.nc-filter-live-input').on("change keyup", function(){
				_doFilter();
			});
			
			obj.$element.delegate('a.nc-filter-collapse','click',function(){
				obj.closePanel();
			});
			
			obj.$element.delegate('a.nc-filter-item','click', function(event){
				event.preventDefault();
				var $this = $(this);
				var $li = $this.closest("li");
				var clickedId = $this.data("id");
				var itemPath = $li.data("path");
				var newPath = itemPath + '/' + clickedId;
				
				var _object = obj.getObjectByPath(newPath);
				var subLevels = _object.subLevel || [];
				
				var _showDistanceFilter = $li.data("showDistanceFilter");
				var _subLevelTooltip = $li.data("subLevelTooltip");
				obj.breadcrumbs.push({
					text: $.trim($this.text()),
					path: newPath,
					showDistanceFilter: _showDistanceFilter,
					subLevelTooltip: _subLevelTooltip,
					haveSubLevels: (!$li.data("nomore"))
				});				
				
				if (_showDistanceFilter){
					obj.$distanceFilter.show();
				} else {
					obj.$distanceFilter.hide();
					obj.$distanceFilter.find('input').removeAttr("checked");
					obj.$distanceFilter.find('select').val(25);
				}
				
				obj.proccessSubLevels(subLevels,  newPath);
				obj.updateTop();
			})
			
			obj.$distanceFilter.find('input.nc-filter-check-distance, select').change(function(){
				var breadcrumbObj = obj.breadcrumbs[obj.breadcrumbs.length-1];
				
				$.each(obj.userGeoFilters, function(i, item){
					if (item.path == breadcrumbObj.path){
						item.useDistanceFilter = obj.$distanceFilter.find('input[type="checkbox"]')[0].checked;
						item.distance = item.useDistanceFilter ? obj.$distanceFilter.find('select').val() : -1;
						return false;
					}
				})
				obj.triggerOnFilter();
			});			
			
			obj.$list.delegate('input[type="checkbox"]','change', function(){
				var $this = $(this),
					$li = $this.closest("li"),
					path = $li.data("path"),
					id = $this.next().find('a.nc-filter-item').data("id");
				
				if (this.checked){
					obj.add(path, id);
				} else {
					obj.remove(path, id);
				}
				obj.triggerOnFilter();
			});			
		}
		
		obj.add = function(path, id){
			var p = path.length ? path : "root";
			
			function _insertNew(){
				var _new = {
					path : p,
					items: [id]
				};
				obj.userGeoFilters.push(_new);
			}
			
			var objPath;
			if (obj.userGeoFilters.length){
				objPath = $.grep(obj.userGeoFilters,function(item){
					return item.path == p;
				})[0];
				if (objPath) {
					objPath.items.push(id);
				} else {
					_insertNew()
				}
			} else {
				_insertNew()
			}
		}
		
		obj.remove = function(path, id){
			var p = path.length ? path : "root";
			var objPath = $.grep(obj.userGeoFilters,function(item){
				return item.path == p;
			})[0];
			
			if (objPath) {
				objPath.items = $.grep(objPath.items, function(item){
					return item != id;
				});
			}
		}
		
		obj.triggerOnFilter = function(){
			var fullPathsArray = [];
			$.each(obj.userGeoFilters, function(i, item){
				var path = (item.path == "root") ? "" : item.path;
				var checkedArr = item.items ? item.items : [];
				$.each(checkedArr, function(i, checkedId){
					var newpath = path + '/' + checkedId;
					fullPathsArray.push(newpath);
				})
			});
			obj.options.onFilter( obj.userGeoFilters, fullPathsArray );
		}		
		
		obj.getObjectByPath = function(path){
			var pathArray = path.split('/');
			var currObj, nextFromRoot = false;
			$.each(pathArray, function(i, p){
				if (p == ""){
					nextFromRoot = true;
					currObj = obj.geoFilters;
				} else {
					var runObj = currObj.subLevel;
					if (nextFromRoot) {
						runObj = currObj;
						nextFromRoot = false;
					}
					
					$.each(runObj, function(i, item){
						if (item.id == p){
							currObj = item;
							return false;
						}
					});
				}
			});
			return currObj;
		}
		
		obj.proccessSubLevels = function(array, path){
			obj.$list.find('[data-toggle="tooltip"]').tooltip("hide");
			obj.$list.empty();

			$.each(array, function(i, object){
				obj.insertFilter(object, path);
			});
			
			// check for selected
			$.each(obj.userGeoFilters, function(i, item){
				var p = path.length ? path : "root";
				if (item.path == p){
					var arr = item.items || [];
					$.each(arr, function(i, selectedId){
						var $link = obj.$list.find('a[data-id="'+selectedId+'"]');
						if ($link.length){
							$link.closest('li').find('input[type="checkbox"]').attr("checked","checked");
						}
					});
					
					if (obj.$distanceFilter.is(":visible")){
						if (item.useDistanceFilter){
							obj.$distanceFilter.find('input.nc-filter-check-distance')[0].checked = true;
							obj.$distanceFilter.find('select').val(item.distance);
						} else {
							obj.$distanceFilter.find('input').removeAttr("checked");
							obj.$distanceFilter.find('select').val(25);
						}
					}
					return false;
				}
			})
			
		}
		
		obj.init = function(){
			nc.get(obj.options.tmplUrl, true, function(html){
				obj.$element.empty().append(html);
				
				obj.$breadcrumbs = obj.$element.find("#geo-filter-breadcrumbs-body");
				obj.$body = obj.$element.find("#geo-filter-body");
				obj.$list = obj.$element.find("#geo-filters");
				obj.$distanceFilter = obj.$element.find("#geo-filter-by-distance");
				obj.$liveInput = obj.$element.find("#geo-filter-live");
				obj.$colLiveInput = obj.$element.find("#col-geo-filter-live");
				obj.$toggle = obj.$element.find("a.geo-filter-toggle");
				obj.$title = obj.$element.find(".nc-filter-title");

				obj.delegates();
				obj.requestData();
				
				//obj.$title.html(obj.options.rootTitle);
			})
		}
		
		obj.init();
	}
}( jQuery ));