/**
 * Project: NCTourism
 * Page: contact.html
 * Last Modified: 08-05-2015
 * Author: Alexander Bulei (alexander.bulei@gmail.com)
**/
 
$(function(){
	// includes
	nc.include('tmpl/header.html','begin');
	nc.include('tmpl/footer.html');
	nc.include('tmpl/login.html');
	
	$("input,textarea", "#contact-form").not("[type=submit]").jqBootstrapValidation({
        preventSubmit: true,
        submitSuccess: function($form, event) {
            event.preventDefault();
			var _serverUrl = '';
            // get values from FORM			
			var _params = $form.serialize();
			
            $.ajax({
                url: _serverUrl,
                type: "POST",
                data: _params,
                cache: false,
                success: function() {
                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
                error: function() {
                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
            })
        },
        filter: function() {
            return $(this).is(":visible");
        }
    });
})
