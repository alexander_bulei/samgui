/**
 * Project: NCTourism
 * Page: destinations.html
 * Last Modified: 08-03-2016
 * Author: Alexander Bulei (alexander.bulei@gmail.com)
**/

function sectionResponsive(){
    var wh = $(window).height();
    $("#nct-fullpage .section").height(wh);
}

$(function(){
    SmoothScroll({ stepSize: 100 });
    
	// includes
	nc.include('tmpl/header.html','begin');
	nc.include('tmpl/footer.html');
	nc.include('tmpl/login.html');

    sectionResponsive();
    
    var sliderOptions = {
        maxItems: 4,
        itemWidth: 200,
        itemMargin: 10,
        animation: "slide",
        slideshow: false
    }, newOpts;
    
    // MAIN SLIDER
    newOpts = $.extend({},sliderOptions, { customDirectionNav: $("#main-slider-nav a") });
    $("#nct-main-slider").flexslider(newOpts);
    
    
    newOpts = $.extend({},sliderOptions, { customDirectionNav: $("#city-section-slider-nav a") });
    $("#nct-city-slider").flexslider(newOpts);
    
    newOpts = $.extend({},sliderOptions, { customDirectionNav: $("#section4-slider-nav a") });
    $("#nct-section4-slider").flexslider(newOpts);

    newOpts = $.extend({},sliderOptions, { customDirectionNav: $("#section5-slider-nav a") });
    $("#nct-section5-slider").flexslider(newOpts);

    newOpts = $.extend({},sliderOptions, { customDirectionNav: $("#section6-slider-nav a") });
    $("#nct-section6-slider").flexslider(newOpts);
    
    //SHOW SLIDERS
    $(".nct-flex-slider").css("visibility","visible")
   
    $(window).on("resize", function(e){
       sectionResponsive();
    });
    
    $(".page-navigation a").click(function(e){
        e.preventDefault();
        var $section = $("#" + $(this).data("section") );
        $(this).closest(".page-navigation").find('a.active').removeClass("active");
        $(this).addClass("active");
        
        $('body, html').stop(true, true).animate({
            scrollTop: $section.offset().top
        },300);
        
    });
});    