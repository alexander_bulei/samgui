self.onmessage = function(e) {
  var result = {}
  if (e.data.url){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", e.data.url, true);
    xhr.onload = function (e) {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          result.data = JSON.parse(xhr.responseText);
        } else {
          result.error = xhr.statusText;
        }
        self.postMessage(result);
      }
    };
    xhr.onerror = function (e) {
      result.error = xhr.statusText;
      self.postMessage(result);
    };
    xhr.send(null);
  }
};