/**
 * Project: NCTourism
 * Page: General script for all pages
 * Last Modified: 09062016.1
 * Author: Alexander Bulei (alexander.bulei@gmail.com)
**/

function execDelay(func, delay){
	setTimeout(func,delay);
}
  
function initDatePickers($checkin,$checkout){
	/*Checkin input*/
	$checkin.datetimepicker({
		format: 'MM/DD/YYYY',
    showClear: true,
    useCurrent: false,
    clearBtnText: 'Clear'
	});
	
	$checkin.closest('.input-group').find('.input-group-addon').on('click', function(e){
		$checkin.data("DateTimePicker").show();
	});
	
	$checkin.on('dp.change', function(){		
		$(this).removeClass("required-error");
		var dpicker = $(this).data("DateTimePicker");
		if (dpicker) {
			var dObj = dpicker.date();
      if (dObj) {
        var _newDate =  moment(dObj._d)._d;
        $checkout.data("DateTimePicker").minDate(_newDate);
      } else {
        $checkout.data("DateTimePicker").clear();  
      }
		}
	});
  
	/*Checkout input*/
	$checkout.datetimepicker({
		format: 'MM/DD/YYYY',
		useCurrent: false,
    showClear: true,
    clearBtnText: 'Clear'
	});

	$checkout.closest('.input-group').find('.input-group-addon').on('click', function(e){
		$checkout.data("DateTimePicker").show();
	});
	
	$checkout.on('dp.change', function(){
		$(this).removeClass("required-error");
    var dpicker = $(this).data("DateTimePicker");
    if (dpicker && !dpicker.date()) {
      $checkin.data("DateTimePicker").clear();  
    }
	});
}

function checkCustomResponsive(){
	if ($(window).width() <= 992){
		$(".row-list-prop-form").after($(".row-list-prop-bottom"));
	} else {
		$(".row-list-prop-body").after($(".row-list-prop-bottom"));
	}	
}

function nc_editmode($element, editCallback, restoreCallback){
	var mode = $element.data("mode") ? $element.data("mode") : "view";
	if (mode == "view"){
		$element.data("mode","edit");
		$element.data("restoreInnerHtml", $element.html());
		$element.addClass("btn btn-success fronend-save-btn").html("<i class='fa fa-check'></i> Done");
		editCallback();
	} else {
		var $innerHtml = $element.data("restoreInnerHtml");
		$element.data("mode","view");
		$element.removeClass("btn btn-success fronend-save-btn").html($innerHtml);
		restoreCallback();
	}
}

function _open(e){
	var $wrapper = $(".back-filter-container");
	var $invoker = $("a.invoke-link", $wrapper);
	if ($wrapper.hasClass("openned")){			
		$wrapper.stop().animate({
			left: "-=204"
		}, 700, function() {
			$invoker.find("i").addClass("fa-search").removeClass("fa-times");
		});
		$wrapper.removeClass('openned');
	} else {			
		$invoker.find("i").removeClass("fa-search").addClass("fa-times");
		$wrapper.stop().animate({
			left: "+=204"
		}, 700, function() {
			
		});
		$wrapper.addClass('openned');			
	}
}

function createNCBSModal(id, options){
	var _options = {
		title: '',
		buttons: {
			cancel: true,
			save: true,
			cancelText: 'Cancel',
			saveText: 'Save'
		}
	};
	
	_options = $.extend(true, _options, options);
	
	var $modal = $("#" + id);
	if (!$modal.length){
		var	html = '<div class="modal fade nc-modal" id="'+id+'"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title">'+_options.title+'</h4></div><div class="modal-body"></div>';
		if (_options.buttons) {
			html += '<div class="modal-footer">';
			if (_options.buttons.cancel){
				html += '<button type="button" class="btn btn-default" data-dismiss="modal">'+_options.buttons.cancelText+'</button>';
			}
			if (_options.buttons.save){
				html += '<button type="button" class="btn btn-primary">'+_options.buttons.saveText+'</button>';
			}
			html += '</div>'; 
		}
		html += '</div></div></div>';
		$('body').append(html);
	}
	return $modal;
}

function backToSearchFilters(){
	var $container = $(".back-filter-container");
	$container.css("left", "-204px");
	$("a.invoke-link i").removeClass("fa-times").addClass("fa-search");	
	$("a.invoke-link", $container).on("click", _open);	
}

function initializeAdminBar(){
	var $bar = $(".admin-top-navbar.topfixed");
	if ($bar.length){
		// change the position
		$bar.insertBefore($("header")).show();
		$('body').addClass("have-adminbar");
	}
}

// ON-READY
$(function() {
	$(".aspect-fit").parent().imgLiquid({fill:true});
	$(window).on("resize", checkCustomResponsive);
	
	nc.include('tmpl/list-my-prop.html');
});