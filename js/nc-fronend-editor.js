/**
 * Project: NCTourism
 * Description: Font-End Editor for property-single.html
 * Last Modified: 08-06-2015
 * Author: Alexander Bulei (alexander.bulei@gmail.com)
**/

if (typeof NCFEE != "undefined"){

	NCFEE = {
		_$containers : null,
		_options : {
			saveHandler: function(){},
			onEdit: function(){},
		},
		init: function(options){
			this._$options = $options;
		}
		edit: function($containers){
			var self = this;
			this._$containers = $containers;
			
			$containers.each(function(i, $container){
				$container.find("[data-ncfee='true']").each(function(i,element){
					var $el = $(element);
					var type = $el.data("ncfeeType");
					
					switch (type){
						case "input":
							var $input = $("<input>").attr({
								"class": "form-control ncfee-element",
								"type" : "text",
								"value": $el.text()
							});
							var _oldHtml = $el[0].outerHTML;
							$el.replaceWith($input);
							$el.data("ncfee-restore-html", _oldHtml);
							break;
						case "select":
						
							break;
					}
					self._options.onEdit();
				});
			});
		},
		save: function(){
			
		}
	}
}