$(function(){
	schemeManeger = {
		request: function(url, callback){
			$.ajax({
				"url": url,
				"success": callback,
				"error": function(jqXHR, textStatus, errorThrown){
					alert(jqXHR.responseText);
				}
			});
		},		
		buildColorScheme: function(obj){
			var colorSchemeTmpl = '';
			
			if (obj.adminTopBarBg) {
				colorSchemeTmpl = '.admin-top-navbar .navbar .navbar-inner{ background: ' + obj.adminTopBarBg + ' }';
				colorSchemeTmpl += '.admin-top-navbar .navbar .navbar-inner .navbar-header .navbar-account .account-area > li > a .badge{ box-shadow: 1px 1px 0 '+obj.adminTopBarBg+'; }';
				colorSchemeTmpl += '.admin-top-navbar .navbar .navbar-inner .navbar-header .navbar-account .account-area > li.open > a{ background-color: #333; }';
				colorSchemeTmpl += '.admin-top-navbar .navbar .navbar-inner .navbar-header .navbar-account .account-area > li .dropdown-menu.dropdown-notifications li.dropdown-footer { background-color: '+obj.adminTopBarBg+'; }';
				colorSchemeTmpl += '#top-navigation ul.sub-menu > li:first-child { border-color: #333; }';
				colorSchemeTmpl += '.admin-top-navbar .navbar .navbar-inner .navbar-header .navbar-account.setting-open .setting{ background-color:#333; }'
			}
			
			if (obj.green){
				colorSchemeTmpl += '.bordered-palegreen, .bordered-green{ border-color: '+obj.green.color+'!important;}';
				if (obj.green.button){
					colorSchemeTmpl += '.btn-success, .btn-palegreen{border-color:'+obj.green.button.border+'!important;background: '+obj.green.color+'!important;}.btn-success:hover, .btn-palegreen:hover{background:'+obj.green.button.hover+'!important;}';
				}
			}
			
			if (obj.blue){
				//.tooltip-info+.tooltip>.tooltip-inner
				colorSchemeTmpl += '.bordered-sky, .bordered-blue, .nav-tabs>li.active.tab-blue>a, .modal-blue .modal-header, .wizard ul li.active .step{ border-color: '+obj.blue.color+'!important;}.h1.row-title:before, h2.row-title:before, h3.row-title:before, h4.row-title:before, h5.row-title:before, h6.row-title:before, .bg-blue, .wizard ul li.active:before{background-color: '+obj.blue.color+'!important; }.wizard ul li.active .step{ color: '+obj.blue.color+'}';
				if (obj.blue.button){
					colorSchemeTmpl += '.btn-blue,.btn-info{border-color:'+obj.blue.button.border+'!important;background: '+obj.blue.color+'!important;}.btn-blue:hover{background:'+obj.blue.button.hover+'!important;}';
				}
			}
			
			if (obj.yellow){
				colorSchemeTmpl += '.before-warning:before{ background-color: '+obj.yellow.color+'!important;}.bordered-gold{ border-color: '+obj.yellow.color+'!important;}';
				if (obj.yellow.button){
					colorSchemeTmpl += '.btn-warning{border-color:'+obj.yellow.button.border+'!important;background: '+obj.yellow.color+'!important;}.btn-warning:hover{background:'+obj.yellow.button.hover+'!important;}';
				}
			}
			
			if (obj.red){
				if (obj.red.button){
					colorSchemeTmpl += '.btn-danger{border-color:'+obj.red.button.border+'!important;background: '+obj.red.color+'!important;}.btn-danger:hover{background:'+obj.red.button.hover+'!important;}.bg-lightred{ background-color:'+obj.red.color+'!important}';
				}
			}			
			
			if (obj.pink){
				colorSchemeTmpl += '.bordered-pink{ border-color: '+obj.pink.color+'!important;}';
				if (obj.pink.button){
					colorSchemeTmpl += '.btn-danger{border-color:'+obj.pink.button.border+'!important;background: '+obj.pink.color+'!important;}.btn-danger:hover{background:'+obj.pink.button.hover+'!important;}';
				}
			}			
			
			return colorSchemeTmpl;
		},
		loadColorScheme: function(url){
			var self = this;
			if ((url) && (url.length)) {
				this.request(url, function(response){
					var style = self.buildColorScheme(response);
					self.renderStyle(style);
				});
			}
		},
		renderStyle : function(style){
			$('head').append('<style id="dynamic-scheme" type="text/css">'+style+'/style>');
		}		
	}
})