/**
 * Project: NCTourism
 * Description: search-todo.html
 * Last Modified: 23-09-2015
 * Author: Alexander Bulei (alexander.bulei@gmail.com)
**/

 var INIT_RECORDS, MEM_RECORDS = [], _WORKER, PAGE_STATE = { startRecord: 0 };
 var _MAP_INST;
 var _MARKERS = [];
 var _INFO_BOX;
 
 // Sets the map on all markers in the array.
function setAllMap(map) {
  for (var i = 0; i < _MARKERS.length; i++) {
    _MARKERS[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setAllMap(null);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
  clearMarkers();
  _MARKERS = [];
}
 
 function loadMap(arr){
	var _arr = arr || [];
	
	if (_arr.length){
		var $map = $("#view-map");
		// init map
		var firstItem = _arr[0];
		var _MAP_INST = new google.maps.Map($map[0],{
			center: new google.maps.LatLng(firstItem.latLng[0],firstItem.latLng[1]),
			zoom:11,
			mapTypeId:google.maps.MapTypeId.ROADMAP,
			streetViewControl:false,
			scrollwheel:false
		});
		
		google.maps.event.addListener(_MAP_INST, "click", function(event) {
			if (typeof _INFO_BOX != "undefined") {
				_INFO_BOX.close();
			}			
		});
		
		$.each(_arr, function(i, item){
			var latLng = item.latLng || [];
			if (latLng.length){
				var marker = new google.maps.Marker({
					position: new google.maps.LatLng(latLng[0],latLng[1]),
					map: _MAP_INST,
					title: item.hotelName,
					icon : 'img/marker.png',
					id : item.id					
				});
				
				google.maps.event.addListener(marker, 'click', function() {
					var _marker = this;
					
					if (typeof _INFO_BOX != "undefined") {
						_INFO_BOX.close();
					}
					
					nc.get('demo/quickview.json', true, function(item){
						var dataTmpl = {
							id: item.id,							
							hotelName: item.hotelName,
							location: item.location,
							address: item.address,
							hotelPhone: item.hotelPhone,
							rating: item.rate,
							description: item.description
						}					
						var $html = $.tmpl("map", dataTmpl);
						
						addImagesToSlider(item, $html.find('ul.thumb-slider') );
						
						var myOptions = {
							content: $html[0],
							disableAutoPan: false,
							maxWidth: 0,
							pixelOffset: new google.maps.Size(25, -300),
							zIndex: null,
							boxStyle: { 
								background: "#FFF",
								opacity: 1,
								width: "400px",
								height: "400px",
								border: "3px solid #D02A38"
							},
							closeBoxMargin: "10px 2px 2px 2px",
							closeBoxURL: "",
							infoBoxClearance: new google.maps.Size(1, 1),
							isHidden: false,
							pane: "floatPane",
							enableEventPropagation: false,
							alignBottom: false
						};

						_INFO_BOX = new InfoBox(myOptions);						
						_INFO_BOX.addListener("domready", function() {
							var $body = $("#infobox-body");
							$(".infoBox").css("height","auto");
							initRating($body);
							initSlider($body,{ nav: true });
							bindQuickviewLinks($body);
							$(".aspect-fit").parent().imgLiquid({fill:true});
						});						
						
						_INFO_BOX.open(_MAP_INST, _marker);
					});
					
				});

				/*
				google.maps.event.addListener(marker, 'mouseout', function() {
					_INFO_BOX.close();					
				});				
				*/
				
				_MARKERS.push(marker);
			}
		});
	} else {
		if (typeof _MAP_INST != "undefined") {
			_MAP_INST.setZoom(1);
			deleteMarkers();
		}		
	}
 }
 
 function initRating($container){
	var $inputRating = $container.find('input.rating');
	$inputRating.rating({
		size : 'xxs',
		showCaption: false,
		showClear: false,
		readonly: true
	});
 }
 
 function initSlider($container, addOpts){
	$container.find(".thumb-slider").each(function(i, slider){
		var _$slider = $(slider);
		var _opts = {
			auto: false,
			speed: 800,
			pause: true,
			defaultPaused: true,
			timeout: 3000
		};
		_opts = $.extend(true, _opts, addOpts || {} );
		_$slider.responsiveSlides(_opts);		
	});
 }
 
 function addImagesToSlider(obj, $slider){
	if ($.isArray(obj.images) && (obj.images.length) ){
		$.each(obj.images, function(i, imageObj){
			var _$li = $('<li></li>'),
			$a = $('<a class="view-prop-details" href="javascript:void(0);"></a>');
			$img = $('<img/>').attr({
				"src": imageObj.src,
				"class": "aspect-fit",
				"alt": imageObj.alt
			});
			$a.append($img).appendTo(_$li);
			$slider.append(_$li);					
		});
	}	 
 }
  
function loadMoreRecords(){
	var $last = $('.search-result-body li.main:last');
	var MORE_RECORDS = [],
		max = getMaxVisible(),
		end = PAGE_STATE.startRecord + max;
		
	end = (end <= MEM_RECORDS.length) ? end : MEM_RECORDS.length;

	for (var i = PAGE_STATE.startRecord; i < end; i++) {
		MORE_RECORDS.push( MEM_RECORDS[i] );
	}
	PAGE_STATE.startRecord = end;
	
	processRecordsArrays(MORE_RECORDS);
}  
  
function makeSearch(){
	$('.search-result-body').empty();
	processRecordsArrays(MEM_RECORDS || []);
}	
 
function getMaxVisible(){
	return $(".btn-group.view button.active").data("id") == 'grid' ? nc.config.pages.searchTodo.gridNumVisibleRecords : nc.config.pages.searchTodo.listNumVisibleRecords;
} 
 
function processRecordsArrays(array){
	var $body = $('.search-result-body'),
		$activeMode = $(".btn-group.view button.active"),
		type = $activeMode.data("id"),
		$map = $("#view-map"),
		$mapRow = $(".row-search-map");
		
	$body.removeClass("grid list map");
	$body.addClass(type);		

	if ($activeMode.data("id") != "map"){
		$mapRow.hide();
		var max = getMaxVisible();
		$.each(array, function(i, item){
				if (i < max) {
					var dataTmpl = {
						id: item.id,
						hotelName: item.hotelName,
						hotelPhone: item.hotelPhone,
						address: item.address,
						location: item.location,
						rating: item.rate,
						description: item.description
					}
					
					var $html;
					if ($activeMode.data("id") == "grid"){
						$html = $.tmpl("grid", dataTmpl);
					} else if ($activeMode.data("id") == "list"){
						$html = $.tmpl("list", dataTmpl);
					}

					$body.append($html);

					var $thumbSlider = $body.find('.result-item-container:last .thumb-slider');
					addImagesToSlider(item,$thumbSlider);
				}
			});
			
			
			initRating($body);
			initSlider($body, { nav: true });
			bindQuickviewLinks($body);
			
			$(".view-prop-details").on("click", function(e){
				var id = $(this).closest("li.main").data("id");
				window.location.href = 'property-single.html?id=' + id;
			});
			
			$(".aspect-fit").parent().imgLiquid({fill:true});
	} else {
		$mapRow.show();
		loadMap(array);
	}
		
	if (PAGE_STATE.startRecord < MEM_RECORDS.length) {
		$(".row-show-more").show();
	} else {
		$(".row-show-more").hide();
	}

}

 function bindQuickviewLinks($where){
	$("a.quickview", $where).on("click", function(e){
		var $a = $(this);
		var $modal = $("#quickViewModal");
		var id = $a.closest("li").data("id");
		
		nc.get('demo/quickview.json', true, function(item){
			var dataTmpl = {
				id: item.id,
				hotelName: item.hotelName,
				hotelPhone: item.hotelPhone,
				location: item.location,
				address: item.address,
				rating: item.rate,
				reviews: item.reviews,
				description: item.description
			}					
			var $html = $.tmpl("quickview", dataTmpl);
			// extra-hotel
			var _arr = item.hotelInfo || [];
			if (_arr.length) {
				$extraInfo = $html.find('div.extra-hotel');
				$.each(_arr, function(i, ex){
					$("<div class='row-extra-hotel'><i class='fa fa-check-circle text-green'></i> "+ex+"</div>").appendTo($extraInfo);
				})
			}
			var $body = $modal.find('.quick-view-body');
			$body.empty().append($html);

			addImagesToSlider(item, $body.find('.thumb-slider') );
			
			initRating($body);
			initSlider($body);
			
			var latLngArr = item.latLng || [];
			if (latLngArr.length){
				$modal.off('shown.bs.modal').on('shown.bs.modal', function () {
					var $map = $(this).find(".hotel-map");
					var map = new google.maps.Map($map[0],{
						center:new google.maps.LatLng(latLngArr[0],latLngArr[1]),
						zoom:11,
						mapTypeId:google.maps.MapTypeId.ROADMAP,
						streetViewControl:false,
						scrollwheel:false
					});
					var marker = new google.maps.Marker({
						position: new google.maps.LatLng(latLngArr[0],latLngArr[1]),
						map: map,
						title: item.hotelName
					});					
				});
			}
		});

		$modal.data("id", id).modal("show");
	});
}

function initSearchPage(){
	nc.get('demo/data.json', true, function(data){
		MEM_RECORDS = INIT_RECORDS = data ? data : [];
		sendToWorker({
			sortby: "popularity",
			order: "desc",
			array: MEM_RECORDS
		});		
	});
}

function sendToWorker(data){
	stopWorker();
	startWorker();
	_WORKER.postMessage(data);
}

function stopWorker() { 
	if (typeof _WORKER != "undefined"){
		_WORKER.terminate();
		_WORKER = undefined;
	}
}

function startWorker() {
    if(typeof(Worker) !== "undefined") {
        if(typeof(_WORKER) == "undefined") {
            _WORKER = new Worker("js/sort-worker.js");
        }
		
        _WORKER.onmessage = function(event) {
			MEM_RECORDS = event.data;
			stopWorker();
			processRecordsArrays(MEM_RECORDS);
        };
    } else {
        alert("No Web Worker support.");
    }
}

function filterRecords(filter){
	MEM_RECORDS = $.grep(INIT_RECORDS, function(record){
		var integer;
		
		/** CHECK CITY/TOWN**/
		integer = parseInt(record.townId);
		if (filter.cities.length > 0){
			if ($.inArray(integer, filter.cities) == -1){
				return false;
			}
		}
		
		/** CHECK BEDROOMS **/
		integer = parseInt(record.bedRooms);
		if ((filter.bedrooms[0] > integer) || (integer > filter.bedrooms[1])){
			return false;
		}
		
		/** CHECK BATHROOMS **/
		integer = parseInt(record.bathRooms);
		if ((filter.bathrooms[0] > integer) || (integer > filter.bathrooms[1])){
			return false;
		}
		
		/** CHECK SLEEPS **/
		integer = parseInt(record.guests);
		if ((filter.sleeps[0] > integer) || (integer > filter.sleeps[1])){
			return false;
		}
		
		/** CHECK AMENTITIES **/
		if (filter.amentities.length > 0){
			var result = false;
			$.each(filter.amentities, function(i, filtred){
				if (!result){
					result = ($.inArray(filtred, record.amentities) != -1);
				}
				if (result) {
					return false; // EXIT FROM EACH
				}
			});
			if (!result) {
				return false; // EXCLUDE FROM GREP
			}
		}
		
		return true;
	});
	makeSearch();
}
 
$(function(){
	// includes
	
	nc.include('tmpl/header.html','begin');
	nc.include('tmpl/footer.html');
	nc.include('tmpl/login.html');
	nc.searchSidebar( $(".filter-container"), function(filter){
		filterRecords(filter);
	});
	
	$(".btn-group.view button").on("click", function(e){
		var $btn = $(this);
		$btn.addClass("active");		
		$other = $(".btn-group.view button");
		$.each($other, function(i, button){
			var $button = $(button);
			if (!$button.is($btn)){
				$button.removeClass("active");
			}
		});
		PAGE_STATE.startRecord = getMaxVisible();
		makeSearch();
	});
		
		
	$("#search").on('click', function(e){
		//makeSearch();
	});
	
	var _TMPL_GRID = '<li class="main" data-id="${id}"><div class="result-item-container"><div class="result-item-wrapper"><ul class="thumb-slider"></ul><div class="rate-block"><input type="number" class="rating" value="${rating}"/></div><div class="row no-margins padding-lr-10"><div class="col-xs-12 col-md-12 col-lg-12 no-padding text-left hotelname"><a href="javascript:void(0);" class="hotelname-link">${hotelName}</a></div></div><div class="row no-margins row-location padding-lr-10"><div class="col-xs-12 col-md-12 col-lg-12 no-padding text-left"><i class="fa fa-map-marker"></i> ${location}</div></div><div class="row no-margins padding-lr-10"><div class="col-xs-7 col-md-7 col-lg-7 no-padding text-left"><a href="tel:${hotelPhone}" class="hotel-phone"><i class="fa fa-phone"></i> ${hotelPhone}</a></div><div class="col-xs-5 col-md-5 col-lg-5 no-padding text-right"><a class="quickview" href="javascript:void(0);">Quick View <i class="fa fa-angle-double-right"></i></a></div></div><div class="row no-margins"><div class="col-xs-12 col-md-12 col-lg-12 no-padding"><button class="btn btn-book-now btn-nc-green">DETAILS <i class="fa fa-arrow-right"></i></button></div></div></div></li>';
	
	var _TMPL_LIST = '<li class="main" data-id="${id}"><div class="row result-item-container"><div class="col-xs-12 col-md-4 col-lg-4"><ul class="thumb-slider"></ul></div><div class="col-xs-12 col-md-8 col-lg-8"><div class="inside-row"><div class="inline list-hotelname"><a href="javascript:void(0);" class="hotelname-link">${hotelName}</a></div><div class="inline pull-right"><input type="number" class="rating" value="${rating}"/></div></div><div class="inside-row"><div class="inline"><i class="fa fa-map-marker"></i> ${location}</div><div class="inline pull-right list">${address}</div></div><div class="inside-row"><div class="inline"><i class="fa fa-phone"></i><a href="tel:${hotelPhone}" class="hotel-phone"> ${hotelPhone}</a></div></div><hr style="margin-right:20px;"/><div class="inside-row description">${description}</div></div><div class="inline buttons"><a class="quickview" href="javascript:void(0);">Quick View <i class="fa fa-angle-double-right"></i></a><button class="btn btn-book-now btn-nc-green">DETAILS <i class="fa fa-arrow-right"></i></button></div></div></li>';
	
	var _TMPL_QUICKVIEW = '<div class="row no-margins quickview"><div class="col-xs-12 col-md-6 col-lg-6 no-padding qv-col-left"><div class="row row-quickview row-slider no-margins"><div class="col-xs-12 col-md-12 col-lg-12 no-padding"><ul class="thumb-slider"></ul></div></div><div class="row row-quickview"><div class="col-xs-8 col-md-8 col-lg-8 no-padding text-left"><div class="row row-inside qv-hotel-name">${hotelName}</div><div class="row row-inside"><div class="col-xs-12 col-md-12 col-lg-12 no-padding text-left"><div><i class="fa fa-map-marker"></i> ${location}</div><div>${address}</div></div></div><div class="row row-inside"><div class="col-xs-12 col-md-12 col-lg-12 no-padding text-left"><i class="fa fa-phone"></i><a href="tel:${hotelPhone}" class="hotel-phone"> ${hotelPhone}</a></div></div></div><div class="col-xs-4 col-md-4 col-lg-4 no-padding text-right"><input type="number" class="rating" value="${rating}"/><div class="reviews">${reviews} Reviews</div></div></div><div class="row row-quickview row-map no-margins"><div class="col-xs-12 col-md-12 col-lg-12 no-padding"><div class="hotel-map"></div></div></div></div><div class="col-xs-12 col-md-6 col-lg-6 padding-right-0"><h4>Details</h4><div class="row row-quickview no-margins"><div class="col-xs-12 col-md-12 col-lg-12 no-padding extra"></div></div><div class="row row-quickview no-margins"><div class="col-xs-12 col-md-12 col-lg-12 no-padding">${description}</div></div><div class="row row-quickview no-margins"><div class="col-xs-12 col-md-12 col-lg-12 no-padding extra-hotel"></div></div><div class="row row-quickview no-margins"><div class="col-xs-12 col-md-12 col-lg-12 no-padding"><button class="btn btn-nc-blue">Full Details</button></div></div></div></div>';
	
	var _TMPL_MAP = '<div id="infobox-body"><ul class="thumb-slider"></ul><div class="row row-map no-margins"><div class="col-xs-6 col-md-6 map-hotelname"><a href="javascript:void(0);" class="hotelname-link">${hotelName}</a></div><div class="col-xs-6 col-md-6 text-right"><input type="number" class="rating" value="${rating}"></div></div><div class="row row-map no-margins row-map-location"><div class="col-xs-6 col-md-6"><i class="fa fa-map-marker"></i> ${location}</div></div><div class="row row-map no-margins"><div class="col-xs-6 col-md-6"><i class="fa fa-phone"></i><a href="tel:${hotelPhone}" class="hotel-phone"> ${hotelPhone}</a></div><div class="col-xs-6 col-md-6 text-right xs-padding-top15"><a class="quickview" href="javascript:void(0);">Quick View <i class="fa fa-angle-double-right"></i></a></div></div><div class="row no-margins row-map-book"><button class="btn btn-map-red">DETAILS <i class="fa fa-arrow-right"></i></button></div></div>';
	
	$.template( "grid" , _TMPL_GRID );
	$.template( "list" , _TMPL_LIST );
	$.template( "map" , _TMPL_MAP );
	$.template( "quickview" , _TMPL_QUICKVIEW );
	
	
	/* init datepickers */
	initDatePickers( $('input.datepicker.checkin'), $('input.datepicker.checkout') );
	
	$("#btn-load-more").on("click", function(e){
		loadMoreRecords();
	});
	
	/**TOGGLE SORT BUTTONS**/
	var sortArray = ["desc","asc"];
	var sortIcons = ['<i class="fa fa-sort-desc"></i>','<i class="fa fa-sort-asc"></i>'];
	$(".col-sort-options button").click(function(){
		var $button = $(this), next, btnTxt;
		if ($button.hasClass("active")){
			var sort = parseInt($button.data("sort"));
			next = (sort == (sortArray.length-1)) ? 0 : sort+1;
		} else {
			var $active = $(".col-sort-options button.active");
			$active.removeClass("active").data("sort",-1);
			$active.html( $.trim($active.text()) );
			next = 0;
		}
		btnTxt = $.trim($button.text());
		$button.addClass("active").html(btnTxt + sortIcons[next]).data("sort", next);
		
		// SORT MEMORY ARRAY
		var sort = {
			sortby: btnTxt.toLowerCase(),
			order: sortArray[next],
			array: MEM_RECORDS
		}	
		$('.search-result-body').empty();
		sendToWorker(sort);
		
	});
	/**END TOGGLE SORT BUTTONS**/
	PAGE_STATE.startRecord = nc.config.pages.search.gridNumVisibleRecords;
	initSearchPage();
});
