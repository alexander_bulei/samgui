/**
 * Project: NCTourism
 * Page: NCTourism Core
 * Last Modified: 27-08-2015
 * Author: Alexander Bulei (alexander.bulei@gmail.com)
**/

var nc = nc || {};
$(function(){
	nc = $.extend(true,nc, {
		error: function(msg){
			toastr.options = {
			  "closeButton": false,
			  "debug": false,
			  "newestOnTop": false,
			  "progressBar": true,
			  "positionClass": "toast-bottom-right",
			  "preventDuplicates": false,
			  "onclick": null,
			  "showDuration": "300",
			  "hideDuration": "1000",
			  "timeOut": "5000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}
			toastr["error"](msg)
		},
		send: function(url, params, callback, async){
			var fn = (typeof callback != "undefined") ? callback : $.noop;
			this.get(url, async || true, fn, params);
		},
		get: function(url, async, callback, params){
			$.ajax({
				"url": url,
				"async": async,
				"data": (typeof params != "undefined") ? params : {},
				"success": callback,
				"error": function(jqXHR, textStatus, errorThrown){
					alert(jqXHR.responseText);
				}
			});
		},
		include: function(filename, where, callback){
			this.get(filename, true, function(html){
				var _callback = callback ? callback : function(){};
				if ((typeof where != "undefined") || (where == "begin")){
					$('body').prepend(html);
				} else {
					$('body').append(html);
				}
				_callback();
				checkCustomResponsive();
			});
		},
		searchSidebar: function($container, onFilterCallback){
			this.get('tmpl/search-sidebar.html', true, function(html){
				$container.empty().append(html);
				if ((typeof initSidebar != "undefined") && ($.isFunction(initSidebar))) {
					initSidebar(onFilterCallback); 
				}
			});
		},
		convertListingToFrontEndModel: function(data) {
			var result = {
				id: data.ListingID,
				roomPrice: '$' + data.Rate1Amount,
				period: ' per ' + data.Rate1Unit,
				hotelName: data.ListingTitle,
				location: data.ListingCity,
				bedroom: 0,
				guests: data.ListingCustomInt1,
				bathrooms: 0,
				rating: data.Rating,
				description: data.ListingDescription,
				ListingImages: data.ListingImages,
				extra: [],
				hotelInfo: []
			}

			if (data.ListingObjects != null) {

				if (data.ListingObjects[0] != null && data.ListingObjects[0].ObjectQuantity) {
					result.bedroom = data.ListingObjects[0].ObjectQuantity;
				}

				if (data.ListingObjects[1] != null && data.ListingObjects[1].ObjectQuantity) {
					result.bathrooms = data.ListingObjects[1].ObjectQuantity;
				}
			}

			if (data.ListingAttributes.search("JA1") > 0) { result.extra.push("Pet Friendly"); }
			if (data.ListingAttributes.search("JC1") > 0) { result.extra.push("Handcap Accessible"); }
			if (data.ListingAttributes.search("HD1") > 0) { result.extra.push("Parking"); }

			$.each(data.Attributes, function (i, item) {
				result.hotelInfo.push(item.AttributeDescription);
			});

			result.latLng = [data.ListingLatitude, data.ListingLongitude];

			return result;
		},
		mapListingImagesToFEModel: function(db){
			var result = {
				id: db.id,
				originSrc: db.originSrc,
				src: db.src,
				disabled: db.disabled,
				imgurl: db.imgurl
			}
			return result;
		},
		mapReviewToFEModel: function(array){
			var result = [];
			$.each(array, function(i, review){
				var r = {
					id : review.id,
					avatar : review.avatar,
					name : review.name,
					comment : review.comment,
					rate : review.rate,
					locationdate : review.locationdate,
					answer : review.answer
				}
				result.push(r);
			});
			return result;
		},
		mapReviewsToFEModel: function(db){
			var result = {
				total: db.total,
				rating: {
					avg: db.rating.avg,
					five: db.rating.five,
					four: db.rating.four,
					three: db.rating.three,
					two: db.rating.two,
					one: db.rating.one,
				},
				reviews: this.mapReviewToFEModel(db.reviews)
			}
			return result;
		},
		config: {
			pages: {
				search:{
					gridNumVisibleRecords: 6,
					listNumVisibleRecords: 5
				},
				searchTodo:{
					gridNumVisibleRecords: 6,
					listNumVisibleRecords: 5
				},
				searchAll:{
					gridNumVisibleRecords: 6,
					listNumVisibleRecords: 5
				},
				propertySingle:{
					reviewsMoreNumRecords: 3,
					requests: {
						bedrooms: "demo/bedrooms.json",
						bedroomAmentities: "demo/bedroom-amentities.json",
						bedroomTypes: "demo/room-bedroom-type.json",
						bathrooms: "demo/bathrooms.json",
						bathroomAmentities: "demo/bedroom-amentities.json",
						bathroomTypes: "demo/room-bathrooms-type.json",
						amentities: "demo/amentities.json",
						amentitiesTypes: "demo/amentities-types.json",
						amentityAmentities: "demo/amentities-all.json"
					}
				}
			}
		}
	})
})