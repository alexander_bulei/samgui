/** 
* Images Preloader
* VERSION: 25052016.1
* Author: alexander.bulei@gmail.com
**/

var ABPRELOADER = function(settings){
  this.NOIMAGE_URL = settings.noImageUrl || false;
  this.debug = (settings && settings.debug) || false;
  this.processingCount = 0;
  this.prefetchList = [];
};

ABPRELOADER.prototype.addSourceOnly = function(src, callbackFn){
  if (src && src.length){
    this.prefetchList.push({
      source: src,
      loaded: false,
      processing: false,
      after: callbackFn || $.noop
    });    
  }
}

ABPRELOADER.prototype.add = function(src, $realImg, callbackFn){
  if (src && src.length){
    this.prefetchList.push({
      source: src,
      $image: $realImg,
      loaded: false,
      processing: false,
      after: callbackFn || $.noop
    });
  }
}

ABPRELOADER.prototype._processList = function(list){
  var self = this;
  $.each(list, function(){
    var prefetchObj = this;
    
    if (!prefetchObj.processing && !prefetchObj.loaded){      
      var prefetchImage = $(new Image());  
      
      prefetchImage.one("error", function(){
        prefetchObj.loaded = false;
        prefetchObj.processing = false;
        if (self.NOIMAGE_URL){
          if (prefetchObj.$image){
            prefetchObj.$image.attr("src", self.NOIMAGE_URL);
          } else {
            prefetchObj.after(self.NOIMAGE_URL);
          }
        }
        console.error("Fail to load the image: " + prefetchObj.source);
      });
      
      prefetchObj.processing = true;
      self.processingCount++;
      prefetchImage.one("load", function() {
        if (prefetchObj.$image){
          prefetchObj.$image.attr("src", prefetchObj.source);
          prefetchObj.$image.parent().addClass("img-loaded");
        }
        prefetchObj.loaded = true;
        prefetchObj.processing = false;
        self.processingCount--;
        prefetchObj.after(prefetchObj.$image || prefetchObj.source);
        prefetchImage.remove();
        if (self.debug){
          console.log("AB Image Preloaded: " + prefetchObj.source);
        }
      });
      prefetchImage.attr("src", prefetchObj.source);
    }
  });  
}

ABPRELOADER.prototype.process = function(){
  this._processList(this.prefetchList);
}