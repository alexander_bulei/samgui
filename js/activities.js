/**
 * Project: NCTourism
 * Page: activities.html
 * Last Modified: 17-07-2015
 * Author: Alexander Bulei (alexander.bulei@gmail.com)
**/
 
var _ACTIVITY_MAP_INST;
var _ACTIVITY_SIDE_MAP_INST, _ACTIVITY_SIDE_MAP_MARKER_INST;
var _ACTIVITY_GEOAC_INST;
var _ACTIVITY_MAP_MARKER_INST;
 
function checkResponsive(){
	var wWidth = $(window).width();
	var $rightBar = $("#right-side");
	var $rowBody = $(".row-body");
	if (wWidth <= 992){
		$rightBar.addClass("col-xs-offset-1").prependTo($rowBody);
	} else {
		$rightBar.removeClass("col-xs-offset-1").appendTo($rowBody);
	}
}

function initSlider(){
	nc.get('demo/images.json', true, function(result){
		var $ul = $("#main-slider");
		var arr = result || [];
		$.each(arr, function(i, data){
			var _data = nc.mapListingImagesToFEModel(data);
			// only not disabled images
			if (!_data.disabled){
				var html = '<li><a href="#"><img class="aspect-fit" src="'+_data.src+'"/></a></li>';
				$ul.append(html);
			}
		});
		
		$ul.responsiveSlides({        
			speed: 800,
			pause: true,
			defaultPaused: true,
			timeout: 3000,
			auto : true,
			pager : true,
			thumb : "img",
			navContainer: ".thumb-container",
			nav: true		
		});
		
		//thumbnail control
		var html = '<div class="main-slider-thumbcontrol"><div class="up" style="visibility: hidden"><i class="fa fa-chevron-up"></i></div><div class="down"><i class="fa fa-chevron-down"></i></div></div>';
		var $thumbnailsCnt = $(".thumb-container")
		var $thumb = $(".rslides1_tabs");
		$thumbnailsCnt.append(html);
		
		$thumbnailsCnt.find("img").addClass("aspect-fit");
		
		$("a.rslides_nav.prev", $thumbnailsCnt).appendTo(".nav-container");
		$("a.rslides_nav.next", $thumbnailsCnt).appendTo(".nav-container");

		$thumbnailsCnt.find('div.up,div.down').on("click", function(e){
			var $this = $(this);
			var sT = $thumb.scrollTop(), newST;
			if ($this.hasClass('up')){
				newST = sT - 65;
			} else {
				newST = sT + 65;
			}
			
			(newST <= 0) ? $("div.up").css("visibility","hidden") : $("div.up").css("visibility","visible");
			$thumb.animate({ scrollTop: newST }, 600);
		});
		
		$(".aspect-fit").parent().imgLiquid({fill:true});
		
	});	
}
 
$(function(){
	// includes	
	nc.include('tmpl/header.html','begin');
	nc.include('tmpl/footer.html');
	nc.include('tmpl/login.html');	
	
	$('div.panel-collapse').on('show.bs.collapse', function () {		
		var $panel = $(this);
		var $header = $panel.prev();
		$header.addClass('active');
		$header.find('span.pull-right i').removeClass('fa-chevron-right').addClass('fa-chevron-down');	
	}).on('hidden.bs.collapse', function () {
		var $panel = $(this);
		var $header = $panel.prev();
		$header.removeClass('active');
		$header.find('span.pull-right i').removeClass('fa-chevron-down').addClass('fa-chevron-right');
	});

	$('div.panel-collapse').on('shown.bs.collapse', function () {	
		var $panel = $(this);
		//google map
		if (($panel.attr("id") == "locationTab") && (!_ACTIVITY_MAP_INST)){
			var $map = $("#activity-map");
			$map.data("latlng", "[41.3833333,2.1833333]");
			_ACTIVITY_MAP_INST = new google.maps.Map($map[0],{
				center: new google.maps.LatLng(41.3833333,2.1833333),
				zoom:11,
				mapTypeId:google.maps.MapTypeId.ROADMAP,
				streetViewControl:false,
				scrollwheel:false
			});
			_ACTIVITY_MAP_MARKER_INST = new google.maps.Marker({
				position: new google.maps.LatLng(41.3833333,2.1833333),
				map: _ACTIVITY_MAP_INST,
				title: "W BARCELONA",
				icon : 'img/marker.png'				
			});			
			
			if (!_ACTIVITY_GEOAC_INST){
				var $input = $("#geoac-locationTab");
				_ACTIVITY_GEOAC_INST = new google.maps.places.Autocomplete($input[0],{ types: [] });
				google.maps.event.addListener(_ACTIVITY_GEOAC_INST, 'place_changed', function() {
					var place = _ACTIVITY_GEOAC_INST.getPlace();
					if ((place) && (_ACTIVITY_MAP_MARKER_INST)){
						_ACTIVITY_MAP_INST.setCenter(place.geometry.location);
						_ACTIVITY_MAP_MARKER_INST.setPosition(place.geometry.location);
						var _arr = [];
						_arr.push(place.geometry.location.lat());
						_arr.push(place.geometry.location.lng());
						$map.data("latlng", JSON.stringify(_arr) );
					}
				});
			}
			
		}
	});
	
	//init slider
	initSlider();
	
	//init rating
	initRating();
	function initRating(){
		$(".rating-input").rating({
			size : 'xxs',
			showCaption: false,
			showClear: false,
			readonly: true
		});		
	}

	//bind fav icon
	$("i.fav-icon").parent().on("click", function(e){
		var $i = $(this).find('i');
		if ($i.hasClass("fa-heart-o")){
			$i.removeClass('fa-heart-o').addClass("fa-heart");
		}else{
			$i.addClass('fa-heart-o').removeClass("fa-heart");
		}
	});	
	
	checkResponsive();
	$(window).on("resize", function(){
		checkResponsive();
	});
	
	var TEMP_GA_INST;
	function editTitleLoc(){
		// TITLE
		var $label = $(".activity-title");
		var $input = $("<input class='form-control activity-title-input input-sm' type='text'>").val( $label.text() );
		$label.replaceWith($input);
		
		//LOCATION
		$label = $(".activity-location");
		var oldV = $label.text();
		$input = $("<input class='form-control activity-location-input input-sm' type='text'>").val(oldV);
		$label.replaceWith($input);
		$input.data("oldValue",oldV);
		
		TEMP_GA_INST = new google.maps.places.Autocomplete($input[0],{ types: [] });
	}
	
	function restoreTitleLoc(){
		var $input = $(".activity-title-input");
		var $label = $("<label class='activity-title'>"+$input.val()+"</label>");
		$input.replaceWith($label);
		
		//LOCATION
		$input = $(".activity-location-input");		
		var place = TEMP_GA_INST.getPlace();
		var v;
		if (place){
			v = place.formatted_address;
		} else if ($input.val() == "") {
			v = $input.data("oldValue");
		}else {
			v = $input.val();			
		}
		$label = $("<label class='activity-location' style='display:block;'><i style='display:inline-block;' class='fa fa-map-marker'></i> "+v+"</label>");
		$input.replaceWith($label);
	}	
	
	$(".edit-title-loc").on("click", function(){
		nc_editmode($(this),editTitleLoc,restoreTitleLoc);
	});
	
	
	//bind toggle buttons
	$("i.btn-toggle-check").parent().on("click", function(e){
		var $i = $(this).find('i');
		var $btn = $("." + $i.data("btn"));
		if ($i.hasClass("btn-toggle-check-on")){
			$i.removeClass('btn-toggle-check-on fa-check-square').addClass("fa-minus-square btn-toggle-check-off");
			$btn.parent().addClass("disabled");
		}else{
			$i.addClass('btn-toggle-check-on fa-check-square').removeClass("fa-minus-square btn-toggle-check-off");
			$btn.parent().removeClass("disabled");
		}		
	});	
	
	// IMAGE EDITOR
	function editImages(){
		$("#main-slider").empty().hide();
		$(".thumb-container").empty().hide();
		$(".nav-container").empty().hide();
		
		nc.get('demo/images.json', true, function(result){
			var $c = $(".main-slider-wrapper");
			var opts = {
				saveCallback: function(data, callback){
					var response = $.extend(true, data, {
						id: Math.floor((Math.random() * 100) + 1),
						disabled: false
					});
					callback(response);
				},
				onready: function(){
					$('body').scrollTop( $(".ncim-upload-btn").offset().top );
				}
			}
			NCIM.init($c,result,opts);			
		});
	}
	
	function restoreImages(){
		NCIM.destroy();
		$("#main-slider").show();
		$(".thumb-container").show();
		$(".nav-container").show();
		initSlider();		
	}	
	
	$("#edit-images").on("click", function(){
		nc_editmode($(this),editImages,restoreImages);
	});		
	
	
	//description TAB
	
	function editDescriptionTab(){
		var $tab = $("#descriptionTab");
		var $element, $input;
		
		//title
		$element = $(".sub-title",$tab);
		$input = $("<input class='form-control activity-desctab-title-input input-sm' type='text'>").val( $element.text() );
		$element.replaceWith($input);
		
		//description
		$element = $(".activity-desctab-desc",$tab);
		var $parent = $element.parent();
		
		$wysiwyg = $('<div id="editor-wysiwyg"></div>');
		$wysiwyg.html($element.html());
		
		$element.remove();
		
		$parent.append($wysiwyg);
		
		$wysiwyg.summernote({
			height: 250,
			minHeight: 250,
			maxHeight: 450,
			focus: true
		});
	}
	
	function restoreDescriptionTab(){
		var $tab = $("#descriptionTab");
		var $element, $input;

		//title
		$input = $(".activity-desctab-title-input",$tab);
		$element = $("<div class='inline-block sub-title title-color'>"+$input.val()+"</div>");
		$input.replaceWith($element);
		
		//description
		$wysiwyg = $("#editor-wysiwyg",$tab);
		$parent = $wysiwyg.parent();
		$element = $("<div class='activity-desctab-desc'>"+$wysiwyg.code()+"</div> ");
		$parent.append($element);
		$wysiwyg.destroy().remove();
	}
	
	$(".edit-description-tab").on("click", function(){
		nc_editmode($(this),editDescriptionTab,restoreDescriptionTab);
	});
	
	//LOCATION TAB
	function editLocationTab(){
		var $map = $("#activity-map");
		var latlng = JSON.parse($map.data("latlng"));
		var g_latlng = new google.maps.LatLng(latlng[0],latlng[1]);
		_ACTIVITY_MAP_INST.setCenter(g_latlng);
		_ACTIVITY_MAP_MARKER_INST.setPosition(g_latlng);			
		$("#geoac-locationTab").val("");
		$("#geoac-locationTab").show();
	}
	
	function restoreLocationTab(){
		var $map = $("#activity-map");
		$("#geoac-locationTab").hide();
	}
	
	$("#edit-location").on("click", function(){
		nc_editmode($(this),editLocationTab,restoreLocationTab);
	});	
	
	
	//REVIEW TAB
	var html_review = '<div class="row row-review" data-id="${id}"><div class="col-xs-1 col-md-1 col-lg-1 col-image"><div><img src="${avatar}" width="50" height="50" class="img-circle"></div><div class="text-center person">${name}</div></div><div class="col-xs-11 col-md-11 col-lg-11 no-padding" style="width:99%"><div class="popover right popover-review"><div class="arrow"></div><div class="popover-content"><div class="row-comment"><div style="padding-right:10px;">${comment}</div><div class="col-xs-3 col-md-3 col-lg-3 text-center col-rating border-left" style="width:100px"><input class="rating-input" value="${rate}" type="number"></div></div></div></div></div></div><div class="row row-review-answer" data-id="${id}"><div class="col-xs-1 col-md-1 col-lg-1"></div><div class="col-xs-11 col-md-11 col-lg-11 no-padding"><div class="review-answer"><strong>Owners Response: </strong>${answer}</div></div></div><div class="row row-who-review" data-id="${id}"><div class="col-xs-12 col-md-12 col-lg-12 no-padding text-right">${locationdate}</div></div>';
	$.template("review", html_review);
	
	nc.get('demo/reviews.json', true, function(result){
		var data = nc.mapReviewsToFEModel(result);
		
		var $div = $("#reviewsTab-summary-rate");
		
		$div.find("label.label-score").text(data.rating.avg);
		
		$div.find("input").val(data.rating.avg).rating({
			size : 'xxs',
			showCaption: false,
			showClear: false,
			readonly: true
		});
		
		$div.find("label.label-reviews").text(data.total + ' reviews');
		
		//build rate filters
		var $tmpl, prec;
		var $container = $("#reviewsTab-filter-container");
		
		//5*
		prec = Math.round((data.rating.five * 100) / data.total);  
		$tmpl = '<div><span style="width:20px;display: inline-block;">' + data.rating.five + '</span><div class="checkbox checkbox-default"><input type="checkbox"><label></label></div><input type="number" value="5"/><div class="progress" style="margin-left:10px;width:150px;margin-bottom:-1px;height:10px;"><div class="progress-bar" role="progressbar" aria-valuenow="'+prec+'" aria-valuemin="0" aria-valuemax="100" style="width: '+prec+'%;"></div></div></div>';
		$container.append($tmpl);
		
		//4*
		prec = Math.round((data.rating.four * 100) / data.total);  
		$tmpl = '<div><span style="width:20px;display: inline-block;">' + data.rating.four + '</span><div class="checkbox checkbox-default"><input type="checkbox"><label></label></div><input type="number" value="4"/><div class="progress" style="margin-left:10px;width:150px;margin-bottom:-1px;height:10px;"><div class="progress-bar" role="progressbar" aria-valuenow="'+prec+'" aria-valuemin="0" aria-valuemax="100" style="width: '+prec+'%;"></div></div></div>';
		$container.append($tmpl);
		
		//3*
		prec = Math.round((data.rating.three * 100) / data.total);  
		$tmpl = '<div><span style="width:20px;display: inline-block;">' + data.rating.three + '</span><div class="checkbox checkbox-default"><input type="checkbox"><label></label></div><input type="number" value="3"/><div class="progress" style="margin-left:10px;width:150px;margin-bottom:-1px;height:10px;"><div class="progress-bar" role="progressbar" aria-valuenow="'+prec+'" aria-valuemin="0" aria-valuemax="100" style="width: '+prec+'%;"></div></div></div>';
		$container.append($tmpl);
		
		//2*
		prec = Math.round((data.rating.two * 100) / data.total);  
		$tmpl = '<div><span style="width:20px;display: inline-block;">' + data.rating.two + '</span><div class="checkbox checkbox-default"><input type="checkbox"><label></label></div><input type="number" value="2"/><div class="progress" style="margin-left:10px;width:150px;margin-bottom:-1px;height:10px;"><div class="progress-bar" role="progressbar" aria-valuenow="'+prec+'" aria-valuemin="0" aria-valuemax="100" style="width: '+prec+'%;"></div></div></div>';
		$container.append($tmpl);
		
		//1*
		prec = Math.round((data.rating.one * 100) / data.total);  
		$tmpl = '<div><span style="width:20px;display: inline-block;">' + data.rating.one + '</span><div class="checkbox checkbox-default"><input type="checkbox"><label></label></div><input type="number" value="1"/><div class="progress" style="margin-left:10px;width:150px;margin-bottom:-1px;height:10px;"><div class="progress-bar" role="progressbar" aria-valuenow="'+prec+'" aria-valuemin="0" aria-valuemax="100" style="width: '+prec+'%;"></div></div></div>';
		$container.append($tmpl);
		
		
		$container.find('input[type="number"]').rating({
			size : 'xxs',
			showCaption: false,
			showClear: false,
			readonly: true
		});
		
		var _arr = data.reviews || [];
		var $body = $(".reviews-body")
		$body.empty();
		$.each(_arr, function(i, obj){
			var _html = $.tmpl("review", obj);
			
			if ((i % 2) == 1){
				_html.addClass("row-review-offset")
			}
			
			$body.append(_html);
			
			if (!obj.answer){
				$body.find(".row-review-answer[data-id='"+obj.id+"']").hide();
			}
			
		});
		$("#btn-load-more").data("recordsShown", _arr.length);
		initRating();
	});
	
	// LOAD MORE
	$("#btn-load-more").click(function(){
		var $this = $(this);
		var params = {
			start: $this.data("recordsShown") || 0,
			max: 3
		}
		nc.get('demo/reviews.json', true, function(result){
			var _arr = result ? nc.mapReviewsToFEModel(result) : [];
			var $body = $(".reviews-body");
			var _count = $body.find(".row-review").length || 0;
			$.each(_arr.reviews, function(i, obj){
				var _html = $.tmpl("review", obj);
				if (((_count+i) % 2) == 1){
					_html.addClass("row-review-offset")
				}
				$body.append(_html);
				if (!obj.answer){
					$body.find(".row-review-answer[data-id='"+obj.id+"']").hide();
				}
			});
			$this.data("recordsShown", params.start + params.max);
			initRating();
		}, params);		
	});	

	var $map = $("#activities-side-map");	
	_ACTIVITY_SIDE_MAP_INST = new google.maps.Map($map[0],{
		center: new google.maps.LatLng(41.3833333,2.1833333),
		zoom:11,
		mapTypeId:google.maps.MapTypeId.ROADMAP,
		streetViewControl:false,
		scrollwheel:false
	});
	_ACTIVITY_SIDE_MAP_MARKER_INST = new google.maps.Marker({
		position: new google.maps.LatLng(41.3833333,2.1833333),
		map: _ACTIVITY_SIDE_MAP_INST,
		title: "W BARCELONA",
		icon : 'img/marker.png'				
	});
	
	$("#share-panel").jsSocials({
		shares: ["email", "twitter", "facebook", "googleplus", "pinterest"]
    });
	
	$("#editReviewsModalIFrame").attr("src","backend/manage-reviews-frame.html");
	
	function _responsiveModal($modal){
		var minHeight = Math.round($(window).height() - ($(window).height() / 5));
		$modal.find('iframe').css({"min-height": minHeight + 'px'});
	}
	
	$("#edit-reviews").on("click", function(e){
		var $modal = $("#editReviewsModal");
		_responsiveModal($modal);
		$modal.modal({
		  backdrop: 'static',
		  keyboard: true
		}).modal("show");		
	});	
	
	$(window).on("resize", function(){
		_responsiveModal( $("#editReviewsModal") );
	});
	
	$('[data-toggle="tooltip"]').tooltip();
});